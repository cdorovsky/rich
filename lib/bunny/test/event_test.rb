require 'minitest/autorun'
require 'fileutils'
require 'json'
require "active_record"
require "#{ FileUtils.pwd }/app/models/direct_offer_visitor"
require "#{ FileUtils.pwd }/app/models/direct_offer_redirect"
require "#{ FileUtils.pwd }/app/models/direct_offer_show"
require "#{ FileUtils.pwd }/lib/bunny/event"

dbconfig = YAML::load( File.open( "#{ FileUtils.pwd }/config/database.yml" ) )
ActiveRecord::Base.establish_connection( dbconfig[ 'test' ] )

class EventTest < Minitest::Spec

  def setup
    super
    @json_show  = "{\"offer\":{\"dealer_id\":1,\"offer_id\":200,\"enabled\":true,\"approved\":true,\"hashid\":\"0zuO6\",\"selfmade\":null,\"adult\":null,\"cost\":0.32,\"apps_os\":\"android\",\"seller_url\":\"http://youmi.go2cloud.org/aff_c?aff_id=1498&aff_sub=&offer_id=334\"},\"request\":{\"referrer\":null,\"ip\":\"52.28.141.86\",\"request_id\":\"7b60b053-47d5-4434-8a2e-b09091a75085\",\"country\":\"US\",\"user_agent\":\"Mozilla/5.0 (iPhone; CPU iPhone OS 7_1_2 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Version/7.0 Mobile/11D257 Safari/9537.53\",\"user_platform\":\"ipod_iphone\",\"timestamp\":\"1434028590\",\"locale\":\"en\"},\"event\":\"show\",\"params\":{\"splat\":[],\"captures\":[\"0zuO6\"],\"redirect_code\":\"0zuO6\"}}"
    @show_adapt = {"request_id"=>"7b60b053-47d5-4434-8a2e-b09091a75085", "referer"=>nil, "sub_id"=>nil, "sub_id2"=>nil, "sub_id3"=>nil, "seller_url"=>"http://youmi.go2cloud.org/aff_c?aff_id=1498&aff_sub=&offer_id=334", "url_params"=>nil, "dealer_id"=>1, "offer_id"=>200, "direct_offer_id"=>nil, "country_id"=>nil, "country_name"=>nil, "country_code"=>"US", "ip"=>"52.28.141.86", "user_agent"=>"Mozilla/5.0 (iPhone; CPU iPhone OS 7_1_2 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Version/7.0 Mobile/11D257 Safari/9537.53", "user_platform"=>"ipod_iphone", "user_device"=>"ipod_iphone", "timestamp"=>"1434028590", "uniq_visitor"=>nil, "converted"=>false, "triggered_at"=>nil}

    @json_redirect  = "{\"offer\":{\"dealer_id\":1,\"offer_id\":200,\"enabled\":true,\"approved\":true,\"hashid\":\"0zuO6\",\"selfmade\":null,\"adult\":null,\"cost\":0.32,\"apps_os\":\"android\",\"seller_url\":\"http://youmi.go2cloud.org/aff_c?aff_id=1498&aff_sub=&offer_id=334\"},\"request\":{\"referrer\":null,\"ip\":\"52.28.141.86\",\"request_id\":\"7b60b053-47d5-4434-8a2e-b09091a75085\",\"country\":\"US\",\"user_agent\":\"Mozilla/5.0 (iPhone; CPU iPhone OS 7_1_2 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Version/7.0 Mobile/11D257 Safari/9537.53\",\"user_platform\":\"ipod_iphone\",\"timestamp\":\"1434028590\",\"locale\":\"en\"},\"event\":\"redirect\",\"params\":{\"splat\":[],\"captures\":[\"0zuO6\"],\"redirect_code\":\"0zuO6\"},\"redirect_url\":\"http://youmi.go2cloud.org/aff_c?aff_id=1498&aff_sub=123&offer_id=334\"}"
    @redirect_adapt = {"request_id"=>"7b60b053-47d5-4434-8a2e-b09091a75085", "referer"=>nil, "sub_id"=>nil, "sub_id2"=>nil, "sub_id3"=>nil, "seller_url"=>"http://youmi.go2cloud.org/aff_c?aff_id=1498&aff_sub=&offer_id=334", "url_params"=>nil, "dealer_id"=>1, "offer_id"=>200, "direct_offer_id"=>nil, "country_id"=>nil, "country_name"=>nil, "country_code"=>"US", "ip"=>"52.28.141.86", "user_agent"=>"Mozilla/5.0 (iPhone; CPU iPhone OS 7_1_2 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Version/7.0 Mobile/11D257 Safari/9537.53", "user_platform"=>"ipod_iphone", "user_device"=>"ipod_iphone", "timestamp"=>"1434028590", "uniq_visitor"=>nil, "redirect_url"=>"http://youmi.go2cloud.org/aff_c?aff_id=1498&aff_sub=123&offer_id=334", "converted"=>false, "triggered_at"=>nil}

    @wrong_data = 'wrong_json_data'
  
    @ar_show     = Event.new( @json_show )
    @ar_redirect = Event.new( @json_redirect )
    @ar_wrong    = Event.new( @wrong_data )
  end

  def test_show_attributes_parsing
    assert_equal @ar_show.congruence_word, 'Show'
    assert_equal @ar_show.congruence.class, DirectOfferShow
    assert_equal @ar_show.message, JSON.parse( @json_show )
    assert_equal @ar_show.adapt, @show_adapt 
  end

  def test_redirect_attributes_parsing
    assert_equal @ar_redirect.congruence_word, 'Redirect'
    assert_equal @ar_redirect.congruence.class, DirectOfferRedirect
    assert_equal @ar_redirect.message, JSON.parse( @json_redirect )
    assert_equal @ar_redirect.adapt, @redirect_adapt 
  end

  def test_wrong_request_attributes_parsing
    assert_equal @ar_wrong.congruence_word, nil
    assert_equal @ar_wrong.message, nil
  end

end
