require 'minitest/autorun'
require 'fileutils'
require 'json'
require "active_record"
require "#{ FileUtils.pwd }/app/models/direct_offer_visitor"
require "#{ FileUtils.pwd }/app/models/direct_offer_redirect"
require "#{ FileUtils.pwd }/app/models/direct_offer_show"
require "#{ FileUtils.pwd }/lib/bunny/event"

dbconfig = YAML::load( File.open( "#{ FileUtils.pwd }/config/database.yml" ) )
ActiveRecord::Base.establish_connection( dbconfig[ 'test' ] )

class DirectOfferVisitorTest < Minitest::Spec

  def setup
    super
    @json_show          = "{\"offer\":{\"dealer_id\":1,\"offer_id\":200,\"enabled\":true,\"approved\":true,\"hashid\":\"0zuO6\",\"selfmade\":null,\"adult\":null,\"cost\":0.32,\"apps_os\":\"android\",\"seller_url\":\"http://youmi.go2cloud.org/aff_c?aff_id=1498&aff_sub=&offer_id=334\"},\"request\":{\"referrer\":null,\"ip\":\"52.28.141.86\",\"request_id\":\"7b60b053-47d5-4434-8a2e-b09091a75085\",\"country\":\"US\",\"user_agent\":\"Mozilla/5.0 (iPhone; CPU iPhone OS 7_1_2 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Version/7.0 Mobile/11D257 Safari/9537.53\",\"user_platform\":\"ipod_iphone\",\"timestamp\":\"1434028590\",\"locale\":\"en\"},\"event\":\"show\",\"params\":{\"splat\":[],\"captures\":[\"0zuO6\"],\"redirect_code\":\"0zuO6\"}}"
    @json_redirect      = "{\"offer\":{\"dealer_id\":1,\"offer_id\":200,\"enabled\":true,\"approved\":true,\"hashid\":\"0zuO6\",\"selfmade\":null,\"adult\":null,\"cost\":0.32,\"apps_os\":\"android\",\"seller_url\":\"http://youmi.go2cloud.org/aff_c?aff_id=1498&aff_sub=&offer_id=334\"},\"request\":{\"referrer\":null,\"ip\":\"52.28.141.86\",\"request_id\":\"7b60b053-47d5-4434-8a2e-b09091a75085\",\"country\":\"US\",\"user_agent\":\"Mozilla/5.0 (iPhone; CPU iPhone OS 7_1_2 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Version/7.0 Mobile/11D257 Safari/9537.53\",\"user_platform\":\"ipod_iphone\",\"timestamp\":\"1434028590\",\"locale\":\"en\"},\"event\":\"redirect\",\"params\":{\"splat\":[],\"captures\":[\"0zuO6\"],\"redirect_code\":\"0zuO6\"},\"redirect_url\":\"http://youmi.go2cloud.org/aff_c?aff_id=1498&aff_sub=123&offer_id=334\"}"

    @ar_show         = DirectOfferVisitor.new( @json_show ).event.congruence
    @ar_redirect     = DirectOfferVisitor.new( @json_redirect ).event.congruence
  end

  def test_show_attributes_parsing
    assert_equal @ar_show.class, DirectOfferShow
    expected_params = Event.new( @json_show ).adapt
    expected_params.each do | column_name, expected_value |
      assert_equal @ar_show.attributes[ column_name ], expected_value
    end
  end

  def test_redirect_attributes_parsing
    assert_equal @ar_redirect.class, DirectOfferRedirect
    expected_params = Event.new( @json_redirect ).adapt
    expected_params.each do | column_name, expected_value |
      assert_equal @ar_redirect.attributes[ column_name ], expected_value
    end
  end

end
