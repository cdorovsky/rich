require "bunny"
require "yaml"
require "json"
require "fileutils"
require "active_record"

dbconfig = YAML::load( File.open( "#{ FileUtils.pwd }/config/database.yml" ) )
env = ENV['RACK_ENV'] ? ENV['RACK_ENV'] : 'development'

ActiveRecord::Base.establish_connection( dbconfig[ env ] )
$bunny = Bunny.new ENV['CLOUDAMQP_URL']
$bunny.start
$bunny_channel = $bunny.create_channel
q = $bunny_channel.queue( 'direct_offers', auto_delete: false )

require "#{ FileUtils.pwd }/app/models/direct_offer_show.rb"
require "#{ FileUtils.pwd }/app/models/direct_offer_visitor.rb"
require "#{ FileUtils.pwd }/app/models/direct_offer_redirect.rb"


direct_offer_shows = []
direct_offer_redirects = []
q.subscribe( manual_ack: true ) do | delivery_info, properties, event_json |
  visitor = DirectOfferVisitor.new( event_json )
  case visitor.fact
  when 'visitor'
    direct_offer_shows << visitor
  when 'redirect'
    direct_offer_redirects << visitor
  end
end

p direct_offer_shows
p direct_offer_redirects
#DirectOfferShow.import direct_offer_shows
#DirectOfferSetRedirect.import direct_offer_redirects
