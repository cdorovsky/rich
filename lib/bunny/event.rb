class Event

  LIST = %w( redirect show )

  attr_accessor :message, :fact, :congruence_word, :congruence

  def initialize( event_json )
    @message = pull_data( event_json )
    if @message
      @fact = @message[ 'event' ]
    end
    @congruence_word = @fact ? ( LIST & [ @fact ] ).first.capitalize : nil
    @congruence = @fact ? congruence_class.new( adapt ) : nil
  end

  def pull_data( event_json )
    begin
      JSON.parse( event_json )
    rescue
      nil
    end
  end

  def congruence_class
    Object.const_get( "DirectOffer#{ @congruence_word }" )
  end

  def adapt
    if @congruence_word == 'Show'
      { 
        "request_id"      => @message['request']['request_id'],
        "referer"         => @message['request']['referrer'], 
        "sub_id"          => @message['params']['sub_id'], 
        "sub_id2"         => @message['params']['sub_id2'], 
        "sub_id3"         => @message['params']['sub_id3'], 
        "seller_url"      => @message['offer']['seller_url'], 
        "url_params"      => @message['rack.url_scheme'], 
        "dealer_id"       => @message['offer']['dealer_id'], 
        "offer_id"        => @message['offer']['offer_id'], 
        "direct_offer_id" => nil, 
        "country_id"      => nil, 
        "country_name"    => nil, 
        "country_code"    => @message['request']['country'], 
        "ip"              => @message['request']['ip'], 
        "user_agent"      => @message['request']['user_agent'],
        "user_platform"   => @message['request']['user_platform'], 
        "user_device"     => @message['request']['user_platform'],
        "timestamp"       => @message['request']['timestamp'], 
        "uniq_visitor"    => @message['params']['uniq'], 
        "converted"       => false, 
        "triggered_at"    => nil 
      }
    elsif @congruence_word == 'Redirect'
      { 
        "request_id"      => @message['request']['request_id'],
        "referer"         => @message['request']['referrer'], 
        "sub_id"          => @message['params']['sub_id'], 
        "sub_id2"         => @message['params']['sub_id2'], 
        "sub_id3"         => @message['params']['sub_id3'], 
        "seller_url"      => @message['offer']['seller_url'], 
        "url_params"      => @message['rack.url_scheme'], 
        "dealer_id"       => @message['offer']['dealer_id'], 
        "offer_id"        => @message['offer']['offer_id'], 
        "direct_offer_id" => nil, 
        "country_id"      => nil, 
        "country_name"    => nil, 
        "country_code"    => @message['request']['country'], 
        "ip"              => @message['request']['ip'], 
        "user_agent"      => @message['request']['user_agent'],
        "user_platform"   => @message['request']['user_platform'], 
        "user_device"     => @message['request']['user_platform'],
        "timestamp"       => @message['request']['timestamp'], 
        "uniq_visitor"    => @message['params']['uniq'], 
        "redirect_url"    => @message['redirect_url'],
        "converted"       => false, 
        "triggered_at"    => nil 
      }
    end
  end

end
