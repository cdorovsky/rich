namespace :dealer_balance do
  desc "WeeklyIncreaseForCheckout"
  task( :weekly_increase_for_checkout => :environment ) do
    DealerIncrement.weekly_increase_for_checkout
  end
end
