namespace :auto do
  desc "AutoBuyout"
  task(:auto_buyout=> :environment) do
    VisitorQueue.check_expire
  end
end
