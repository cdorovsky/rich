namespace :offers do
  desc "HourlyOfferUpdater"
  task( :hourly_offer_updater  => :environment ) do
  	OfferLimit.check_and_reset_limit_counters
    Offer.disabling_robot
  end
end
