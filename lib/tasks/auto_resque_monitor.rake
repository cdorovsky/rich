namespace :auto do
  desc "ResqueMonitor"
  task(:resque_monitor => :environment) do
    ResqueMonitor.check_resque_queue
  end
end