namespace :mongo do
  desc "Load mongo to Postgresql"
  task( :load_to_pg => :environment ) do
    MongoStat.load_to_pg
    dealer_ids = DealerIncrement.recent_dealer_ids
    DealerIncrement.pay_to_balance( dealer_ids )
    Dashboard.enqueue_dealer_results( dealer_ids )
    Postback.send_to_partners
  end
end
