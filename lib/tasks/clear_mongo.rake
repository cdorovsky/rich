namespace :mongo do
  desc "Load mongo to Postgresql"
  task( :clear_scoped=> :environment ) do
    MongoStat.clear_scoped
  end
end
