namespace :auto do
  desc "BuyerSessionCleaner"
  task(:buyer_session_cleaner => :environment) do
    BuyerSession.delete_outdated_redis_records
    BuyerSession.delete_outdated_buyer_sessions
    BuyerRequest.delete_outdated_buyer_requests
  end
end