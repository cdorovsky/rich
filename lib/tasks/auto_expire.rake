namespace :auto do
  desc "AutoExpire"
  task(:auto_expire=> :environment) do
    VisitorQueue.check_expire
  end
end
