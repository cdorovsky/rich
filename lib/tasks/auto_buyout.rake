namespace :auto do
  desc "AutoBuyout"
  task(:auto_buyout=> :environment) do
    Subscribe.to_buyout.each { | subscribe | subscribe.buyout }
  end
end
