namespace :auto do
  desc "AutoPayments"
  task(:auto_payment  => :environment) do
    MoneyOptions.auto_output
  end
end
