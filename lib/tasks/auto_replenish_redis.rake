namespace :auto do
  desc "ReplenishRedis"
  task(:replenish_redis => :environment) do
    $redis.flushdb
    Precacher.replenish_cache
    Offer.mass_reload_offers_to_redis
  end
end
