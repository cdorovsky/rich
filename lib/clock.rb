require File.expand_path('../../config/boot',        __FILE__)
require File.expand_path('../../config/environment', __FILE__)
require 'clockwork'

include Clockwork

every( 10.seconds, 'Importing data from mongo') { MongoEventContainer.send_expired_to_sidekiq( Time.zone.now.to_i ) }
every( 1.minute,   'Importing data from mongo') { MongoEventContainer.delete_old }
every( 1.minute,   'Sidekiq monitor'          ) { SidekiqMonitor.check_resque_queue }
every( 30.minutes, 'Postback support sender'  ) { Postback.support_sender }
