source 'https://rubygems.org'
source 'https://rails-assets.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.1.2'
gem 'therubyracer', require: 'v8'
gem 'devise', '3.2.4'
gem 'warden', '1.2.3'
gem 'cancan'

gem 'simple_enum'

gem 'pg'
gem 'mongoid', '~> 4.0.0'
gem 'activerecord-import'
gem 'statsd-ruby'
gem 'bunny'
gem 'sneakers'

gem 'clockwork'
gem 'sidekiq'

gem 'smsc'
gem 'geocoder'
gem 'byebug'
gem 'gmaps4rails'
gem 'useragent'
gem 'newrelic_rpm'
gem 'rest-client'
gem 'resque', "~> 1.22.0", :require => "resque/server"
gem 'rmagick', require: false
gem 'cloudinary'
gem 'carrierwave'
gem 'puma'
#gem 'unicorn'
gem 'rufus-scheduler'
gem 'nokogiri'
gem 'mailgunner', '~> 1.3.0'
gem 'jquery-rails'
gem 'sass-rails', '~> 4.0.0'
gem 'compass-rails'
gem 'slim-rails'
gem 'slim'
gem 'foundation-rails'
gem 'foundation-icons-sass-rails'
gem 'skim'
gem 'eco'
gem 'eventmachine', '1.0.7', :git => "git://github.com/eventmachine/eventmachine.git"
gem 'gon'
gem 'will_paginate', '~> 3.0'
gem 'js-routes'
gem 'spinjs-rails'
gem 'zeroclipboard-rails'
gem 'dropzonejs-rails'
gem 'redcarpet'
gem 'highstock-rails'
gem 'whenever'
gem 'geoip'
gem 'redis'
gem 'resque-workers-lock'
gem 'rqrcode_png'
gem 'ckeditor'
gem 'hashids'
gem 'wannabe_bool'

gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.0.0'
gem 'turbolinks'
gem 'jbuilder', '~> 1.2'
gem 'browser'
gem 'dotenv-rails', :groups => [:development, :test]

group :development do
  gem 'thin'
  gem 'awesome_print'
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'letter_opener'
end

group :doc do
  gem 'sdoc', require: false
end

group :test do
  gem 'rspec-rails'
  gem 'factory_girl_rails'
  gem 'capybara'
  gem 'guard-rspec'
  gem 'database_cleaner'
  gem 'resque_spec'
end

group :production do
  gem 'rails_12factor'
  gem 'unicorn-worker-killer'
end

ruby '2.2.1'
