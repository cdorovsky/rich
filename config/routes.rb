require 'sidekiq/web'
Rich::Application.routes.draw do

  get "redirect_settings/show"
  get "redirect_settings/update"
  get "redirect_settings/destroy"
  devise_for :dealers , controllers: {
    registrations: 'registrations',
    sessions: 'sessions',
    passwords: 'passwords'
  }

  constraints subdomain: 'api' do
    scope module: 'api' do
      get "/apps_advert/:provider" => "seller_api#apps_advert"
      get "/welcome/:id.js"        => 'script#welcome'
      get "/thankyou/:id.js"       => 'script#thankyou'
      get "/confirm/:id.js"        => 'script#confirm'
      get '/'                      => 'api#property'
    end
  end

  userapi_domain = if Rails.env.production?
    'richpays.com'
  else
    'rich.dev'
  end
  constraints domain: userapi_domain, subdomain: 'userapi' do
    scope module: 'dealer_api' do
      get '/'                             => 'application#property'
      get  'offers'                       => 'offers#index', :defaults => { :format => :json }
      get  'offers/:id'                   => 'offers#show', :defaults => { :format => :json }
      get  'campaigns'                    => 'campaigns#index', :defaults => { :format => :json }
      get  'campaigns/:id'                => 'campaigns#show', :defaults => { :format => :json }
      post 'campaigns/:id/mass_offer_set' => 'campaigns#mass_offer_set', :defaults => { :format => :json }
      post 'campaigns/:id/set_all_offers' => 'campaigns#set_all_offers', :defaults => { :format => :json }
    end
  end

unless Rails.env.development?
    constraints domain: 'richpays.net' do
      get  '/robots.txt' => 'home#robots'
    end
  end

  unless Rails.env.development?
    constraints domain: 'apst.me' do
      get  '/'                            => 'home#property'
      scope module: 'api' do
        get  '/agreement'                 => 'landing_subscribers#agreement'
        get  '/unsubscribe'               => 'landing_subscribers#unsubscribe'
        post '/unsubscribe'               => 'landing_subscribers#sms_unsubscribe'
        get  "/:code"                     => 'buyer_session#handler'
        get  "/:code/:anything"           => 'buyer_session#handler'
        post "/:code/send-link"           => 'landing_subscribers#sms_subscribe'
        post "/:code/:anything/send-link" => 'landing_subscribers#sms_subscribe'
        get  '/robots.txt'                => 'api#robots'
        get  '/'                          => 'api#property'
      end
    end
    [ ['itunes', 'apple-com.info' ], [ 'play', 'google-com.me' ] ].each do | subdomain, domain |
      constraints domain: domain, subdomain: subdomain do
        scope module: 'api' do
          get  '/agreement'         => 'landing_subscribers#agreement'
          get  '/unsubscribe'       => 'landing_subscribers#unsubscribe'
          post '/unsubscribe'       => 'landing_subscribers#sms_unsubscribe'
          get  '/:code'             => 'buyer_session#handler'
          get  '/:code/:anything'   => 'buyer_session#handler'
          post "/:code/send-link"   => 'landing_subscribers#sms_subscribe'
          get  '/robots.txt'        => 'api#robots'
          get  '/'                  => 'api#property'
        end
      end
    end
  end

    scope module: :personal do
      resources :faqs
      resources :news
      resources :money
      resources :wallets
      resources :referrers
      resources :notifications
      resources :money_options
      resources :dealer_balance
      resources :requests_history
      resources :countries, only: [ :index ]
      resources :offers, only: [ :index, :show ]
      resources :direct_offers, only: [ :index, :show, :create, :update ], param: :hashid
      get 'offers/:id/countries' => 'offers#countries'
      resources :offer_sets, only: [ :show, :new, :create, :update ]
      resources :payment_requests, only: [ :new, :create ]

      match :current_dealer, to: 'settings#show',   via: :get
      match :current_dealer, to: 'settings#update', via: :put
      get   :sidebar,        to: 'sidebar_notifications#show'

      get 'settings/phone_update', to: 'settings#request_confirmation_code'

      resources :dashboard
      match 'dashboard_campaigns',      to: 'dashboard#campaigns',              via: :get
      match 'dashboard_actions',        to: 'dashboard#conversions',            via: :get
      match 'dashboard_offers',         to: 'dashboard#offers',                 via: :get
      match 'dashboard_offers/:id',     to: 'dashboard#offer',                  via: :get
      match 'dashboard_create_request', to: 'dashboard#create_payment_request', via: :post
      match 'dashboard',                to: 'dashboard#index',                  via: :get

      match 'stats/table',           to: 'dealer_stats#table',           via: :get, as: :table_dealer_stats,           :defaults => { :format => :json }
      match 'stats/dashboard_graph', to: 'dealer_stats#dashboard_graph', via: :get, as: :dashboard_graph_dealer_stats, :defaults => { :format => :json }
      match 'stats/conversions',     to: 'dealer_stats#conversion',      via: :get, as: :conversion_dealer_stats,      :defaults => { :format => :json }
      get   'stats/subscribes',      to: 'dealer_stats#subscribes'

      resources :tickets
      match 'tickets/:id/ticket_messages', to: 'tickets#ticket_messages', via: :get, as: :ticket_messages, :defaults => { :format => :json }

      resources :campaigns
      get   'campaigns_light',              to: 'campaigns#light_index'
      get   'campaigns/:id/weblanding',     to: 'campaigns#weblanding'
      get   'campaigns/:id/check_pdf',      to: 'campaigns#check_pdf'
    end

    namespace :admin do
      get '/'       => 'dashboard#index',    as: :index, via: [ :get, :post ]
      get 'login'   => 'session#new',        as: :login
      post 'login'  => 'session#admin_auth', as: :auth
      post 'resend' => 'session#resend',     as: :resend
      post 'renew'  => 'session#renew',      as: :renew
      get 'logout'  => 'session#destroy',    as: :logout

      resources :sellers, as: :sellers do
        get :common_stats, as: :common_stats
        get :recent_stats, as: :recent_stats, via: [ :get, :post ]
      end

      resources :dealer_stats, only: [ :index ], defaults: { format: :json }
      resources :timing_stats, only: [ :index ], defaults: { format: :json }
      get 'dealer_stats/top_cr', to: 'dealer_stats#top_cr', defaults: { format: :json }

      get  'dealers_list',        to: 'dealers#dealers_list',      defaults: { format: :json }
      post 'dealer_mailings',     to: 'dealer_emails#send_emails', defaults: { format: :json }
      get  'dealer_mailings',     to: 'dealer_emails#index',       defaults: { format: :json }
      get  'dealer_mailings/:id', to: 'dealer_emails#show',        defaults: { format: :json }
      get  'dealers/get_scope',   to: 'dealers#get_scope',         defaults: { format: :json }

      get 'cached_dealers', to: 'dealers#cached', defaults: { format: :json }
      get 'cached_offers', to: 'offers#cached', defaults: { format: :json }

      resources :admin_requests, only: [ :index, :create ]

      resources :dealers, as: :dealers do
        post :change_balance, as: :change_balance
        get  :become, as: :become
      end

      get :requests_for_dealer, to: 'dealers#requests', defaults: { format: :json }

      resources :direct_offers, only: [ :index, :create, :update ]

      resources :payments
      post 'payments/:id/as_csv', to: 'payments#as_csv'
      resources :payment_requests

      resources :offers, as: :offers do
        get :disable, as: :disable, to: 'offers#disable'
      end

      resources :tickets, as: :tickets
      match 'tickets/:id/ticket_messages', to: 'tickets#ticket_messages', via: :get, as: :ticket_messages, :defaults => { :format => :json }
      resources :news, as: :news
      resources :faqs, as: :faqs
      resources :offer_icons, only: [ :create ]
      resources :offer_screenshots, only: [ :create, :destroy ]
      post :mass_offer_switch, to: 'offers#mass_switch'

      get :affilate_offer, as: :affilate_offer, to: 'affilate_offer#show'
    end


  post :support, to: 'home#support'
  authenticate :dealer, lambda { |u| u.is_an_admin } do
    mount Sidekiq::Web => '/sidekiq'
  end
  resources :notification_settings, only: [:show, :update]

  if Rails.env.test? or Rails.env.development?
    scope module: 'api' do
      get  "/s/:id"                         => 'buyer_session#handler'
      get  "/w/:id"                         => 'buyer_session#wapclick'
      get  "/c/:id"                         => 'buyer_session#create'
      get  "/o/:id"                         => 'direct_offer#show',  constraints: { subdomain: 'target' }
      get  "/o/:id"                         => 'buyer_session#show'
      get  "/t/:id"                         => 'buyer_session#tb'
      get  "/k/:id"                         => 'buyer_session#clicker'
    end
  end

  api_domains = if Rails.env.development?
    %w( api5.dev api9.dev mobile333.dev )
  else
    %w( api5.net api9.net mobile333.com xeclick.com dorovsky.com )
  end
  api_domains.each do | api_domain |
    constraints domain: api_domain do
      get  '/'                                => 'home#index', constraints: { domain: 'dorovsky.com' }
      scope module: 'api' do
        get "/apps_advert/:provider"          => "seller_api#apps_advert"
        get  "/s/:id"                         => 'buyer_session#handler'
        get  "/w/:id"                         => 'buyer_session#wapclick'
        post "/s/:id"                         => 'api#http_200_ok'
        get  "/c/:id"                         => 'buyer_session#create'
        get  "/o/:id"                         => 'buyer_session#target_show', constraints: { subdomain: 'target' }
        get  "/o/:id"                         => 'buyer_session#show'
        get  "/t/:id"                         => 'buyer_session#tb'
        get  "/k/:id"                         => 'buyer_session#clicker'
        get  "/c/:code/:anything"             => 'buyer_session#create'
        post "/:code/send-link"               => 'landing_subscribers#sms_subscribe'
        post "/c/:code/send-link"             => 'landing_subscribers#sms_subscribe'
        post "/c/:code/:anything/send-link"   => 'landing_subscribers#sms_subscribe'
        get  '/unsubscribe'                   => 'landing_subscribers#unsubscribe'
        post "/unsubscribe"                   => 'landing_subscribers#sms_unsubscribe'
        get  '/agreement'                     => 'landing_subscribers#agreement'
        get  '/robots.txt'                    => 'api#robots'
        get  '/'                              => 'api#property'
      end
    end
  end

  get "campaign/:id" => 'home#http_200_ok'
  root to: 'home#index'
end
