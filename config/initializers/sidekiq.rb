if ENV["RAILS_ENV"] == 'production'
  Sidekiq.configure_server do |config|
    config.redis = { url: ENV['REDISCLOUD_URL'], namespace: 'sidekiq' }
    database_url = ENV['DATABASE_URL']
    if database_url
      ENV['DATABASE_URL'] = "#{database_url}?pool=#{ ENV['MAX_THREADS'] }"
      ActiveRecord::Base.establish_connection
    end
  end
  Sidekiq.configure_client do |config|
    config.redis = { url: ENV['REDISCLOUD_URL'], namespace: 'sidekiq' }
  end
end
