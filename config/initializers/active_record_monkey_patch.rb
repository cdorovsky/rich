class ActiveRecord::Base

  def human_value( attribute=:'', prefix='' )
    klass = self.class.name.downcase
    value = self.send( attribute ).downcase.to_s
    value = "#{ prefix }_#{ value }" if prefix.present?
    I18n.t "activerecord.attributes.#{ klass }.#{ attribute }.#{ value }"
  end

  def self.symbolized_name
    name.underscore.to_sym
  end

  def symbolized_name
    self.class.symbolized_name
  end

end