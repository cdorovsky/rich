require 'yaml'
module Settings
  class << self

    def load_settings
      result = {}
      config_pathes = get_pathes
      config_pathes.each do | path |
        value = get_values_for path
        key   = get_key_for path
        result[ key ] = value
      end
      result
    end
    
    def get_pathes
      path_to_settings = "#{Rails.root}/config/settings/"
      entries = Dir.entries( path_to_settings )
      entries.delete('.')
      entries.delete('..')
      entries.map! do |filename|
        "#{path_to_settings}#{filename}"
      end
    end
  
    def get_values_for path
      settings = YAML.load_file path
      settings.symbolize_keys 
    end
  
    def get_key_for path
      path.match(/(\w+)\.yml$/)[1].to_sym # '/some/path/in/system/pseudo_sub.yml' => :pseudo_sub
    end

  end

end

SETTINGS = Settings.load_settings
