require 'sneakers'

statsd = Statsd.new(ENV['STATSD_HOST'], 9125)
amqp_url = ENV['CLOUDAMQP_URL'] || ('amqp://guest:guest@localhost:5672')
Sneakers.configure( :amqp => amqp_url, :daemonize => false, workers: 1, threads: 10 )
