class ChunkyPNG::Image

  def to_file

    image_data = split_base64( to_data_url )
    image_data_string = image_data[ :data ]
    image_data_binary = Base64.decode64( image_data_string )

    temp_img_file = Tempfile.new( 'data_url-upload' )
    temp_img_file.binmode
    temp_img_file << image_data_binary
    temp_img_file.rewind

    img_params = {
      filename: "data-uri-img.#{image_data[ :extension ]}",
      type: image_data[ :type ],
      tempfile: temp_img_file
    }
    image = ActionDispatch::Http::UploadedFile.new( img_params )

  end

  private

  def split_base64( uri_str )

    if uri_str.match( %r{^data:(.*?);(.*?),(.*)$} )
      uri = {}
      uri[:type] = $1
      uri[:encoder] = $2
      uri[:data] = $3
      uri[:extension] = $1.split( '/' )[1]
      uri
    else
      nil
    end

  end

end