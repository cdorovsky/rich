* ## Как отключить оффер с оповещениями ##

```
#!ruby

@offer.offer_disabler.disabling_notifications( :instant, @offer )
```

* ## Как быстро выгрузить реферерры для Жени ##

```
#!ruby

MongoEventContainer.find_by( dealer_id: 5, condition: 'actual' ).mongo_event_atomics.where(event_name: 'offer_set').map { |event| event.extras[ :referer ] }
```

* ## Как обновить кэш всем пользователям ##

```
#!ruby
Dealer.all.each(&:cache_dashboard)
```

* ## Как настроить sublime на использование пробелов вместо табов ##

```
#!json

"translate_tabs_to_spaces": true
```



* ## Как выгрузить постбэки ##

```
#!ruby

Postback.where( postback_enabled: true, sended: false ).each do |postback|
  if postback.ready_to_send_postback?
    postback.find_and_relate_conversion!
    postback.send_notification
    postback.sended = true
    postback.save!
  end
end
```
