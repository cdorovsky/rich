require 'rails_helper'

RSpec.describe OfferLimit, :type => :model do

  before :context do
    @campaign = create :campaign
    @dealer = @campaign.dealer
    @offer = create :offer_performance
    @support_offer = create :offer_performance, support: true
    @russia = Country.find_by( abbr: 'RU' ) || create( :russia )
    @country_all = Country.find_by( name: 'All' ) || create( :country_all )
    @offer.countries << @russia
    @offer.countries << @country_all
    @support_offer.countries << @russia
    @support_offer.countries << @country_all
    @campaign.offers = [ @offer, @support_offer ]
    @offer_set = @campaign.offer_sets.where( offer_id: @offer.id ).first
    @support_offer_set = @campaign.offer_sets.where( offer_id: @support_offer.id ).first
  end

  context 'limits callbacks' do

    it 'limit status after create of new offer with 0 limit' do
      expect( @offer.offer_limit.daily_limit_status ).to eq( 'unlimited' )
    end

    it 'limit status after save of offer with not 0 limit' do
      @offer.daily_install_limit = 1000
      @offer.save!
      expect( @offer.offer_limit.daily_limit_status ).to eq( 'available' )
    end

    it 'limit status after save of offer with 0 limit' do
      @offer.daily_install_limit = 1000
      @offer.save!
      @offer.daily_install_limit = 0
      @offer.save!
      expect( @offer.offer_limit.daily_limit_status ).to eq( 'unlimited' )
    end

  end

  context 'limit increments and checks' do

    it 'resets limit counters correctly' do
      offer_id = @offer.id
      @offer.update( disabled: true, daily_install_limit: 1000 )
      @offer.offer_limit.update( install_count: 1000 )
      @offer.offer_limit.reset_limit_counter
      expect( Offer.find( offer_id ).offer_limit.install_count ).to eq( 0 )
      expect( Offer.find( offer_id ).offer_limit.daily_limit_status ).to eq( 'available' )
      expect( Offer.find( offer_id ).daily_limit_status ).to eq( 'available' )
      expect( Offer.find( offer_id ).disabled ).to eq( false )
    end

    it 'increment install_count correctly' do
      @offer.update( daily_install_limit: 1000, install_count: 0  )
      offer_id = @offer.id
      amount   = 10
      @offer.offer_limit.increment!( :install_count, amount )
      expect( Offer.find( offer_id ).offer_limit.install_count ).to eq( 10 )
    end

    it '20% under cap limit left' do
      Offer.find( @offer.id ).update( daily_install_limit: 1000 )
      offer_id = @offer.id
      amount   = 801
      Offer.find( @offer.id ).offer_limit.increment!( :install_count, amount )
      expect( Offer.find( offer_id ).offer_limit.daily_limit_status ).to eq( 'expiring' )
      expect( Offer.find( offer_id ).daily_limit_status ).to eq( 'expiring' )
    end

    it '20% under cap limit left' do
      Offer.find( @offer.id ).update( daily_install_limit: 1000, install_count: 0, daily_limit_status: 'available' )
      offer_id = @offer.id
      amount   = 800
      Offer.find( @offer.id ).offer_limit.increment!( :install_count, amount )
      amount   = 200
      Offer.find( @offer.id ).offer_limit.increment!( :install_count, amount )
      expect( Offer.find( offer_id ).daily_limit_status ).to eq( 'exceeded' )
      expect( Offer.find( offer_id ).offer_limit.daily_limit_status ).to eq( 'exceeded' )
      expect( Offer.find( offer_id ).disabled ).to eq( true )
    end

  end

end
