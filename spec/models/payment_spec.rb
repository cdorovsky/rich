require 'rails_helper'

RSpec.describe Payment, type: :model do

  before :context do
    @dealer = create :dealer
    @wallet = create :wallet, dealer: @dealer
    @dealer.dealer_balance.increment!( :confirmed, 10000 )
    @dealer.dealer_balance.increment!( :for_checkout, 10000 )
    100.times { PaymentRequest.trigger( @dealer, @wallet.id, rand(10..123) ) }
    @requests = PaymentRequest.limit(33)
    @first_few_reqs_ids = @requests.limit(15).pluck(:id)
    @last_few_reqs_ids = @requests.pluck(:id) - @first_few_reqs_ids
    @payment = Payment.create_with_requests( @requests.pluck(:id) )
  end

  it 'creating payment' do
    expect( @payment.payment_requests.is_all?( :status, 'pending' ) ).to eq( true )
    expect( @payment.payment_requests.is_all?( :payment_id, @payment.id ) ).to eq( true )
    expect( @payment.wallet_type ).to eq( @wallet.kind )
  end

  it 'dont do anything with wrong upd params' do
    @payment.process_payout( payment_action: 'upyachka', request_ids: @requests.pluck(:id) )
    expect( @payment.paid ).to eq( false )
    expect( @payment.payment_requests.is_all?( :status, 'pending' ) ).to eq( true )
  end

  it 'cancels some requests' do
    @payment.process_payout( payment_action: 'cancel', request_ids: @first_few_reqs_ids )
    expect( @requests.where( id: @first_few_reqs_ids ).is_all?( :status, 'cancelled' ) ).to eq( true )
  end

  it 'completes some requests and mark payment as paid' do
    @payment.process_payout( payment_action: 'complete', request_ids: @last_few_reqs_ids )
    @payment.process_payout( payment_action: 'cancel', request_ids: @first_few_reqs_ids )
    expect( @requests.where( id: @last_few_reqs_ids ).is_all?( :status, 'completed' ) ).to eq( true )
    expect( @payment.reload.paid ).to eq( true )
  end

end
