require 'rails_helper'

RSpec.describe DealerIncrement, type: :model do

  before :context do
    @dealer = create :dealer
    @dealer_two = create :dealer, reg_params: @dealer.hashid
    @dealer_increment = create :basic_dealer_increment, dealer_id: @dealer_two.id
    DealerIncrement.pay_to_balance( @dealer_two.id )
    @referrer_increment = DealerIncrement.referrer.find_by( reason_id: @dealer_increment.id )
  end

  context 'referral increments' do

    it 'generates referrer increment' do
      expect( @referrer_increment.reason ).to eq( @dealer_increment )
    end

    it 'ref increment got right reason increment count' do
      count_of_referrer_incrs = DealerIncrement.referrer.where( reason_id: @dealer_increment.id ).count
      expect( count_of_referrer_incrs ).to eq( 1 )
    end

    it 'has right ref revenue according to rate' do
      counted_revenue = ( @referrer_increment.reason.confirmed * @dealer.referrer_rate ).round(4)
      expect( counted_revenue ).to eq( @referrer_increment.confirmed )
    end

    it 'not creates ref increment if not offer_set_show increment triggers' do
      create :dealer_increment, dealer_id: @dealer_two.id
      DealerIncrement.pay_to_balance( @dealer_two.id )
      expect( DealerIncrement.referrer.count ).to eq( DealerIncrement.offer_set_show.count )
    end

  end

end
