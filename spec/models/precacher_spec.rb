require 'rails_helper'

RSpec.describe Precacher, :type => :model do

  before( :context ) do
    $redis.flushdb
    @dealer   = create :dealer
    @apps_advert  = create :apps_advert
    @campaign = create :campaign, dealer: @dealer
    @campaign.offers << @apps_advert

    @stats_params = PrecacherParams.stats_params

    DealerStat::APPS_ADVERT_EVENTS.each do | event |
      options = { campaign: @campaign, offer: @apps_advert, current: Time.zone.now.to_i }
      DealerStat.trigger( event, options )
    end

  end

  context 'stats' do
    it 'caches stats when trying to get it' do
      resp = Precacher.load( DealerStat, @dealer, params: @stats_params[0] )
      expected_stats_template = Precacher.new( klass: DealerStat, dealer: @dealer, params: @stats_params[0] ).template
      expect( resp.length ).to eq( expected_stats_template.length )
    end

    it 'can recognize deprecated cache' do
      precacher = Precacher.new( klass: DealerStat, dealer: @dealer, params: @stats_params[1], options: { expiring: 1.day.ago.to_i } )
      precacher.fetch!
      expect( precacher.data_is_actual ).to eq( false )
    end

    it 'recognizes newly cached data as actual' do
      precacher = Precacher.new( klass: DealerStat, dealer: @dealer, params: @stats_params[2], options: {} )
      precacher.fetch!
      expect( precacher.data_is_actual ).to eq( true )
    end

    it 'caches five scopes for dealer without dealer action' do
      $redis.flushdb
      Precacher.replenish_stats_cache_for_dealer( @dealer.id )
      expect( $redis.keys.count ).to eq( PrecacherParams.stats_params.count )
    end

    it 'accepts expiration option' do
      expiration = 900.day.from_now.to_i
      precacher = Precacher.new( klass: DealerStat, dealer: @dealer, params: @stats_params[2], options: { expiring: expiration } )
      precacher.fetch!
      expect( precacher.expiring_time ).to eq( expiration )
    end

    it 'accepts composite records' do
      prec_opts = { sub_keys: [ :dealer_stats, :offers ] }
      precacher = Precacher.new( klass: Dashboard, dealer: @dealer, params: @stats_params[2], options: prec_opts )
      precacher.fetch!
      expect( precacher.sub_keys ).to eq( precacher.template.keys )
    end
  end

end