require 'rails_helper'

RSpec.describe Offer, :type => :model do

  before :context do
    @offer = create :offer_performance
    100.times { create( :random_country ) }
    @available_countries = Country.order( 'id DESC' ).limit( 60 )
    @restricted_countries = Country.order( 'id ASC' ).limit( 60 )
    @offer.countries = @available_countries
    @offer.restricted_countries = @restricted_countries
  end

  context 'countries' do

    it 'sets correct country list' do
      expect( @offer.countries.pluck( :id ) ).to match_array( @available_countries.pluck( :id ) )
    end

    it 'sets correct restricted country list' do
      expect( @offer.restricted_countries.pluck( :id ) ).to eq( @restricted_countries.pluck( :id ).sort )
    end

    it 'got diff between available and restricted countries' do
      countries_diff_from_offer = @offer.countries.pluck( :id ) - @offer.restricted_countries.pluck( :id )
      countries_diff_from_countries = @available_countries.pluck( :id ) - @restricted_countries.pluck( :id )
      expect( countries_diff_from_offer ).to match_array( countries_diff_from_countries )
    end

  end

end
