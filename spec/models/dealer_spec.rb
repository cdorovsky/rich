require 'rails_helper'

RSpec.describe Dealer, type: :model do

  before :context do
    @dealer = create :dealer
  end

  context 'has api_key' do
    it 'generates after create' do
      expect( @dealer.api_key.length ).to eq( 16 )
    end
  end

  context 'callback assigned attributes' do
    it 'saves referrer_id from reg_params' do
      @second_dealer = create :dealer, reg_params: @dealer.hashid
      expect( @second_dealer.referrer_id ).to eq( @dealer.id )
    end
  end

  context 'reg_params' do
    it 'can assign referrer and referal' do
      @refone = create :dealer
      @reftwo = create :dealer, reg_params: @refone.hashid
      expect( @reftwo.referrer ).to eq( @refone )
      expect( @refone.referals.first ).to eq( @reftwo )
    end
  end
end
