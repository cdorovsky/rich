require 'rails_helper'


RSpec.describe SellerRequest, :type => :model do

  before :context do
    @dealer = create :dealer

    @appflood_offer_set_show       = create :appflood_offer_set_show, dealer: @dealer
    @appflooddirect_offer_set_show = create :appflooddirect_offer_set_show, dealer: @dealer
    @everyads_offer_set_show       = create :everyads_offer_set_show, dealer: @dealer
    @glispa_offer_set_show         = create :glispa_offer_set_show, dealer: @dealer
    @iconpeak_offer_set_show       = create :iconpeak_offer_set_show, dealer: @dealer
    @matomy_offer_set_show         = create :matomy_offer_set_show, dealer: @dealer
    @mobvista_offer_set_show       = create :mobvista_offer_set_show, dealer: @dealer
    @mobvistadirect_offer_set_show = create :mobvistadirect_offer_set_show, dealer: @dealer
    @performance_offer_set_show    = create :performance_offer_set_show, dealer: @dealer
    @unileadnetwork_offer_set_show = create :unileadnetwork_offer_set_show, dealer: @dealer
    @yeahmobi_offer_set_show       = create :yeahmobi_offer_set_show, dealer: @dealer

    @appflood_params       = { provider: 'appflood',       aff_sub:   @appflood_offer_set_show.mongo_id }
    @appflooddirect_params = { provider: 'appflooddirect', aff_sub:   @appflooddirect_offer_set_show.mongo_id }
    @everyads_params       = { provider: 'everyads',       aff_sub:   @everyads_offer_set_show.mongo_id }
    @glispa_params         = { provider: 'glispa',         placement: @glispa_offer_set_show.mongo_id }
    @iconpeak_params       = { provider: 'iconpeak',       aff_sub:   @iconpeak_offer_set_show.mongo_id }
    @matomy_params         = { provider: 'matomy',         postback:  @matomy_offer_set_show.mongo_id }
    @mobvista_params       = { provider: 'mobvista',       aff_sub:   @mobvista_offer_set_show.mongo_id }
    @mobvistadirect_params = { provider: 'mobvistadirect', aff_sub:   @mobvistadirect_offer_set_show.mongo_id }
    @performance_params    = { provider: 'performance',    aff_sub:   @performance_offer_set_show.mongo_id }
    @unileadnetwork_params = { provider: 'unileadnetwork', aff_sub:   @unileadnetwork_offer_set_show.mongo_id }
    @yeahmobi_params       = { provider: 'yeahmobi',       aff_sub:   @yeahmobi_offer_set_show.mongo_id }
  end

  context 'trigger' do
    it 'appflood' do
      SellerRequest.new( @appflood_params )
      expect( MongoEventAtomic.where( 'extras.mongo_id' => @appflood_params[ :aff_sub ] ).count ).to eq( 1 )
    end
    it 'appflooddirect' do
      SellerRequest.new( @appflooddirect_params )
      expect( MongoEventAtomic.where( 'extras.mongo_id' => @appflooddirect_params[ :aff_sub ] ).count ).to eq( 1 )
    end
    it 'everyads' do
      SellerRequest.new( @everyads_params )
      expect( MongoEventAtomic.where( 'extras.mongo_id' => @everyads_params[ :aff_sub ] ).count ).to eq( 1 )
    end
    it 'glispa' do
      SellerRequest.new( @glispa_params )
      expect( MongoEventAtomic.where( 'extras.mongo_id' => @glispa_params[ :placement ] ).count ).to eq( 1 )
    end
    it 'iconpeak' do
      SellerRequest.new( @iconpeak_params )
      expect( MongoEventAtomic.where( 'extras.mongo_id' => @iconpeak_params[ :aff_sub ] ).count ).to eq( 1 )
    end
    it 'matomy' do
      SellerRequest.new( @matomy_params )
      expect( MongoEventAtomic.where( 'extras.mongo_id' => @matomy_params[ :postback ] ).count ).to eq( 1 )
    end
    it 'mobvista' do
      SellerRequest.new( @mobvista_params )
      expect( MongoEventAtomic.where( 'extras.mongo_id' => @mobvista_params[ :aff_sub ] ).count ).to eq( 1 )
    end
    it 'mobvistadirect' do
      SellerRequest.new( @mobvistadirect_params )
      expect( MongoEventAtomic.where( 'extras.mongo_id' => @mobvistadirect_params[ :aff_sub ] ).count ).to eq( 1 )
    end
    it 'performance' do
      SellerRequest.new( @performance_params )
      expect( MongoEventAtomic.where( 'extras.mongo_id' => @performance_params[ :aff_sub ] ).count ).to eq( 1 )
    end
    it 'unileadnetwork' do
      SellerRequest.new( @unileadnetwork_params )
      expect( MongoEventAtomic.where( 'extras.mongo_id' => @unileadnetwork_params[ :aff_sub ] ).count ).to eq( 1 )
    end
    it 'yeahmobi' do
      SellerRequest.new( @yeahmobi_params )
      expect( MongoEventAtomic.where( 'extras.mongo_id' => @yeahmobi_params[ :aff_sub ] ).count ).to eq( 1 )
    end
  end

end
