require 'rails_helper'

RSpec.describe Ticket, :type => :model do

  before :context do
    @ticket = create :ticket
    @invalid_ticket = Ticket.create
  end


  context 'structure' do

    it 'validations' do
      expect( @invalid_ticket.errors.present? ).to eq( true )
    end

  end

  context 'creation' do

    it 'appears to be read' do
      expect( @ticket.read ).to eq( true )
    end

  end

  context 'admin answer simulation' do

    it 'makes ticket unread' do
      @ticket.message = 'privet ya admin'
      @ticket.message_counter += 1
      @ticket.last_answered = 'support'
      message = @ticket.create_message( ownership: :support )
      @ticket.notify_dealer
      @ticket.save
      expect( @ticket.read ).to eq( false )
    end

  end

end
