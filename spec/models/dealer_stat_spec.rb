require 'rails_helper'

RSpec.describe DealerStat, :type => :model do

  before( :context ) do
    @dealer   = create :dealer
    @apps_advert  = create :apps_advert
    @campaign = create :campaign, dealer: @dealer
    @campaign.offers << @apps_advert

    DealerStat::APPS_ADVERT_EVENTS.each do | event |
      options = { campaign: @campaign, offer: @apps_advert, current: Time.zone.now.to_i }
      DealerStat.trigger( event, options )
    end

    current_time  = Time.zone.now
    current_month = Time.zone.now.month
    current_day   = Time.zone.now.day
    @current_hour = Time.zone.now.hour
    @wrong_hour   = @current_hour + 1

    basic_search = { no_grouping: true, hourly: true, hour: @current_hour, day: current_day, month: current_month, 
                     current: @current_hour, dealer_id: @dealer.id, purpose: "apps_advert", "apps_advert" => true }

    @forced_wrong_hour_search = basic_search
    @forced_wrong_hour_search[ :hour ] = @wrong_hour

    @forced_wrong_curent_search = basic_search
    @forced_wrong_curent_search[ :current ] = @wrong_hour

  end

  context 'structure' do

    it 'set dealer_id' do
      expect( DealerStat.all.pluck( :dealer_id ) ).to include( @dealer.id )
    end

    it 'set campaign_id for campaign_grouping' do
      expect( DealerStat.campaign_grouping.pluck( :campaign_id ) ).to include( @campaign.id )
    end
    
    it 'dont set campaign_id for no_grouping and offer_grouping' do
      expect( DealerStat.no_grouping.pluck( :campaign_id ).sample ).to eq( nil )
      expect( DealerStat.offer_grouping.pluck( :campaign_id ).sample ).to eq( nil )
    end

    it 'set offer_id for offer_grouping' do
      expect( DealerStat.offer_grouping.pluck( :offer_id ) ).to include( @apps_advert.id )
    end
    
    it 'dont set offer_id for no_grouping and campaign_grouping' do
      expect( DealerStat.no_grouping.pluck( :offer_id ).sample ).to eq( nil )
      expect( DealerStat.campaign_grouping.pluck( :offer_id ).sample ).to eq( nil )
    end
    
    it 'hourly stats has hour, day, month' do
      DealerStat.hourly.each do | stat |
        expect( stat.hour  ).to_not eq( nil )
        expect( stat.day   ).to_not eq( nil )
        expect( stat.month ).to_not eq( nil )
      end
    end

    it 'daily stats has day, month' do
      DealerStat.daily.each do | stat |
        expect( stat.hour  ).to     eq( nil )
        expect( stat.day   ).to_not eq( nil )
        expect( stat.month ).to_not eq( nil )
      end
    end

    it 'monthly stats has month' do
      DealerStat.monthly.each do | stat |
        expect( stat.hour  ).to     eq( nil )
        expect( stat.day   ).to     eq( nil )
        expect( stat.month ).to_not eq( nil )
      end
    end

  end

  context 'loading collections' do

    it 'load subscribes is hash' do
      expect( DealerStat.load_subscribes( @dealer ).class ).to eq( Hash )
    end

    it 'load subscribes has three keys: mts, megafon, beeline' do
      expect( DealerStat.load_subscribes( @dealer ).keys ).to match_array( %i( beeline megafon mts ) )
    end

  end

end
