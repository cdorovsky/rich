require 'rails_helper'

RSpec.describe AdminRequest, :type => :model do

  before :context do
    @dealer = create :dealer
  end

  context 'trigger admin_request' do

    it 'creates request' do
      @dealer.dealer_balance.update( confirmed: 100 )
      admin_request = AdminRequest.trigger( @dealer, -50 )
      expect( admin_request.valid? ).to eq( true )
    end

    it 'increases balance by value' do
      @dealer.dealer_balance.update( confirmed: 100 )
      rand_num = rand( 150 )
      expect{ AdminRequest.trigger( @dealer, rand_num ) }.to change{ @dealer.dealer_balance.confirmed }.by( rand_num )
    end

    it 'triggers manually created admin request' do
      admin_request = AdminRequest.new( dealer_id: @dealer.id, value: -12 )
      prev_val = @dealer.dealer_balance.confirmed
      admin_request.save
      expect( admin_request.value ).to eq( DealerIncrement.find_by( dealer_id: @dealer.id, reason_type: 'AdminRequest' ).confirmed )
    end

  end

end
