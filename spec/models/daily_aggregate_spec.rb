require 'rails_helper'

RSpec.describe DailyAggregate, :type => :model do

  before :context do
    @campaign = create :campaign
    @dealer = @campaign.dealer
    @offer = create :offer_performance
    @russia = Country.find_by( abbr: 'RU' ) || create( :russia )
    @country_all = Country.find_by( name: 'All' ) || create( :country_all )
    @offer.countries << @russia
    @offer.countries << @country_all
  end

  context 'daily aggregate triggering' do

    it 'short_date idenfies correctly' do
      expect( DailyAggregate.current_short_date ).to eq( Time.zone.now.strftime( '%F' ) )
    end

    it 'trigger app_install from mongo stat' do
      counter = 10
      revenue = @dealer.dealer_revenue( @offer ) * counter
      DailyAggregate.trigger( :app_install, campaign: @campaign, offer: @offer, amount: counter )
      search = { short_date: Time.zone.now.strftime( '%F' ), dealer: @dealer, campaign: @campaign, offer: @offer }
      current_aggregate = DailyAggregate.find_by( search )
      expect( current_aggregate.daily_install_count ).to eq( counter )
      expect( current_aggregate.daily_revenue ).to eq( revenue )
    end

    it 'trigger uniq_visitor from visitor_queue' do
      counter = 10
      DailyAggregate.trigger( :visitor, campaign: @campaign, offer: @offer, amount: counter )
      search = { short_date: Time.zone.now.strftime( '%F' ), dealer: @dealer, campaign: @campaign, offer: @offer }
      current_aggregate = DailyAggregate.find_by( search )
      expect( current_aggregate.daily_visitor_count ).to eq( counter )
    end

    it 'trigger uniq_visitor from visitor_queue' do
      counter = 10
      revenue = @dealer.dealer_revenue( @offer ) * counter
      DailyAggregate.trigger( :app_install, campaign: @campaign, offer: @offer, amount: counter )
      DailyAggregate.trigger( :visitor, campaign: @campaign, offer: @offer, amount: counter )
      search = { short_date: Time.zone.now.strftime( '%F' ), dealer: @dealer, campaign: @campaign, offer: @offer }
      current_aggregate = DailyAggregate.find_by( search )
      expect( DailyAggregate.count ).to eq( 1 )
      expect( current_aggregate.daily_install_count ).to eq( counter )
      expect( current_aggregate.daily_revenue ).to eq( revenue )
      expect( current_aggregate.daily_visitor_count ).to eq( counter )
    end

  end

end
