require 'rails_helper'

RSpec.describe Offer, :type => :model do

  before :context do
    $redis.flushdb
    @campaign = create :campaign
    @dealer = @campaign.dealer
    @offer = create :offer_performance
    @support_offer = create :offer_performance, support: true
    @russia = Country.find_by( abbr: 'RU' ) || create( :russia )
    @country_all = Country.find_by( name: 'All' ) || create( :country_all )
    @offer.countries << @russia
    @offer.countries << @country_all
    @support_offer.countries << @russia
    @support_offer.countries << @country_all
    @campaign.offers = [ @offer, @support_offer ]
    @offer_set = @campaign.offer_sets.where( offer_id: @offer.id ).first
    @support_offer_set = @campaign.offer_sets.where( offer_id: @support_offer.id ).first
  end

  context 'offers in redis' do

    it 'ios offer in redis' do
      Rediska.load_platform
      expect( JSON.parse( $redis.get 'ipad_offers' ).include?( @offer_set.id ) ).to eq( true )
    end

    it 'support ios offer in redis' do
      Rediska.load_platform
      expect( JSON.parse( $redis.get 'ipad_support_offers' ).include?( @support_offer_set.id ) ).to eq( true )
    end

  end

end
