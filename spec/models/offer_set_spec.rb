require 'rails_helper'

RSpec.describe OfferSet, :type => :model do

  before( :context ) do
    @offer    = create :apps_advert
    @campaign = create :campaign
  end

  context 'structure' do
    offer_set = OfferSet.new
    it 'has no campaign_id and offer_id' do
      offer_set.campaign_id = nil
      offer_set.offer_id = nil
      offer_set.save
      expect( offer_set ).to be_a_new( OfferSet )
    end

    it 'has no offer_id' do
      offer_set.campaign_id = @campaign.id
      offer_set.offer_id = nil
      offer_set.save
      expect( offer_set ).to be_a_new( OfferSet )
    end

    it 'has offer_id and campaign_id' do
      offer_set.campaign_id = @campaign.id
      offer_set.offer_id = @offer.id
      offer_set.save
      expect( offer_set ).not_to be_a_new( OfferSet )
    end

  end

  context 'qr queue' do
    it 'must set a job to queue' do
      ResqueSpec.reset!
      offer_set = OfferSet.new
      offer_set.campaign = @campaign
      offer_set.offer    = @offer
      offer_set.save
      expect( offer_set.qr_link ).not_to eq( nil )
    end
  end

end
