require 'rails_helper'

RSpec.describe SidekiqMonitor, :type => :model do

  before :context do
    @dev_dealer = create :dev_dealer
    @russia = Country.find_by( abbr: 'RU' ) || create( :russia )
    @campaign = create :campaign, dealer: @dev_dealer
    @offer = create :offer_performance
    @offer.countries << @russia
    @campaign.offers << @offer
  end

  context 'checking amount of queue' do

    it 'queue < 100' do
      Sidekiq::Queue.new.clear
      expect( SidekiqMonitor.check_resque_queue ).to eq( false )
    end

    it 'queue >= 100' do
      200.times { RedisCacher.perform_async( @dev_dealer.id ) }
      expect( SidekiqMonitor.check_resque_queue ).to eq( true )
      Sidekiq::Queue.new.clear
    end

  end

end
