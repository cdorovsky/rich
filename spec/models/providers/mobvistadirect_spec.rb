require 'rails_helper'

RSpec.describe Providers::AppsAdvert::Mobvistadirect::Sender, :type => :model do

  before :context do
    @campaign = create :campaign
    @dealer = @campaign.dealer
    @offer = create :mobvistadirect_offer
    @russia = Country.find_by( abbr: 'RU' ) || create( :russia )
    @country_all = Country.find_by( name: 'All' ) || create( :country_all )
    @offer.countries << @russia
    @offer.countries << @country_all
    @campaign.offers = [ @offer ]
    @offer_set = @campaign.offer_sets.where( offer_id: @offer.id ).first
    Rediska.load_all
  end

  context 'offers in redis' do

    it 'ios offer in redis' do
      Rediska.load_platform
      @redis_offer = JSON.parse( $redis.get( "offer_set_#{ @offer_set.id }" ) ).with_indifferent_access
      redirect_ulr = Providers::AppsAdvert::Mobvistadirect::Sender.redirect( 'test_mongo_id', @redis_offer )
      @correct_url = "http://td.lenzmx.com/click?mb_pl=android&mb_nt=cb1333&mb_campid=lm_pc_global&aff_sub=test_mongo_id&mb_subid=#{ @dealer.id }"
      expect( redirect_ulr ).to eq( @correct_url )
    end

  end

end
