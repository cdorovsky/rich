require 'rails_helper'

RSpec.describe RequestInformation, :type => :model do

  before :context do
    @ip_ru         = '217.118.82.88'
    @ip_de         = '5.83.128.0'
    @ip_us         = '206.17.20.75'
    @ip_unreadable = 'wrong_ip'
    @android       = 'Mozilla/5.0 (Android; CPU iPhone OS 7_1_2 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Version/7.0 Mobile/11D257 Safari/9537.53'
    @linux         = 'Mozilla/5.0 (Linux; CPU iPhone OS 7_1_2 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Version/7.0 Mobile/11D257 Safari/9537.53'
    @ipad          = 'Mozilla/5.0 (iPad; CPU iPhone OS 7_1_2 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Version/7.0 Mobile/11D257 Safari/9537.53'
    @iphone        = 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_1_2 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Version/7.0 Mobile/11D257 Safari/9537.53'
    @ipod          = 'Mozilla/5.0 (iPod; CPU iPhone OS 7_1_2 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Version/7.0 Mobile/11D257 Safari/9537.53'
    @blackberry    = 'Mozilla/5.0 (Blackberry; CPU iPhone OS 7_1_2 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Version/7.0 Mobile/11D257 Safari/9537.53'
    @windows       = 'Mozilla/5.0 (Windows; CPU iPhone OS 7_1_2 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Version/7.0 Mobile/11D257 Safari/9537.53'
    @midp          = 'Mozilla/5.0 (midp; CPU iPhone OS 7_1_2 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Version/7.0 Mobile/11D257 Safari/9537.53'
    @symbian       = 'Mozilla/5.0 (symbian; CPU iPhone OS 7_1_2 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Version/7.0 Mobile/11D257 Safari/9537.53'
  end

  context 'request information countries' do

    it 'ru_country' do
      request = double( remote_ip: @ip_ru, user_agent: @android )
      allow( request ).to receive( :env ).and_return( Hash.new )
      ru_request = RequestInformation.new( request )
      expect( ru_request.country ).to eq( 'RU' )
    end

    it 'german_country' do
      request = double( remote_ip: @ip_de, user_agent: @android )
      allow( request ).to receive( :env ).and_return( Hash.new )
      de_request = RequestInformation.new( request )
      expect( de_request.country ).to eq( 'DE' )
    end

    it 'usa_country' do
      request = double( remote_ip: @ip_us, user_agent: @android )
      allow( request ).to receive( :env ).and_return( Hash.new )
      us_request = RequestInformation.new( request )
      expect( us_request.country ).to eq( 'US' )
    end

    it 'na_country' do
      request = double( remote_ip: @ip_unreadable, user_agent: @android )
      allow( request ).to receive( :env ).and_return( Hash.new )
      na_request = RequestInformation.new( request )
      expect( na_request.country ).to eq( 'N/A' )
    end

  end

  context 'request information platforms' do
 
    it 'android_platform_android' do
      request = double( remote_ip: @ip_ru, user_agent: @android )
      allow( request ).to receive( :env ).and_return( Hash.new )
      android_request = RequestInformation.new( request )
      expect( android_request.user_platform ).to eq( 'android' )
    end 

    it 'android_platform_linux' do
      request = double( remote_ip: @ip_ru, user_agent: @linux )
      allow( request ).to receive( :env ).and_return( Hash.new )
      android_request = RequestInformation.new( request )
      expect( android_request.user_platform ).to eq( 'android' )
    end

    it 'ipad_platform' do
      request = double( remote_ip: @ip_ru, user_agent: @ipad )
      allow( request ).to receive( :env ).and_return( Hash.new )
      ipad_request = RequestInformation.new( request )
      expect( ipad_request.user_platform ).to eq( 'ipad' )
    end 

    it 'iphone_platform' do
      request = double( remote_ip: @ip_ru, user_agent: @iphone )
      allow( request ).to receive( :env ).and_return( Hash.new )
      ipod_iphone_request = RequestInformation.new( request )
      expect( ipod_iphone_request.user_platform ).to eq( 'ipod_iphone' )
    end 

    it 'ipod_platform' do
      request = double( remote_ip: @ip_ru, user_agent: @ipod )
      allow( request ).to receive( :env ).and_return( Hash.new )
      ipod_iphone_request = RequestInformation.new( request )
      expect( ipod_iphone_request.user_platform ).to eq( 'ipod_iphone' )
    end 

    it 'mobile_platform_blackberry' do
      request = double( remote_ip: @ip_ru, user_agent: @blackberry )
      allow( request ).to receive( :env ).and_return( Hash.new )
      mobile_request = RequestInformation.new( request )
      expect( mobile_request.user_platform ).to eq( 'mobile' )
    end 

    it 'mobile_platform_windows' do
      request = double( remote_ip: @ip_ru, user_agent: @windows )
      allow( request ).to receive( :env ).and_return( Hash.new )
      mobile_request = RequestInformation.new( request )
      expect( mobile_request.user_platform ).to eq( 'mobile' )
    end 

    it 'mobile_platform_midp' do
      request = double( remote_ip: @ip_ru, user_agent: @midp )
      allow( request ).to receive( :env ).and_return( Hash.new )
      mobile_request = RequestInformation.new( request )
      expect( mobile_request.user_platform ).to eq( 'mobile' )
    end 

    it 'mobile_platform_symbian' do
      request = double( remote_ip: @ip_ru, user_agent: @symbian )
      allow( request ).to receive( :env ).and_return( Hash.new )
      mobile_request = RequestInformation.new( request )
      expect( mobile_request.user_platform ).to eq( 'mobile' )
    end 

    it 'nil_platform' do
      request = double( remote_ip: @ip_ru, user_agent: nil )
      allow( request ).to receive( :env ).and_return( Hash.new )
      pc_request = RequestInformation.new( request )
      expect( pc_request.user_platform ).to eq( 'pc' )
    end 

  end

end
