require 'rails_helper'

RSpec.describe CampaignLimiter, :type => :model do

  before :context do
    @campaign = create :campaign
    @limiter  = @campaign.campaign_limiter
    @days_full_hash  = { "d1"=>true, "d2"=>true, "d3"=>true, "d4"=>true, "d5"=>true, "d6"=>true, "d0"=>true }
    @hours_full_hash = { "h0"=>true, "h1"=>true, "h2"=>true, "h3"=>true, "h4"=>true, "h5"=>true, "h6"=>true, 
                         "h7"=>true, "h8"=>true, "h9"=>true, "h10"=>true, "h11"=>true, "h12"=>true, "h13"=>true, 
                         "h14"=>true, "h15"=>true, "h16"=>true, "h17"=>true, "h18"=>true, "h19"=>true, "h20"=>true, 
                         "h21"=>true, "h22"=>true, "h23"=>true }
  end

  context 'inner model functionality' do

    it 'autocreation' do
      expect( @limiter ).not_to eq( nil )
    end

    it 'days_hash' do
      expect( @limiter.days_hash ).to eq( @days_full_hash )
    end

    it 'hours_hash' do
      expect( @limiter.hours_hash ).to eq( @hours_full_hash )
    end

    it 'day check with all days on' do
      expect( CampaignLimiter.day_check( 0, 127 ) ).to eq( true )
    end

    it 'day check with sunday off' do
      expect(CampaignLimiter.day_check( 0, 126 ) ).to eq( false )
    end

    it 'hour check with all hours on' do
      expect( CampaignLimiter.hour_check( 0, 16777215 ) ).to eq( true )
    end

    it 'hour check with 23 off' do
      expect( CampaignLimiter.hour_check( 23, 16777214 ) ).to eq( false )
    end

  end

end
