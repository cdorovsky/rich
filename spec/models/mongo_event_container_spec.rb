require 'rails_helper'

RSpec.describe MongoEventContainer, :type => :model do

  before :context do
    @russia      = Country.find_by( abbr: 'RU' )  || create( :russia )
    @country_all = Country.find_by( abbr: 'ALL' ) || create( :country_all )
    @ip_ru   = '217.118.82.88'
    @iphone  = 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_1_2 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Version/7.0 Mobile/11D257 Safari/9537.53'

    @campaign = create :campaign
    @dealer = @campaign.dealer
    @offer     = create :ipod_iphone_offer
    @campaign.offers = [ @offer ]
    @offer_set = @campaign.offer_sets.find_by( offer_id: @offer )
    @request_params = { 'HTTP_REFERER' => 'http://google.com' }
    @web_params = { id: @offer_set.id, sub_id: 'sub_id', sub_id2: 'sub_id2', sub_id3: 'sub_id3' }

    @appflood_offer_set_show = create :appflood_offer_set_show, campaign: @campaign, dealer: @dealer, offer: @offer, ip: IPAddr.new( @ip_ru ), offer_set: @offer_set, sub_id: '1', sub_id2: '2', sub_id3: '3'
    @appflood_params = { provider: 'appflood', aff_sub: @appflood_offer_set_show.mongo_id }

    Rediska.load_all
  end

  context 'import' do

    it 'visitors' do
      request        = double( remote_ip: @ip_ru, user_agent: @iphone, format: 'html' )
      allow( request ).to receive( :env ).and_return( @request_params )
      buyer_request = BuyerRequest.new( request, @web_params )
      mongo_container = MongoEventContainer.actual( @dealer.id ).first
      mongo_container.import_to_pg
      offer_set_show = OfferSetShow.find_by( dealer_id: @dealer.id, campaign_id: @campaign.id, offer_set_id: @offer_set.id, referer: 'http://google.com' )
      expect( offer_set_show.country_code ).to eq( @russia.abbr )
      expect( offer_set_show.user_platform ).to eq( 'ipod_iphone' )
    end

    it 'uniq js_push' do
      DealerStat.where( dealer_id: @dealer.id ).delete_all
      Clicker.new( id: @offer_set.id, kind: 'push', uniq: true )
      mongo_container = MongoEventContainer.actual( @dealer.id ).first
      mongo_container.import_to_pg
      expect( DealerStat.where( dealer_id: @dealer.id ).last.uniq_js_visitor ).to eq( 1 )
      expect( DealerStat.where( dealer_id: @dealer.id ).last.js_visitor ).to eq( 1 )
    end

    it 'not uniq js_push' do
      DealerStat.where( dealer_id: @dealer.id ).delete_all
      Clicker.new( id: @offer_set.id, kind: 'push', uniq: true )
      Clicker.new( id: @offer_set.id, kind: 'push' )
      mongo_container = MongoEventContainer.actual( @dealer.id ).first
      mongo_container.import_to_pg
      expect( DealerStat.where( dealer_id: @dealer.id ).last.uniq_js_visitor ).to eq( 1 )
      expect( DealerStat.where( dealer_id: @dealer.id ).last.js_visitor ).to eq( 2 )
    end

    it 'uniq clicker-visitor' do
      DealerStat.where( dealer_id: @dealer.id ).delete_all
      Clicker.new( id: @offer_set.id, uniq: true )
      mongo_container = MongoEventContainer.actual( @dealer.id ).first
      mongo_container.import_to_pg
      expect( DealerStat.where( dealer_id: @dealer.id ).last.uniq_visitor ).to eq( 1 )
      expect( DealerStat.where( dealer_id: @dealer.id ).last.visitor ).to eq( 1 )
    end

    it 'not uniq clicker-visitor' do
      DealerStat.where( dealer_id: @dealer.id ).delete_all
      Clicker.new( id: @offer_set.id, uniq: true )
      Clicker.new( id: @offer_set.id )
      mongo_container = MongoEventContainer.actual( @dealer.id ).first
      mongo_container.import_to_pg
      expect( DealerStat.where( dealer_id: @dealer.id ).last.uniq_visitor ).to eq( 1 )
      expect( DealerStat.where( dealer_id: @dealer.id ).last.visitor ).to eq( 2 )
    end

    it 'conversions 2 same and now double conversions' do
      SellerRequest.new( @appflood_params )
      SellerRequest.new( @appflood_params )
      mongo_container = MongoEventContainer.actual( @dealer.id ).first
      mongo_container.import_to_pg
      expect( Conversion.where(      dealer_id: @dealer.id ).count ).to eq( 1 )
      expect( DealerIncrement.where( dealer_id: @dealer.id ).count ).to eq( 1 )
      conversion       = Conversion.find_by(      dealer_id: @dealer.id )
      dealer_increment = DealerIncrement.find_by( dealer_id: @dealer.id )
      expect( conversion.mongo_id ).to       eq( @appflood_params[ :aff_sub ] )
      expect( conversion.mongo_id ).to       eq( @appflood_offer_set_show.mongo_id )
      expect( conversion.offer_id ).to       eq( @appflood_offer_set_show.offer_id )
      expect( conversion.campaign_id ).to    eq( @appflood_offer_set_show.campaign_id )
      expect( conversion.dealer_id ).to      eq( @appflood_offer_set_show.dealer_id )
      expect( conversion.offer_name ).to     eq( @offer.name )
      expect( conversion.dealer_revenue ).to eq( @dealer.dealer_revenue( @offer ) )
      expect( conversion.campaign_name ).to  eq( @campaign.name )
      expect( conversion.country ).to        eq( @russia )
      expect( conversion.kind ).to           eq( @offer.kind )
      expect( conversion.sub_id ).to         eq( @appflood_offer_set_show.sub_id )
      expect( conversion.sub_id2 ).to        eq( @appflood_offer_set_show.sub_id2 )
      expect( conversion.sub_id3 ).to        eq( @appflood_offer_set_show.sub_id3)
      expect( dealer_increment.dealer_balance ).to eq( @dealer.dealer_balance )
      expect( dealer_increment.dealer_id ).to      eq( @dealer.id )
      expect( dealer_increment.offer_id ).to       eq( @offer.id )
      expect( dealer_increment.campaign_id ).to    eq( @campaign.id )
      expect( dealer_increment.confirmed ).to      eq( @dealer.dealer_revenue( @offer) )
      expect( dealer_increment.reason ).to         eq( @appflood_offer_set_show )
    end

  end

end
