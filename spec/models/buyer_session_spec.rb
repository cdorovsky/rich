require 'rails_helper'

RSpec.describe BuyerSession, :type => :model do

  before :context do
    
    $redis.flushdb
    Country.delete_all
    @russia      = Country.find_by( abbr: 'RU' )  || create( :russia )
    @usa         = Country.find_by( abbr: 'US' )  || create( :usa )
    @thai        = Country.find_by( abbr: 'TH' )  || create( :thai )
    @country_all = Country.find_by( abbr: 'ALL' ) || create( :country_all )
    @na_country  = Country.find_by( abbr: 'N/A' ) || create( :na_country )

    @campaign       = create :campaign
    @other_campaign = create :campaign

    @ip_ru   = '217.118.82.88'
    @ip_us   = '207.5.112.114'
    @ip_tai  = '125.24.79.169'
    @no_ip   = '0.0.0.0'

    @russian_offer      = create :russian_offer
    @tai_offer          = create :tai_offer
    @american_offer     = create :american_offer
    @world_offer        = create :global_offer
    @campaign.offers    = [ @russian_offer, @tai_offer, @american_offer, @world_offer ]
    @russian_offer_set  = @campaign.offer_sets.find_by( offer_id: @russian_offer.id )
    @tai_offer_set      = @campaign.offer_sets.find_by( offer_id: @tai_offer.id )
    @american_offer_set = @campaign.offer_sets.find_by( offer_id: @american_offer.id )
    @world_offer_set    = @campaign.offer_sets.find_by( offer_id: @world_offer.id )

    @iphone  = 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_1_2 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Version/7.0 Mobile/11D257 Safari/9537.53'
    @ipod    = 'Mozilla/5.0 (iPod; U; CPU like Mac OS X; en) AppleWebKit/420.1 (KHTML, like Gecko) Version/3.0 Mobile/3A101a Safari/419.3'
    @ipad    = 'Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) version/4.0.4 Mobile/7B367 Safari/531.21.10'
    @android = 'Mozilla/5.0 (Linux; U; Android 4.0.3; ko-kr; LG-L160L Build/IML74K) AppleWebkit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30'
    @mobile      = 'Mozilla/5.0 (Linux; U; Android 4.0.3; ko-kr; LG-L160L Build/IML74K) AppleWebkit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30'

    @ipad_offer            = create :ipad_offer
    @ipod_iphone_offer     = create :ipod_iphone_offer
    @ios_offer             = create :ios_offer
    @android_offer         = create :android_offer
    @mobile_offer          = create :mobile_offer
    @other_campaign.offers = [ @ipad_offer, @ipod_iphone_offer, @ios_offer, @android_offer, @mobile_offer ]
    @ipad_offer_set        = @other_campaign.offer_sets.find_by( offer_id: @ipad_offer )
    @ipod_iphone_offer_set = @other_campaign.offer_sets.find_by( offer_id: @ipod_iphone_offer )
    @ios_offer_set         = @other_campaign.offer_sets.find_by( offer_id: @ios_offer )
    @android_offer_set     = @other_campaign.offer_sets.find_by( offer_id: @android_offer )
    @mobile_offer_set      = @other_campaign.offer_sets.find_by( offer_id: @mobile_offer )

    Rediska.load_all
  end

  context 'country filter' do

    it 'russian ip' do
      request        = double( remote_ip: @ip_ru, user_agent: @iphone )
      allow( request ).to receive( :env ).and_return( Hash.new )
      @buyer_session = BuyerSession.new( request, id: @campaign.hash_code )
      expect( @buyer_session.offers_to_show ).to match_array( [ @russian_offer_set.id, @world_offer_set.id ] )
    end

    it 'american ip' do
      request        = double( remote_ip: @ip_us, user_agent: @iphone )
      allow( request ).to receive( :env ).and_return( Hash.new )
      @buyer_session = BuyerSession.new( request, id: @campaign.hash_code )
      expect( @buyer_session.offers_to_show ).to match_array( [ @american_offer_set.id, @world_offer_set.id ] )
    end

    it 'thai ip' do
      request        = double( remote_ip: @ip_tai, user_agent: @iphone )
      allow( request ).to receive( :env ).and_return( Hash.new )
      @buyer_session = BuyerSession.new( request, id: @campaign.hash_code )
      expect( @buyer_session.offers_to_show ).to match_array( [ @tai_offer_set.id, @world_offer_set.id ] )
    end

    it 'no ip' do
      request        = double( remote_ip: @no_ip, user_agent: @iphone )
      allow( request ).to receive( :env ).and_return( Hash.new )
      @buyer_session = BuyerSession.new( request, id: @campaign.hash_code )
      expect( @buyer_session.offers_to_show ).to match_array( [ @world_offer_set.id ] )
    end

  end

  context 'platform filter' do

    it 'ipad' do
      request        = double( remote_ip: @no_ip, user_agent: @ipad )
      allow( request ).to receive( :env ).and_return( Hash.new )
      @buyer_session = BuyerSession.new( request, id: @other_campaign.hash_code )
      expect( @buyer_session.offers_to_show ).to match_array( [ @ipad_offer_set.id, @ios_offer_set.id, @mobile_offer_set.id ] )
    end

    it 'ipod' do
      request        = double( remote_ip: @no_ip, user_agent: @ipod )
      allow( request ).to receive( :env ).and_return( Hash.new )
      @buyer_session = BuyerSession.new( request, id: @other_campaign.hash_code )
      expect( @buyer_session.offers_to_show ).to match_array( [ @ipod_iphone_offer_set.id, @ios_offer_set.id, @mobile_offer_set.id ] )
    end

    it 'iphone' do
      request        = double( remote_ip: @no_ip, user_agent: @iphone )
      allow( request ).to receive( :env ).and_return( Hash.new )
      @buyer_session = BuyerSession.new( request, id: @other_campaign.hash_code )
      expect( @buyer_session.offers_to_show ).to match_array( [ @ipod_iphone_offer_set.id, @ios_offer_set.id, @mobile_offer_set.id ] )
    end

    it 'android'  do
      request        = double( remote_ip: @no_ip, user_agent: @android )
      allow( request ).to receive( :env ).and_return( Hash.new )
      @buyer_session = BuyerSession.new( request, id: @other_campaign.hash_code )
      expect( @buyer_session.offers_to_show ).to match_array( [ @android_offer_set.id, @mobile_offer_set.id ] )
    end

    it 'mobile' do
      request        = double( remote_ip: @no_ip, user_agent: @mobile )
      allow( request ).to receive( :env ).and_return( Hash.new )
      @buyer_session = BuyerSession.new( request, id: @other_campaign.hash_code )
      expect( @buyer_session.offers_to_show.sort ).to eq( [ @mobile_offer_set.id, @android_offer_set.id ].sort )
    end

  end

end
