require 'rails_helper'

RSpec.describe PaymentRequest, :type => :model do

  before :context do
    @dealer = create :dealer
    @wallet = create :wallet, dealer: @dealer
  end

  context 'check for_checkout validations' do

    it 'for_checkout less then requested' do
      payment_request = PaymentRequest.trigger( @dealer, @wallet.id, 100 )
      expect( payment_request ).to eq( false )
    end

    it 'for_checkout less then requested, but current confirmed ballance is enough' do
      @dealer.dealer_balance.increment!( :confirmed, 100 )
      payment_request = PaymentRequest.trigger( @dealer, @wallet.id, 100 )
      expect( payment_request ).to eq( false )
    end

    it 'request is less then 10$' do
      @dealer.dealer_balance.increment!( :confirmed, 100 )
      payment_request = PaymentRequest.trigger( @dealer, @wallet.id, 5 )
      expect( payment_request ).to eq( false )
    end

    it 'request is equal 10$' do
      @dealer.dealer_balance.increment!( :for_checkout, 100 )
      payment_request = PaymentRequest.trigger( @dealer, @wallet.id, 10 )
      expect( payment_request.valid? ).to eq( true )
    end

    it 'for_checkout enough then requested, but current confirmed ballance is enough' do
      @dealer.dealer_balance.increment!( :for_checkout, 110 )
      payment_request = PaymentRequest.trigger( @dealer, @wallet.id, 100 )
      expect( payment_request.valid? ).to eq( true )
    end

    it 'for_checkout enough then requested, but current confirmed ballance is enough' do
      @dealer.dealer_balance.increment!( :confirmed, 100 )
      @dealer.dealer_balance.increment!( :for_checkout, 200 )
      payment_request = PaymentRequest.trigger( @dealer, @wallet.id, 100 )
      expect( payment_request.valid? ).to eq( true )
    end
  end

end
