require 'rails_helper'

RSpec.describe TbReferrer, type: :model do

  before :context do
    @dealer = create :dealer
    @russia      = Country.find_by( abbr: 'RU' )  || create( :russia )
    @country_all = Country.find_by( abbr: 'ALL' ) || create( :country_all )
    @campaign       = create :campaign

    @android_offer  = create( :russian_offer, apps_os: 'android' )
    @appflooddirect = create :appflooddirect_offer
    @campaign.offers = [ @android_offer ]
    @appflood_offer_set       = @campaign.offer_sets.find_by( offer_id: @appflood )
    @appflooddirect_offer_set = @campaign.offer_sets.find_by( offer_id: @appflooddirect )
    @android_platform = 'android'
    @ipad_platform    = 'ipad'
    @ru_abbr = 'RU'
    @en_abbr = 'EN'
    @redirect_code = @campaign.hash_code

    Rediska.load_all
  end

  context 'OfferFilter.tb_reason' do

    it 'should return undefined if theres no tb' do
      tb_reason = OffersFilter.tb_reason( @android_platform, @ru_abbr, @campaign.offer_sets.pluck( :id ) )
      expect( tb_reason ).to eq( 'undefined' )
    end

    it 'should return empty_campaign if campaign has no enabled offers' do
      tb_reason = OffersFilter.tb_reason( @ipad_platform, @ru_abbr, [] )
      expect( tb_reason ).to eq( 'empty_campaign' )
    end

    it 'should return platform if platform is incorrect' do
      tb_reason = OffersFilter.tb_reason( @ipad_platform, @ru_abbr, @campaign.offer_sets.pluck( :id ) )
      expect( tb_reason ).to eq( 'platform' )
    end

    it 'should return country if country is incorrect' do
      tb_reason = OffersFilter.tb_reason( @android_platform, @en_abbr, @campaign.offer_sets.pluck( :id ) )
      expect( tb_reason ).to eq( 'country' )
    end

  end

  context 'traffic_back with :campaign' do

    it 'reason empty campaign' do
      MongoEventContainer.delete_all
      MongoEventAtomic.delete_all
      tb_reason = OffersFilter.tb_reason( @ipad_platform, @ru_abbr, [] )
      params = { id: @redirect_code, referer: 'test', reason: tb_reason }
      TrafficBack.new( :campaign, params )
      expect( MongoEventAtomic.where( event_name: 'traffic_back' ).last.extras ).to eq( {"referer"=>"test", "reason"=>"empty_campaign"} )
    end

    it 'reason platform' do
      MongoEventContainer.delete_all
      MongoEventAtomic.delete_all
      tb_reason = OffersFilter.tb_reason( @ipad_platform, @ru_abbr, @campaign.offer_sets.pluck( :id ) )
      params = { id: @redirect_code, referer: 'test', reason: tb_reason }
      TrafficBack.new( :campaign, params )
      expect( MongoEventAtomic.where( event_name: 'traffic_back' ).last.extras ).to eq( {"referer"=>"test", "reason"=>"platform"} )
    end

    it 'reason country' do
      MongoEventContainer.delete_all
      MongoEventAtomic.delete_all
      tb_reason = OffersFilter.tb_reason( @android_platform, @en_abbr, @campaign.offer_sets.pluck( :id ) )
      params = { id: @redirect_code, referer: 'test', reason: tb_reason }
      TrafficBack.new( :campaign, params )
      expect( MongoEventAtomic.where( event_name: 'traffic_back' ).last.extras ).to eq( {"referer"=>"test", "reason"=>"country"} )
    end

  end

end
