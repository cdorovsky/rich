require 'rails_helper'

RSpec.describe Campaign, :type => :model do

  context 'structure' do

    it 'validations' do
      camp = Campaign.new( name: 'campaign', trafficback_url: 'http://asd.com', m_trafficback_url: 'http://asd.com' )
      user_domain = UserDomain.create( url: 'http://sameurl.com' )
      camp.user_domains = [] << user_domain
      camp.user_domains << user_domain.dup
      camp.save
      expect( camp.errors.any? ).to eq( true )
    end

  end

end
