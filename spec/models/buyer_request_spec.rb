require 'rails_helper'

RSpec.describe BuyerRequest, :type => :model do

  before :context do

    $redis.flushdb
    Country.delete_all
    @russia      = Country.find_by( abbr: 'RU' )  || create( :russia )
    @country_all = Country.find_by( abbr: 'ALL' ) || create( :country_all )
    @campaign    = create :campaign
    @dealer      = @campaign.dealer

    @appflood       = create :appflood_offer
    @appflooddirect = create :appflooddirect_offer
    @everyads       = create :everyads_offer
    @glispa         = create :glispa_offer
    @iconpeak       = create :iconpeak_offer
    @matomy         = create :matomy_offer
    @mobvista       = create :mobvista_offer
    @mobvistadirect = create :mobvistadirect_offer
    @performance    = create :performance_offer
    @unileadnetwork = create :unileadnetwork_offer
    @yeahmobi       = create :yeahmobi_offer

    @campaign.offers = [ @appflood, @appflooddirect, @everyads, @glispa, @iconpeak, @matomy, @mobvista, @mobvistadirect, @performance, @unileadnetwork, @yeahmobi ]
    @appflood_offer_set       = @campaign.offer_sets.find_by( offer_id: @appflood )
    @appflooddirect_offer_set = @campaign.offer_sets.find_by( offer_id: @appflooddirect )
    @everyads_offer_set       = @campaign.offer_sets.find_by( offer_id: @everyads )
    @glispa_offer_set         = @campaign.offer_sets.find_by( offer_id: @glispa )
    @iconpeak_offer_set       = @campaign.offer_sets.find_by( offer_id: @iconpeak )
    @matomy_offer_set         = @campaign.offer_sets.find_by( offer_id: @matomy )
    @mobvista_offer_set       = @campaign.offer_sets.find_by( offer_id: @mobvista )
    @mobvistadirect_offer_set = @campaign.offer_sets.find_by( offer_id: @mobvistadirect )
    @performance_offer_set    = @campaign.offer_sets.find_by( offer_id: @performance )
    @unileadnetwork_offer_set = @campaign.offer_sets.find_by( offer_id: @unileadnetwork )
    @yeahmobi_offer_set       = @campaign.offer_sets.find_by( offer_id: @yeahmobi )

    @iphone  = 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_1_2 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Version/7.0 Mobile/11D257 Safari/9537.53'
    @ip_ru   = '217.118.82.88'
    Rediska.load_all
  end

  context 'redirect url' do

    it 'appflood' do
      request        = double( remote_ip: @ip_ru, user_agent: @iphone, format: 'json' )
      allow( request ).to receive( :env ).and_return( Hash.new )
      buyer_request = BuyerRequest.new( request, id: @appflood_offer_set.id )
      redirect_url = Providers::AppsAdvert::Appflood::Sender.redirect( buyer_request.uniq_id, external_id: @appflood.external_id )
      expect( buyer_request.redirect_url ).to eq( redirect_url )
    end

    it 'appflood' do
      request        = double( remote_ip: @ip_ru, user_agent: @iphone, format: 'text/html' )
      allow( request ).to receive( :env ).and_return( Hash.new )
      buyer_request = BuyerRequest.new( request, id: @appflood_offer_set.id )
      redirect_url = Providers::AppsAdvert::Appflood::Sender.redirect( buyer_request.uniq_id, external_id: @appflood.external_id )
      expect( buyer_request.redirect_url ).to eq( redirect_url )
    end

    it 'appflooddirect' do
      request        = double( remote_ip: @ip_ru, user_agent: @iphone, format: 'json' )
      allow( request ).to receive( :env ).and_return( Hash.new )
      buyer_request = BuyerRequest.new( request, id: @appflooddirect_offer_set.id )
      redirect_url = Providers::AppsAdvert::Appflooddirect::Sender.redirect( buyer_request.uniq_id, external_id: @appflooddirect.external_id )
      expect( buyer_request.redirect_url ).to eq( redirect_url )
    end

    it 'appflooddirect' do
      request        = double( remote_ip: @ip_ru, user_agent: @iphone, format: 'text/html' )
      allow( request ).to receive( :env ).and_return( Hash.new )
      buyer_request = BuyerRequest.new( request, id: @appflooddirect_offer_set.id )
      redirect_url = Providers::AppsAdvert::Appflooddirect::Sender.redirect( buyer_request.uniq_id, external_id: @appflooddirect.external_id )
      expect( buyer_request.redirect_url ).to eq( redirect_url )
    end

    it 'everyads' do
      request        = double( remote_ip: @ip_ru, user_agent: @iphone, format: 'json' )
      allow( request ).to receive( :env ).and_return( Hash.new )
      buyer_request = BuyerRequest.new( request, id: @everyads_offer_set.id )
      redirect_url = Providers::AppsAdvert::Everyads::Sender.redirect( buyer_request.uniq_id, external_id: @everyads.external_id, apps_os: @everyads.apps_os )
      expect( buyer_request.redirect_url ).to eq( redirect_url )
    end

    it 'everyads' do
      request        = double( remote_ip: @ip_ru, user_agent: @iphone, format: 'text/html' )
      allow( request ).to receive( :env ).and_return( Hash.new )
      buyer_request = BuyerRequest.new( request, id: @everyads_offer_set.id )
      redirect_url = Providers::AppsAdvert::Everyads::Sender.redirect( buyer_request.uniq_id, external_id: @everyads.external_id, apps_os: @everyads.apps_os )
      expect( buyer_request.redirect_url ).to eq( redirect_url )
    end

    it 'glispa' do
      request        = double( remote_ip: @ip_ru, user_agent: @iphone, format: 'json' )
      allow( request ).to receive( :env ).and_return( Hash.new )
      buyer_request = BuyerRequest.new( request, id: @glispa_offer_set.id )
      redirect_url = Providers::AppsAdvert::Glispa::Sender.redirect( buyer_request.uniq_id, seller_url: @glispa.url )
      expect( buyer_request.redirect_url ).to eq( redirect_url )
    end

    it 'glispa' do
      request        = double( remote_ip: @ip_ru, user_agent: @iphone, format: 'text/html' )
      allow( request ).to receive( :env ).and_return( Hash.new )
      buyer_request = BuyerRequest.new( request, id: @glispa_offer_set.id )
      redirect_url = Providers::AppsAdvert::Glispa::Sender.redirect( buyer_request.uniq_id, seller_url: @glispa.url )
      expect( buyer_request.redirect_url ).to eq( redirect_url )
    end

    it 'iconpeak' do
      request        = double( remote_ip: @ip_ru, user_agent: @iphone, format: 'json' )
      allow( request ).to receive( :env ).and_return( Hash.new )
      buyer_request = BuyerRequest.new( request, id: @iconpeak_offer_set.id )
      redirect_url = Providers::AppsAdvert::Iconpeak::Sender.redirect( buyer_request.uniq_id, external_id: @iconpeak.external_id )
      expect( buyer_request.redirect_url ).to eq( redirect_url )
    end

    it 'iconpeak' do
      request        = double( remote_ip: @ip_ru, user_agent: @iphone, format: 'text/html' )
      allow( request ).to receive( :env ).and_return( Hash.new )
      buyer_request = BuyerRequest.new( request, id: @iconpeak_offer_set.id )
      redirect_url = Providers::AppsAdvert::Iconpeak::Sender.redirect( buyer_request.uniq_id, external_id: @iconpeak.external_id )
      expect( buyer_request.redirect_url ).to eq( redirect_url )
    end

    it 'matomy' do
      request        = double( remote_ip: @ip_ru, user_agent: @iphone, format: 'json' )
      allow( request ).to receive( :env ).and_return( Hash.new )
      buyer_request = BuyerRequest.new( request, id: @matomy_offer_set.id )
      redirect_url = Providers::AppsAdvert::Matomy::Sender.redirect( buyer_request.uniq_id, seller_url: @matomy.url )
      expect( buyer_request.redirect_url ).to eq( redirect_url )
    end

    it 'matomy' do
      request        = double( remote_ip: @ip_ru, user_agent: @iphone, format: 'text/html' )
      allow( request ).to receive( :env ).and_return( Hash.new )
      buyer_request = BuyerRequest.new( request, id: @matomy_offer_set.id )
      redirect_url = Providers::AppsAdvert::Matomy::Sender.redirect( buyer_request.uniq_id, seller_url: @matomy.url )
      expect( buyer_request.redirect_url ).to eq( redirect_url )
    end

    it 'mobvista' do
      request        = double( remote_ip: @ip_ru, user_agent: @iphone, format: 'json' )
      allow( request ).to receive( :env ).and_return( Hash.new )
      buyer_request = BuyerRequest.new( request, id: @mobvista_offer_set.id )
      redirect_url = Providers::AppsAdvert::Mobvista::Sender.redirect( buyer_request.uniq_id, external_id: @mobvista.external_id )
      expect( buyer_request.redirect_url ).to eq( redirect_url )
    end

    it 'mobvista' do
      request        = double( remote_ip: @ip_ru, user_agent: @iphone, format: 'text/html' )
      allow( request ).to receive( :env ).and_return( Hash.new )
      buyer_request = BuyerRequest.new( request, id: @mobvista_offer_set.id )
      redirect_url = Providers::AppsAdvert::Mobvista::Sender.redirect( buyer_request.uniq_id, external_id: @mobvista.external_id )
      expect( buyer_request.redirect_url ).to eq( redirect_url )
    end

    it 'mobvistadirect' do
      request        = double( remote_ip: @ip_ru, user_agent: @iphone, format: 'json' )
      allow( request ).to receive( :env ).and_return( Hash.new )
      buyer_request = BuyerRequest.new( request, id: @mobvistadirect_offer_set.id )
      redirect_url = Providers::AppsAdvert::Mobvistadirect::Sender.redirect( buyer_request.uniq_id, seller_url: @mobvistadirect.url, dealer_id: @dealer.id )
      expect( buyer_request.redirect_url ).to eq( redirect_url )
    end

    it 'mobvistadirect' do
      request        = double( remote_ip: @ip_ru, user_agent: @iphone, format: 'text/html' )
      allow( request ).to receive( :env ).and_return( Hash.new )
      buyer_request = BuyerRequest.new( request, id: @mobvistadirect_offer_set.id )
      redirect_url = Providers::AppsAdvert::Mobvistadirect::Sender.redirect( buyer_request.uniq_id, seller_url: @mobvistadirect.url, dealer_id: @dealer.id )
      expect( buyer_request.redirect_url ).to eq( redirect_url )
    end

    it 'performance' do
      request        = double( remote_ip: @ip_ru, user_agent: @iphone, format: 'json' )
      allow( request ).to receive( :env ).and_return( Hash.new )
      buyer_request = BuyerRequest.new( request, id: @performance_offer_set.id )
      redirect_url = Providers::AppsAdvert::Performance::Sender.redirect( buyer_request.uniq_id, external_id: @performance.external_id )
      expect( buyer_request.redirect_url ).to eq( redirect_url )
    end

    it 'performance' do
      request        = double( remote_ip: @ip_ru, user_agent: @iphone, format: 'text/html' )
      allow( request ).to receive( :env ).and_return( Hash.new )
      buyer_request = BuyerRequest.new( request, id: @performance_offer_set.id )
      redirect_url = Providers::AppsAdvert::Performance::Sender.redirect( buyer_request.uniq_id, external_id: @performance.external_id )
      expect( buyer_request.redirect_url ).to eq( redirect_url )
    end

    it 'unileadnetwork' do
      request        = double( remote_ip: @ip_ru, user_agent: @iphone, format: 'json' )
      allow( request ).to receive( :env ).and_return( Hash.new )
      buyer_request = BuyerRequest.new( request, id: @unileadnetwork_offer_set.id )
      redirect_url = Providers::AppsAdvert::Unileadnetwork::Sender.redirect( buyer_request.uniq_id, external_id: @unileadnetwork.external_id )
      expect( buyer_request.redirect_url ).to eq( redirect_url )
    end

    it 'unileadnetwork' do
      request        = double( remote_ip: @ip_ru, user_agent: @iphone, format: 'text/html' )
      allow( request ).to receive( :env ).and_return( Hash.new )
      buyer_request = BuyerRequest.new( request, id: @unileadnetwork_offer_set.id )
      redirect_url = Providers::AppsAdvert::Unileadnetwork::Sender.redirect( buyer_request.uniq_id, external_id: @unileadnetwork.external_id )
      expect( buyer_request.redirect_url ).to eq( redirect_url )
    end

    it 'yeahmobi' do
      request        = double( remote_ip: @ip_ru, user_agent: @iphone, format: 'json' )
      allow( request ).to receive( :env ).and_return( Hash.new )
      buyer_request = BuyerRequest.new( request, id: @yeahmobi_offer_set.id )
      redirect_url = Providers::AppsAdvert::Yeahmobi::Sender.redirect( buyer_request.uniq_id, external_id: @yeahmobi.external_id, seller_url: @yeahmobi.url )
      expect( buyer_request.redirect_url ).to eq( redirect_url )
    end

    it 'yeahmobi' do
      request        = double( remote_ip: @ip_ru, user_agent: @iphone, format: 'text/html' )
      allow( request ).to receive( :env ).and_return( Hash.new )
      buyer_request = BuyerRequest.new( request, id: @yeahmobi_offer_set.id )
      redirect_url = Providers::AppsAdvert::Yeahmobi::Sender.redirect( buyer_request.uniq_id, external_id: @yeahmobi.external_id, seller_url: @yeahmobi.url )
      expect( buyer_request.redirect_url ).to eq( redirect_url )
    end

  end

end