require 'rails_helper'

describe 'dealer stats' do

  before( :context ) do
    @dealer = create( :dealer )
    @offers = %w( apps_advert ).map { | offer | create( offer ) }
    @campaign = create 'campaign'

    @counters_and_amounts = {
      visitor: 7,
      uniq_visitor: 7,
      traffic_back: 7,
      money_total: 7.0,
      money_waiting: 7.0,
      money_confirmed: 7.0,
      money_declined: 7.0,
      conversion_total: 7,
      conversion_confirmed: 7,
      conversion_waiting: 7,
      conversion_declined: 7,
      conversion_subscribed: 7,
      conversion_unsubscribed: 7,
      conversion_rebill: 7,
      money_from_rebill: 7.0,
      money_from_buyout: 7.0,
      uniq_traffic_back: 7,
      download: 7,
      subscribe: 7,
      rebill: 7,
      unsubscribe: 7
    }
    @offers.each do | offer_sample |
      @counters_and_amounts.each do | counter_name, counter_amount |
        stat_options = {
          counter: counter_name,
          campaign: @campaign,
          amount: counter_amount
        }
        stat_options[ :offer ] = offer_sample unless /traffic_back/ =~ counter_name
        #stat = DealerStat.trigger stat_options
      end
    end
    post_via_redirect '/dealers/sign_in', 'dealer[email]' => @dealer.email, 'dealer[password]' => @dealer.password

  end

  it 'common' do
    get 'stats/table'
    expect( response ).to be_success
    expect( response ).to have_http_status(200)
    answer = JSON.parse response.body
    answer.each do |stat| 
      expect( stat[ 'common' ] ).to eq( true )
      expect( stat[ 'no_grouping' ] ).to eq( true )
      expect( stat[ 'daily' ] ).to eq( true )
    end
  end

  it 'common hourly' do
    start  = Time.zone.now.strftime( '%F' )
    finish = 1.day.ago.strftime( '%F' )
    get "stats/table?date_start=#{ start }&date_end=#{ finish }"
    expect( response ).to be_success
    expect( response ).to have_http_status(200)
    answer = JSON.parse response.body
    answer.each do |stat| 
      expect( stat[ 'common' ] ).to eq( true )
      expect( stat[ 'no_grouping' ] ).to eq( true )
      expect( stat[ 'hourly' ] ).to eq( true )
    end
  end

  it 'common monthly' do
    start  = Time.zone.now.strftime( '%F' )
    finish = 6.month.ago.strftime( '%F' )
    get "stats/table?date_start=#{ start }&date_end=#{ finish }"
    expect( response ).to be_success
    expect( response ).to have_http_status(200)
    answer = JSON.parse response.body
    answer.each do |stat| 
      expect( stat[ 'common' ] ).to eq( true )
      expect( stat[ 'no_grouping' ] ).to eq( true )
      expect( stat[ 'monthly' ] ).to eq( true )
    end
  end

  it 'wapclick' do
    get 'stats/table?kind=wapclick'
    expect( response ).to be_success
    expect( response ).to have_http_status(200)
    answer = JSON.parse response.body
    answer.each do |stat| 
      expect( stat[ 'wapclick' ] ).to eq( true )
      expect( stat[ 'no_grouping' ] ).to eq( true )
      expect( stat[ 'daily' ] ).to eq( true )
    end
  end

  it 'wapclick campaign grouping' do
    get "stats/table?kind=wapclick&grouping=campaign_grouping"
    expect( response ).to be_success
    expect( response ).to have_http_status(200)
    answer = JSON.parse response.body
    answer.each do |stat| 
      expect( stat[ 'wapclick' ] ).to eq( true )
      expect( stat[ 'campaign_grouping' ] ).to eq( true )
      expect( stat[ 'daily' ] ).to eq( true )
    end
  end

  it 'wapclick offer grouping' do
    get "stats/table?kind=wapclick&grouping=offer_grouping"
    expect( response ).to be_success
    expect( response ).to have_http_status(200)
    answer = JSON.parse response.body
    answer.each do |stat| 
      expect( stat[ 'wapclick' ] ).to eq( true )
      expect( stat[ 'offer_grouping' ] ).to eq( true )
      expect( stat[ 'daily' ] ).to eq( true )
    end
  end

  it 'wapclick hourly' do
    start  = Time.zone.now.strftime( '%F' )
    finish = 1.day.ago.strftime( '%F' )
    get "stats/table?kind=wapclick&date_start=#{ start }&date_end=#{ finish }"
    expect( response ).to be_success
    expect( response ).to have_http_status(200)
    answer = JSON.parse response.body
    answer.each do |stat| 
      expect( stat[ 'wapclick' ] ).to eq( true )
      expect( stat[ 'no_grouping' ] ).to eq( true )
      expect( stat[ 'hourly' ] ).to eq( true )
    end
  end

  it 'wapclick hourly campaign grouping' do
    start  = Time.zone.now.strftime( '%F' )
    finish = 1.day.ago.strftime( '%F' )
    get "stats/table?kind=wapclick&date_start=#{ start }&date_end=#{ finish }&grouping=campaign_grouping"
    expect( response ).to be_success
    expect( response ).to have_http_status(200)
    answer = JSON.parse response.body
    answer.each do |stat| 
      expect( stat[ 'wapclick' ] ).to eq( true )
      expect( stat[ 'campaign_grouping' ] ).to eq( true )
      expect( stat[ 'hourly' ] ).to eq( true )
    end
  end

  it 'wapclick hourly offer grouping' do
    start  = Time.zone.now.strftime( '%F' )
    finish = 1.day.ago.strftime( '%F' )
    get "stats/table?kind=wapclick&date_start=#{ start }&date_end=#{ finish }&grouping=offer_grouping"
    expect( response ).to be_success
    expect( response ).to have_http_status(200)
    answer = JSON.parse response.body
    answer.each do |stat| 
      expect( stat[ 'wapclick' ] ).to eq( true )
      expect( stat[ 'offer_grouping' ] ).to eq( true )
      expect( stat[ 'hourly' ] ).to eq( true )
    end
  end


  it 'wapclick monthly' do
    start  = Time.zone.now.strftime( '%F' )
    finish = 6.month.ago.strftime( '%F' )
    get "stats/table?kind=wapclick&date_start=#{ start }&date_end=#{ finish }"
    expect( response ).to be_success
    expect( response ).to have_http_status(200)
    answer = JSON.parse response.body
    answer.each do |stat| 
      expect( stat[ 'wapclick' ] ).to eq( true )
      expect( stat[ 'no_grouping' ] ).to eq( true )
      expect( stat[ 'monthly' ] ).to eq( true )
    end
  end

  it 'wapclick monthly campaign grouping' do
    start  = Time.zone.now.strftime( '%F' )
    finish = 6.month.ago.strftime( '%F' )
    get "stats/table?kind=wapclick&date_start=#{ start }&date_end=#{ finish }&grouping=campaign_grouping"
    expect( response ).to be_success
    expect( response ).to have_http_status(200)
    answer = JSON.parse response.body
    answer.each do |stat| 
      expect( stat[ 'wapclick' ] ).to eq( true )
      expect( stat[ 'campaign_grouping' ] ).to eq( true )
      expect( stat[ 'monthly' ] ).to eq( true )
    end
  end

  it 'wapclick monthly offer grouping' do
    start  = Time.zone.now.strftime( '%F' )
    finish = 6.month.ago.strftime( '%F' )
    get "stats/table?kind=wapclick&date_start=#{ start }&date_end=#{ finish }&grouping=offer_grouping"
    expect( response ).to be_success
    expect( response ).to have_http_status(200)
    answer = JSON.parse response.body
    answer.each do |stat| 
      expect( stat[ 'wapclick' ] ).to eq( true )
      expect( stat[ 'offer_grouping' ] ).to eq( true )
      expect( stat[ 'monthly' ] ).to eq( true )
    end
  end

  it 'cpa' do
    get 'stats/table?kind=cpa'
    expect( response ).to be_success
    expect( response ).to have_http_status(200)
    answer = JSON.parse response.body
    answer.each do |stat| 
      expect( stat[ 'cpa' ] ).to eq( true )
      expect( stat[ 'no_grouping' ] ).to eq( true )
      expect( stat[ 'daily' ] ).to eq( true )
    end
  end

  it 'cpa campaign grouping' do
    get "stats/table?kind=cpa&grouping=campaign_grouping"
    expect( response ).to be_success
    expect( response ).to have_http_status(200)
    answer = JSON.parse response.body
    answer.each do |stat| 
      expect( stat[ 'cpa' ] ).to eq( true )
      expect( stat[ 'campaign_grouping' ] ).to eq( true )
      expect( stat[ 'daily' ] ).to eq( true )
    end
  end

  it 'cpa offer grouping' do
    get "stats/table?kind=cpa&grouping=offer_grouping"
    expect( response ).to be_success
    expect( response ).to have_http_status(200)
    answer = JSON.parse response.body
    answer.each do |stat| 
      expect( stat[ 'cpa' ] ).to eq( true )
      expect( stat[ 'offer_grouping' ] ).to eq( true )
      expect( stat[ 'daily' ] ).to eq( true )
    end
  end

  it 'cpa hourly' do
    start  = Time.zone.now.strftime( '%F' )
    finish = 1.day.ago.strftime( '%F' )
    get "stats/table?kind=cpa&date_start=#{ start }&date_end=#{ finish }"
    expect( response ).to be_success
    expect( response ).to have_http_status(200)
    answer = JSON.parse response.body
    answer.each do |stat| 
      expect( stat[ 'cpa' ] ).to eq( true )
      expect( stat[ 'no_grouping' ] ).to eq( true )
      expect( stat[ 'hourly' ] ).to eq( true )
    end
  end

  it 'cpa hourly campaign grouping' do
    start  = Time.zone.now.strftime( '%F' )
    finish = 1.day.ago.strftime( '%F' )
    get "stats/table?kind=cpa&date_start=#{ start }&date_end=#{ finish }&grouping=campaign_grouping"
    expect( response ).to be_success
    expect( response ).to have_http_status(200)
    answer = JSON.parse response.body
    answer.each do |stat| 
      expect( stat[ 'cpa' ] ).to eq( true )
      expect( stat[ 'campaign_grouping' ] ).to eq( true )
      expect( stat[ 'hourly' ] ).to eq( true )
    end
  end

  it 'cpa hourly offer grouping' do
    start  = Time.zone.now.strftime( '%F' )
    finish = 1.day.ago.strftime( '%F' )
    get "stats/table?kind=cpa&date_start=#{ start }&date_end=#{ finish }&grouping=offer_grouping"
    expect( response ).to be_success
    expect( response ).to have_http_status(200)
    answer = JSON.parse response.body
    answer.each do |stat| 
      expect( stat[ 'cpa' ] ).to eq( true )
      expect( stat[ 'offer_grouping' ] ).to eq( true )
      expect( stat[ 'hourly' ] ).to eq( true )
    end
  end

  it 'cpa monthly' do
    start  = Time.zone.now.strftime( '%F' )
    finish = 6.month.ago.strftime( '%F' )
    get "stats/table?kind=cpa&date_start=#{ start }&date_end=#{ finish }"
    expect( response ).to be_success
    expect( response ).to have_http_status(200)
    answer = JSON.parse response.body
    answer.each do |stat| 
      expect( stat[ 'cpa' ] ).to eq( true )
      expect( stat[ 'no_grouping' ] ).to eq( true )
      expect( stat[ 'monthly' ] ).to eq( true )
    end
  end

  it 'cpa monthly campaign grouping' do
    start  = Time.zone.now.strftime( '%F' )
    finish = 6.month.ago.strftime( '%F' )
    get "stats/table?kind=cpa&date_start=#{ start }&date_end=#{ finish }&grouping=campaign_grouping"
    expect( response ).to be_success
    expect( response ).to have_http_status(200)
    answer = JSON.parse response.body
    answer.each do |stat| 
      expect( stat[ 'cpa' ] ).to eq( true )
      expect( stat[ 'campaign_grouping' ] ).to eq( true )
      expect( stat[ 'monthly' ] ).to eq( true )
    end
  end

  it 'cpa monthly offer grouping' do
    start  = Time.zone.now.strftime( '%F' )
    finish = 6.month.ago.strftime( '%F' )
    get "stats/table?kind=cpa&date_start=#{ start }&date_end=#{ finish }&grouping=offer_grouping"
    expect( response ).to be_success
    expect( response ).to have_http_status(200)
    answer = JSON.parse response.body
    answer.each do |stat| 
      expect( stat[ 'cpa' ] ).to eq( true )
      expect( stat[ 'offer_grouping' ] ).to eq( true )
      expect( stat[ 'monthly' ] ).to eq( true )
    end
  end

  it 'apps_advert' do
    get 'stats/table?kind=apps_advert'
    expect( response ).to be_success
    expect( response ).to have_http_status(200)
    answer = JSON.parse response.body
    answer.each do |stat| 
      expect( stat[ 'apps_advert' ] ).to eq( true )
      expect( stat[ 'no_grouping' ] ).to eq( true )
      expect( stat[ 'daily' ] ).to eq( true )
    end
  end

  it 'apps_advert campaign grouping' do
    get "stats/table?kind=apps_advert&grouping=campaign_grouping"
    expect( response ).to be_success
    expect( response ).to have_http_status(200)
    answer = JSON.parse response.body
    answer.each do |stat| 
      expect( stat[ 'apps_advert' ] ).to eq( true )
      expect( stat[ 'campaign_grouping' ] ).to eq( true )
      expect( stat[ 'daily' ] ).to eq( true )
    end
  end

  it 'apps_advert offer grouping' do
    get "stats/table?kind=apps_advert&grouping=offer_grouping"
    expect( response ).to be_success
    expect( response ).to have_http_status(200)
    answer = JSON.parse response.body
    answer.each do |stat| 
      expect( stat[ 'apps_advert' ] ).to eq( true )
      expect( stat[ 'offer_grouping' ] ).to eq( true )
      expect( stat[ 'daily' ] ).to eq( true )
    end
  end

  it 'apps_advert hourly' do
    start  = Time.zone.now.strftime( '%F' )
    finish = 1.day.ago.strftime( '%F' )
    get "stats/table?kind=apps_advert&date_start=#{ start }&date_end=#{ finish }"
    expect( response ).to be_success
    expect( response ).to have_http_status(200)
    answer = JSON.parse response.body
    answer.each do |stat| 
      expect( stat[ 'apps_advert' ] ).to eq( true )
      expect( stat[ 'no_grouping' ] ).to eq( true )
      expect( stat[ 'hourly' ] ).to eq( true )
    end
  end

  it 'apps_advert hourly campaign grouping' do
    start  = Time.zone.now.strftime( '%F' )
    finish = 1.day.ago.strftime( '%F' )
    get "stats/table?kind=apps_advert&date_start=#{ start }&date_end=#{ finish }&grouping=campaign_grouping"
    expect( response ).to be_success
    expect( response ).to have_http_status(200)
    answer = JSON.parse response.body
    answer.each do |stat| 
      expect( stat[ 'apps_advert' ] ).to eq( true )
      expect( stat[ 'campaign_grouping' ] ).to eq( true )
      expect( stat[ 'hourly' ] ).to eq( true )
    end
  end

  it 'apps_advert hourly offer grouping' do
    start  = Time.zone.now.strftime( '%F' )
    finish = 1.day.ago.strftime( '%F' )
    get "stats/table?kind=apps_advert&date_start=#{ start }&date_end=#{ finish }&grouping=offer_grouping"
    expect( response ).to be_success
    expect( response ).to have_http_status(200)
    answer = JSON.parse response.body
    answer.each do |stat| 
      expect( stat[ 'apps_advert' ] ).to eq( true )
      expect( stat[ 'offer_grouping' ] ).to eq( true )
      expect( stat[ 'hourly' ] ).to eq( true )
    end
  end

  it 'apps_advert monthly' do
    start  = Time.zone.now.strftime( '%F' )
    finish = 6.month.ago.strftime( '%F' )
    get "stats/table?kind=apps_advert&date_start=#{ start }&date_end=#{ finish }"
    expect( response ).to be_success
    expect( response ).to have_http_status(200)
    answer = JSON.parse response.body
    answer.each do |stat| 
      expect( stat[ 'apps_advert' ] ).to eq( true )
      expect( stat[ 'no_grouping' ] ).to eq( true )
      expect( stat[ 'monthly' ] ).to eq( true )
    end
  end

  it 'apps_advert monthly campaign grouping' do
    start  = Time.zone.now.strftime( '%F' )
    finish = 6.month.ago.strftime( '%F' )
    get "stats/table?kind=apps_advert&date_start=#{ start }&date_end=#{ finish }&grouping=campaign_grouping"
    expect( response ).to be_success
    expect( response ).to have_http_status(200)
    answer = JSON.parse response.body
    answer.each do |stat| 
      expect( stat[ 'apps_advert' ] ).to eq( true )
      expect( stat[ 'campaign_grouping' ] ).to eq( true )
      expect( stat[ 'monthly' ] ).to eq( true )
    end
  end

  it 'apps_advert monthly offer grouping' do
    start  = Time.zone.now.strftime( '%F' )
    finish = 6.month.ago.strftime( '%F' )
    get "stats/table?kind=apps_advert&date_start=#{ start }&date_end=#{ finish }&grouping=offer_grouping"
    expect( response ).to be_success
    expect( response ).to have_http_status(200)
    answer = JSON.parse response.body
    answer.each do |stat| 
      expect( stat[ 'apps_advert' ] ).to eq( true )
      expect( stat[ 'offer_grouping' ] ).to eq( true )
      expect( stat[ 'monthly' ] ).to eq( true )
    end
  end


end
