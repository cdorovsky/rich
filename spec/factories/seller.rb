FactoryGirl.define do

  factory :plasticmedia, class: Seller do
    seller_name          'plasticmedia'
    provider_system_name 'plasticmedia'
    kind                 'wapclick'
  end

  factory :billing8, class: Seller do
    seller_name          'billing8'
    provider_system_name 'billing8'
    kind                 'wapclick'
  end

  factory :onlypay, class: Seller do
    seller_name          'onlypay'
    provider_system_name 'onlypay'
    kind                 'wapclick'
  end

  factory :shemrock, class: Seller do
    seller_name          'shemrock'
    provider_system_name 'shemrock'
    kind                 'wapclick'
  end

  factory :performance, class: Seller do
    seller_name          'performance'
    provider_system_name 'performance'
    kind                 'apps_advert'
  end

  factory :appflood, class: Seller do
    seller_name          'appflood'
    provider_system_name 'appflood'
    kind                 'apps_advert'
  end

  factory :appflooddirect, class: Seller do
    seller_name          'appflooddirect'
    provider_system_name 'appflooddirect'
    kind                 'apps_advert'
  end

  factory :iconpeak, class: Seller do
    seller_name          'iconpeak'
    provider_system_name 'iconpeak'
    kind                 'apps_advert'
  end

  factory :matomy, class: Seller do
    seller_name          'matomy'
    provider_system_name 'matomy'
    kind                 'apps_advert'
  end

  factory :everyads, class: Seller do
    seller_name          'everyads'
    provider_system_name 'everyads'
    kind                 'apps_advert'
  end
 
  factory :mobvista, class: Seller do
    seller_name          'mobvista'
    provider_system_name 'mobvista'
    kind                 'apps_advert'
  end 

  factory :mobvistadirect, class: Seller do
    seller_name          'mobvistadirect'
    provider_system_name 'mobvistadirect'
    kind                 'apps_advert'  
  end

  factory :glispa, class: Seller do
    seller_name          'glispa'
    provider_system_name 'glispa'
    kind                 'apps_advert'  
  end

  factory :unileadnetwork, class: Seller do
    seller_name          'unileadnetwork'
    provider_system_name 'unileadnetwork'
    kind                 'apps_advert'  
  end

  factory :yeahmobi, class: Seller do
    seller_name          'yeahmobi'
    provider_system_name 'yeahmobi'
    kind                 'apps_advert'  
  end 

end
