FactoryGirl.define do
  factory :wallet do
    kind 'WMZ'
    number 'Z303295799594'
    name 'test'
  end

  factory :bank_wallet do
    kind 'Bank'
    number '0000000000000000'
    name 'bank card'
  end
end
