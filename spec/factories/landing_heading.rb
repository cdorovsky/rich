FactoryGirl.define do

  factory :default_app_heading, class: LandingHeading do
    is_default true
    app_kind 'application'
    body 'default app heading'
  end

  factory :default_game_heading, class: LandingHeading do
    is_default true
    app_kind 'game'
    body 'default game heading'
  end

end
