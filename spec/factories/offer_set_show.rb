FactoryGirl.define do

  factory :performance_static_sequence, class: OfferSetShow do
    mongo_id      { BSON::ObjectId.new.to_s }
    referer       'referer'
    sub_id        'sub1'
    sub_id2       'sub2'
    sub_id3       'sun3'
    dealer_id     1
    campaign_id   1
    offer_id      1
    offer_set_id  1
    country_id    1
    country_name  'Russian Federation'
    country_code  'RU'
    ip            '192.168.0.1'
    user_agent    'Long Android Agent'
    user_platform 'android'
  end

  factory :appflood_offer_set_show, class: OfferSetShow do
    offer { create :appflood_offer }
    mongo_id { BSON::ObjectId.new.to_s }
  end
  factory :appflooddirect_offer_set_show, class: OfferSetShow do
    offer { create :appflooddirect_offer }
    mongo_id { BSON::ObjectId.new.to_s }
  end
  factory :everyads_offer_set_show, class: OfferSetShow do
    offer { create :everyads_offer }
    mongo_id { BSON::ObjectId.new.to_s }
  end
  factory :glispa_offer_set_show, class: OfferSetShow do
    offer { create :glispa_offer }
    mongo_id { BSON::ObjectId.new.to_s }
  end
  factory :iconpeak_offer_set_show, class: OfferSetShow do
    offer { create :iconpeak_offer }
    mongo_id { BSON::ObjectId.new.to_s }
  end
  factory :matomy_offer_set_show, class: OfferSetShow do
    offer { create :matomy_offer }
    mongo_id { BSON::ObjectId.new.to_s }
  end
  factory :mobvista_offer_set_show, class: OfferSetShow do
    offer { create :mobvista_offer }
    mongo_id { BSON::ObjectId.new.to_s }
  end
  factory :mobvistadirect_offer_set_show, class: OfferSetShow do
    offer { create :mobvistadirect_offer }
    mongo_id { BSON::ObjectId.new.to_s }
  end
  factory :performance_offer_set_show, class: OfferSetShow do
    offer { create :performance_offer }
    mongo_id { BSON::ObjectId.new.to_s }
  end
  factory :unileadnetwork_offer_set_show, class: OfferSetShow do
    offer { create :unileadnetwork_offer }
    mongo_id { BSON::ObjectId.new.to_s }
  end
  factory :yeahmobi_offer_set_show, class: OfferSetShow do
    offer { create :yeahmobi_offer }
    mongo_id { BSON::ObjectId.new.to_s }
  end
end
