FactoryGirl.define do

  factory :prev_hour_stat, class: DealerStat do
    md5id                  (1..999).to_a.sample
    hourly                 true
    visitor                100
    uniq_visitor           100
    conversion_app_install 100
    common                 true
    no_grouping            true
    created_at             1.hour.ago.utc
  end

  factory :cur_hour_stat, class: DealerStat do
    md5id                  (1..999).to_a.sample
    hourly                 true
    visitor                100
    uniq_visitor           100
    conversion_app_install 100
    common                 true
    no_grouping            true
  end

  factory :old_hour_stat, class: DealerStat do
    md5id                  (1..999).to_a.sample
    hourly                 true
    visitor                (1..999).to_a.sample
    uniq_visitor           (1..999).to_a.sample
    conversion_app_install (1..999).to_a.sample
    common                 true
    no_grouping            true
    created_at             2.hour.ago.utc
  end

  factory :prev_day_stat, class: DealerStat do
    md5id                  (1..999).to_a.sample
    daily                 true
    visitor                (1..999).to_a.sample
    uniq_visitor           (1..999).to_a.sample
    conversion_app_install (1..999).to_a.sample
    common                 true
    no_grouping            true
    created_at             1.day.ago.utc
  end

  factory :cur_day_stat, class: DealerStat do
    md5id                  (1..999).to_a.sample
    daily                 true
    visitor                (1..999).to_a.sample
    uniq_visitor           (1..999).to_a.sample
    conversion_app_install (1..999).to_a.sample
    common                 true
    no_grouping            true
  end

  factory :old_day_stat, class: DealerStat do
    md5id                  (1..999).to_a.sample
    daily                 true
    visitor                (1..999).to_a.sample
    uniq_visitor           (1..999).to_a.sample
    conversion_app_install (1..999).to_a.sample
    common                 true
    no_grouping            true
    created_at             2.day.ago.utc
  end

end
