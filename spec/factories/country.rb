FactoryGirl.define do

  factory :russia,      class: Country do
    name 'Russian Federation'
    abbr 'RU'
  end

  factory :country_all, class: Country do
    name 'All'
    abbr 'ALL'
  end 

  factory :na_country,  class: Country do
  	name 'N/A'
  	abbr 'N/A'
  end

  factory :usa,  class: Country do
    name 'United States'
    abbr 'US'
  end

  factory :thai,  class: Country do
    name 'Thailand'
    abbr 'TH'
  end

  factory :random_country, class: Country do
    name SecureRandom.hex(8)
    abbr SecureRandom.hex(8)
  end

end
