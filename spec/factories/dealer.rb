FactoryGirl.define do

  factory :dealer do
    name               'dealer'
    password           'usualdealer'
    skype              'echo123'
    phone              '79349643446'
    role               { [ 'dealer' ] }
    wallets            { [ create( :wallet ) ] }
    sequence( :email ) { |n| "dealer#{ n }@richpays.com" }
  end

  factory :dev_dealer, class: Dealer do
    name               'developer'
    password           'usualdealer'
    skype              'developer'
    phone              '79349643446'
    email              'demo@richpays.com'
    role               { [ 'dealer' ] }
    wallets            { [ create( :wallet ) ] }
  end

  factory :subscribe_buyer, class: Dealer do
    name               'subscribe_buyer'
    password           'subscribe_buyer'
    skype              'echo123'
    role               { [ 'subscribe_buyer' ] }
    wallets            { [ create( :wallet ) ] }
    sequence( :email ) { |n| "subscribe_buyer#{ n }@richpays.com" }
  end

end
