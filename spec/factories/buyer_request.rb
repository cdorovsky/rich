FactoryGirl.define do

  factory :buyer_request do
  	buyer_session  { create( :buyer_session ) }
  	dealer         { create( :dealer )   }  
  	request_format 'text/html'
  	redirect_url   'http://test.com' 
  	sub_id         'sub_id' 
  	sub_id2        'sub_id2' 
  	sub_id3        'sub_id3'
  	referer_url    'none'
    ip             '213.141.129.117'
    country_code   'RU'
    country_name   'Russian Federation'
    user_agent     'iphone'
    user_platform  'iphone'
  end  

end
