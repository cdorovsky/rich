FactoryGirl.define do

  factory :ticket, class: Ticket do
    title 'foo'
    message 'bar'
    last_answered 'dealer'
    read true
    message_counter 1
    dealer { create :dealer }
  end

end
