FactoryGirl.define do

  factory :apps_advert, class: Offer do
    seller       { create( :performance ) }
    kind         'apps_advert'
    name         'apps advert offer'
    url          'http://google.com'
    apps_os      'ios'
    tariff 0.123
    features_list  [ 'feature' ]
    country_list 'world'
    restricted_country_list ''
  end

  factory :offer_performance, class: Offer do
    seller         { create( :performance ) }
    apps_os        'ios'
    kind           'apps_advert'
    tariff         0.07
    name           'AppAdvert Offer'
    external_id    2120
    url            'http://goodfiles.org'
    country_list   'world'
    features_list  [ 'feature' ]
    restricted_country_list ''
  end

  factory :offer_glispa, class: Offer do
    seller         { create( :glispa ) }
    apps_os        'ios'
    kind           'apps_advert'
    tariff         1.8
    name           'AppAdvert Glispa Offer'
    external_id    2010
    url            'http://ads.glispa.com/sw/66469/CD31197/102ca510916c41c784f87956e00f33'
    country_list   'world'
    features_list  [ 'feature' ]
    restricted_country_list ''
  end

  factory :offer_unileadnetwork, class: Offer do
    seller         { create( :unileadnetwork ) }
    apps_os        'ios'
    kind           'apps_advert'
    tariff         1.8
    name           'AppAdvert unileadnetwork Offer'
    external_id    2010
    url            'http://go.unilead.net/aff_c'
    country_list   'world'
    features_list  [ 'feature' ]
    restricted_country_list ''
  end

  factory :offer_yeahmobi, class: Offer do
    seller         { create( :yeahmobi ) }
    apps_os        'ios'
    kind           'apps_advert'
    tariff         1.8
    name           'AppAdvert yeahmobi Offer'
    external_id    2010
    url            'http://yeahmobi.go2cloud.org/aff_c?offer_id=26731&aff_id=19964'
    country_list   'world'
    features_list  [ 'feature' ]
    restricted_country_list ''
  end

  factory :offer_performance_wl, class: Offer do
    seller          { create( :performance ) }
    apps_os         'ios'
    kind            'apps_advert'
    tariff          0.07
    name            'AppAdvert Offer'
    external_id     2120
    url             'http://goodfiles.org'
    country_list    'world'
    features_list   [ 'feature' ]
    rating          5
    downloads_count 123
    votes_count     123
    before( :create ) do | offer |
      offer.offer_icon = OfferIcon.create
      offer.offer_screenshots << OfferScreenshot.create
    end
    after( :create ) do | offer |
      offer.update_column(:weblanding_ready, true)
    end
    restricted_country_list ''
  end

  factory :russian_offer, class: Offer do
    seller       { create( :performance ) }
    kind         'apps_advert'
    name         'apps advert offer'
    url          'http://google.com'
    apps_os      'ios'
    tariff 10
    features_list  [ 'feature' ]
    country_list 'Russian Federation'
    restricted_country_list ''
  end

  factory :american_offer, class: Offer do
    seller       { create( :performance ) }
    kind         'apps_advert'
    name         'apps advert offer'
    url          'http://google.com'
    apps_os      'ios'
    tariff 8
    features_list  [ 'feature' ]
    country_list 'United States'
    restricted_country_list ''
  end

  factory :tai_offer, class: Offer do
    seller       { create( :performance ) }
    kind         'apps_advert'
    name         'apps advert offer'
    url          'http://google.com'
    apps_os      'ios'
    tariff 6
    features_list  [ 'feature' ]
    country_list 'Thailand'
    restricted_country_list ''
  end

  factory :global_offer, class: Offer do
    seller       { create( :performance ) }
    kind         'apps_advert'
    name         'apps advert offer'
    url          'http://google.com'
    apps_os      'ios'
    tariff 4
    features_list  [ 'feature' ]
    country_list 'All'
    restricted_country_list ''
  end

  factory :ipad_offer, class: Offer do
    seller       { create( :performance ) }
    kind         'apps_advert'
    name         'apps advert offer'
    url          'http://google.com'
    apps_os      'ipad'
    tariff 4
    features_list  [ 'feature' ]
    country_list 'All'
    restricted_country_list ''
  end

  factory :ipod_iphone_offer, class: Offer do
    seller       { create( :performance ) }
    kind         'apps_advert'
    name         'apps advert offer'
    url          'http://google.com'
    apps_os      'ipod+iphone'
    js_dialogue  'hello world'
    tariff 4
    features_list  [ 'feature' ]
    country_list 'All'
    restricted_country_list ''
  end

  factory :ios_offer, class: Offer do
    seller       { create( :performance ) }
    kind         'apps_advert'
    name         'apps advert offer'
    url          'http://google.com'
    apps_os      'ios'
    tariff 4
    features_list  [ 'feature' ]
    country_list 'All'
    restricted_country_list ''
  end

  factory :android_offer, class: Offer do
    seller       { create( :performance ) }
    kind         'apps_advert'
    name         'apps advert offer'
    url          'http://google.com'
    apps_os      'android'
    tariff 4
    features_list  [ 'feature' ]
    country_list 'All'
    restricted_country_list ''
  end

  factory :mobile_offer, class: Offer do
    seller       { create( :performance ) }
    kind         'apps_advert'
    name         'apps advert offer'
    url          'http://google.com'
    apps_os      'mobile'
    tariff 4
    features_list  [ 'feature' ]
    country_list 'All'
    restricted_country_list ''
  end

  factory :pc_offer, class: Offer do
    seller       { create( :performance ) }
    kind         'apps_advert'
    name         'apps advert offer'
    url          'http://google.com'
    apps_os      'android'
    description  'pc offer description'
    votes_count  10
    app_kind     'game'
    landing_name 'some pc offer'
    landing_description 'some desc'
    offer_icon        { create( :offer_icon ) }
    offer_screenshots { [ create( :offer_screenshot ) ] }
    downloads_count 10
    weblanding_ready true
    tariff 4
    features_list  [ 'feature' ]
    country_list 'All'
    restricted_country_list ''
  end

  factory :appflood_offer, class: Offer do
    seller { create( :appflood ) }
    external_id  1762
    kind         'apps_advert'
    name         'apps advert offer'
    url          'http://google.com'
    apps_os      'ios'
    tariff 0.123
    features_list  [ 'feature' ]
    country_list 'All'
    restricted_country_list ''
  end

  factory :appflooddirect_offer, class: Offer do
    seller { create( :appflooddirect ) }
    external_id  727
    kind         'apps_advert'
    name         'apps advert offer'
    url          'http://google.com'
    apps_os      'ios'
    tariff 0.123
    features_list  [ 'feature' ]
    country_list 'All'
    restricted_country_list ''
  end

  factory :everyads_offer, class: Offer do
    seller { create( :everyads ) }
    external_id  3216
    kind         'apps_advert'
    name         'apps advert offer'
    url          'http://google.com'
    apps_os      'ios'
    tariff 0.123
    features_list  [ 'feature' ]
    country_list 'All'
    restricted_country_list ''
  end

  factory :glispa_offer, class: Offer do
    seller { create( :glispa ) }
    kind         'apps_advert'
    name         'apps advert offer'
    url          'http://ads.glispa.com/sw/74419/CD36819/777'
    apps_os      'ios'
    tariff 0.123
    features_list  [ 'feature' ]
    country_list 'All'
    restricted_country_list ''
  end

  factory :iconpeak_offer, class: Offer do
    seller { create( :iconpeak ) }
    kind         'apps_advert'
    name         'apps advert offer'
    url          'http://google.com'
    apps_os      'ios'
    tariff 0.123
    features_list  [ 'feature' ]
    country_list 'All'
    restricted_country_list ''
  end

  factory :matomy_offer, class: Offer do
    seller { create( :matomy ) }
    kind         'apps_advert'
    name         'apps advert offer'
    url          'http://network.adsmarket.com/click/jmhxmWCgqZmMZnGbY8p6w4iQa5pnoIGUkWSYnWWhe8OJZWyZY56plYllcJteng'
    apps_os      'ios'
    tariff 0.123
    features_list  [ 'feature' ]
    country_list 'All'
    restricted_country_list ''
  end

  factory :mobvista_offer, class: Offer do
    seller { create( :mobvista ) }
    external_id  2486
    kind         'apps_advert'
    name         'apps advert offer'
    url          'http://google.com'
    apps_os      'ios'
    tariff 0.123
    features_list  [ 'feature' ]
    country_list 'All'
    restricted_country_list ''
  end

  factory :mobvistadirect_offer, class: Offer do
    seller { create( :mobvistadirect ) }
    kind         'apps_advert'
    name         'apps advert offer'
    url          'http://td.lenzmx.com/click?mb_pl=android&mb_nt=cb1333&mb_campid=lm_pc_global'
    apps_os      'ios'
    tariff 0.123
    features_list  [ 'feature' ]
    country_list 'All'
    restricted_country_list ''
  end

  factory :performance_offer, class: Offer do
    seller { create( :performance ) }
    kind         'apps_advert'
    name         'apps advert offer'
    url          'http://google.com'
    apps_os      'ios'
    external_id  6071
    tariff 0.123
    features_list  [ 'feature' ]
    country_list 'All'
    restricted_country_list ''
  end

  factory :unileadnetwork_offer, class: Offer do
    seller { create( :unileadnetwork ) }
    external_id  1104 
    kind         'apps_advert'
    name         'apps advert offer'
    url          'http://google.com'
    apps_os      'ios'
    tariff 0.123
    features_list  [ 'feature' ]
    country_list 'All'
    restricted_country_list ''
  end

  factory :yeahmobi_offer, class: Offer do
    seller { create( :yeahmobi ) }
    kind         'apps_advert'
    name         'apps advert offer'
    url          'http://yeahmobi.go2cloud.org/aff_c?offer_id=36754&aff_id=19964'
    apps_os      'ios'
    tariff 0.123
    features_list  [ 'feature' ]
    country_list 'All'
    restricted_country_list ''
  end

end
