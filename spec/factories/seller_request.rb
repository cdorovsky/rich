FactoryGirl.define do

  factory :seller_request_apps_advert, class: SellerRequest do
    campaign          { create( :campaign ) }
  	seller            { create( :perfomance ) }
    offer             { create( :apps_advert ) }
    dealer            { campaign.dealer }
    country           { create( :russia ) }
  	seller_name       'performance'
  	seller_kind       'apps_advert'
  	transaction_id    { rand( 100000..999999 ).to_s }
    sub_id            '123456789'
    sub_id2           'abcdefgh'
    sub_id3           'jklmno123'
    offer_dealer_revenue 10
  end

  factory :seller_request_wapclick, class: SellerRequest do
  	seller            { create( :billing8 ) }
  	seller_name       'billing8'
  	seller_kind       'wapclick'
  end

end
