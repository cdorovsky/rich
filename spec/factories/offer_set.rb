FactoryGirl.define do

  factory :offer_set, class: OfferSet do
    campaign { create( :campaign ) }
    offer    { create( :apps_advert ) } 
  end

end
