FactoryGirl.define do

  factory :dealer_increment, class: DealerIncrement do
    reason_type 'SellerRequest'
    confirmed (1..10).to_a.sample
    paid false
  end

  factory :basic_dealer_increment, class: DealerIncrement do
    reason_type 'OfferSetShow'
    confirmed (1..10).to_a.sample
    paid false
  end

  factory :prev_hour_increment, class: DealerIncrement do
    reason_type 'SellerRequest'
    confirmed (1..10).to_a.sample
    created_at 1.hour.ago.utc
  end

  factory :cur_hour_increment, class: DealerIncrement do
    reason_type 'SellerRequest'
    confirmed (1..10).to_a.sample
  end

  factory :old_hour_increment, class: DealerIncrement do
    reason_type 'SellerRequest'
    confirmed (1..10).to_a.sample
    created_at 2.hour.ago.utc
  end

  factory :prev_day_increment, class: DealerIncrement do
    reason_type 'SellerRequest'
    confirmed (1..10).to_a.sample
    created_at 1.day.ago.utc
  end

  factory :cur_day_increment, class: DealerIncrement do
    reason_type 'SellerRequest'
    confirmed (1..10).to_a.sample
  end

  factory :old_day_increment, class: DealerIncrement do
    reason_type 'SellerRequest'
    confirmed (1..10).to_a.sample
    created_at 2.day.ago.utc
  end

end
