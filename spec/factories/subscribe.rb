FactoryGirl.define do

  factory :normal_subscribe, class: Subscribe do
    billing { %w( performance onlypay billing8 ).sample }
    sequence( :rebill_count ) { |n| n - 1 }
  end

  factory :shemrok_subscribe, class: Subscribe do
    billing 'shemrok'
    sequence( :rebill_count ) { |n| n - 1}
  end

  factory :onlypay_subscribe, class: Subscribe do
    billing 'onlypay'
  end

  factory :billing8_subscribe, class: Subscribe do
    billing 'billing8'
  end

end
