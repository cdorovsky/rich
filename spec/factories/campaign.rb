FactoryGirl.define do

  factory :campaign do
    dealer            { create( :dealer ) }
    name              'test campaign'
    wapclick_ready    true
    trafficback_url   'http://google.com'
    m_trafficback_url 'http://google.com/mobile/'
    postback_url      'http://google.com'
    before( :create ) { |campaign| campaign.user_domains << create( :user_domain ) }
  end

end
