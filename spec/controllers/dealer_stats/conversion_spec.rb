require 'rails_helper'

 RSpec.describe Personal::DealerStatsController, :type => :controller do
  render_views
  login_dealer

  before( :context ) do
    @dealer   = create :dealer
    @offers   = %w( apps_advert ).map { | offer | create( offer ) }
    @campaign = create :campaign
    @russia   = Country.find_by( abbr: 'RU' )  || create( :russia )
    @usa      = Country.find_by( abbr: 'US' )  || create( :usa )

    @offers.each do |offer|
      Conversion.create( offer_id:       offer.id,
                         triggered_at:   Time.zone.now,
                         campaign_id:    @campaign.id,
                         dealer_id:      @dealer.id,
                         offer_name:     "RecentOffer",
                         dealer_revenue: 0.06,
                         source_url:     "teasernet.com",
                         campaign_name:  @campaign.name,
                         country:        @russia,
                         kind:           offer.kind,
                         crypted_id:     Conversion.generate_uniq_crypted_id )
    end

    @offers.each do |offer|
      Conversion.create( offer_id:       offer.id,
                         triggered_at:   14.days.ago,
                         campaign_id:    @campaign.id,
                         dealer_id:      @dealer.id,
                         offer_name:     "OldOffer",
                         dealer_revenue: 0.06,
                         source_url:     "teasernet.com",
                         campaign_name:  @campaign.name,
                         country:        @russia,
                         kind:           offer.kind,
                         created_at:     14.days.ago )
    end

    @common_default_params             = { format: :json }
    @apps_advert_kind_params           = { format: :json, offer_kind: 'apps_advert' }
    @offers_name_is_old_offer          = { format: :json, offer_name: 'OldOffer' }
    @offers_name_is_recent_offer       = { format: :json, offer_name: 'RecentOffer' }
    @country_is_correct                = { format: :json, country_list: [ 'Russian Federation', 'USA' ] }
    @country_is_incorrect              = { format: :json, country_list: [ 'USA', 'Uganda' ]  }

  end

  context 'when no params in request' do

    it 'response should be success' do
      get :conversion, @common_default_params
      expect( response ).to be_success
    end

    it 'response shoudl return status 200' do
      get :conversion, @common_default_params
      expect( response ).to have_http_status(200)
    end

    it 'response should contain same numver of records for last week' do
      get :conversion, @common_default_params
      expect( assigns( :conversions ).count ).to eq( Conversion.where( triggered_at: 7.days.ago.beginning_of_day..Time.now ).count )
    end

    it 'response should contain same numver of records for last week' do
      get :conversion, @common_default_params
      assigns( :conversions ).each { |con| expect( con.created_at ).to be_within( (3.5).days ).of( Time.now - ( 3.5 ).days ) }
    end

  end

  context 'when kind param in request' do

    it 'is apps_advert and kind in conversions is apps_advert' do
      get :conversion, @apps_advert_kind_params
      assigns( :conversions ).each { |con| expect( con.kind ).to eq( 'apps_advert' ) }
    end

    it 'is same number number as apps_advert' do
      get :conversion, @apps_advert_kind_params
      expect( assigns( :conversions ).count).to eq( Conversion.where( kind: 'apps_advert' ).count )
    end

  end

  context 'when offer name in params' do

    it 'is OldOffer and same in conversions' do
      get :conversion, @offers_name_is_old_offer
      assigns( :conversions ).each { |con| expect( con.offer_name ).to eq( 'OldOffer' ) }
    end

    it 'is same number number as name OldOffer' do
      get :conversion, @offers_name_is_old_offer
      expect( assigns( :conversions ).count).to eq( Conversion.where( offer_name: 'OldOffer' ).count )
    end

    it 'is RecentOffer and same in conversions' do
      get :conversion, @offers_name_is_recent_offer
      assigns( :conversions ).each { |con| expect( con.offer_name ).to eq( 'RecentOffer' ) }
    end

    it 'is same number number as name RecentOffer' do
      get :conversion, @offers_name_is_recent_offer
      expect( assigns( :conversions ).count).to eq( Conversion.where( offer_name: 'RecentOffer' ).count )
    end

  end

  context 'when country in params' do

    it 'is correct and conversions exists' do
      get :conversion, @country_is_correct
      assigns( :conversions ).each { |con| expect( con.country ).to eq( 'Russian Federation' ) }
    end

    it 'is correct and conversions do not exists' do
      get :conversion, @country_is_incorrect
      expect( assigns( :conversions ).count).to eq( Conversion.where( country: @usa ).count )
    end

  end

 end
