require 'rails_helper'

RSpec.describe DealerApi::OffersController, type: :controller do

  render_views

  before :context do
    @dealer = create :dealer
    10.times {|x| create :offer_performance_wl }
  end

  context 'auth' do
    it 'not authorizes with empty params' do
      get :index
      expect( JSON.parse( response.body ) ).to eq( { 'status' => '401' } )
    end

    it 'authorizes with correct params' do
      time = Time.now.to_i.to_s
      sign = Digest::MD5.hexdigest( @dealer.api_key + time )
      get :index, dealer_id: @dealer.id, timestamp: time, sign: sign, format: 'json'
      expect( JSON.parse( response.body ).count ).to eq( Offer.enabled.count )
    end

    it 'not authorizes with empty params' do
      get :show, dealer_id: @dealer.id, id: Offer.first.id, sign: 'no_sign', format: 'json', timestamp: '0'
      expect( JSON.parse( response.body ) ).to eq( { 'status' => '401' } )
    end

    it 'shows offer if all params correct' do
      time = Time.now.to_i.to_s
      sign = Digest::MD5.hexdigest( @dealer.api_key + time )
      get :show, dealer_id: @dealer.id, timestamp: time, sign: sign, id: Offer.first.id, format: 'json'
      expect( JSON.parse( response.body )[ "name" ] ).to eq( Offer.first.name )
    end
  end
end
