require 'rails_helper'

RSpec.describe DealerApi::CampaignsController, type: :controller do

  render_views

  before :context do
    @dealer = create :dealer
    10.times {|x| create :campaign, dealer: @dealer }
    10.times {|x| create :offer_performance_wl }
    @campaign = @dealer.campaigns.first
  end

  context 'auth' do
    it 'not authorizes with empty params' do
      get :index, format: 'json'
      expect( JSON.parse( response.body ) ).to eq( { 'status' => '401' } )
    end

    it 'authorizes with correct params' do
      time = Time.now.to_i.to_s
      sign = Digest::MD5.hexdigest( @dealer.api_key + time )
      get :index, dealer_id: @dealer.id, timestamp: time, sign: sign, format: 'json'
      expect( JSON.parse( response.body ).count ).to eq( @dealer.campaigns.count )
    end

    it 'not authorizes with wrong sign' do
      post :set_all_offers, dealer_id: 1, timestamp: '123', sign: 'hurr', id: 454, format: 'json'
      expect( JSON.parse( response.body ) ).to eq( { 'status' => '401' } )
    end

    it 'adds all enabled offers to campaign' do
      @campaign.set_offers!( Offer.enabled.pluck( :id ) )
      expect( @campaign.offers.count ).to eq( Offer.enabled.count )
    end

    it 'not authorizes with wrong sign' do
      post :mass_offer_set, dealer_id: @dealer.id, timestamp: '123', sign: 'hurr', id: @campaign.id, offer_ids: [1,2,3], format: 'json'
      expect( JSON.parse( response.body ) ).to eq( { 'status' => '401' } )
    end

    it 'removes all offers if ampty array is present as argument' do
      @campaign.set_offers!( [] )
      expect( @campaign.offers.count ).to eq( 0 )
    end

    it 'adds multiple offers to campaign' do
      offer_ids = Offer.limit( 3 ).pluck( :id )
      @campaign.set_offers!( offer_ids )
      expect( @campaign.offers.count ).to eq( offer_ids.count )
    end
  end
end
