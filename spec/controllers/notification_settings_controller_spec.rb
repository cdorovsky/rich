require 'rails_helper'

RSpec.describe NotificationSettingsController, type: :controller do

  render_views

  before :context do
    @dealer = create :dealer
    @notification_settings = @dealer.notification_settings
  end

  context 'settings' do

    describe 'standalone page' do

      it 'shows settings using link' do
        get :show, id: @notification_settings.hashid
        expect( assigns( :n_settings ) ).to eq( @notification_settings )
      end

      it 'updates settings using correct link' do
        post :update, {"notification_settings"=>{"ticket_email"=>false, "offer_email"=>false},
              "id"=>@notification_settings.hashid,
              "controller"=>"notification_settings",
              "action"=>"update"}
        expect( assigns( :n_settings ).ticket_email ).to eq( false )
        expect( assigns( :n_settings ).offer_email ).to eq( false )
      end

    end

  end

end
