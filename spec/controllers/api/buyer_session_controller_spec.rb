require 'rails_helper'

RSpec.describe Api::BuyerSessionController, type: :controller do

  render_views

  before :context do
    
    Country.delete_all
    @russia      = Country.find_by( abbr: 'RU' )  || create( :russia )
    @usa         = Country.find_by( abbr: 'US' )  || create( :usa )
    @thai        = Country.find_by( abbr: 'TH' )  || create( :thai )
    @country_all = Country.find_by( abbr: 'ALL' ) || create( :country_all )
    @na_country  = Country.find_by( abbr: 'N/A' ) || create( :na_country )

    @campaign               = create :campaign
    @campaign_redirect_code = @campaign.hash_code


    @ip_ru  = '217.118.82.88'
    @ip_us  = '207.5.112.114'
    @ip_tai = '125.24.79.169'
    @no_ip  = '0.0.0.0'

    @beeline_ip = BeelineIps.ip_list.sample
    @mts_ip     = MtsIps.ip_list.sample
    @megafon_ip = MegafonIps.ip_list.sample

    @russian_android_offer         = create :russian_offer,  apps_os: 'android'
    @tai_ios_offer                 = create :tai_offer,      apps_os: 'ios'
    @american_mobile_android_offer = create :american_offer, apps_os: 'mobile'
    @world_mobile_offer            = create :global_offer,   apps_os: 'mobile'

    @campaign.offers = [ @russian_android_offer, 
	                     @tai_ios_offer, 
	                     @american_mobile_android_offer, 
	                     @world_mobile_offer ]

    @russian_android_offer_set         = @campaign.offer_sets.find_by( offer_id: @russian_android_offer.id )
    @tai_ios_offer_set                 = @campaign.offer_sets.find_by( offer_id: @tai_ios_offer.id )
    @american_mobile_android_offer_set = @campaign.offer_sets.find_by( offer_id: @american_mobile_android_offer.id )
    @world_mobile_offer_set            = @campaign.offer_sets.find_by( offer_id: @world_mobile_offer.id )

    @iphone  = 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_1_2 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Version/7.0 Mobile/11D257 Safari/9537.53'
    @ipod    = 'Mozilla/5.0 (iPod; U; CPU like Mac OS X; en) AppleWebKit/420.1 (KHTML, like Gecko) Version/3.0 Mobile/3A101a Safari/419.3'
    @ipad    = 'Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) version/4.0.4 Mobile/7B367 Safari/531.21.10'
    @android = 'Mozilla/5.0 (Linux; U; Android 4.0.3; ko-kr; LG-L160L Build/IML74K) AppleWebkit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30'
    @mobile  = 'Mozilla/5.0 (Linux; U; Android 4.0.3; ko-kr; LG-L160L Build/IML74K) AppleWebkit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30'

    Rediska.load_all
    Rediska.empty_adekvat_dealers
  
  end

  context 'action wapclick' do

  	it 'beeline_ips do not see ads' do
      allow_any_instance_of( ActionDispatch::Request ).to receive( :remote_ip ).and_return( @beeline_ip )
      get :wapclick, id: @campaign_redirect_code
      expect( response.body   ).to eq( ' ' )
      expect( response.status ).to eq( 200 )
    end

    it 'mts_ips do not see ads' do
      allow_any_instance_of( ActionDispatch::Request ).to receive( :remote_ip ).and_return( @mts_ip )
      get :wapclick, id: @campaign_redirect_code
      expect( response.body   ).to eq( ' ' )
      expect( response.status ).to eq( 200 )
    end

    it 'megafon_ips do not see ads' do
      allow_any_instance_of( ActionDispatch::Request ).to receive( :remote_ip ).and_return( @megafon_ip )
      get :wapclick, id: @campaign_redirect_code
      expect( response.body   ).to eq( ' ' )
      expect( response.status ).to eq( 200 )
    end

    it 'non-mobile ru_ip see ads' do
      allow_any_instance_of( ActionDispatch::Request ).to receive( :remote_ip ).and_return( @ru_ip )
      get :wapclick, id: @campaign_redirect_code
      expect( response.body ).to include( 'script', "window.redirectCode = \"#{ @campaign_redirect_code }\"")
    end   

  end

  context 'action handler' do

    it 'campaign redirect_code' do
      get :handler, id: @campaign_redirect_code
      expect( response.body ).to include( 'script', "window.redirectCode = \"#{ @campaign_redirect_code }\"")
    end

  end 

  context 'action create' do
  
    it 'ru_ip android device' do
      allow_any_instance_of( ActionDispatch::Request ).to receive( :remote_ip  ).and_return( @ip_ru )
      allow_any_instance_of( ActionDispatch::Request ).to receive( :user_agent ).and_return( @android )
      get :create, format: 'json', id: @campaign_redirect_code
      expect( response.body ).to include( 'offers', '[1,4]', 'country', 'RU', 'platform', 'android' )
    end

    it 'ru_ip iphone device' do
      allow_any_instance_of( ActionDispatch::Request ).to receive( :remote_ip  ).and_return( @ip_ru )
      allow_any_instance_of( ActionDispatch::Request ).to receive( :user_agent ).and_return( @iphone )
      get :create, format: 'json', id: @campaign_redirect_code
      expect( response.body ).to include( 'offers', '[4]', 'country', 'RU', 'platform', 'ipod_iphone' )
    end

    it 'tai_ip iphone device' do
      allow_any_instance_of( ActionDispatch::Request ).to receive( :remote_ip  ).and_return( @ip_tai )
      allow_any_instance_of( ActionDispatch::Request ).to receive( :user_agent ).and_return( @iphone )
      get :create, format: 'json', id: @campaign_redirect_code
      expect( response.body ).to include( 'offers', '[2,4]', 'country', 'TH', 'platform', 'ipod_iphone' )
    end

    it 'tai_ip android device' do
      allow_any_instance_of( ActionDispatch::Request ).to receive( :remote_ip  ).and_return( @ip_tai )
      allow_any_instance_of( ActionDispatch::Request ).to receive( :user_agent ).and_return( @android )
      get :create, format: 'json', id: @campaign_redirect_code
      expect( response.body ).to include( 'offers', '[4]', 'country', 'TH', 'platform', 'android' )
    end

    it 'ip_us android device' do
      allow_any_instance_of( ActionDispatch::Request ).to receive( :remote_ip  ).and_return( @ip_us )
      allow_any_instance_of( ActionDispatch::Request ).to receive( :user_agent ).and_return( @android )
      get :create, format: 'json', id: @campaign_redirect_code
      expect( response.body ).to include( 'offers', '[3,4]', 'country', 'US', 'platform', 'android' )
    end

    it 'ip_us iphone device' do
      allow_any_instance_of( ActionDispatch::Request ).to receive( :remote_ip  ).and_return( @ip_us )
      allow_any_instance_of( ActionDispatch::Request ).to receive( :user_agent ).and_return( @iphone )
      get :create, format: 'json', id: @campaign_redirect_code
      expect( response.body ).to include( 'offers', '[3,4]', 'country', 'US', 'platform', 'ipod_iphone' )
    end

    it 'no_ip pc device' do
      allow_any_instance_of( ActionDispatch::Request ).to receive( :remote_ip  ).and_return( @no_ip )
      get :create, format: 'json', id: @campaign_redirect_code
      expect( response.body ).to include( 'offers', '[]', 'country', 'N/A', 'platform', 'pc' )
    end

  end

  context 'action show' do

    it 'return correct json' do
      allow_any_instance_of( ActionDispatch::Request ).to receive( :remote_ip  ).and_return( @ip_ru )
      allow_any_instance_of( ActionDispatch::Request ).to receive( :user_agent ).and_return( @android )
      get :show, format: 'json', id: @russian_android_offer_set.id
      expect( response.body ).to include( 'id', 'enabled', 'redirect_url', 'message', 'img_url' )
    end

    it 'do not count visitor from js offer request' do
      MongoEventContainer.delete_all
      MongoEventAtomic.delete_all
      allow_any_instance_of( ActionDispatch::Request ).to receive( :remote_ip  ).and_return( @ip_ru )
      allow_any_instance_of( ActionDispatch::Request ).to receive( :user_agent ).and_return( @android )
      get :show, format: 'json', id: @russian_android_offer_set.id
      expect( MongoEventAtomic.last.event_name ).to eq( 'offer_set' )
      expect( MongoEventAtomic.last.counter ).to eq( 0 )
    end

  end

  context 'action clicker' do

    it 'do not count js_push as uniq' do
      MongoEventContainer.delete_all
      MongoEventAtomic.delete_all
      allow_any_instance_of( ActionDispatch::Request ).to receive( :remote_ip  ).and_return( @ip_ru )
      allow_any_instance_of( ActionDispatch::Request ).to receive( :user_agent ).and_return( @android )
      get :clicker, format: 'json', id: @russian_android_offer_set.id, kind: 'push', uniq: true
      expect( MongoEventAtomic.last.event_name ).to eq( 'js_push' )
      expect( MongoEventAtomic.last.counter ).to eq( 1 )
      expect( MongoEventAtomic.last.extras[ 'uniq' ] ).to eq( true )
    end

    it 'do not count js_push as uniq' do
      MongoEventContainer.delete_all
      MongoEventAtomic.delete_all
      allow_any_instance_of( ActionDispatch::Request ).to receive( :remote_ip  ).and_return( @ip_ru )
      allow_any_instance_of( ActionDispatch::Request ).to receive( :user_agent ).and_return( @android )
      get :clicker, format: 'json', id: @russian_android_offer_set.id, kind: 'push'
      expect( MongoEventAtomic.last.event_name ).to eq( 'js_push' )
      expect( MongoEventAtomic.last.counter ).to eq( 1 )
      expect( MongoEventAtomic.last.extras[ 'uniq' ] ).to eq( nil )
    end

    it 'do not count js_push as uniq' do
      MongoEventContainer.delete_all
      MongoEventAtomic.delete_all
      allow_any_instance_of( ActionDispatch::Request ).to receive( :remote_ip  ).and_return( @ip_ru )
      allow_any_instance_of( ActionDispatch::Request ).to receive( :user_agent ).and_return( @android )
      get :clicker, format: 'json', id: @russian_android_offer_set.id, uniq: true
      expect( MongoEventAtomic.last.event_name ).to eq( 'clicker' )
      expect( MongoEventAtomic.last.counter ).to eq( 1 )
      expect( MongoEventAtomic.last.extras[ 'uniq' ] ).to eq( true )
    end

    it 'do not count js_push as uniq' do
      MongoEventContainer.delete_all
      MongoEventAtomic.delete_all
      allow_any_instance_of( ActionDispatch::Request ).to receive( :remote_ip  ).and_return( @ip_ru )
      allow_any_instance_of( ActionDispatch::Request ).to receive( :user_agent ).and_return( @android )
      get :clicker, format: 'json', id: @russian_android_offer_set.id
      expect( MongoEventAtomic.last.event_name ).to eq( 'clicker' )
      expect( MongoEventAtomic.last.counter ).to eq( 1 )
      expect( MongoEventAtomic.last.extras[ 'uniq' ] ).to eq( nil )
    end

  end
end
