module ControllerMacros

  def login_dealer
    before( :each ) do
      request.env[ 'devise.mapping' ] = Devise.mappings[ :dealer ]
      sign_in @dealer
    end
  end

end
