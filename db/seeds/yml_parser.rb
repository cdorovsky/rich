module YmlParser
  class << self

    def parse( model )
      parsed_models = YAML.load_file("#{ Rails.root }/lib/#{ model.to_s }.yml")
      model = model.to_s.singularize.camelize.constantize
      ar_objects = []
      parsed_models.each do | model_key, model_params |
        instance = model.new( model_params )
        ar_objects << instance
      end
      model.import ar_objects
    end

  end
end
