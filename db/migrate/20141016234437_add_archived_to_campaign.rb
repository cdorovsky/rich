class AddArchivedToCampaign < ActiveRecord::Migration
  def change
    add_column :campaigns, :archived, :boolean, default: false
  end
end
