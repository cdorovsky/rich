class AddBuyerRequestIdToTrafficBack < ActiveRecord::Migration
  def change
    add_column :traffic_backs, :buyer_request_id, :integer
  end
end
