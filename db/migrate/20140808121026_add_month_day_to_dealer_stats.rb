class AddMonthDayToDealerStats < ActiveRecord::Migration
  def change
    add_column :dealer_stats, :month, :integer
    add_column :dealer_stats, :day, :integer
    add_column :dealer_stats, :hour, :integer
    DealerStat.all.each do | stat |
      stat.month = stat.created_at.month
      stat.day   = stat.created_at.day
      stat.hour  = stat.created_at.hour
      stat.save
    end
  end
end
