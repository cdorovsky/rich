class CreateDecrementBackups < ActiveRecord::Migration
  def change
    create_table :decrement_backups do |t|
      t.integer :dealer_id
      t.float :confirmed, default: 0.0
      t.float :waiting, default: 0.0
      t.boolean :rolled_back, default: false
      t.timestamps
    end
  end
end
