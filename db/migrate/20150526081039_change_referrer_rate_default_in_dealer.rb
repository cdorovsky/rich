class ChangeReferrerRateDefaultInDealer < ActiveRecord::Migration
  def change
    Dealer.update_all( referrer_rate: 0.055 )
    change_column_default :dealers, :referrer_rate, 0.055
  end
end
