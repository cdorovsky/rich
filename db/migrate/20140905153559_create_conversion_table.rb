class CreateConversionTable < ActiveRecord::Migration
  def change
    create_table :conversions do |t|
      t.integer :offer_id 
      t.integer :campaign_id
      t.integer :dealer_id
      t.string  :offer_name
      t.float   :dealer_revenue
      t.string  :source_url
      t.string  :campaign_name
      t.string  :country
      t.string  :kind
      t.timestamps
    end
  end
end
