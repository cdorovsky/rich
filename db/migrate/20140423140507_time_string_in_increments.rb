class TimeStringInIncrements < ActiveRecord::Migration
  def change
    remove_column :seller_increments, :string
    add_column :seller_increments, :time, :string
  end
end
