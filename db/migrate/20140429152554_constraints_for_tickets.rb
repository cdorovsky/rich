class ConstraintsForTickets < ActiveRecord::Migration
  def change
    execute <<-SQL
        ALTER TABLE tickets
        ADD CONSTRAINT fk_tickets_dealers
        FOREIGN KEY (dealer_id)
        REFERENCES dealers(id)
    SQL
  end
end
