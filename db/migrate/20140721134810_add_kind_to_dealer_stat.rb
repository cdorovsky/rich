class AddKindToDealerStat < ActiveRecord::Migration
  def change
    add_column :dealer_stats, :offer_kind, :string
  end
end
