class AddDialogTranslationsToOffer < ActiveRecord::Migration
  def change
    add_column :offers, :dialog_translations, :text, default: ''
  end
end
