class AddOnlypayStatsToDealerIncrement < ActiveRecord::Migration
  def change
    add_column :dealer_increments, :onlypay_view, :integer
    add_column :dealer_increments, :onlypay_redirect, :integer
    add_column :dealer_increments, :onlypay_subscribe, :integer
    add_column :dealer_increments, :onlypay_rebill, :integer
    add_column :dealer_increments, :onlypay_unsubscribe, :integer
  end
end
