class AddReasonToDealerIncrement < ActiveRecord::Migration
  def change
    add_column :dealer_increments, :campaign_id,       :integer
    add_column :dealer_increments, :offer_id,          :integer
    add_column :dealer_increments, :reason_id,         :integer
    add_column :dealer_increments, :reason_type,       :string
  end
end
