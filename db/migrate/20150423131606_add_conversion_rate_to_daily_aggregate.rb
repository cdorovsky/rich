class AddConversionRateToDailyAggregate < ActiveRecord::Migration
  def change
    add_column :daily_aggregates, :conversion_rate, :float, default: 0.00
  end
end
