class AddUnsubscribeToDealerStats < ActiveRecord::Migration
  def change
    add_column :dealer_stats, :unsubscribe_wapclick, :integer, default: 0
  end
end
