class CreateAppInstallations < ActiveRecord::Migration
  def change
    create_table :app_installations do |t|
      t.integer :dealer_id
      t.integer :campaign_id
      t.integer :offer_id
      t.integer :postback_id
      t.text    :transaction_id
      t.timestamps
    end
  end
end
