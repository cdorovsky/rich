class ChangeHourTypeInSellerIncrements < ActiveRecord::Migration
  def change
    change_column :seller_increments, :hour, 'integer USING CAST(hour AS integer)'
  end
end
