class AddDefaultesToDealerIncrements < ActiveRecord::Migration
  def change
    change_column :dealer_increments, :waiting, :float, default: 0.0
    change_column :dealer_increments, :confirmed, :float, default: 0.0 
  end
end
