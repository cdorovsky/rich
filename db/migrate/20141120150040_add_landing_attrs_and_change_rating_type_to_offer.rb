class AddLandingAttrsAndChangeRatingTypeToOffer < ActiveRecord::Migration
  def change
    add_column :offers, :landing_name, :string
    add_column :offers, :landing_description, :text
    change_column :offers, :rating, :float, default: 5.0
  end
end
