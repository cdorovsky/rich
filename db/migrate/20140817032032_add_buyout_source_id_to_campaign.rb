class AddBuyoutSourceIdToCampaign < ActiveRecord::Migration
  def change
    add_column :campaigns, :buyout_source_id, :integer
    add_column :campaigns, :buyout_target_id, :integer
  end
end
