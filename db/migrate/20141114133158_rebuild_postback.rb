class RebuildPostback < ActiveRecord::Migration
  def change
    remove_column :postbacks, :postback_code
    rename_column :postbacks, :country, :country_name
    add_column    :postbacks, :datetime, :string
    add_column    :postbacks, :country_id, :integer
    add_column    :postbacks, :conversion_type, :string
    add_column    :postbacks, :conversion_id, :integer
    add_column    :postbacks, :conversion_code, :string
    add_column    :postbacks, :offer_name, :text
    add_column    :postbacks, :profit_usd, :float
  end
end
