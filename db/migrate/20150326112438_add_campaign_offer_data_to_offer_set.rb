class AddCampaignOfferDataToOfferSet < ActiveRecord::Migration
  def change
  	change_column :offer_sets, :message,     :text
  	add_column    :offer_sets, :tb_url,      :string
  	add_column    :offer_sets, :m_tb_url,    :string
  	add_column    :offer_sets, :preview_url, :string
  end
end
