class AddRebilledAtToSubscribe < ActiveRecord::Migration
  def change
    add_column :subscribes, :rebilled_at, :datetime
  end
end
