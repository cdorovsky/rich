class AddDescriptionToAdminRequest < ActiveRecord::Migration
  def change
    add_column :admin_requests, :description, :text
  end
end
