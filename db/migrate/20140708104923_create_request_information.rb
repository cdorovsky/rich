class CreateRequestInformation < ActiveRecord::Migration
  def change
    create_table :request_informations do |t|
      t.string :ip
      t.string :user_agent
      t.timestamps
    end
  end
end
