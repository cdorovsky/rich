class RemoveBuyerRequestIdFromConversion < ActiveRecord::Migration
  def change
    remove_column :conversions, :buyer_request_id, :integer 
  end
end
