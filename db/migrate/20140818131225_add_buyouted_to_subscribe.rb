class AddBuyoutedToSubscribe < ActiveRecord::Migration
  def change
    add_column :subscribes, :buyouted, :boolean, default: false
  end
end
