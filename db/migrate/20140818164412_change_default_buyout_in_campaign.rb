class ChangeDefaultBuyoutInCampaign < ActiveRecord::Migration
  def change
    change_column :campaigns, :megafon_buyout, :boolean, default: false
    change_column :campaigns, :beeline_buyout, :boolean, default: false
  end
end
