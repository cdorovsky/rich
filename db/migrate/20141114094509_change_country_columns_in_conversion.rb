class ChangeCountryColumnsInConversion < ActiveRecord::Migration
  def change
    rename_column :conversions, :country, :country_name
    add_column :conversions, :country_id, :integer
  end
end
