class AddFieldsToDirectOffer < ActiveRecord::Migration
  def change
    add_column :direct_offers, :seller_id,   :integer
    add_column :direct_offers, :seller_name, :string
    add_column :direct_offers, :external_id, :integer
    add_column :direct_offers, :apps_os,     :string
    add_column :direct_offers, :offer_name,  :string
    add_column :direct_offers, :offer_kind,  :string
    add_column :direct_offers, :cost,        :float
  end
end
