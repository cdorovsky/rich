class AddCostToOfferSets < ActiveRecord::Migration
  def change
    add_column :offer_sets, :cost, :float, default: 0.0
  end
end
