class AddRoleToDealer < ActiveRecord::Migration
  def change
    add_column :dealers, :role, :string, default: 'dealer'
  end
end
