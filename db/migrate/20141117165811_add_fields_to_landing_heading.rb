class AddFieldsToLandingHeading < ActiveRecord::Migration
  def change
    add_column :landing_headings, :is_default, :boolean, default: false
    add_column :landing_headings, :app_kind, :string
    remove_column :landing_headings, :offer_id
  end
end
