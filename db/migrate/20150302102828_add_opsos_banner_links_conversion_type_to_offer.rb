class AddOpsosBannerLinksConversionTypeToOffer < ActiveRecord::Migration
  def change
    add_column :offers, :opsos, :string, default: ''
    add_column :offers, :banner_links, :string, default: ''
    add_column :offers, :conversion_type, :string, default: ''
  end
end
