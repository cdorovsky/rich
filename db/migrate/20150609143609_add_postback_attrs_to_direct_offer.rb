class AddPostbackAttrsToDirectOffer < ActiveRecord::Migration
  def change
    add_column :direct_offers, :postback, :boolean, default: false
    add_column :direct_offers, :postback_action, :string, default: 'POST'
    add_column :direct_offers, :postback_url, :text
  end
end
