class AddIndexesToDealerStat < ActiveRecord::Migration
  def change
    add_index :dealer_stats, [ :dealer_id, :campaign_id, :offer_id,
                               :hourly, :daily, :monthly,
                               :current, :month, :day, :hour,
                               :no_grouping, :campaign_grouping, :offer_grouping,
                               :common, :wapclick, :cpa, :apps_advert, :purpose ], name: 'main_index'
  end
end
