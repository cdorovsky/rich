class AddConversionReason < ActiveRecord::Migration
  def change
    add_column :conversions, :conversion_reason, :string
  end
end
