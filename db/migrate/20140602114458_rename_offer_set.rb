class RenameOfferSet < ActiveRecord::Migration
  def change
    rename_table :offer_set, :offer_sets
  end
end
