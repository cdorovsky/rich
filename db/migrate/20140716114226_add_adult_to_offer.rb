class AddAdultToOffer < ActiveRecord::Migration
  def change
    add_column :offers, :adult, :boolean, default: false
  end
end
