class AddDefaultsToSellerBalanceMoney < ActiveRecord::Migration
  def change
    change_column :seller_balances, :waiting, :float, default: 0
    change_column :seller_balances, :confirmed, :float, default: 0
  end
end
