class AddOsVersionToOffer < ActiveRecord::Migration
  def change
    add_column :offers, :os_version, :string
  end
end
