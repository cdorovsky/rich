class CreateLandingHeadingSets < ActiveRecord::Migration
  def change
    create_table :landing_heading_sets do |t|
      t.integer :landing_heading_id
      t.integer :offer_id

      t.timestamps
    end
  end
end
