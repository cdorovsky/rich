class CreateOffers < ActiveRecord::Migration
  def change
    create_table :offers do |t|
      t.integer :seller_id
      t.string :name
      t.string :url
      t.string :code

      t.timestamps
    end
  end
end
