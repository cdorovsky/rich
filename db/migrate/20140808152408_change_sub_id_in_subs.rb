class ChangeSubIdInSubs < ActiveRecord::Migration
  def change
    change_column :subscribes, :sub_id, :bigint, default: 0
  end
end
