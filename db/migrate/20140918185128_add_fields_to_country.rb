class AddFieldsToCountry < ActiveRecord::Migration
  def change
    add_column :countries, :abbr, :string, default: ''
    add_column :countries, :image_url, :string, default: ''
  end
end
