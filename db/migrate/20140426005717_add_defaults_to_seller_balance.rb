class AddDefaultsToSellerBalance < ActiveRecord::Migration
  def change
    change_column :seller_balances, :wapclick_view, :integer, default: 0
    change_column :seller_balances, :wapclick_redirect, :integer, default: 0
    change_column :seller_balances, :wapclick_subscribe, :integer, default: 0
    change_column :seller_balances, :wapclick_rebill, :integer, default: 0
    change_column :seller_balances, :wapclick_unsubscribe, :integer, default: 0
  end
end
