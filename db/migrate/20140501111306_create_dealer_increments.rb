class CreateDealerIncrements < ActiveRecord::Migration
  def change
    create_table :dealer_increments do |t|
      t.integer :hour
      t.integer :dealer_id
      t.float :waiting_increment
      t.float :confirmed_increment
      t.timestamps
    end
  end
end
