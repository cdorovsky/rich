class AddDeepLvlToDealerStats < ActiveRecord::Migration
  def change
    add_column :dealer_stats, :default_deep_lvl,  :boolean, default: false
    add_column :dealer_stats, :campaign_deep_lvl, :boolean, default: false
    add_column :dealer_stats, :offer_deep_lvl,    :boolean, default: false
  end
end
