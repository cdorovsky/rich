class AddConversionBuyoutToDealerStat < ActiveRecord::Migration
  def change
    add_column :dealer_stats, :conversion_buyouted, :integer, default: 0
  end
end
