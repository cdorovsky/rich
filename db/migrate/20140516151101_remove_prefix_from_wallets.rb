class RemovePrefixFromWallets < ActiveRecord::Migration
  def change
    remove_column :wallets, :prefix
  end
end
