class AddUserDeviceToBuyerRequest < ActiveRecord::Migration
  def change
    add_column :buyer_requests, :user_device, :string, null: false, default: 'mobile'
  end
end
