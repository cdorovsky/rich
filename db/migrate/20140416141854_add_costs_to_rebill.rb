class AddCostsToRebill < ActiveRecord::Migration
  def change
    add_column :rebills, :full_cost, :bigint
  end
end
