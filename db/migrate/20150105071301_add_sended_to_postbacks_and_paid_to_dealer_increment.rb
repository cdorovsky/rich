class AddSendedToPostbacksAndPaidToDealerIncrement < ActiveRecord::Migration
  def change
    add_column :postbacks,        :sended, :boolean, default: false
    add_column :dealer_increments, :paid,   :boolean, default: false
  end
end
