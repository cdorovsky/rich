class AddCountryCodeToPostback < ActiveRecord::Migration
  def change
    add_column :postbacks, :country_code, :string
    add_column :conversions, :country_code, :string
  end
end
