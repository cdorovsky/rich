class RemoveDefaultFromDialogsInOffer < ActiveRecord::Migration
  def change
    change_column_default :offers, :dialog_translations, nil
  end
end
