class AddDealerBalanceIdToDealerIncrement < ActiveRecord::Migration
  def change
    add_column :dealer_increments, :dealer_balance_id, :integer
  end
end
