class AddPostbackIdToSellerRequest < ActiveRecord::Migration
  def change
    add_column :seller_requests, :postback_id, :integer
  end
end
