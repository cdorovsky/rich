class AddCountryAndSubIDsToSellerRequest < ActiveRecord::Migration
  def change
    add_column :seller_requests, :country_id, :integer
    add_column :seller_requests, :sub_id, :text
    add_column :seller_requests, :sub_id2, :text
    add_column :seller_requests, :sub_id3, :text
  end
end
