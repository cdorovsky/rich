class AddOfferIdToDealerStat < ActiveRecord::Migration
  def change
    add_column :dealer_stats, :offer_id, :integer
  end
end
