class AddIndexMongoIdToOfferSetShow < ActiveRecord::Migration
  def change
    add_index :offer_set_shows, :mongo_id
  end
end
