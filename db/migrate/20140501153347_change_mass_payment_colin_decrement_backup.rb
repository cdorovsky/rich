class ChangeMassPaymentColinDecrementBackup < ActiveRecord::Migration
  def change
    add_column :decrement_backups, :payment_type, :string
    remove_column :decrement_backups, :mass_payment 
  end
end
