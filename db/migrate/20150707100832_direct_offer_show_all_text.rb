class DirectOfferShowAllText < ActiveRecord::Migration
  def change
    change_column :direct_offer_shows, :request_id, :text
    change_column :direct_offer_shows, :referer,    :text
    change_column :direct_offer_shows, :country_name, :text
    change_column :direct_offer_shows, :country_code, :text
    change_column :direct_offer_shows, :user_platform, :text
    change_column :direct_offer_shows, :user_device, :text
    change_column :direct_offer_shows, :timestamp, :text
    change_column :direct_offer_shows, :from, :text
  end
end
