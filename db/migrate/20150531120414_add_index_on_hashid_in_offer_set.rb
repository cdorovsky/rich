class AddIndexOnHashidInOfferSet < ActiveRecord::Migration
  def change
    add_index :offer_sets, :hashid, name: "index_hashid_on_offer_set", unique: true, using: :btree
  end
end
