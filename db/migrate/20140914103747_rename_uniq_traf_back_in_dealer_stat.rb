class RenameUniqTrafBackInDealerStat < ActiveRecord::Migration
  def change
    rename_column :dealer_stats, :uniq_traffic_back, :uniq_traf_back 
  end
end
