class ChangeMoneyInRequestLogs < ActiveRecord::Migration
  def change
    rename_column :request_logs, :full_cost, :tariff
    rename_column :request_logs, :payout, :dealer_revenue
  end
end
