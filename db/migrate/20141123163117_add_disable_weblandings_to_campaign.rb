class AddDisableWeblandingsToCampaign < ActiveRecord::Migration
  def change
    add_column :campaigns, :disable_weblandings, :boolean, default: false
  end
end
