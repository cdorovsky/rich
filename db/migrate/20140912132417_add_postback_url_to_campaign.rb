class AddPostbackUrlToCampaign < ActiveRecord::Migration
  def change
    add_column :campaigns, :postback_url, :string
  end
end
