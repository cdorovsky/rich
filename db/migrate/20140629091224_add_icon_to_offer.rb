class AddIconToOffer < ActiveRecord::Migration
  def change
    add_column :offers, :icon, :string
  end
end
