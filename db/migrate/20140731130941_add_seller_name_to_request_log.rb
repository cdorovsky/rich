class AddSellerNameToRequestLog < ActiveRecord::Migration
  def change
    add_column :request_logs, :seller_name, :string
  end
end
