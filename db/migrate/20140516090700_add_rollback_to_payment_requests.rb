class AddRollbackToPaymentRequests < ActiveRecord::Migration
  def change
    add_column :payment_requests, :payment_id, :integer
    add_column :payment_requests, :rolled_back, :boolean, default: false
    add_column :payment_requests, :payment_type, :string
    rename_column :payment_requests, :value, :confirmed
  end
end
