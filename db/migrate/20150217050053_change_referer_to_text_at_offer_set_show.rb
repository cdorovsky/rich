class ChangeRefererToTextAtOfferSetShow < ActiveRecord::Migration
  def change
    change_column :offer_set_shows, :referer, :text
  end
end
