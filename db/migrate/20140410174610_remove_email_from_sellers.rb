class RemoveEmailFromSellers < ActiveRecord::Migration
  def change
    remove_column :sellers, :email, :string
  end
end
