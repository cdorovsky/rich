class CountrySwitch < ActiveRecord::Migration
  def change
    unless Rails.env.test?
      Country.all.order('id DESC').each do | country |
        country.update( id: country.id + 6 )
      end
      OfferCountrySet.all.each do | ocs |
        ocs.update( country_id: ocs.country_id + 6 )
      end
      %w( Russian\ Federation Ukraine Kazakhstan United\ States United\ Kingdom ).each_with_index do | c, i |
        country = Country.find_by_name( c )
        country.offer_country_sets.update_all( country_id: i + 1 )
        country.update( id: i + 1 )
      end
    end
  end
end
