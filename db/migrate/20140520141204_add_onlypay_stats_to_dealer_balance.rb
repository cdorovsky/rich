class AddOnlypayStatsToDealerBalance < ActiveRecord::Migration
  def change
    add_column :dealer_balances, :onlypay_view, :integer
    add_column :dealer_balances, :onlypay_redirect, :integer
    add_column :dealer_balances, :onlypay_subscribe, :integer
    add_column :dealer_balances, :onlypay_rebill, :integer
    add_column :dealer_balances, :onlypay_unsubscribe, :integer
  end
end
