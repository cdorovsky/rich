class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.string :title
      t.text :content
      t.integer :dealer_id

      t.timestamps
    end
  end
end
