class AddTicketCountAndDeleteContent < ActiveRecord::Migration
  def change
    remove_column :tickets, :content, :text
    add_column    :tickets, :message_counter, :integer
  end
end
