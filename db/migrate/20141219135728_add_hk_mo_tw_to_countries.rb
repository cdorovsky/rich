class AddHkMoTwToCountries < ActiveRecord::Migration
  def change
    ActiveRecord::Base.transaction do
      Country.find_or_create_by( id: 257, name: "Hong Kong", abbr: "HK", image_url: "./assets/flags/hk.png" )
      Country.find_or_create_by( id: 258, name: "Macau", abbr: "MO", image_url: "./assets/flags/mo.png" )
      Country.find_or_create_by( id: 259, name: "Taiwan", abbr: "TW", image_url: "./assets/flags/tw.png" )
    end
  end
end
