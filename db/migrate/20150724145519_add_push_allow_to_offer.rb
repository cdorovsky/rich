class AddPushAllowToOffer < ActiveRecord::Migration
  def change
    add_column :offers, :allow_push, :boolean
  end
end
