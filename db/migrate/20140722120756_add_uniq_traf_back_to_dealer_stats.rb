class AddUniqTrafBackToDealerStats < ActiveRecord::Migration
  def change
    add_column :dealer_stats, :uniq_traffic_back_counter, :integer, default: 0
  end
end
