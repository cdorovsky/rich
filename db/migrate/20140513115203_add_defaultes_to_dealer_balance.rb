class AddDefaultesToDealerBalance < ActiveRecord::Migration
  def change
    change_column :dealer_balances, :waiting, :float, default: 0.0
    change_column :dealer_balances, :confirmed, :float, default: 0.0
    change_column :dealer_balances, :requested, :float, default: 0.0
  end
end
