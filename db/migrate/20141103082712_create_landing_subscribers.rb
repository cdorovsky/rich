class CreateLandingSubscribers < ActiveRecord::Migration
  def change
    create_table :landing_subscribers do |t|
      t.integer :country_id
      t.integer :offer_set_id
      t.boolean :active, default: false
      t.string  :phone, default: ''
      t.string  :os, default: ''

      t.timestamps
    end
  end
end
