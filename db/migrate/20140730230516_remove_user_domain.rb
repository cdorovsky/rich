class RemoveUserDomain < ActiveRecord::Migration
  def change
    drop_table :user_domains if ActiveRecord::Base.connection.table_exists? :user_domains
  end
end
