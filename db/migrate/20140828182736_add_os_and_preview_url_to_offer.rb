class AddOsAndPreviewUrlToOffer < ActiveRecord::Migration
  def change
    add_column :offers, :apps_os, :string
    add_column :offers, :preview_url, :string
  end
end
