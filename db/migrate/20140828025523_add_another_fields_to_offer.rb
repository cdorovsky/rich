class AddAnotherFieldsToOffer < ActiveRecord::Migration
  def change
    add_column :offers, :motivated, :boolean, default: false
    add_column :offers, :redirect, :boolean, default: false
    add_column :offers, :our_profit, :float, default: 0.0
    add_column :offers, :partners_profit, :float, default: 0.0
  end
end
