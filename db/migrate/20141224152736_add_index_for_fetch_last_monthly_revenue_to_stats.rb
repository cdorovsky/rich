class AddIndexForFetchLastMonthlyRevenueToStats < ActiveRecord::Migration
  def change
    add_index :dealer_stats, [ :monthly, :dealer_id ], name: 'for_campaigns'
  end
end
