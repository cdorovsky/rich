class DropDealerHourStats < ActiveRecord::Migration
  def change
    drop_table :dealer_hour_stats if ActiveRecord::Base.connection.table_exists? :dealer_hour_stats
  end
end
