class AddSourceUrlToBuyerRequest < ActiveRecord::Migration
  def change
    add_column :buyer_requests, :referer_url, :text, null: false
  end
end
