class AddMessageToTicket < ActiveRecord::Migration
  def change
    add_column :tickets, :message, :string
  end
end
