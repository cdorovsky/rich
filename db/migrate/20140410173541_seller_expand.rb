class SellerExpand < ActiveRecord::Migration
  def change
    add_column :sellers, :seller_name, :string
    add_column :sellers, :seller_url, :string
    add_column :sellers, :contact_name, :string
    add_column :sellers, :contact_phone, :string
    add_column :sellers, :contact_email, :string
    remove_column :sellers, :login, :string
    remove_column :sellers, :password, :string
  end
end
