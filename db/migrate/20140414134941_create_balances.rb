class CreateBalances < ActiveRecord::Migration
  def change
    create_table :balances do |t|
      t.integer :dealer_id
      t.string :basic_currency
      t.string :user_currency

      t.timestamps
    end
  end
end
