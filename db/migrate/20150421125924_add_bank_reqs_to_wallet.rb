class AddBankReqsToWallet < ActiveRecord::Migration
  def change
    add_column :wallets, :reciever,            :string
    add_column :wallets, :reciever_adress,     :text
    add_column :wallets, :bank_name,           :string
    add_column :wallets, :bank_adress,         :text
    add_column :wallets, :bank_city,           :string
    add_column :wallets, :bank_country,        :string
    add_column :wallets, :account_number_iban, :string
    add_column :wallets, :swift_code,          :string
    add_column :wallets, :details,             :text
  end
end
