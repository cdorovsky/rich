class AddForCkeckoutToDealerBalance < ActiveRecord::Migration
  def change
    add_column :dealer_balances, :for_checkout, :float, default: 0.00
  end
end
