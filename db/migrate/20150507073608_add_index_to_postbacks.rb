class AddIndexToPostbacks < ActiveRecord::Migration
  def change
    add_index :postbacks, [ :dealer_id, :campaign_id, :offer_id ], name: 'for_search'
  end
end
