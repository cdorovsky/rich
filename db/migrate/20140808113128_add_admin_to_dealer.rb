class AddAdminToDealer < ActiveRecord::Migration
  def change
    add_column :dealers, :is_an_admin, :boolean, default: false
  end
end
