class AddRegionsToOffer < ActiveRecord::Migration
  def change
    add_column :offers, :russia,  :boolean
    add_column :offers, :europe,  :boolean
    add_column :offers, :asia,    :boolean
    add_column :offers, :america, :boolean
  end
end
