class CreatePaymentRequests < ActiveRecord::Migration
  def change
    create_table :payment_requests do |t|
      t.integer  :dealer_id
      t.float    :value, default: 0.0
      t.boolean  :paid, default: false
      t.timestamps
    end
  end
end
