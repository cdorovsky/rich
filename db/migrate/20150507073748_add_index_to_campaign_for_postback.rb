class AddIndexToCampaignForPostback < ActiveRecord::Migration
  def change
    add_index :campaigns, [ :sent_postback, :postback_url ], name: 'for_postback'
  end
end
