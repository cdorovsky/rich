class AddRegParamsToDealer < ActiveRecord::Migration
  def change
    add_column :dealers, :reg_params, :string, default: ''
  end
end
