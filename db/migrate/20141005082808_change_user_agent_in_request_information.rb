class ChangeUserAgentInRequestInformation < ActiveRecord::Migration
  def change
    change_column :request_informations, :user_agent, :text
  end
end
