class CreatePostback < ActiveRecord::Migration
  def change
    create_table :postbacks do |t|
      t.string  :postback_code
      t.string  :postback_url
      t.integer :dealer_id 
      t.integer :campaign_id
      t.integer :offer_id
      t.string  :sub_id
      t.string  :sub_id2
      t.string  :sub_id3
      t.string  :country
      t.timestamps
    end
  end
end
