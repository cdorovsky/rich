class AddDealerIdToSellerRequest < ActiveRecord::Migration
  def change
    add_column :seller_requests, :dealer_id, :integer
  end
end
