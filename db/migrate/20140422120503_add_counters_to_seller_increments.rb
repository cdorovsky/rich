class AddCountersToSellerIncrements < ActiveRecord::Migration
  def change
    add_column :seller_increments, :wapclick_view, :integer
    add_column :seller_increments, :wapclick_redirect, :integer 
    add_column :seller_increments, :wapclick_subscribe, :integer
    add_column :seller_increments, :wapclick_rebill, :integer 
    add_column :seller_increments, :wapclick_unsubcribe, :integer 
  end
end
