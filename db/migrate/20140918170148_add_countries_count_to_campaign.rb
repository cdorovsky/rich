class AddCountriesCountToCampaign < ActiveRecord::Migration
  def change
    add_column :campaigns, :countries_count, :integer, default: 0
  end
end
