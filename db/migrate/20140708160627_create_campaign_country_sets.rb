class CreateCampaignCountrySets < ActiveRecord::Migration
  def change
    create_table :campaign_country_sets do |t|
      t.integer :country_id
      t.integer :campaign_id

      t.timestamps
    end
  end
end
