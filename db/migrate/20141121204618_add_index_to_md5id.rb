class AddIndexToMd5id < ActiveRecord::Migration
  def change
    remove_index :dealer_stats, name: 'main_index'
    add_index :dealer_stats, [ :md5id ], name: 'md5index'
  end
end
