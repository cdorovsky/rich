class AddConversionInstallToDealerStat < ActiveRecord::Migration
  def change
    add_column :dealer_stats, :conversion_install, :integer, default: 0
  end
end
