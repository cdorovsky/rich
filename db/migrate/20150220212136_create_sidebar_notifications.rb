class CreateSidebarNotifications < ActiveRecord::Migration
  def change
    create_table :sidebar_notifications do |t|
      t.integer :unread_tickets_count, default: 0
      t.integer :dealer_id

      t.timestamps
    end
  end
end
