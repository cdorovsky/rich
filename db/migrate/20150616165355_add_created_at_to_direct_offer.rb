class AddCreatedAtToDirectOffer < ActiveRecord::Migration
  def change
    add_column :direct_offers, :created_at, :datetime
  end
end
