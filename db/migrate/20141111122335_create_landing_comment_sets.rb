class CreateLandingCommentSets < ActiveRecord::Migration
  def change
    create_table :landing_comment_sets do |t|
      t.integer :offer_id
      t.integer :offer_comment_id

      t.timestamps
    end
  end
end
