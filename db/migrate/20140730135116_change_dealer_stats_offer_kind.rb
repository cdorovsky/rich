class ChangeDealerStatsOfferKind < ActiveRecord::Migration
  def change
    remove_column :dealer_stats, :offer_kind
    add_column :dealer_stats, :wapclick_flag,    :boolean, default: :false
    add_column :dealer_stats, :cpa_flag,         :boolean, default: :false
    add_column :dealer_stats, :apps_advert_flag, :boolean, default: :false
  end
end
