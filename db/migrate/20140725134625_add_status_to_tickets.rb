class AddStatusToTickets < ActiveRecord::Migration
  def change
    add_column :tickets, :closed, :boolean, default: false 
  end
end
