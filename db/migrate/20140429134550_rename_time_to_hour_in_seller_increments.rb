class RenameTimeToHourInSellerIncrements < ActiveRecord::Migration
  def change
    rename_column :seller_increments, :time, :hour
  end
end
