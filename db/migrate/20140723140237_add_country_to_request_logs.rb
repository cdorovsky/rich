class AddCountryToRequestLogs < ActiveRecord::Migration
  def change
    add_column :request_logs, :country, :string
  end
end
