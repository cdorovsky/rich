class AddOfferKindInOffer < ActiveRecord::Migration
  def change
    add_column :offers, :kind, :string, default: "cpa"
  end
end
