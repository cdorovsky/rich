class SetDefaultsToOffers < ActiveRecord::Migration
  def change
    change_column :offers, :russia, :boolean, default: false
    change_column :offers, :europe, :boolean, default: false
    change_column :offers, :asia, :boolean, default: false
    change_column :offers, :america, :boolean, default: false
  end
end
