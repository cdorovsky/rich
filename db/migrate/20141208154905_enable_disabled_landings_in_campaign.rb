class EnableDisabledLandingsInCampaign < ActiveRecord::Migration
  def change
    Campaign.all.update_all( disable_weblandings: false )
  end
end
