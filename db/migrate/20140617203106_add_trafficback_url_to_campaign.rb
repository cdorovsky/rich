class AddTrafficbackUrlToCampaign < ActiveRecord::Migration
  def change
    add_column :campaigns, :trafficback_url, :string
  end
end
