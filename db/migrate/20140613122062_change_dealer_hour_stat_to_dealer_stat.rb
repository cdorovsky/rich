class ChangeDealerHourStatToDealerStat < ActiveRecord::Migration
  def change
    rename_table :dealer_hour_stats, :dealer_stats
  end
end
