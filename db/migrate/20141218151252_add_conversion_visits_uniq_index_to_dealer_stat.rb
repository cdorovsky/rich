class AddConversionVisitsUniqIndexToDealerStat < ActiveRecord::Migration
  def change
    add_index :dealer_stats, [ :visitor, :uniq_visitor, :conversion_app_install ], name: 'for_dealer_profit'
  end
end
