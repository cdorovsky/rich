class CreateCountrySets < ActiveRecord::Migration
  def change
    create_table :country_sets do |t|
      t.integer :campaign_id
      t.boolean :russia
      t.boolean :europe
      t.boolean :asia
      t.boolean :america
      t.timestamps
    end
  end
end
