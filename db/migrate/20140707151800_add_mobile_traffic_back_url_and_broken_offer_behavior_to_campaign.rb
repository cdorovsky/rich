class AddMobileTrafficBackUrlAndBrokenOfferBehaviorToCampaign < ActiveRecord::Migration
  def change
    add_column :campaigns, :m_trafficback_url, :string
    add_column :campaigns, :broken_offer_behavior, :string
  end
end
