class AddIndexUniqDirectOfferHashcode < ActiveRecord::Migration
  def change
    add_index :direct_offers, :hashid, unique: true
  end
end
