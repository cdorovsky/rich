class RemovePostbackIdFromAppInstallation < ActiveRecord::Migration
  def change
    remove_column :app_installations, :postback_id
  end
end
