class RenameMassPaymentToPayments < ActiveRecord::Migration
  def change
    rename_table :mass_payments, :payments
  end
end
