class CreateUserDomains < ActiveRecord::Migration
  def change
    create_table :user_domains do |t|
      t.string :url, default: ''
      t.integer :campaign_id
      t.boolean :wapclick_ready, default: false

      t.timestamps
    end
  end
end
