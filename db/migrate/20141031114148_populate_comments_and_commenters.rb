require 'open-uri'

class PopulateCommentsAndCommenters < ActiveRecord::Migration

  def up
    @apps_comments = [
        ['Сергей Велес', '4','Пока полностью доволен. Работает быстро, удобное меню. Советую!', 'photo_1.jpg'],
        ['Machine555', '5', 'Классное приложение! Все кому показал завидуют и качают себе', 'photo_2.jpg'],
        ['Elena22-33', '5', 'Это просто потрясающе, работоспособность на ура, удобно, не думала что так понравится, но это случилось. Спасибо создателям!', 'photo_4.jpg'],
        ['Putin 1', '4', 'Работает нормально', 'photo_3.jpg'],
        ['grePhone', '5', 'Радует каждая мелочь', 'photo_5.jpg'],
        ['Daler911', '5','Всем советую','photo_6.jpg'],
        ['viktoRo', '3', 'Все время пользуюсь это прогой... Единственное не нравится иконка', 'photo_9.jpg'],
        ['AlexanderGoodMan', '4', 'Спасибо за хорошее приложение', 'photo_7.jpg'],
        ['Greenkatya-a', '5', 'Все! Тут я точно довольна', 'photo_8.jpg'],
        ['Maxim K', '4', 'Полёт нормальный', 'photo_10.jpg'],
        ['Sasha90', '5', 'Очень выручает!', 'photo_11.jpg']
                    ]

    @game_comments = [
        ['Roman Kolmakov', '5', 'Все работает Игра замечательная.', 'photo_1.jpg'],
        ['Петр Пикулев', '3', 'Игра отличная Есть некоторые недоработки, иногда не могу зайти в игру.', 'photo_2.jpg'],
        ['Anastasia380', '5', 'Нам нравится)) У меня в нее брат играет, теперь и муж подсел. А когда его дома нет, могу и я поиграть. В общем думаю если начнете играть, то уже не сможете остановитесь) Очень затягивает))', 'photo_4.jpg'],
        ['Rustam I', '5', 'Игра очень даже ничего, очень хорошая графика. Не думал, что так затянет. Глюков и рекламы практически нет. Конечно есть маленькие недостатки, но у кого их нет?! Любителям поиграть в транспорте - советую.', 'photo_7.jpg'],
        ['Aлександр-дачков', '5', 'Классная игрушка И мнеи сыну понравилась, играем по очереди на планшете) Иногда даже не можем поделить!', 'photo_5.jpg'],
        ['Annnnnnnna', '5', 'Игруха класс играю давно но сегодня весь день не могу, занята, надеюсь вечером появится время', 'photo_8.jpg'],
        ['Julianana', '5', 'Суперская игра! отличная игра, не могу оторваться уже 2 дня!', 'photo_11.jpg'],
        ['TerminatorMSK', '5', 'Ну хоть эта игра не разочаровала, в отличии от многих!', 'photo_10.jpg'],
        ['Sergzzzz', '4', 'Играю пару месяцев, игрушка не плохая, но уже легко проходить стало', 'photo_3.jpg'],
        ['AndreyKo', '5', 'Отличная игра!!!!! С такой игрой действительной можно забыть обо всём', 'photo_6.jpg']
                    ]

    [ [ @apps_comments, 'application' ], [ @game_comments, 'game' ] ].each do | dafuq |
      app_kind = dafuq[1]
      dafuq[0].each do |comment|
        avatar_url = 'https://dl.dropboxusercontent.com/u/30972674/web_land_google_play_avatar/' + comment[3]
        commenter = Commenter.new( name: comment[0] )
        unless Rails.env.test?
            commenter.remote_avatar_url = avatar_url
        end
        commenter.offer_comments = [ OfferComment.new( app_kind: app_kind, body: comment[2], rating: comment[1] ) ]
        commenter.save
      end

    end
  end

  def down
    Commenter.all.destroy_all
    OfferComment.all.destroy_all
  end

end
