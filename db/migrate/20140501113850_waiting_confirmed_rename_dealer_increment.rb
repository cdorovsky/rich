class WaitingConfirmedRenameDealerIncrement < ActiveRecord::Migration
  def change
    rename_column :dealer_increments, :waiting_increment, :waiting
    rename_column :dealer_increments, :confirmed_increment, :confirmed
  end
end
