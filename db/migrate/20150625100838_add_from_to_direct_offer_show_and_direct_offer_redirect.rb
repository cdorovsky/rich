class AddFromToDirectOfferShowAndDirectOfferRedirect < ActiveRecord::Migration
  def change
    add_column :direct_offer_shows, :from, :string
    add_column :direct_offer_redirects, :from, :string
  end
end
