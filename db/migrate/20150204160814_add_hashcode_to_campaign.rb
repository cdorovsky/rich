class AddHashcodeToCampaign < ActiveRecord::Migration
  def change
    add_column :campaigns, :hash_code, :string
  end
end
