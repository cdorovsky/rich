class AddTriggeredAtTimestampToPostbacksAndConversions < ActiveRecord::Migration
  def change
    add_column :postbacks,   :triggered_at_timestamp, :integer
    add_column :conversions, :triggered_at_timestamp, :integer
  end
end
