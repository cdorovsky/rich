class CreateDealerEmails < ActiveRecord::Migration
  def change
    create_table :dealer_emails do |t|
      t.string :name, default: ''
      t.text :dealer_ids, default: '[]'
      t.string :subject, null: false
      t.text :body, null: false
      t.string :message_type

      t.timestamps
    end
  end
end
