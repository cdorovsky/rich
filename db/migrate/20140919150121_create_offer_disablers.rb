class CreateOfferDisablers < ActiveRecord::Migration
  def change
    create_table :offer_disablers do |t|
      t.integer  :offer_id
      t.boolean  :offer_disabled,            default: false
      t.datetime :disabling_time
      t.boolean  :plan_notification_sent,    default: false
      t.boolean  :instant_notification_sent, default: false
      t.timestamps
    end
  end
end
