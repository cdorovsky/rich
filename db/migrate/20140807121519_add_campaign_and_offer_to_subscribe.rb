class AddCampaignAndOfferToSubscribe < ActiveRecord::Migration
  def change
    add_column :subscribes, :campaign_id, :integer
    add_column :subscribes, :offer_id, :integer
  end
end
