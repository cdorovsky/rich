class RemovePostbackIdFromConversions < ActiveRecord::Migration
  def change
    remove_column :conversions, :postback_id
  end
end
