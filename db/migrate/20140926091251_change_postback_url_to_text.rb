class ChangePostbackUrlToText < ActiveRecord::Migration
  def change
    change_column :campaigns, :postback_url, :text
  end
end
