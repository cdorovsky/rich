class AddIndexToPaymentRequest < ActiveRecord::Migration
  def change
    add_index :payment_requests, [ :confirmed, :payment_type, :dealer_id ], name: 'for_dealer_profits'
  end
end
