class AddDropColumnsInDealerStats < ActiveRecord::Migration
  def change

    add_column :dealer_stats, :conversion_subscribed,   :integer, default: 0
    add_column :dealer_stats, :conversion_unsubscribed, :integer, default: 0
    add_column :dealer_stats, :conversion_rebill,       :integer, default: 0
    add_column :dealer_stats, :money_from_rebill,       :float,   default: 0.0
    add_column :dealer_stats, :money_from_buyout,       :float,   default: 0.0

    remove_column :dealer_stats, :money_apps_advert
    remove_column :dealer_stats, :money_cpa

  end
end
