class ChangeAustraliaToCampaign < ActiveRecord::Migration
  def change
    change_column :campaigns, :australia, :boolean, default: false
  end
end
