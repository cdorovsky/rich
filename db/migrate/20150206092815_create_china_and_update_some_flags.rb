class CreateChinaAndUpdateSomeFlags < ActiveRecord::Migration
  def change
    Country.find_or_create_by(name: 'Croatia').update(abbr: 'HR', image_url: './assets/flags/hr.png')
    Country.find_or_create_by(name: 'Sao Tome Principe').update(abbr: 'ST', image_url: './assets/flags/st.png')
    Country.find_or_create_by(id: 261, name: 'China', abbr: 'CN', image_url: './assets/flags/cn.png')
  end
end
