class ChangeDefaultActiveAtSubscribes < ActiveRecord::Migration
  def change
    change_column :subscribes, :active, :boolean, default: true
  end
end
