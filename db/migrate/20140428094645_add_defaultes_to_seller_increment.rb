class AddDefaultesToSellerIncrement < ActiveRecord::Migration
  def change
    change_column :seller_increments, :waiting, :float, default: 0
    change_column :seller_increments, :confirmed, :float, default: 0 
    change_column :seller_increments, :wapclick_view, :integer, default: 0
    change_column :seller_increments, :wapclick_redirect, :integer, default: 0
    change_column :seller_increments, :wapclick_subscribe, :integer, default: 0
    change_column :seller_increments, :wapclick_rebill, :integer, default: 0
    change_column :seller_increments, :wapclick_unsubscribe, :integer, default: 0
  end
end
