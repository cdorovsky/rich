class AddRegularTypeToDealerStat < ActiveRecord::Migration
  def change
    add_column :dealer_stats, :hourly,  :boolean
    add_column :dealer_stats, :daily,   :boolean
    add_column :dealer_stats, :monthly, :boolean
    add_column :dealer_stats, :day,     :integer
    add_column :dealer_stats, :month,   :integer
  end
end
