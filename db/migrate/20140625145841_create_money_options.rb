class CreateMoneyOptions < ActiveRecord::Migration
  def change
    create_table :money_options do |t|
      t.integer  :dealer_id
      t.integer  :wallet_id
      t.string   :pay_type, default: 'time' 
      t.string   :pay_timing, default: 'weekly'
      t.integer  :pay_day, default: 1
      t.integer  :pay_value, default: 1000
      t.timestamps
    end
  end
end
