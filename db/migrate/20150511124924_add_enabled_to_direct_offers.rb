class AddEnabledToDirectOffers < ActiveRecord::Migration
  def change
    add_column :direct_offers, :enabled, :boolean, default: true
  end
end
