class CreateOfferSetShows < ActiveRecord::Migration
  def change
    create_table :offer_set_shows do |t|
      t.string :mongo_id 
      t.string :referer
      t.text   :sub_id
      t.text   :sub_id2
      t.text   :sub_id3
      t.integer :dealer_id
      t.integer :campaign_id
      t.integer :offer_id
      t.integer :offer_set_id
      t.integer :country_id
      t.string  :country_name
      t.string  :country_code
      t.inet    :ip
      t.text    :user_agent
      t.string  :user_platform
      t.string  :user_device

      t.datetime :triggered_at
      t.timestamps
    end
  end
end
