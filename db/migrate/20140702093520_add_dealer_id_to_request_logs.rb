class AddDealerIdToRequestLogs < ActiveRecord::Migration
  def change
    add_column :request_logs, :dealer_id, :integer
  end
end
