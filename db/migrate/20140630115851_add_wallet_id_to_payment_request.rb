class AddWalletIdToPaymentRequest < ActiveRecord::Migration
  def change
    add_column :payment_requests, :wallet_id, :integer
  end
end
