class AddRemoteIpToRequestLogs < ActiveRecord::Migration
  def change
    add_column :request_logs, :remote_ip, :string
  end
end
