class AddReferrerIdToDealerIncrement < ActiveRecord::Migration
  def change
    add_column :dealer_increments, :referrer_id, :integer
  end
end
