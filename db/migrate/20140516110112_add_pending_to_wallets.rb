class AddPendingToWallets < ActiveRecord::Migration
  def change
    add_column :wallets, :pending, :boolean, default: false
  end
end
