class CreateLandingVisitors < ActiveRecord::Migration
  def change
    create_table :landing_visitors do |t|
      t.integer :offer_set_id
      t.integer :landing_subscriber_id

      t.timestamps
    end
  end
end
