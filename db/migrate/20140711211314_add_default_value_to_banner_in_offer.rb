class AddDefaultValueToBannerInOffer < ActiveRecord::Migration
  def change
    change_column :offers, :banner, :string, default: 'http://res.cloudinary.com/hm54rrv18/image/upload/v1405113114/300x300-placeholder_shgsxe.png'
  end
end
