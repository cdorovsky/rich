class AddPaymentRequestIdToPayments < ActiveRecord::Migration
  def change
    add_column :payments, :payment_request_id, :integer
  end
end
