class AddDealerIdToNotification < ActiveRecord::Migration
  def change
    add_column :notifications, :dealer_id, :integer
  end
end
