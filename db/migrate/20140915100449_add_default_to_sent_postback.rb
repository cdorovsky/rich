class AddDefaultToSentPostback < ActiveRecord::Migration
  def change
    change_column :campaigns, :sent_postback, :boolean, default: :false
  end
end
