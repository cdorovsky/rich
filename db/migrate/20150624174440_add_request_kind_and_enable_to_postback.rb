class AddRequestKindAndEnableToPostback < ActiveRecord::Migration
  def change
    add_column :postbacks, :postback_enabled, :boolean, default: false
    add_column :postbacks, :request_kind, :string, default: 'GET'
  end
end
