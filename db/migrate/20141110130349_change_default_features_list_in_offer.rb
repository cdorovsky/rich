class ChangeDefaultFeaturesListInOffer < ActiveRecord::Migration
  def change
    change_column :offers, :features_list, :text, default: nil
  end
end
