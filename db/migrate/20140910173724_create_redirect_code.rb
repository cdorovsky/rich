class CreateRedirectCode < ActiveRecord::Migration
  def change
    create_table :redirect_codes do |t|
      t.string   :code
      t.string   :code_kind
      t.integer  :campaign_id
      t.integer  :offer_set_id
      t.timestamps
    end
  end
end
