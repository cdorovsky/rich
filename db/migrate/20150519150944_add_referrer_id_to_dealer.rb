class AddReferrerIdToDealer < ActiveRecord::Migration
  def change
    add_column :dealers, :referrer_id, :integer
  end
end
