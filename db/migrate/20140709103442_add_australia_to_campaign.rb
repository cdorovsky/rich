class AddAustraliaToCampaign < ActiveRecord::Migration
  def change
    add_column :campaigns, :australia, :boolean
  end
end
