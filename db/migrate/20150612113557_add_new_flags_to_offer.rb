class AddNewFlagsToOffer < ActiveRecord::Migration
  def change
    add_column :offers, :featured, :boolean, default: false
    add_column :offers, :auto_approve, :boolean, default: false
  end
end
