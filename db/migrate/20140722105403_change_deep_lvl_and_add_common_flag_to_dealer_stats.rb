class ChangeDeepLvlAndAddCommonFlagToDealerStats < ActiveRecord::Migration
  def change
    rename_column :dealer_stats, :default_deep_lvl,  :no_grouping
    rename_column :dealer_stats, :campaign_deep_lvl, :campaign_grouping
    rename_column :dealer_stats, :offer_deep_lvl,    :offer_grouping
  end
end
