class CreateTariffs < ActiveRecord::Migration
  def change
    unless ActiveRecord::Base.connection.table_exists? :tariffs
      create_table :tariffs do |t|
        t.string :name
        t.float  :dealer_percent
        t.timestamps
      end
    end
  end
end
