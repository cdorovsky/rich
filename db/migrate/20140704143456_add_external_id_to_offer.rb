class AddExternalIdToOffer < ActiveRecord::Migration
  def change
    add_column :offers, :external_id, :integer
  end
end
