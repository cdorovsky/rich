class RemoveFieldsFromOffer < ActiveRecord::Migration
  def change
    remove_column :offers, :russia, :string
    remove_column :offers, :europe, :string
    remove_column :offers, :asia, :string
    remove_column :offers, :america, :string
  end
end
