class ChangeBrokenOfferBehaviorinCampaign < ActiveRecord::Migration
  def change
    change_column :campaigns, :broken_offer_behavior, :string, default: 'traffic_back'
  end
end
