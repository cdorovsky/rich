class CreateRedirectSettings < ActiveRecord::Migration
  def change
    create_table :redirect_settings do |t|
      t.integer :campaign_id

      %w( 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 ).each do |hr|
        t.boolean hr, default: false
      end

      %w( monday tuesday wednesday thursday friday saturday sunday ).each do |day|
        t.boolean day, default: false
      end

      t.timestamps
    end
  end
end
