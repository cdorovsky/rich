class ChangeNewsFaqsColumnTypesToText < ActiveRecord::Migration
  def change
    change_column :news, :content, :text
    change_column :faqs, :answer, :text
  end
end
