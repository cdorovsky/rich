class DirectOfferRedirectAllText < ActiveRecord::Migration
  def change
    change_column :direct_offer_redirects, :request_id, :text
    change_column :direct_offer_redirects, :referer, :text
    change_column :direct_offer_redirects, :country_name, :text
    change_column :direct_offer_redirects, :country_code, :text
    change_column :direct_offer_redirects, :user_platform, :text
    change_column :direct_offer_redirects, :user_device, :text
    change_column :direct_offer_redirects, :timestamp, :text
    change_column :direct_offer_redirects, :from, :text
  end
end
