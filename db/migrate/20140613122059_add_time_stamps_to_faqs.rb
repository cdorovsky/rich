class AddTimeStampsToFaqs < ActiveRecord::Migration
  def change
    add_column :faqs, :created_at, :datetime
    add_column :faqs, :updated_at, :datetime
  end
end
