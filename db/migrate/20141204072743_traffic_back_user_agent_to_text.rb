class TrafficBackUserAgentToText < ActiveRecord::Migration
  def change
    change_column :traffic_backs, :user_agent, :text
  end
end
