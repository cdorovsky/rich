class AddDefaultsToDealerHourStats < ActiveRecord::Migration
  def change

    change_column :dealer_hour_stats, :visitor_counter,      :integer, default: 0
    change_column :dealer_hour_stats, :uniq_visitor_counter, :integer, default: 0
    change_column :dealer_hour_stats, :traffic_back_counter, :integer, default: 0

    change_column :dealer_hour_stats, :money_total,     :float, default: 0
    change_column :dealer_hour_stats, :money_waiting,   :float, default: 0
    change_column :dealer_hour_stats, :money_confirmed, :float, default: 0
    change_column :dealer_hour_stats, :money_declined,  :float, default: 0

    change_column :dealer_hour_stats, :money_wapclick, :float, default: 0
    change_column :dealer_hour_stats, :money_apps,     :float, default: 0
    change_column :dealer_hour_stats, :money_cpa,      :float, default: 0

  end
end
