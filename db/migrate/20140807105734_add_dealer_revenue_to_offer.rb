class AddDealerRevenueToOffer < ActiveRecord::Migration
  def change
    add_column :offers, :dealer_revenue, :float, default: 0
    change_column :offers, :tariff, :float, default: 0
  end
end
