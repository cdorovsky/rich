class AddCommonFlagToDealerStats < ActiveRecord::Migration
  def change
    add_column :dealer_stats, :common_flag, :boolean, default: false
  end
end
