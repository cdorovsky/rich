class ChangeSubIdToSubscribeIdInRebills < ActiveRecord::Migration
  def change
    rename_column :rebills, :sub_id, :subscribe_id
  end
end
