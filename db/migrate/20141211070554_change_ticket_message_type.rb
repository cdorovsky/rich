class ChangeTicketMessageType < ActiveRecord::Migration
  def change
    change_column :tickets, :message, :text
  end
end
