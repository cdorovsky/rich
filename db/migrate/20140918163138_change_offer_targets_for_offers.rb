class ChangeOfferTargetsForOffers < ActiveRecord::Migration
  def up
    Offer.where( kind: %W( wapclick apps_advert ) ).each { |o| o.offer_targets.destroy_all }

    Offer.where( kind: 'wapclick' ).each do | offer |
      offer.offer_targets.create(
        name: 'Ребилл', 
        price: ( offer.tariff * 0.8 ).round(2),
        description: ''
      )
    end

    Offer.where( kind: 'apps_advert' ).each do | offer |
      offer.offer_targets.create(
        name: 'Установка приложения', 
        price: ( offer.tariff * 0.8 ).round(2),
        description: ''
      )
    end
  end

  def down
    Offer.where( kind: %W( wapclick apps_advert ) ).each { |o| o.offer_targets.destroy_all }
  end
end
