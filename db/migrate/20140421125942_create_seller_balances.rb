class CreateSellerBalances < ActiveRecord::Migration
  def change
    create_table :seller_balances do |t|
      t.integer :seller_id
      t.float :waiting
      t.float :confirmed      
      t.timestamps
    end
  end
end
