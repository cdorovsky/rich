class RemoveOnlypayViewsFromEverywhere < ActiveRecord::Migration
  def change
    remove_column :seller_balances, :onlypay_view
    remove_column :seller_increments, :onlypay_view
    remove_column :dealer_balances, :onlypay_view
    remove_column :dealer_increments, :onlypay_view
 end
end
