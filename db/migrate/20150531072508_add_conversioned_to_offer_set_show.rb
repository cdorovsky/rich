class AddConversionedToOfferSetShow < ActiveRecord::Migration
  def change
    add_column :offer_set_shows, :converted, :boolean, default: false
  end
end
