class AddMongoIdToPostbacks < ActiveRecord::Migration
  def change
    add_column :postbacks, :mongo_id, :string
  end
end
