class AddTypeToRequestLogs < ActiveRecord::Migration
  def change
    add_column :request_logs, :request_type, :string
  end
end
