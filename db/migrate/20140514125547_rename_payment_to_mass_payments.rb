class RenamePaymentToMassPayments < ActiveRecord::Migration
  def change
    rename_table :payments, :mass_payments
  end
end
