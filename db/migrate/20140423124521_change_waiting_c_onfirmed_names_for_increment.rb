class ChangeWaitingCOnfirmedNamesForIncrement < ActiveRecord::Migration
  def change
    rename_column :seller_increments, :waiting_increment, :waiting
    rename_column :seller_increments, :confirmed_increment, :confirmed
    rename_column :seller_increments, :wapclick_unsubcribe, :wapclick_unsubscribe
  end
end
