class PopulateLandingHeadings < ActiveRecord::Migration
  def up
    LandingHeading.create(
      is_default: true,
      app_kind: 'application',
      body: ':collision::ok_hand: Приложение недели. :iphone: Временно бесплатно в честь промо-акции. Не упусти возможность! :sunglasses::+1:
             <small>(установи сейчас и не потеряй <strong>99.00 рублей</strong> когда приложение вновь станет платным)</small>'
    )
    LandingHeading.create(
      is_default: true,
      app_kind: 'game',
      body: ':loudspeaker::stuck_out_tongue_closed_eyes: Хочешь ураган эмоций? :rocket: Установи эту хитовую, увлекательную игру и получи их! :bomb::point_down:
             <small>(сделай это сейчас и не потеряй <strong>99.00 рублей</strong>)</small>'
    )
  end
  def down
    LandingHeading.where( is_default: true ).delete_all
  end
end
