class CreateBuyerSessions < ActiveRecord::Migration
  def change
    create_table :buyer_sessions do |t|
      t.string   :session_key
      t.datetime :outdate_moment
      t.timestamps
    end
  end
end
