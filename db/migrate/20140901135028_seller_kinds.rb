class SellerKinds < ActiveRecord::Migration
  def change
    Seller.where( 
      provider_system_name: %W( performance appflood matomy ) 
    ).update_all( kind: :apps_advert )
    Seller.where(
      provider_system_name: %W( onlypay shemrock billing8 plasticmedia )
    ).update_all( kind: :wapclick )
  end
end
