class AddSellerRevenueToDailyAggregate < ActiveRecord::Migration
  def change
    add_column :daily_aggregates, :seller_revenue, :float, default: 0.00
  end
end
