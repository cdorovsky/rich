class CreateSubscribes < ActiveRecord::Migration
  def change
    create_table :subscribes do |t|
      t.integer :dealer_id
      t.string :phone
      t.integer :rebill_count
      t.boolean :active
      t.integer :sub_id
      t.string :country
      t.string :operator
      t.string :platform
      t.string :billing
      t.string :country_code
      t.timestamps
    end
  end
end
