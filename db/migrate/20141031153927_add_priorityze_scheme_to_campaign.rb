class AddPriorityzeSchemeToCampaign < ActiveRecord::Migration
  def change
    add_column :campaigns, :offer_scheme, :string, null: false, default: 'cost'
  end
end
