class RenameSubscriptionStatusToActionSellerRequest < ActiveRecord::Migration
  def change
    rename_column :seller_requests, :subscription_status, :subscription_action
  end
end
