class AddIndexToOfferSet < ActiveRecord::Migration
  def change
    add_index :offer_sets, [ :campaign_id, :offer_id ], name: 'for_quick_relations'
  end
end
