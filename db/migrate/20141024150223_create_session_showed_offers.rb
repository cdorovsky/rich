class CreateSessionShowedOffers < ActiveRecord::Migration
  def change
    create_table :session_showed_offers do |t|
      t.integer :buyer_session_id, null: false
      t.integer :campaign_id,      null: false
      t.integer :dealer_id,        null: false
      t.integer :offer_id,         null: false
      t.integer :show_counter,     null: false, default: 0
      t.timestamps
    end
  end
end
