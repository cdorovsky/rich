class AddDisablingTimeToOffer < ActiveRecord::Migration
  def change
    add_column :offers, :disabling_time, :datetime
  end
end
