class CreateRestrictedOfferCountrySets < ActiveRecord::Migration
  def change
    create_table :restricted_offer_country_sets do |t|
      t.integer :offer_id
      t.integer :country_id
    end
  end
end
