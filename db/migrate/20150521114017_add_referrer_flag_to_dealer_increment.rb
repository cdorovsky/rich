class AddReferrerFlagToDealerIncrement < ActiveRecord::Migration
  def change
    add_column :dealer_increments, :referrer, :boolean, default: false
  end
end
