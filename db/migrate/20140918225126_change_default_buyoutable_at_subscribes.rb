class ChangeDefaultBuyoutableAtSubscribes < ActiveRecord::Migration
  def change
    change_column :subscribes, :buyoutable, :boolean, default: true
  end
end
