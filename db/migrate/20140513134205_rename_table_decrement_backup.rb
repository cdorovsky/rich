class RenameTableDecrementBackup < ActiveRecord::Migration
  def change
    rename_table :decrement_backups, :payment_backups
  end
end
