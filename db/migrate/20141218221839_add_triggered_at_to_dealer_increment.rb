class AddTriggeredAtToDealerIncrement < ActiveRecord::Migration
  def change
    add_column :dealer_increments, :triggered_at, :datetime
  end
end
