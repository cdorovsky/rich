class AddBuyoutableToSubscribes < ActiveRecord::Migration
  def change
    add_column :subscribes, :buyoutable, :boolean, default: true
  end
end
