class CreateWallets < ActiveRecord::Migration
  def change
    create_table :wallets do |t|
      t.integer :dealer_id
      t.string :type
      t.string :prefix
      t.string :number

      t.timestamps
    end
  end
end
