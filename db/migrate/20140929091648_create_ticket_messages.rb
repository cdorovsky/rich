class CreateTicketMessages < ActiveRecord::Migration
  def change
    create_table :ticket_messages do |t|
      t.integer   :dealer_id
      t.integer   :ticket_id
      t.text      :message_text
      t.string    :ownership
      t.integer   :number
      t.timestamps
    end
  end
end
