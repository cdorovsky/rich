class AddTariffIdToDealer < ActiveRecord::Migration
  def change
    add_column :dealers, :tariff_id, :integer
  end
end
