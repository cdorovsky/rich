class AddTriggeredToAdminRequest < ActiveRecord::Migration
  def change
    add_column :admin_requests, :triggered, :boolean
  end
end
