class AddReferralCodeToDealers < ActiveRecord::Migration
  def change
    add_column :dealers, :referral_code, :string
  end
end
