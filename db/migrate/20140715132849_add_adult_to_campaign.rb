class AddAdultToCampaign < ActiveRecord::Migration
  def change
    add_column :campaigns, :adult, :boolean, default: false
  end
end
