class AddDisabledFieldToOffer < ActiveRecord::Migration
  def change
    add_column :offers, :disabled, :boolean, default: false
  end
end
