class CreateOfferComments < ActiveRecord::Migration
  def change
    create_table :offer_comments do |t|
      t.text :body, default: ''
      t.integer :rating, default: 5
      t.integer :commenter_id

      t.timestamps
    end
  end
end
