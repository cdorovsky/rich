class AddDefaultValueToOperatorNameInOffer < ActiveRecord::Migration
  def change
    change_column :offers, :operator_name, :string, default: ''
  end
end
