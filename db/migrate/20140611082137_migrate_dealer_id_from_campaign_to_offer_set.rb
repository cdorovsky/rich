class MigrateDealerIdFromCampaignToOfferSet < ActiveRecord::Migration
  def change
    unless Campaign.count.zero?
      Campaign.all.each do | c |
        c.offer_sets.update_all( dealer_id: c.dealer_id )
      end
    end
  end
end
