class AddConversionsToDealerStats < ActiveRecord::Migration
  def change
    add_column :dealer_stats, :conversion_total, :integer, default: 0
    add_column :dealer_stats, :conversion_confirmed, :integer, default: 0
    add_column :dealer_stats, :conversion_waiting, :integer, default: 0
    add_column :dealer_stats, :conversion_ined, :integer, default: 0
  end
end
