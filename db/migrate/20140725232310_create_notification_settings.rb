class CreateNotificationSettings < ActiveRecord::Migration
  def change
    create_table :notification_settings do |t|
      t.integer :dealer_id
      t.integer :tickets_cd, default: 2
      t.integer :program_cd, default: 2
      t.boolean :news, default: false
      t.boolean :payout, default: false
      t.boolean :urgent_tickets, default: false
      t.boolean :my_campaigns, default: false
      t.boolean :new_offers, default: false
      t.boolean :balance, default: false

      t.timestamps
    end
  end
end
