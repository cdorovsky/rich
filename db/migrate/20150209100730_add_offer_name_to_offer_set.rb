class AddOfferNameToOfferSet < ActiveRecord::Migration
  def change
    add_column :offer_sets, :offer_name, :string
  end
end
