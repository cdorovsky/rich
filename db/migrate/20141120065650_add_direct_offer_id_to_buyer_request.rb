class AddDirectOfferIdToBuyerRequest < ActiveRecord::Migration
  def change
    remove_column :buyer_requests, :direct_offer_id
    add_column :buyer_requests, :direct_offer_id, :integer
  end
end
