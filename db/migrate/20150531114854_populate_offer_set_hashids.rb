class PopulateOfferSetHashids < ActiveRecord::Migration
  def change
    offer_set_hashid = Hashids.new 'some salt for offer set', 8
    OfferSet.where( hashid: nil ).find_each do | os |
      hashid = offer_set_hashid.encode( os.campaign_id, os.offer_id )
      os.update_column( :hashid, hashid )
    end
  end
end
