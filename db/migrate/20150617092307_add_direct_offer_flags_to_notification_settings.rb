class AddDirectOfferFlagsToNotificationSettings < ActiveRecord::Migration
  def change
    add_column :notification_settings, :direct_offer_email, :boolean, default: true
    add_column :notification_settings, :direct_offer_sms, :boolean, default: true
  end
end
