class ChangePostbackUrlInPostbacks < ActiveRecord::Migration
  def change
    change_column :postbacks, :postback_url, :text
  end
end
