class ChangeStats < ActiveRecord::Migration
  def change
    rename_column :dealer_stats, 'subscribe_wapclick',   'subscribe_counter'
    rename_column :dealer_stats, 'rebill_wapclick',      'rebill_counter'
    rename_column :dealer_stats, 'unsubscribe_wapclick', 'unsubscribe_counter'
  end
end
