class AddCamoaignAndOfferToRebill < ActiveRecord::Migration
  def change
    add_column :rebills, :campaign_id, :integer
    add_column :rebills, :offer_id, :integer
  end
end
