class RenameColumnConversionSubscribedToConversinoSubscribeInDealerStat < ActiveRecord::Migration
  def change
    rename_column :dealer_stats, :conversion_subscribed, :conversion_subscribe
  end
end
