class RemoveDealerProfitsIndexFromPaymentRequest < ActiveRecord::Migration
  def change
  	remove_index :payment_requests, name: :for_dealer_profits
  end
end
