class AddRegionsToCampaign < ActiveRecord::Migration
  def change
    add_column :campaigns, :russia, :boolean, default: false
    add_column :campaigns, :europe, :boolean, default: false
    add_column :campaigns, :asia, :boolean, default: false
    add_column :campaigns, :america, :boolean, default: false
  end
end
