class AddFullCostToRequestLogs < ActiveRecord::Migration
  def change
    add_column :request_logs, :full_cost, :float, default: 0
  end
end
