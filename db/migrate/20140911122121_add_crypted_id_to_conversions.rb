class AddCryptedIdToConversions < ActiveRecord::Migration
  def change
    add_column :conversions, :crypted_id, :string
  end
end
