class AddIndexToBuyerRequest < ActiveRecord::Migration
  def change
    add_index :buyer_requests, [ :buyer_session_id, :offer_id, :request_kind ], name: 'buyer_request_index'
  end
end
