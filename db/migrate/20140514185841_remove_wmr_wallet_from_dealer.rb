class RemoveWmrWalletFromDealer < ActiveRecord::Migration
  def change
    remove_column :dealers, :wmr_wallet
  end
end
