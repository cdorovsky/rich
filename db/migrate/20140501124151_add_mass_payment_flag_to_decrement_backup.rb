class AddMassPaymentFlagToDecrementBackup < ActiveRecord::Migration
  def change
    add_column :decrement_backups, :mass_payment, :boolean
  end
end
