class CreateTbReferrers < ActiveRecord::Migration
  def change
    create_table :tb_referrers do |t|
      t.integer :dealer_id
      t.integer :campaign_id
      t.string  :referrer
      t.timestamps
    end
  end
end
