class AddWapclickReadyToCampaign < ActiveRecord::Migration
  def change
    add_column :campaigns, :wapclick_ready, :boolean, default: false
  end
end
