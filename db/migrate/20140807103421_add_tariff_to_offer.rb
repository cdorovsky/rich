class AddTariffToOffer < ActiveRecord::Migration
  def change
    add_column :offers, :tariff, :float
  end
end
