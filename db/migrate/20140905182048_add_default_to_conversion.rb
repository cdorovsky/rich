class AddDefaultToConversion < ActiveRecord::Migration
  def change
    change_column :conversions, :source_url, :string, default: ''
  end
end
