class AddIndexToDailyAggregate < ActiveRecord::Migration
  def change
    add_index :daily_aggregates, [ :dealer_id, :campaign_id, :offer_id ], name: 'for_search_hash'
    add_index :daily_aggregates, :short_date
  end
end
