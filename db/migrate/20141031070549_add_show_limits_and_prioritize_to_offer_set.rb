class AddShowLimitsAndPrioritizeToOfferSet < ActiveRecord::Migration
  def change
    add_column :offer_sets, :show_limit, :integer, default: 10
    add_column :offer_sets, :priority, :integer, default: 1
  end
end
