class TriggeredAtIntegerDirectOfferShowRedirect < ActiveRecord::Migration
  def change
    remove_column :direct_offer_shows,    :triggered_at, :datetime
    remove_column :direct_offer_redirects, :triggered_at, :datetime
    add_column :direct_offer_shows, :triggered_at, :integer
    add_column :direct_offer_redirects, :triggered_at, :integer
  end
end
