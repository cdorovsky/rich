class AddReferrerRateToDealer < ActiveRecord::Migration
  def change
    add_column :dealers, :referrer_rate, :float, default: '0.05'
  end
end
