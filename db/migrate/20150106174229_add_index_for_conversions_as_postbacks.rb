class AddIndexForConversionsAsPostbacks < ActiveRecord::Migration
  def change
    add_index :conversions, [ :dealer_id, :campaign_id, :offer_id,
                              :triggered_at_timestamp, :country_id,
                              :sub_id, :sub_id2, :sub_id3 ], name: 'for_postbacks'

  end
end
