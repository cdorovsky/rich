class AddBlockedToDealer < ActiveRecord::Migration
  def change
    add_column :dealers, :blocked, :boolean, default: false
  end
end
