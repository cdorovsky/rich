class CreateRequestLogs < ActiveRecord::Migration
  def change
    create_table :request_logs do |t|
      t.integer  :campaign_id
      t.integer  :offer_id
      t.string   :transaction_id
      t.string   :datetime
      t.string   :currency
      t.float    :payout
      t.string   :device_id
      t.text     :original_url
      t.timestamps
    end
  end
end
