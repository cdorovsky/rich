class RemoveRedirectUrlNullFalseFromBuyerRequest < ActiveRecord::Migration
  def change
    change_column :buyer_requests, :redirect_url, :text, null: true
  end
end
