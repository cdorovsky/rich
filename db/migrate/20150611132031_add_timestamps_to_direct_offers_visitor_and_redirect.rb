class AddTimestampsToDirectOffersVisitorAndRedirect < ActiveRecord::Migration
  def change
    add_column :direct_offer_shows, :timestamp, :string
    add_column :direct_offer_redirects, :timestamp, :string
  end
end
