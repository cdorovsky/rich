class Adddefaultstodealerstat < ActiveRecord::Migration
  def change
    change_column :dealer_stats, :download_counter, :integer, default: 0
  end
end
