class AddMoreFieldsToOffer < ActiveRecord::Migration
  def change
    add_column :offers, :rating, :integer, default: 5
    add_column :offers, :votes_count, :integer, default: 0
    add_column :offers, :downloads_count, :integer, default: 0
    add_column :offers, :weblanding_ready, :boolean, default: false
    add_column :offers, :app_kind, :string, default: 'application'
    add_column :offers, :features_list, :text, default: ''
    add_column :offers, :qr_link, :string, default: ''
  end
end
