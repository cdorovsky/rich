class SetDefaultsToOfferSet < ActiveRecord::Migration
  def change
    change_column :country_sets, :russia, :boolean, default: false
    change_column :country_sets, :europe, :boolean, default: false
    change_column :country_sets, :asia, :boolean, default: false
    change_column :country_sets, :america, :boolean, default: false
  end
end
