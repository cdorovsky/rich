class BalanceIntToFloat < ActiveRecord::Migration
  def change
    change_column :balances, :waiting,  :float
    change_column :balances, :confirmed, :float
    change_column :rebills, :full_cost, :float
  end
end
