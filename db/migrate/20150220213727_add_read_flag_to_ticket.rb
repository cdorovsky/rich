class AddReadFlagToTicket < ActiveRecord::Migration
  def change
    add_column :tickets, :read, :boolean, default: true
  end
end
