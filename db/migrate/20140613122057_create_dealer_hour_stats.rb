class CreateDealerHourStats < ActiveRecord::Migration
  def change
    create_table :dealer_hour_stats do |t|

      t.integer :dealer_id
      t.integer :campaign_id
      t.integer :hour

      t.integer :visitor_counter
      t.integer :uniq_visitor_counter
      t.integer :traffic_back_counter

      t.float :money_total
      t.float :money_waiting
      t.float :money_confirmed
      t.float :money_declined

      t.float :money_wapclick
      t.float :money_apps
      t.float :money_cpa

      t.timestamps
    end
  end
end
