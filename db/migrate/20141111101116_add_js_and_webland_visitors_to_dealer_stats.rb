class AddJsAndWeblandVisitorsToDealerStats < ActiveRecord::Migration
  def change
    add_column :dealer_stats, :js_visitor,           :integer, default: 0
    add_column :dealer_stats, :uniq_js_visitor,      :integer, default: 0
    add_column :dealer_stats, :webland_visitor,      :integer, default: 0
    add_column :dealer_stats, :uniq_webland_visitor, :integer, default: 0
  end
end
