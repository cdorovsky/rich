class AddFieldsToOffer < ActiveRecord::Migration
  def change
    
    add_column :offers, :description, :text, defautl: ''

    %i( weekly_cr today_cr yesteray_cr weekly_epc today_epc yesteday_epc ).each do | float_col |
      add_column :offers, float_col, :float, default: 0.0
    end

    %i( websites dorways context brand_context teaser target social email cashback clickpopunder brokers ).each do | bool_col |
      add_column :offers, bool_col, :boolean, default: false
    end

  end
end
