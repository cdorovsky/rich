class AddFlagsToCampaign < ActiveRecord::Migration
  def change
    add_column :campaigns, :megafon_buyout, :boolean, default: true
    add_column :campaigns, :beeline_buyout, :boolean, default: true
    add_column :campaigns, :mts_buyout, :boolean, default: false
  end
end
