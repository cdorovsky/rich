class AddIndexToDealerIdInConversions < ActiveRecord::Migration
  def change
    add_index( :conversions, :dealer_id )
  end
end
