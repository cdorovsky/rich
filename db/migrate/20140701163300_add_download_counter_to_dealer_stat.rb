class AddDownloadCounterToDealerStat < ActiveRecord::Migration
  def change
    add_column :dealer_stats, :download_counter, :integer
  end
end
