class ChangeMoneyAppsToMoneyAppsAdvertInDealerStats < ActiveRecord::Migration
  def change
    rename_column :dealer_stats, :money_apps, :money_apps_advert
  end
end
