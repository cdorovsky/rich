class AddIndexToAppInstallation < ActiveRecord::Migration
  def change
    add_index :app_installations, :transaction_id, unique: true
  end
end
