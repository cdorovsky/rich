class AddIndexToConversions < ActiveRecord::Migration
  def change
    add_index :conversions, :crypted_id, unique: true
  end
end
