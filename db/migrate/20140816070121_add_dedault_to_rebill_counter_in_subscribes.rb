class AddDedaultToRebillCounterInSubscribes < ActiveRecord::Migration
  def change
    change_column :subscribes, :rebill_count, :integer, default: 0
  end
end
