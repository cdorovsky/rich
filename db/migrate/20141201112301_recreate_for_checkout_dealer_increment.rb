class RecreateForCheckoutDealerIncrement < ActiveRecord::Migration
  def change
    DealerBalance.all.update_all( for_checkout: 0 )
    DealerIncrement.where( "created_at<?", 7.days.ago.end_of_day ).find_each { |inc| inc.dealer_balance.increment!( :for_checkout, inc.confirmed ) }
  end
end
