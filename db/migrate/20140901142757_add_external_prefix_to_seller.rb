class AddExternalPrefixToSeller < ActiveRecord::Migration
  def change
    add_column :sellers, :prefix, :string
    Seller.where(
      provider_system_name: 'performance'
      ).update_all( prefix: :revenues )
    Seller.where(
      provider_system_name: 'appflood'
      ).update_all( prefix: :appflood )
  end
end
