class MdForMoldova < ActiveRecord::Migration
  def change
    moldova = Country.find_by( name: 'Moldova - Republic of' )
    if moldova
      moldova.update( abbr: 'MD', image_url: './assets/flags/md.png' )
    end
  end
end
