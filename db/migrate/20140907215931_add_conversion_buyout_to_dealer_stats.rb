class AddConversionBuyoutToDealerStats < ActiveRecord::Migration
  def change
    rename_column :dealer_stats, :conversion_buyouted,    :conversion_buyout
    add_column    :dealer_stats, :convirsion_app_install, :integer, default: 0
  end
end
