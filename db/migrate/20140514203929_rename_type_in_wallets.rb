class RenameTypeInWallets < ActiveRecord::Migration
  def change
    rename_column :wallets,  :type, :kind
  end
end
