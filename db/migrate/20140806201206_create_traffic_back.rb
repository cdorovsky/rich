class CreateTrafficBack < ActiveRecord::Migration
  def change
    create_table :traffic_backs do |t|
      t.integer :campaign_id
      t.integer :dealer_id
      t.string  :ip
      t.string  :country
      t.string  :traffic_back_url
      t.string  :request
      t.string  :user_agent
      t.string  :os
      t.string  :reason
      t.timestamps
    end
  end
end
