class AddIndexUniqTiDealerStatMd5id < ActiveRecord::Migration
  def change
    add_index :dealer_stats, :md5id, unique: true
  end
end
