class RenameCampaignOptionsToOfferSet < ActiveRecord::Migration
  def change
    rename_table :campaign_options, :offer_set
  end
end
