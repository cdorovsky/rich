class AddPaidFlagToMassPayments < ActiveRecord::Migration
  def change
    add_column :mass_payments, :paid, :boolean, default: false
  end
end
