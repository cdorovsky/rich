class RenameWaplickToWapclickInDealerStats < ActiveRecord::Migration
  def change
    rename_column :dealer_stats, :waplick, :wapclick
  end
end
