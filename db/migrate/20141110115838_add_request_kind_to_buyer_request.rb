class AddRequestKindToBuyerRequest < ActiveRecord::Migration
  def change
    add_column :buyer_requests, :request_kind, :string, null: false, default: 'html'
  end
end
