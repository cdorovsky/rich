class CreateOfferLimits < ActiveRecord::Migration
  def change
    create_table :offer_limits do |t|
      t.integer :offer_id
      t.integer :cap_time_zone_delta, default: 0
      t.integer :daily_install_limit, default: 0
      t.integer :install_count,       default: 0
      t.string  :daily_limit_status,  default: 'unlimited'
      t.timestamps
    end
  end
end
