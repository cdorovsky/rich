class AddDefaultFalseToRegularTypeInDealerStats < ActiveRecord::Migration
  def change
    change_column :dealer_stats, :hourly, :boolean, default: false
    change_column :dealer_stats, :daily, :boolean, default: false
    change_column :dealer_stats, :monthly, :boolean, default: false
  end
end
