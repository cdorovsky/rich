class AddWaitingAndConfirmedToBalance < ActiveRecord::Migration
  def change
    remove_column :balances, :waiting_balance
    remove_column :balances, :confirmed_balance
    add_column :balances, :waiting, :bigint
    add_column :balances, :confirmed, :bigint
  end
end
