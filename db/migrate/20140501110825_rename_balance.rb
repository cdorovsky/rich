class RenameBalance < ActiveRecord::Migration
  def change
    rename_table :balances, :dealer_balances
  end
end
