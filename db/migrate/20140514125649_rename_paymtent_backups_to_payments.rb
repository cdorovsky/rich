class RenamePaymtentBackupsToPayments < ActiveRecord::Migration
  def change
    rename_table :payment_backups, :payments
  end
end
