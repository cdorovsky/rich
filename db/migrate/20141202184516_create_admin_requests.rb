class CreateAdminRequests < ActiveRecord::Migration
  def change
    create_table :admin_requests do |t|
      t.integer :dealer_id
      t.float   :value
      t.timestamps
    end
  end
end
