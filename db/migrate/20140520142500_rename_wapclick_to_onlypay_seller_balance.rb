class RenameWapclickToOnlypaySellerBalance < ActiveRecord::Migration
  def change
    rename_column :seller_balances, :wapclick_view, :onlypay_view
    rename_column :seller_balances, :wapclick_redirect, :onlypay_redirect
    rename_column :seller_balances, :wapclick_subscribe, :onlypay_subscribe
    rename_column :seller_balances, :wapclick_rebill, :onlypay_rebill
    rename_column :seller_balances, :wapclick_unsubscribe, :onlypay_unsubscribe
  end
end
