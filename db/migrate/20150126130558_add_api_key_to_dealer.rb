class AddApiKeyToDealer < ActiveRecord::Migration
  def change
    add_column :dealers, :api_key, :string, default: '', unique: true, null: false
  end
end
