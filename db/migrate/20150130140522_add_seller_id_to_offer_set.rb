class AddSellerIdToOfferSet < ActiveRecord::Migration
  def change
    add_column :offer_sets, :seller_id, :integer
  end
end
