class AddTriggeredAtToDealerStat < ActiveRecord::Migration
  def change
    add_column :dealer_stats, :triggered_at, :datetime
  end
end
