class AddOfferKindToOfferSet < ActiveRecord::Migration
  def change
    add_column :offer_sets, :offer_kind, :string, default: ''
  end
end
