class DropDayMonthAndRenameHourToCurrentInDealerStats < ActiveRecord::Migration
  def change
    remove_column :dealer_stats, :day
    remove_column :dealer_stats, :month

    rename_column :dealer_stats, :hour, :current

    rename_column :dealer_stats, :money_common, :money_total
    remove_column :dealer_stats, :money
  end
end
