class AddRestrictedCountryListToOffer < ActiveRecord::Migration
  def change
    add_column :offers, :restricted_country_list, :text
  end
end
