class AddIndexToConversionMongoId < ActiveRecord::Migration
  def change
    add_index :conversions, :mongo_id
  end
end
