class RemoveIndexForDealerProfits < ActiveRecord::Migration
  def change
  	remove_index :dealer_stats, name: :for_dealer_profit
  end
end
