class AddCountryListToOffer < ActiveRecord::Migration
  def change
    add_column :offers, :country_list, :text
  end
end
