class AddLendingTypeToCampaignOption < ActiveRecord::Migration
  def self.up
    add_column :campaign_options, :lending_type_cd, :integer
  end

  def self.down
    remove_column :campaign_options, :lending_type_cd
  end
end