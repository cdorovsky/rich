class AddDefaultValueToIconInOffer < ActiveRecord::Migration
  def change
    change_column :offers, :icon, :string, default: 'http://res.cloudinary.com/hm54rrv18/image/upload/v1405173687/defaults/banner_150x75.jpg'
  end
end
