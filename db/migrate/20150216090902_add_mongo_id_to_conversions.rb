class AddMongoIdToConversions < ActiveRecord::Migration
  def change
    add_column :conversions, :mongo_id, :string
  end
end
