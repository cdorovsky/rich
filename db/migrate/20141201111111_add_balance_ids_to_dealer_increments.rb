class AddBalanceIdsToDealerIncrements < ActiveRecord::Migration
  def change
    DealerIncrement.find_each do |increment|
      increment.update( dealer_balance_id: increment.dealer.dealer_balance.id )
    end
  end
end
