class ChangeDefaultSubscribeBuoyutable < ActiveRecord::Migration

  def change
    change_column :subscribes, :buyoutable, :boolean, default: false
  end

end
