class AddHiddenToOffers < ActiveRecord::Migration
  def change
    add_column :offers, :hidden, :boolean, default: false
  end
end
