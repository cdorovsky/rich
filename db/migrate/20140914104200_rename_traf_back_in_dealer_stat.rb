class RenameTrafBackInDealerStat < ActiveRecord::Migration
  def change
    rename_column :dealer_stats, :traffic_back, :traf_back 
  end
end
