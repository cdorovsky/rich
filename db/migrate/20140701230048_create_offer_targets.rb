class CreateOfferTargets < ActiveRecord::Migration
  def change
    create_table :offer_targets do |t|
      t.integer :offer_id
      t.string :name, null: false
      t.text :description
      t.float :price

      t.timestamps
    end
  end
end
