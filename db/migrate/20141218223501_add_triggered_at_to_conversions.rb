class AddTriggeredAtToConversions < ActiveRecord::Migration
  def change
    add_column :conversions, :triggered_at, :datetime
  end
end
