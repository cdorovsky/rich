class RenameColumnsInDealerStats < ActiveRecord::Migration
  def change

    rename_column :dealer_stats, :common_flag,               :common
    rename_column :dealer_stats, :wapclick_flag,             :waplick
    rename_column :dealer_stats, :cpa_flag,                  :cpa
    rename_column :dealer_stats, :apps_advert_flag,          :apps_advert

    rename_column :dealer_stats, :dayly,                     :daily

    rename_column :dealer_stats, :traffic_back_counter,      :traffic_back
    rename_column :dealer_stats, :uniq_traffic_back_counter, :uniq_traffic_back

    rename_column :dealer_stats, :visitor_counter,           :visitor
    rename_column :dealer_stats, :uniq_visitor_counter,      :uniq_visitor

    rename_column :dealer_stats, :subscribe_counter,         :subscribe
    rename_column :dealer_stats, :unsubscribe_counter,       :unsubscribe
    rename_column :dealer_stats, :rebill_counter,            :rebill
    rename_column :dealer_stats, :download_counter,          :download

    rename_column :dealer_stats, :money_total,               :money_common
    rename_column :dealer_stats, :money_wapclick,            :money

    rename_column :dealer_stats, :conversion_ined,           :conversion_declined

  end
end
