class CreateDirectOfferRedirects < ActiveRecord::Migration
  def change
    create_table :direct_offer_redirects do |t|
      t.string  :request_id 
      t.string  :referer
      t.text    :sub_id
      t.text    :sub_id2
      t.text    :sub_id3
      t.text    :reidirect_url
      t.text    :url_params
      t.integer :dealer_id
      t.integer :offer_id
      t.integer :direct_offer_id
      t.integer :country_id
      t.string  :country_name
      t.string  :country_code
      t.inet    :ip
      t.text    :user_agent
      t.string  :user_platform
      t.string  :user_device
      t.boolean :uniq_visitor
      t.boolean :traffic_back
      t.boolean :converted, default: false

      t.datetime :triggered_at
      t.timestamps
    end
  end
end
