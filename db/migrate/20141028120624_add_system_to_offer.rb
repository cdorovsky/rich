class AddSystemToOffer < ActiveRecord::Migration
  def change
    add_column :offers, :system, :boolean, null: false, default: false
  end
end
