class AddFlagsToNotificationSettings < ActiveRecord::Migration
  def change
    add_column :notification_settings, :ticket_email, :boolean, default: true
    add_column :notification_settings, :promo_email, :boolean, default: true
    add_column :notification_settings, :offer_email, :boolean, default: true
    add_column :notification_settings, :ticket_sms, :boolean, default: true
    add_column :notification_settings, :promo_sms, :boolean, default: true
    add_column :notification_settings, :offer_sms, :boolean, default: true
  end
end
