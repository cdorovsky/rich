class AddSendPostbackTrigger < ActiveRecord::Migration
  def change
    add_column :campaigns, :sent_postback, :boolean
  end
end
