class AddEnabledToMoneyOptions < ActiveRecord::Migration
  def change
    add_column :money_options, :enabled, :boolean, default: false
  end
end
