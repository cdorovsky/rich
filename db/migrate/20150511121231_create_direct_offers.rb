class CreateDirectOffers < ActiveRecord::Migration
  def change

    create_table :direct_offers do |t|
      t.integer :dealer_id
      t.integer :offer_id
      t.integer :direct_offer_set
      t.integer :conversion_expected
      t.string  :hashid
      t.string  :selfmade
      t.string  :adult
      t.string  :traffic_sources
      t.boolean :approved
    end

  end
end
