class CreateRebills < ActiveRecord::Migration
  def change
    create_table :rebills do |t|
      t.integer :dealer_id
      t.integer :sub_id
      t.string :phone
      t.string :country
      t.string :operator
      t.string :platform
      t.string :billing
      t.string :country_code
      t.timestamps
    end
  end
end
