class CreateOfferIcons < ActiveRecord::Migration
  def change
    create_table :offer_icons do |t|
      t.string :file
      t.integer :offer_id

      t.timestamps
    end
  end
end
