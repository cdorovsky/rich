class AddUniqIndexesOnDirectOfferShowAndRedirects < ActiveRecord::Migration
  def change
    add_index :direct_offer_shows,     [ :request_id ], name: 'direct_show_uniq_request_ids', unique: true
    add_index :direct_offer_redirects, [ :request_id ], name: 'direct_redirect_uniq_request_ids', unique: true
  end
end
