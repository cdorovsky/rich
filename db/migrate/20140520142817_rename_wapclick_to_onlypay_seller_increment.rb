class RenameWapclickToOnlypaySellerIncrement < ActiveRecord::Migration
  def change
    rename_column :seller_increments, :wapclick_view, :onlypay_view
    rename_column :seller_increments, :wapclick_redirect, :onlypay_redirect
    rename_column :seller_increments, :wapclick_subscribe, :onlypay_subscribe
    rename_column :seller_increments, :wapclick_rebill, :onlypay_rebill
    rename_column :seller_increments, :wapclick_unsubscribe, :onlypay_unsubscribe
  end
end
