class AddCountersToSellerBalance < ActiveRecord::Migration
  def change
    add_column :seller_balances, :wapclick_view, :integer
    add_column :seller_balances, :wapclick_redirect, :integer 
    add_column :seller_balances, :wapclick_subscribe, :integer
    add_column :seller_balances, :wapclick_rebill, :integer 
    add_column :seller_balances, :wapclick_unsubcribe, :integer 
  end
end
