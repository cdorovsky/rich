class AddFieldsToDealer < ActiveRecord::Migration
  def change
    add_column :dealers, :phone, :string
    add_column :dealers, :skype, :string
    add_column :dealers, :icq, :string
    add_column :dealers, :jabber, :string
    add_column :dealers, :name, :string
  end
end
