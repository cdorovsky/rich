class AddRebillIdToSellerRequest < ActiveRecord::Migration
  def change
    add_column :seller_requests, :rebill_id, :integer
  end
end
