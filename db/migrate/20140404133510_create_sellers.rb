class CreateSellers < ActiveRecord::Migration
  def change
    create_table :sellers do |t|
      t.string :login
      t.string :email
      t.string :password
      t.timestamps
    end
  end
end
