class AddLimitsToOffer < ActiveRecord::Migration
  def change
    add_column :offers, :cap_time_zone_delta, :integer, default: 0
    add_column :offers, :daily_install_limit, :integer, default: 0
    add_column :offers, :install_count,       :integer, default: 0
    add_column :offers, :support,             :boolean, default: false
    add_column :offers, :daily_limit_status,  :string, default: 'unlimited'
  end
end
