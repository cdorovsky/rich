class AddRemovedToOfferSet < ActiveRecord::Migration
  def change
    add_column :offer_sets, :removed, :boolean, default: false
  end
end
