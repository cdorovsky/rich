class CreateCampaignLimiters < ActiveRecord::Migration
  def change
    create_table :campaign_limiters do |t|
      t.integer :campaign_id
      t.integer :hours, default: 16777215 # 24 hours 
      t.integer :days, default: 127 # 7 days
      t.integer :show_limits, default: 3

      t.timestamps
    end
  end
end
