class CreateDailyAggregates < ActiveRecord::Migration
  def change
    create_table :daily_aggregates do |t|
      t.integer :dealer_id
      t.integer :campaign_id
      t.integer :offer_id
      t.string  :short_date
      t.integer :daily_visitor_count, default: 0
      t.integer :daily_install_count, default: 0
      t.float   :daily_revenue,       default: 0
      t.timestamps
    end
  end
end
