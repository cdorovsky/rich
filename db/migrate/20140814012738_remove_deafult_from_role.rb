class RemoveDeafultFromRole < ActiveRecord::Migration
  def change
    change_column :dealers, :role, :string, default: nil
  end
end
