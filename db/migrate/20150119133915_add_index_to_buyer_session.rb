class AddIndexToBuyerSession < ActiveRecord::Migration
  def change
  	add_index :buyer_sessions, :session_key, unique: true
  end
end
