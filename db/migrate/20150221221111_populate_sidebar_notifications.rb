class PopulateSidebarNotifications < ActiveRecord::Migration
  def change
    Dealer.all.find_each { | d | SidebarNotifications.create( dealer_id: d.id ) unless d.sidebar_notifications.present? }
  end
end
