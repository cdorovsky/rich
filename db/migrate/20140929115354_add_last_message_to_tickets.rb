class AddLastMessageToTickets < ActiveRecord::Migration
  def change
    add_column :tickets, :last_answered, :string
  end
end
