class AddFieldsToRequestLogs < ActiveRecord::Migration
  def change
    add_column :request_logs, :unique, :boolean
  end
end
