class CreateOfferScreenshots < ActiveRecord::Migration
  def change
    create_table :offer_screenshots do |t|
      t.integer :offer_id
      t.string :file

      t.timestamps
    end
  end
end
