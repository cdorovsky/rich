class RenameDailyToDaylyInDealerStats < ActiveRecord::Migration
  def change
    rename_column :dealer_stats, :daily, :dayly
  end
end
