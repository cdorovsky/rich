class AddPostbackIdToConversion < ActiveRecord::Migration
  def change
    add_column :conversions, :postback_id, :integer
  end
end
