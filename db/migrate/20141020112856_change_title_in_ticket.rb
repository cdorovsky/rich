class ChangeTitleInTicket < ActiveRecord::Migration
  def change
    change_column :tickets, :title, :text, default: ''
  end
end
