class AddQrLinkToOfferSet < ActiveRecord::Migration
  def change
    add_column :offer_sets, :qr_link, :string
  end
end
