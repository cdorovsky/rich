class ChangeMoneyInRebill < ActiveRecord::Migration
  def change
    add_column :rebills, :tariff, :float 
    rename_column :rebills, :full_cost, :dealer_revenue
  end
end
