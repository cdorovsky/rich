class CreateOfferCountrySets < ActiveRecord::Migration
  def change
    create_table :offer_country_sets do |t|
      t.integer :offer_id
      t.integer :country_id

      t.timestamps
    end
  end
end
