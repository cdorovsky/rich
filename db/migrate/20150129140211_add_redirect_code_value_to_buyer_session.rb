class AddRedirectCodeValueToBuyerSession < ActiveRecord::Migration
  def change
    add_column :buyer_sessions, :redirect_code_value, :string
  end
end
