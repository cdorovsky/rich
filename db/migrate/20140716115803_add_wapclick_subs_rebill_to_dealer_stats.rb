class AddWapclickSubsRebillToDealerStats < ActiveRecord::Migration
  def change
    add_column :dealer_stats, :subscribe_wapclick, :integer, default: 0
    add_column :dealer_stats, :rebill_wapclick, :integer, default: 0
  end
end
