class AddWalletTypeToPayment < ActiveRecord::Migration
  def change
    add_column :payments, :wallet_type, :string
  end
end
