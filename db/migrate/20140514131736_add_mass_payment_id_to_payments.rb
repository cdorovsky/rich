class AddMassPaymentIdToPayments < ActiveRecord::Migration
  def change
    add_column :payments, :mass_payment_id, :integer
  end
end
