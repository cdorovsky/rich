class AddRulesToOffer < ActiveRecord::Migration
  def change
    add_column :offers, :rules, :text, default: ''
  end
end
