class AddCampaignIdToSellerRequest < ActiveRecord::Migration
  def change
    add_column :seller_requests, :campaign_id, :integer
    add_column :seller_requests, :offer_id, :integer
  end
end
