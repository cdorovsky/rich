class AddTriggeredAtToAppInstallation < ActiveRecord::Migration
  def change
    add_column :app_installations, :triggered_at, :datetime
  end
end
