class AddWorldToCountry < ActiveRecord::Migration

  def up
    Country.find_or_create_by name: 'All'
  end

  def down
    Country.find_by_name( 'All' ).destroy
  end

end
