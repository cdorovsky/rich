class AddDefaultCountryToRequestLog < ActiveRecord::Migration
  def change
    change_column :request_logs, :country, :string, default: 'N/A'
  end
end
