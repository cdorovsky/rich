class RenameColumnConversionUnsubscribedToConversinoUnsubscribeInDealerStat < ActiveRecord::Migration
  def change
    rename_column :dealer_stats, :conversion_unsubscribed, :conversion_unsubscribe
  end
end
