class AddConversionsToSellerRequest < ActiveRecord::Migration
  def change
    add_column :seller_requests, :conversion_id, :integer
  end
end
