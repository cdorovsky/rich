class AddRequestedToDealerBalance < ActiveRecord::Migration
  def change
    add_column :dealer_balances, :requested, :float, default: 0.0
  end
end
