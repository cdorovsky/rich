class FixUrlColumnsInDirectOfferRedirect < ActiveRecord::Migration
  def change
  	remove_column :direct_offer_redirects, :reidirect_url
  	change_column :direct_offer_redirects, :seller_url, :text
  	change_column :direct_offer_redirects, :redirect_url, :text
  end
end
