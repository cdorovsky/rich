class AddIndexToSessionShowedOffer < ActiveRecord::Migration
  def change
    add_index :session_showed_offers, [ :buyer_session_id, :campaign_id ], name: 'showed_offers'
  end
end
