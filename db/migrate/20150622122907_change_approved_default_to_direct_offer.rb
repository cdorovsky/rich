class ChangeApprovedDefaultToDirectOffer < ActiveRecord::Migration
  def change
    change_column_default :direct_offers, :approved, false
  end
end
