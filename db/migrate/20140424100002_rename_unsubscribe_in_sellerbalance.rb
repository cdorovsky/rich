class RenameUnsubscribeInSellerbalance < ActiveRecord::Migration
  def change
    rename_column :seller_balances, :wapclick_unsubcribe, :wapclick_unsubscribe
  end
end
