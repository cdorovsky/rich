class CreateCommenters < ActiveRecord::Migration
  def change
    create_table :commenters do |t|
      t.string :name, default: 'no name'
      t.string :avatar, default: ''

      t.timestamps
    end
  end
end
