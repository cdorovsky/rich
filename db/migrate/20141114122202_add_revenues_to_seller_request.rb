class AddRevenuesToSellerRequest < ActiveRecord::Migration
  def change
    rename_column :seller_requests, :revenue, :revenue_from_request
    add_column    :seller_requests, :offer_dealer_revenue, :float
  end
end
