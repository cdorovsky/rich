class CreateSellerRequests < ActiveRecord::Migration
  def change
    create_table :seller_requests do |t|
      t.integer :seller_id
      t.string  :seller_name
      t.string  :seller_kind
      t.text    :transaction_id
      t.integer :external_offer_id
      t.integer :app_installation_id
      t.string  :subscription_status
      t.string  :phone
      t.integer :external_sub_id
      t.integer :subscribe_id
      t.float   :revenue
      t.integer :buyer_request_id
      t.timestamps
    end
  end
end
