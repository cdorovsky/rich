class AddBalancesToBalance < ActiveRecord::Migration
  def change
    add_column :balances, :waiting_balance, :bigint
    add_column :balances, :confirmed_balance, :bigint
  end
end
