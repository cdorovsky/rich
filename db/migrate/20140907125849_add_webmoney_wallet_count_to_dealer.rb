class AddWebmoneyWalletCountToDealer < ActiveRecord::Migration
  def change
    add_column :dealers, :webmoney_wallets_count, :integer, default: 1
  end
end
