class AddSellerUrlToDirectOfferRedirect < ActiveRecord::Migration
  def up 
    add_column :direct_offer_redirects, :seller_url, :string
    add_column :direct_offer_redirects, :redirect_url, :string
  end

  def down
    remove_column :direct_offer_redirects, :seller_url, :string
  end
end
