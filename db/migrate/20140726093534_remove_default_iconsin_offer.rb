class RemoveDefaultIconsinOffer < ActiveRecord::Migration
  def change
    change_column :offers, :icon, :string, default: nil
    change_column :offers, :banner, :string, default: nil
  end
end
