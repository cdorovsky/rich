class ChangeSentNotificationInOfferDisabler < ActiveRecord::Migration
  def change
    remove_column :offer_disablers, :plan_notification_sent
    remove_column :offer_disablers, :instant_notification_sent
    add_column    :offer_disablers, :plan_note_sent_time, :datetime
    add_column    :offer_disablers, :instant_note_sent_time, :datetime
  end
end
