class DIrectOfferRequestKindDefaultGet < ActiveRecord::Migration
  def change
    change_column :direct_offers, :postback_action, :string, default: 'GET'
  end
end
