class ChangeStatusInPaymentRequest < ActiveRecord::Migration
  def change
    add_column :payment_requests, :status, :string, default: 'new'
    remove_column :payment_requests, :paid
  end
end
