class AddShortContentToNews < ActiveRecord::Migration
  def change
    add_column :news, :short_content, :text
  end
end
