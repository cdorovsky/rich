class AddArbitrageToCampaign < ActiveRecord::Migration
  def change
    add_column :campaigns, :arbitrage, :boolean, default: false
  end
end
