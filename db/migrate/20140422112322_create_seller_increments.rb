class CreateSellerIncrements < ActiveRecord::Migration
  def change
    create_table :seller_increments do |t|
      t.time :string
      t.integer :seller_id
      t.float :waiting_increment
      t.float :confirmed_increment
      t.timestamps
    end
  end
end
