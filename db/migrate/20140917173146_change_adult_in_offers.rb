class ChangeAdultInOffers < ActiveRecord::Migration
  def up
    change_column :offers, :adult, :string, default: 'common'
    Offer.where( adult: 'true' ).update_all( adult: 'adult' )
    Offer.where( adult: 'false' ).update_all( adult: 'common' )
  end

  def down
    change_column :offers, :adult, :boolean, default: false
    Offer.where( adult: 'adult' ).update_all( adult: true )
    Offer.where( adult: 'adult_common' ).update_all( adult: true )
    Offer.where( adult: 'common' ).update_all( adult: false )
  end
end
