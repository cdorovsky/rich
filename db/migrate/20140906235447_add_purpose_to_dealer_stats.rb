class AddPurposeToDealerStats < ActiveRecord::Migration
  def change
    add_column :dealer_stats, :purpose, :string, null: false, default: 'common'
  end
end
