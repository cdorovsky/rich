class CreateCampaignOptions < ActiveRecord::Migration
  def change
    create_table :campaign_options do |t|
      t.references :offer, index: true
      t.references :campaign, index: true

      t.timestamps
    end
  end
end
