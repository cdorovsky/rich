class CreateBuyerRequests < ActiveRecord::Migration
  def change
    create_table :buyer_requests do |t|
      t.integer :buyer_session_id,      null: false
      t.integer :campaign_id,           null: false
      t.integer :dealer_id,             null: false
      t.integer :offer_id,              null: false
      t.integer :country_id,            null: false
      t.integer :redirect_code_id,      null: false
      t.string  :request_format,        null: false
      t.text    :redirect_url,          null: false
      t.inet    :ip,                    null: false
      t.string  :country_code,          null: false
      t.string  :country_name,          null: false
      t.text    :user_agent,            null: false
      t.string  :user_platform,         null: false
      t.string  :redirect_code_value,   null: false
      t.text    :sub_id
      t.text    :sub_id2
      t.text    :sub_id3
      t.timestamps
    end
  end
end
