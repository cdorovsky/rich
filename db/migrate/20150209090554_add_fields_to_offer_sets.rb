class AddFieldsToOfferSets < ActiveRecord::Migration
  def change
    add_column :offer_sets, :enabled,     :boolean
    add_column :offer_sets, :external_id, :integer
    add_column :offer_sets, :seller_name, :string
    add_column :offer_sets, :message,     :string
    add_column :offer_sets, :seller_url,  :string
    add_column :offer_sets, :apps_os,     :string
  end
end
