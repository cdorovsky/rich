class AddDealerCampaignToBuyerSession < ActiveRecord::Migration
  def change
    add_column :buyer_sessions, :dealer_id,        :integer
    add_column :buyer_sessions, :campaign_id,      :integer
    add_column :buyer_sessions, :redirect_code_id, :integer
  end
end
