class CreateLandingHeadings < ActiveRecord::Migration
  def change
    create_table :landing_headings do |t|
      t.text :body, default: ''
      t.integer :offer_id

      t.timestamps
    end
  end
end
