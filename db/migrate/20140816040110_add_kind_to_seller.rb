class AddKindToSeller < ActiveRecord::Migration
  def change
    add_column :sellers, :kind, :string
  end
end
