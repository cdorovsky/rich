class AddUnknownPlatformDefaultToBuyerRequest < ActiveRecord::Migration
  def change
    change_column :buyer_requests, :user_platform, :string, default: 'unknown'
  end
end
