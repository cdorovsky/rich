class AddDealerIdToOfferSets < ActiveRecord::Migration
  def change
    add_column :offer_sets, :dealer_id, :integer
  end
end
