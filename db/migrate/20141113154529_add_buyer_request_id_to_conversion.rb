class AddBuyerRequestIdToConversion < ActiveRecord::Migration
  def change
    add_column :conversions, :buyer_request_id, :integer
  end
end
