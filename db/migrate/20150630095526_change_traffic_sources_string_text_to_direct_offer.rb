class ChangeTrafficSourcesStringTextToDirectOffer < ActiveRecord::Migration
  def change
    change_column :direct_offers, :traffic_sources, :text
  end
end
