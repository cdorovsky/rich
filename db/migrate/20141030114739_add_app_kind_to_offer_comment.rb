class AddAppKindToOfferComment < ActiveRecord::Migration
  def change
    add_column :offer_comments, :app_kind, :string, default: 'application'
  end
end
