class AddUniqToDealerStat < ActiveRecord::Migration
  def change
    change_column :dealer_stats, :md5id, :string, uniq: true
  end
end
