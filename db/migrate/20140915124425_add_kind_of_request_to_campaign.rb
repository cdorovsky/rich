class AddKindOfRequestToCampaign < ActiveRecord::Migration
  def change
    add_column :campaigns, :request_kind, :string, default: 'get'
  end
end
