# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150724145519) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "pg_stat_statements"

  create_table "action_counters", force: true do |t|
    t.date     "date"
    t.string   "time"
    t.string   "action_type"
    t.integer  "waiting_subscribtions"
    t.integer  "waiting_requests"
    t.integer  "waiting_downloads"
    t.integer  "waiting_install"
    t.integer  "waiting_payment"
    t.integer  "waiting_rebill"
    t.integer  "confirmed_subscribtions"
    t.integer  "confirmed_requests"
    t.integer  "confirmed_downloads"
    t.integer  "confirmed_install"
    t.integer  "confirmed_payment"
    t.integer  "confirmed_rebill"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "admin_requests", force: true do |t|
    t.integer  "dealer_id"
    t.float    "value"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "description"
    t.boolean  "triggered"
  end

  create_table "app_installations", force: true do |t|
    t.integer  "dealer_id"
    t.integer  "campaign_id"
    t.integer  "offer_id"
    t.text     "transaction_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "triggered_at"
  end

  add_index "app_installations", ["transaction_id"], name: "index_app_installations_on_transaction_id", unique: true, using: :btree

  create_table "buyer_requests", force: true do |t|
    t.integer  "buyer_session_id",                        null: false
    t.integer  "campaign_id",                             null: false
    t.integer  "dealer_id",                               null: false
    t.integer  "offer_id",                                null: false
    t.integer  "country_id",                              null: false
    t.integer  "redirect_code_id",                        null: false
    t.string   "request_format",                          null: false
    t.text     "redirect_url"
    t.inet     "ip",                                      null: false
    t.string   "country_code",                            null: false
    t.string   "country_name",                            null: false
    t.text     "user_agent",                              null: false
    t.string   "user_platform",       default: "unknown", null: false
    t.string   "redirect_code_value",                     null: false
    t.text     "sub_id"
    t.text     "sub_id2"
    t.text     "sub_id3"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "referer_url",                             null: false
    t.string   "user_device",         default: "mobile",  null: false
    t.string   "request_kind",        default: "html",    null: false
    t.integer  "direct_offer_id"
  end

  add_index "buyer_requests", ["buyer_session_id", "offer_id", "request_kind"], name: "buyer_request_index", using: :btree

  create_table "buyer_sessions", force: true do |t|
    t.string   "session_key"
    t.datetime "outdate_moment"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "dealer_id"
    t.integer  "campaign_id"
    t.integer  "redirect_code_id"
    t.string   "redirect_code_value"
  end

  add_index "buyer_sessions", ["session_key"], name: "index_buyer_sessions_on_session_key", unique: true, using: :btree

  create_table "campaign_country_sets", force: true do |t|
    t.integer  "country_id"
    t.integer  "campaign_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "campaign_limiters", force: true do |t|
    t.integer  "campaign_id"
    t.integer  "hours",       default: 16777215
    t.integer  "days",        default: 127
    t.integer  "show_limits", default: 3
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "campaigns", force: true do |t|
    t.integer  "dealer_id"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "enabled",               default: true
    t.string   "trafficback_url"
    t.boolean  "russia",                default: false
    t.boolean  "europe",                default: false
    t.boolean  "asia",                  default: false
    t.boolean  "america",               default: false
    t.string   "m_trafficback_url"
    t.string   "broken_offer_behavior", default: "traffic_back"
    t.boolean  "australia",             default: false
    t.boolean  "adult",                 default: false
    t.boolean  "megafon_buyout",        default: false
    t.boolean  "beeline_buyout",        default: false
    t.boolean  "mts_buyout",            default: false
    t.integer  "buyout_source_id"
    t.integer  "buyout_target_id"
    t.text     "postback_url"
    t.boolean  "sent_postback",         default: false
    t.string   "request_kind",          default: "get"
    t.boolean  "arbitrage",             default: false
    t.boolean  "wapclick_ready",        default: false
    t.integer  "countries_count",       default: 0
    t.boolean  "archived",              default: false
    t.string   "offer_scheme",          default: "cost",         null: false
    t.boolean  "disable_weblandings",   default: false
    t.string   "hash_code"
  end

  add_index "campaigns", ["dealer_id"], name: "index_campaigns_on_dealer_id", using: :btree
  add_index "campaigns", ["sent_postback", "postback_url"], name: "for_postback", using: :btree

  create_table "commenters", force: true do |t|
    t.string   "name",       default: "no name"
    t.string   "avatar",     default: ""
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "conversions", force: true do |t|
    t.integer  "offer_id"
    t.integer  "campaign_id"
    t.integer  "dealer_id"
    t.string   "offer_name"
    t.float    "dealer_revenue"
    t.string   "source_url",             default: ""
    t.string   "campaign_name"
    t.string   "country_name"
    t.string   "kind"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "conversion_reason"
    t.string   "crypted_id"
    t.string   "country_code"
    t.string   "sub_id",                 default: ""
    t.string   "sub_id2",                default: ""
    t.string   "sub_id3",                default: ""
    t.integer  "country_id"
    t.datetime "triggered_at"
    t.integer  "triggered_at_timestamp"
    t.string   "mongo_id"
  end

  add_index "conversions", ["crypted_id"], name: "index_conversions_on_crypted_id", unique: true, using: :btree
  add_index "conversions", ["dealer_id", "campaign_id", "offer_id", "triggered_at_timestamp", "country_id", "sub_id", "sub_id2", "sub_id3"], name: "for_postbacks", using: :btree
  add_index "conversions", ["dealer_id"], name: "index_conversions_on_dealer_id", using: :btree
  add_index "conversions", ["mongo_id"], name: "index_conversions_on_mongo_id", using: :btree

  create_table "countries", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "abbr",       default: ""
    t.string   "image_url",  default: ""
  end

  create_table "country_sets", force: true do |t|
    t.integer  "campaign_id"
    t.boolean  "russia",      default: false
    t.boolean  "europe",      default: false
    t.boolean  "asia",        default: false
    t.boolean  "america",     default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "daily_aggregates", force: true do |t|
    t.integer  "dealer_id"
    t.integer  "campaign_id"
    t.integer  "offer_id"
    t.string   "short_date"
    t.integer  "daily_visitor_count", default: 0
    t.integer  "daily_install_count", default: 0
    t.float    "daily_revenue",       default: 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "seller_revenue",      default: 0.0
    t.float    "conversion_rate",     default: 0.0
  end

  add_index "daily_aggregates", ["dealer_id", "campaign_id", "offer_id"], name: "for_search_hash", using: :btree
  add_index "daily_aggregates", ["short_date"], name: "index_daily_aggregates_on_short_date", using: :btree

  create_table "dashboard_counters", force: true do |t|
    t.datetime "date"
    t.string   "time"
    t.integer  "new_partners"
    t.integer  "payments_waiting"
    t.integer  "unique_hosts"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "dealer_balances", force: true do |t|
    t.integer  "dealer_id"
    t.string   "basic_currency"
    t.string   "user_currency"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "waiting",             default: 0.0
    t.float    "confirmed",           default: 0.0
    t.float    "requested",           default: 0.0
    t.integer  "onlypay_redirect"
    t.integer  "onlypay_subscribe"
    t.integer  "onlypay_rebill"
    t.integer  "onlypay_unsubscribe"
    t.float    "for_checkout",        default: 0.0
  end

  create_table "dealer_emails", force: true do |t|
    t.string   "name",         default: ""
    t.text     "dealer_ids",   default: "[]"
    t.string   "subject",                     null: false
    t.text     "body",                        null: false
    t.string   "message_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "dealer_increments", force: true do |t|
    t.integer  "hour"
    t.integer  "dealer_id"
    t.float    "waiting",             default: 0.0
    t.float    "confirmed",           default: 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "onlypay_redirect"
    t.integer  "onlypay_subscribe"
    t.integer  "onlypay_rebill"
    t.integer  "onlypay_unsubscribe"
    t.integer  "dealer_balance_id"
    t.integer  "campaign_id"
    t.integer  "offer_id"
    t.integer  "reason_id"
    t.string   "reason_type"
    t.datetime "triggered_at"
    t.boolean  "paid",                default: false
    t.boolean  "referrer",            default: false
    t.integer  "referrer_id"
  end

  create_table "dealer_stats", force: true do |t|
    t.integer  "dealer_id"
    t.integer  "campaign_id"
    t.integer  "current"
    t.integer  "visitor",                default: 0
    t.integer  "uniq_visitor",           default: 0
    t.integer  "traf_back",              default: 0
    t.float    "money_total",            default: 0.0
    t.float    "money_waiting",          default: 0.0
    t.float    "money_confirmed",        default: 0.0
    t.float    "money_declined",         default: 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "hourly",                 default: false
    t.boolean  "daily",                  default: false
    t.boolean  "monthly",                default: false
    t.integer  "download",               default: 0
    t.integer  "subscribe",              default: 0
    t.integer  "rebill",                 default: 0
    t.integer  "unsubscribe",            default: 0
    t.integer  "offer_id"
    t.boolean  "no_grouping",            default: false
    t.boolean  "campaign_grouping",      default: false
    t.boolean  "offer_grouping",         default: false
    t.boolean  "common",                 default: false
    t.integer  "uniq_traf_back",         default: 0
    t.boolean  "wapclick",               default: false
    t.boolean  "cpa",                    default: false
    t.boolean  "apps_advert",            default: false
    t.integer  "conversion_total",       default: 0
    t.integer  "conversion_confirmed",   default: 0
    t.integer  "conversion_waiting",     default: 0
    t.integer  "conversion_declined",    default: 0
    t.integer  "conversion_subscribe",   default: 0
    t.integer  "conversion_unsubscribe", default: 0
    t.integer  "conversion_rebill",      default: 0
    t.float    "money_from_rebill",      default: 0.0
    t.float    "money_from_buyout",      default: 0.0
    t.integer  "month"
    t.integer  "day"
    t.integer  "hour"
    t.integer  "conversion_buyout",      default: 0
    t.integer  "conversion_install",     default: 0
    t.string   "purpose",                default: "common", null: false
    t.integer  "conversion_app_install", default: 0
    t.integer  "js_visitor",             default: 0
    t.integer  "uniq_js_visitor",        default: 0
    t.integer  "webland_visitor",        default: 0
    t.integer  "uniq_webland_visitor",   default: 0
    t.string   "md5id"
    t.datetime "triggered_at"
  end

  add_index "dealer_stats", ["dealer_id", "campaign_id", "offer_id", "hourly", "daily", "monthly", "current", "month", "day", "hour", "no_grouping", "campaign_grouping", "offer_grouping", "common", "wapclick", "cpa", "apps_advert", "purpose"], name: "main_index", using: :btree
  add_index "dealer_stats", ["md5id"], name: "index_dealer_stats_on_md5id", unique: true, using: :btree
  add_index "dealer_stats", ["md5id"], name: "md5index", using: :btree
  add_index "dealer_stats", ["monthly", "dealer_id"], name: "for_campaigns", using: :btree

  create_table "dealers", force: true do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "referral_code"
    t.string   "phone"
    t.string   "skype"
    t.string   "icq"
    t.string   "jabber"
    t.string   "name"
    t.boolean  "is_an_admin",            default: false
    t.string   "role"
    t.integer  "webmoney_wallets_count", default: 1
    t.boolean  "blocked",                default: false
    t.integer  "tariff_id"
    t.string   "api_key",                default: "",    null: false
    t.string   "reg_params",             default: ""
    t.integer  "referrer_id"
    t.float    "referrer_rate",          default: 0.055
  end

  add_index "dealers", ["email"], name: "index_dealers_on_email", unique: true, using: :btree
  add_index "dealers", ["reset_password_token"], name: "index_dealers_on_reset_password_token", unique: true, using: :btree

  create_table "direct_offer_redirects", force: true do |t|
    t.text     "request_id"
    t.text     "referer"
    t.text     "sub_id"
    t.text     "sub_id2"
    t.text     "sub_id3"
    t.text     "url_params"
    t.integer  "dealer_id"
    t.integer  "offer_id"
    t.integer  "direct_offer_id"
    t.integer  "country_id"
    t.text     "country_name"
    t.text     "country_code"
    t.inet     "ip"
    t.text     "user_agent"
    t.text     "user_platform"
    t.text     "user_device"
    t.boolean  "uniq_visitor"
    t.boolean  "traffic_back"
    t.boolean  "converted",       default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "seller_url"
    t.text     "redirect_url"
    t.text     "timestamp"
    t.text     "from"
    t.integer  "triggered_at"
  end

  add_index "direct_offer_redirects", ["request_id"], name: "direct_redirect_uniq_request_ids", unique: true, using: :btree

  create_table "direct_offer_sets", force: true do |t|
    t.integer "dealer_id"
    t.string  "hashid"
  end

  create_table "direct_offer_shows", force: true do |t|
    t.text     "request_id"
    t.text     "referer"
    t.text     "sub_id"
    t.text     "sub_id2"
    t.text     "sub_id3"
    t.text     "seller_url"
    t.text     "url_params"
    t.integer  "dealer_id"
    t.integer  "offer_id"
    t.integer  "direct_offer_id"
    t.integer  "country_id"
    t.text     "country_name"
    t.text     "country_code"
    t.inet     "ip"
    t.text     "user_agent"
    t.text     "user_platform"
    t.text     "user_device"
    t.boolean  "uniq_visitor"
    t.boolean  "converted",       default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "timestamp"
    t.text     "from"
    t.integer  "triggered_at"
  end

  add_index "direct_offer_shows", ["request_id"], name: "direct_show_uniq_request_ids", unique: true, using: :btree

  create_table "direct_offers", force: true do |t|
    t.integer  "dealer_id"
    t.integer  "offer_id"
    t.integer  "direct_offer_set"
    t.integer  "conversion_expected"
    t.string   "hashid"
    t.string   "selfmade"
    t.string   "adult"
    t.text     "traffic_sources"
    t.boolean  "approved",            default: false
    t.boolean  "enabled",             default: true
    t.integer  "seller_id"
    t.string   "seller_name"
    t.integer  "external_id"
    t.string   "apps_os"
    t.string   "offer_name"
    t.string   "offer_kind"
    t.float    "cost"
    t.boolean  "postback",            default: false
    t.string   "postback_action",     default: "GET"
    t.text     "postback_url"
    t.datetime "created_at"
  end

  add_index "direct_offers", ["hashid"], name: "index_direct_offers_on_hashid", unique: true, using: :btree

  create_table "faqs", force: true do |t|
    t.string   "question"
    t.text     "answer"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "landing_comment_sets", force: true do |t|
    t.integer  "offer_id"
    t.integer  "offer_comment_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "landing_heading_sets", force: true do |t|
    t.integer  "landing_heading_id"
    t.integer  "offer_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "landing_headings", force: true do |t|
    t.text     "body",       default: ""
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_default", default: false
    t.string   "app_kind"
  end

  create_table "landing_subscribers", force: true do |t|
    t.integer  "country_id"
    t.integer  "offer_set_id"
    t.boolean  "active",       default: false
    t.string   "phone",        default: ""
    t.string   "os",           default: ""
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "landing_visitors", force: true do |t|
    t.integer  "offer_set_id"
    t.integer  "landing_subscriber_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "money_options", force: true do |t|
    t.integer  "dealer_id"
    t.integer  "wallet_id"
    t.string   "pay_type",   default: "time"
    t.string   "pay_timing", default: "weekly"
    t.integer  "pay_day",    default: 1
    t.integer  "pay_value",  default: 1000
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "enabled",    default: false
  end

  create_table "news", force: true do |t|
    t.string   "title"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "short_content"
  end

  create_table "notification_settings", force: true do |t|
    t.integer  "dealer_id"
    t.integer  "tickets_cd",         default: 2
    t.integer  "program_cd",         default: 2
    t.boolean  "news",               default: false
    t.boolean  "payout",             default: false
    t.boolean  "urgent_tickets",     default: false
    t.boolean  "my_campaigns",       default: false
    t.boolean  "new_offers",         default: false
    t.boolean  "balance",            default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "ticket_email",       default: true
    t.boolean  "promo_email",        default: true
    t.boolean  "offer_email",        default: true
    t.boolean  "ticket_sms",         default: true
    t.boolean  "promo_sms",          default: true
    t.boolean  "offer_sms",          default: true
    t.boolean  "direct_offer_email", default: true
    t.boolean  "direct_offer_sms",   default: true
  end

  create_table "notifications", force: true do |t|
    t.string   "title"
    t.text     "text"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "dealer_id"
  end

  create_table "offer_comments", force: true do |t|
    t.text     "body",         default: ""
    t.integer  "rating",       default: 5
    t.integer  "commenter_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "app_kind",     default: "application"
  end

  create_table "offer_country_sets", force: true do |t|
    t.integer  "offer_id"
    t.integer  "country_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "offer_disablers", force: true do |t|
    t.integer  "offer_id"
    t.boolean  "offer_disabled",         default: false
    t.datetime "disabling_time"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "plan_note_sent_time"
    t.datetime "instant_note_sent_time"
  end

  create_table "offer_icons", force: true do |t|
    t.string   "file"
    t.integer  "offer_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "offer_limits", force: true do |t|
    t.integer  "offer_id"
    t.integer  "cap_time_zone_delta", default: 0
    t.integer  "daily_install_limit", default: 0
    t.integer  "install_count",       default: 0
    t.string   "daily_limit_status",  default: "unlimited"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "offer_screenshots", force: true do |t|
    t.integer  "offer_id"
    t.string   "file"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "offer_set_shows", force: true do |t|
    t.string   "mongo_id"
    t.text     "referer"
    t.text     "sub_id"
    t.text     "sub_id2"
    t.text     "sub_id3"
    t.integer  "dealer_id"
    t.integer  "campaign_id"
    t.integer  "offer_id"
    t.integer  "offer_set_id"
    t.integer  "country_id"
    t.string   "country_name"
    t.string   "country_code"
    t.inet     "ip"
    t.text     "user_agent"
    t.string   "user_platform"
    t.string   "user_device"
    t.datetime "triggered_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "converted",     default: false
  end

  add_index "offer_set_shows", ["mongo_id"], name: "index_offer_set_shows_on_mongo_id", using: :btree

  create_table "offer_sets", force: true do |t|
    t.integer  "offer_id"
    t.integer  "campaign_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "lending_type_cd"
    t.integer  "dealer_id"
    t.string   "offer_kind",      default: ""
    t.integer  "show_limit",      default: 10
    t.integer  "priority",        default: 1
    t.string   "qr_link"
    t.integer  "seller_id"
    t.boolean  "enabled"
    t.integer  "external_id"
    t.string   "seller_name"
    t.text     "message"
    t.string   "seller_url"
    t.string   "apps_os"
    t.string   "offer_name"
    t.float    "cost",            default: 0.0
    t.boolean  "removed",         default: false
    t.string   "tb_url"
    t.string   "m_tb_url"
    t.string   "preview_url"
    t.string   "hashid"
  end

  add_index "offer_sets", ["campaign_id", "offer_id"], name: "for_quick_relations", using: :btree
  add_index "offer_sets", ["campaign_id"], name: "index_offer_sets_on_campaign_id", using: :btree
  add_index "offer_sets", ["hashid"], name: "index_hashid_on_offer_set", unique: true, using: :btree
  add_index "offer_sets", ["offer_id"], name: "index_offer_sets_on_offer_id", using: :btree

  create_table "offer_targets", force: true do |t|
    t.integer  "offer_id"
    t.string   "name",        null: false
    t.text     "description"
    t.float    "price"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "offers", force: true do |t|
    t.integer  "seller_id"
    t.string   "name"
    t.string   "url"
    t.string   "code"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "icon"
    t.text     "description"
    t.float    "weekly_cr",               default: 0.0
    t.float    "today_cr",                default: 0.0
    t.float    "yesterday_cr",            default: 0.0
    t.float    "weekly_epc",              default: 0.0
    t.float    "today_epc",               default: 0.0
    t.float    "yesterday_epc",           default: 0.0
    t.boolean  "websites",                default: false
    t.boolean  "dorways",                 default: false
    t.boolean  "context",                 default: false
    t.boolean  "brand_context",           default: false
    t.boolean  "teaser",                  default: false
    t.boolean  "target",                  default: false
    t.boolean  "social",                  default: false
    t.boolean  "email",                   default: false
    t.boolean  "cashback",                default: false
    t.boolean  "clickpopunder",           default: false
    t.boolean  "brokers",                 default: false
    t.integer  "external_id"
    t.string   "banner"
    t.string   "kind",                    default: "cpa"
    t.string   "adult",                   default: "common"
    t.boolean  "disabled",                default: false
    t.text     "country_list"
    t.float    "tariff",                  default: 0.0
    t.float    "dealer_revenue",          default: 0.0
    t.string   "apps_os"
    t.string   "preview_url"
    t.string   "operator_name",           default: ""
    t.boolean  "motivated",               default: false
    t.boolean  "redirect",                default: false
    t.float    "our_profit",              default: 0.0
    t.float    "partners_profit",         default: 0.0
    t.text     "rules",                   default: ""
    t.string   "js_dialogue"
    t.datetime "disabling_time"
    t.boolean  "system",                  default: false,         null: false
    t.float    "rating",                  default: 5.0
    t.integer  "votes_count",             default: 0
    t.integer  "downloads_count",         default: 0
    t.boolean  "weblanding_ready",        default: false
    t.string   "app_kind",                default: "application"
    t.text     "features_list"
    t.string   "qr_link",                 default: ""
    t.string   "os_version"
    t.string   "landing_name"
    t.text     "landing_description"
    t.integer  "cap_time_zone_delta",     default: 0
    t.integer  "daily_install_limit",     default: 0
    t.integer  "install_count",           default: 0
    t.boolean  "support",                 default: false
    t.string   "daily_limit_status",      default: "unlimited"
    t.text     "restricted_country_list"
    t.boolean  "hidden",                  default: false
    t.string   "opsos",                   default: ""
    t.string   "banner_links",            default: ""
    t.string   "conversion_type",         default: ""
    t.text     "dialog_translations"
    t.boolean  "featured",                default: false
    t.boolean  "auto_approve",            default: false
    t.boolean  "allow_push"
  end

  create_table "payment_requests", force: true do |t|
    t.integer  "dealer_id"
    t.float    "confirmed",    default: 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "payment_id"
    t.boolean  "rolled_back",  default: false
    t.string   "payment_type"
    t.string   "status",       default: "new"
    t.integer  "wallet_id"
  end

  create_table "payments", force: true do |t|
    t.string   "payment_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "paid",         default: false
    t.string   "wallet_type"
  end

  create_table "postbacks", force: true do |t|
    t.text     "postback_url"
    t.integer  "dealer_id"
    t.integer  "campaign_id"
    t.integer  "offer_id"
    t.string   "sub_id"
    t.string   "sub_id2"
    t.string   "sub_id3"
    t.string   "country_name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "country_code"
    t.datetime "sended_at"
    t.string   "datetime"
    t.integer  "country_id"
    t.string   "conversion_type"
    t.integer  "conversion_id"
    t.string   "conversion_code"
    t.text     "offer_name"
    t.float    "profit_usd"
    t.boolean  "sended",                 default: false
    t.integer  "triggered_at_timestamp"
    t.string   "mongo_id"
    t.boolean  "postback_enabled",       default: false
    t.string   "request_kind",           default: "GET"
  end

  add_index "postbacks", ["dealer_id", "campaign_id", "offer_id"], name: "for_search", using: :btree

  create_table "rebills", force: true do |t|
    t.integer  "dealer_id"
    t.integer  "subscribe_id"
    t.string   "phone"
    t.string   "country"
    t.string   "operator"
    t.string   "platform"
    t.string   "billing"
    t.string   "country_code"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "dealer_revenue"
    t.float    "tariff"
    t.integer  "campaign_id"
    t.integer  "offer_id"
  end

  create_table "redirect_codes", force: true do |t|
    t.string   "code"
    t.string   "code_kind"
    t.integer  "campaign_id"
    t.integer  "offer_set_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "redirect_settings", force: true do |t|
    t.integer  "campaign_id"
    t.boolean  "01",          default: false
    t.boolean  "02",          default: false
    t.boolean  "03",          default: false
    t.boolean  "04",          default: false
    t.boolean  "05",          default: false
    t.boolean  "06",          default: false
    t.boolean  "07",          default: false
    t.boolean  "08",          default: false
    t.boolean  "09",          default: false
    t.boolean  "10",          default: false
    t.boolean  "11",          default: false
    t.boolean  "12",          default: false
    t.boolean  "13",          default: false
    t.boolean  "14",          default: false
    t.boolean  "15",          default: false
    t.boolean  "16",          default: false
    t.boolean  "17",          default: false
    t.boolean  "18",          default: false
    t.boolean  "19",          default: false
    t.boolean  "20",          default: false
    t.boolean  "21",          default: false
    t.boolean  "22",          default: false
    t.boolean  "23",          default: false
    t.boolean  "24",          default: false
    t.boolean  "monday",      default: false
    t.boolean  "tuesday",     default: false
    t.boolean  "wednesday",   default: false
    t.boolean  "thursday",    default: false
    t.boolean  "friday",      default: false
    t.boolean  "saturday",    default: false
    t.boolean  "sunday",      default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "request_informations", force: true do |t|
    t.string   "ip"
    t.text     "user_agent"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "request_logs", force: true do |t|
    t.integer  "campaign_id"
    t.integer  "offer_id"
    t.string   "transaction_id"
    t.string   "datetime"
    t.string   "currency"
    t.float    "dealer_revenue"
    t.string   "device_id"
    t.text     "original_url"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "dealer_id"
    t.boolean  "unique"
    t.string   "remote_ip"
    t.string   "request_type"
    t.string   "country",        default: "N/A"
    t.float    "tariff",         default: 0.0
    t.string   "seller_name"
  end

  create_table "restricted_offer_country_sets", force: true do |t|
    t.integer "offer_id"
    t.integer "country_id"
  end

  create_table "seller_balances", force: true do |t|
    t.integer  "seller_id"
    t.float    "waiting",             default: 0.0
    t.float    "confirmed",           default: 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "onlypay_redirect",    default: 0
    t.integer  "onlypay_subscribe",   default: 0
    t.integer  "onlypay_rebill",      default: 0
    t.integer  "onlypay_unsubscribe", default: 0
  end

  create_table "seller_increments", force: true do |t|
    t.integer  "seller_id"
    t.float    "waiting",             default: 0.0
    t.float    "confirmed",           default: 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "onlypay_redirect",    default: 0
    t.integer  "onlypay_subscribe",   default: 0
    t.integer  "onlypay_rebill",      default: 0
    t.integer  "onlypay_unsubscribe", default: 0
    t.integer  "hour"
  end

  create_table "seller_requests", force: true do |t|
    t.integer  "seller_id"
    t.string   "seller_name"
    t.string   "seller_kind"
    t.text     "transaction_id"
    t.integer  "external_offer_id"
    t.integer  "app_installation_id"
    t.string   "subscription_action"
    t.string   "phone"
    t.integer  "external_sub_id"
    t.integer  "subscribe_id"
    t.float    "revenue_from_request"
    t.integer  "buyer_request_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "rebill_id"
    t.integer  "campaign_id"
    t.integer  "offer_id"
    t.integer  "dealer_id"
    t.integer  "country_id"
    t.text     "sub_id"
    t.text     "sub_id2"
    t.text     "sub_id3"
    t.integer  "conversion_id"
    t.float    "offer_dealer_revenue"
    t.integer  "postback_id"
  end

  create_table "sellers", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "seller_name"
    t.string   "seller_url"
    t.string   "contact_name"
    t.string   "contact_phone"
    t.string   "contact_email"
    t.boolean  "active"
    t.string   "provider_system_name"
    t.string   "kind"
    t.string   "prefix"
  end

  create_table "session_showed_offers", force: true do |t|
    t.integer  "buyer_session_id",             null: false
    t.integer  "campaign_id",                  null: false
    t.integer  "dealer_id",                    null: false
    t.integer  "offer_id",                     null: false
    t.integer  "show_counter",     default: 0, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "session_showed_offers", ["buyer_session_id", "campaign_id"], name: "showed_offers", using: :btree

  create_table "sidebar_notifications", force: true do |t|
    t.integer  "unread_tickets_count", default: 0
    t.integer  "dealer_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "subscribes", force: true do |t|
    t.integer  "dealer_id"
    t.string   "phone"
    t.integer  "rebill_count",           default: 0
    t.boolean  "active",                 default: true
    t.integer  "sub_id",       limit: 8, default: 0
    t.string   "country"
    t.string   "operator"
    t.string   "platform"
    t.string   "billing"
    t.string   "country_code"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "campaign_id"
    t.integer  "offer_id"
    t.boolean  "buyoutable",             default: true
    t.boolean  "buyouted",               default: false
    t.datetime "rebilled_at"
  end

  create_table "tariffs", force: true do |t|
    t.string   "name"
    t.float    "dealer_percent"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tb_referrers", force: true do |t|
    t.integer  "dealer_id"
    t.integer  "campaign_id"
    t.text     "referrer"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "reason"
  end

  create_table "ticket_messages", force: true do |t|
    t.integer  "dealer_id"
    t.integer  "ticket_id"
    t.text     "message_text"
    t.string   "ownership"
    t.integer  "number"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tickets", force: true do |t|
    t.text     "title",           default: ""
    t.integer  "dealer_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "message"
    t.boolean  "closed",          default: false
    t.integer  "message_counter"
    t.string   "last_answered"
    t.boolean  "read",            default: true
  end

  create_table "traffic_backs", force: true do |t|
    t.integer  "campaign_id"
    t.integer  "dealer_id"
    t.string   "ip"
    t.string   "country"
    t.string   "traffic_back_url"
    t.string   "request"
    t.text     "user_agent"
    t.string   "os"
    t.string   "reason"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "buyer_request_id"
  end

  create_table "user_domains", force: true do |t|
    t.string   "url",            default: ""
    t.integer  "campaign_id"
    t.boolean  "wapclick_ready", default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "wallets", force: true do |t|
    t.integer  "dealer_id"
    t.string   "kind"
    t.string   "number"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "pending",             default: false
    t.string   "name"
    t.string   "receiver"
    t.text     "receiver_address"
    t.string   "bank_name"
    t.text     "bank_address"
    t.string   "bank_city"
    t.string   "bank_country"
    t.string   "account_number_iban"
    t.string   "swift_code"
    t.text     "details"
  end

end
