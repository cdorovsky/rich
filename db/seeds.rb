require 'yaml'

require File.expand_path('../seeds/yml_parser', __FILE__)

YmlParser.parse( :sellers )
YmlParser.parse( :offers )
YmlParser.parse( :countries )

country = Country.all.sample

dealer = Dealer.new(
  name:                   'UserName',
  email:                  'user@example.com',
  phone:                  '380633541234',
  skype:                  'jumdp2kill',
  password:               '12345678',
  password_confirmation:  '12345678',
)
wallet = Wallet.create( kind: 'WMZ', number: 'Z303295799594', pending: false, name: 'Главный кошелек' )
dealer.wallets << wallet
dealer.save!

dealer_balance = DealerBalance.create(
  dealer: dealer,
  basic_currency: 'USD',
  waiting: 100.0,
  confirmed: 50.0,
  requested: 50.0,
  for_checkout: 35.0
)

campaign = Campaign.new(
name:                  'CampaignName',
dealer:                dealer,
enabled:               true,
trafficback_url:       'http://www.google.com',
russia:                true,
europe:                true,
asia:                  true,
america:               true,
m_trafficback_url:     'http://www.google.com/mobile/',
australia:             false,
adult:                 false,
megafon_buyout:        false,
beeline_buyout:        false,
mts_buyout:            true,
sent_postback:         false,
request_kind:          'get',
arbitrage:             false,
archived:              false,
offer_scheme:          'cost',
wapclick_ready:        false,
countries_count:       1,
disable_weblandings:   false,
broken_offer_behavior: 'traffic_back'
)
user_domain = UserDomain.create( url: 'http://domain.fm', wapclick_ready: false )
campaign.user_domains << user_domain
campaign.save!

campaign.offers << Offer.ipad.sample
campaign.offers << Offer.ipod_iphone.sample
campaign.offers << Offer.android.sample
#campaign.offers << Offer.mobile.sample
#campaign.offers << Offer.pc.sample


conversions = []
campaign.offers.each do |offer|
  7.downto( 1 ).each do | w |
    24.downto( 1 ).each do | h |
      i = (w * h)

      current_sin = Math.sin( 6 * h ) + 1
      increase_visitors = rand(15)
      amount_sum = current_sin * increase_visitors
      DealerStat.trigger( :visitor,                campaign: campaign, amount: amount_sum, offer: offer, current: i.hour.ago.to_i )
      DealerStat.trigger( :uniq_visitor,           campaign: campaign, amount: (amount_sum / 2).to_i, offer: offer, current: i.hour.ago.to_i )
      DealerStat.trigger( :js_visitor,             campaign: campaign, amount: amount_sum, offer: offer, current: i.hour.ago.to_i )
      DealerStat.trigger( :uniq_js_visitor,        campaign: campaign, amount: (amount_sum / 2).to_i, offer: offer, current: i.hour.ago.to_i )

      increase_5_percents = increase_visitors * 0.1
      DealerStat.trigger( :app_install,            campaign: campaign, amount: amount_sum, offer: offer, current: i.hour.ago.to_i )
      DealerStat.trigger( :uniq_traf_back,         campaign: campaign, amount: amount_sum, current: i.hour.ago.to_i )

      conversion_params = { mongo_id:               BSON::ObjectId.new.to_s,
                            offer:                  offer,
                            campaign:               campaign,
                            dealer:                 dealer,
                            offer_name:             offer.name,
                            dealer_revenue:         dealer.dealer_revenue( offer ),
                            campaign_name:          campaign.name,
                            country:                country,
                            country_name:           country.try( :name ) || '',
                            country_code:           country.try( :abbr ) || '',
                            kind:                   offer.kind,
                            conversion_reason:      :install,
                            crypted_id:             Conversion.generate_uniq_crypted_id,
                            triggered_at:           i.hour.ago,
                            triggered_at_timestamp: i.hour.ago.to_i }


      conversion = Conversion.new conversion_params
      conversions << conversion
    end
  end
end

Conversion.import conversions

faq = Faqs.create(
  question: 'This`s very nice question!'
)

# landing_comment_set = LandingCommentSet.create(
# 	offer: Offer.find_by(id: 7)
# )

# landing_heading = LandingHeading.create(
# 	body: 'This`s Landing Heading body'
# )

# landing_heading_set = LandingHeadingSet.create(
# 	landing_heading: landing_heading,
# 	offer:           Offer.find_by(id: 7)
# )

# offer_comment =  OfferComment.create(
# 	body:      'This body belongs to Offer Comment',
# 	rating:    5,
# 	app_kind:  'application'
# )

# offer_country_set = OfferCountrySet.create(
# 	offer:   Offer.find_by(id: 1),
# 	country: Country.find_by(abbr: 'ALL')
# )

# offer_disabler = OfferDisabler.create(
# 	offer:          Offer.find_by(id: 1),
# 	offer_disabled: false
# )

# offer_icon = OfferIcon.create(
# 	file:  '',
# 	offer: Offer.find_by(id: 1)
# )

# offer_limit = OfferLimit.create(
# 	offer: Offer.find_by(id: 1)
# )

# offer_screenshot = OfferScreenshot.create(
# 	offer: Offer.find_by(id: 1),
# )

# offer_set = OfferSet.create(
# 	offer:       Offer.find_by(id: 1),
# 	campaign:    campaign,
# 	dealer:      dealer,
# 	offer_kind:  'apps_advert',
# 	priority:    1,
# 	seller:      Seller.find_by(id: 1),
# 	seller_name: Seller.find_by(id: 1),
# 	apps_os:     'ios',
# 	offer_name:  Offer.find_by(id: 1),
# 	tb_url:      'http://www.google.com',
# 	m_tb_url:    'http://www.google.com/mobile'
# )

# landing_subscriber = LandingSubscriber.create(
# 	country:      Country.find_by(abbr: 'ALL'),
# 	active:       false,
# 	phone:        dealer.phone,
# 	os:           'MacBookPro',
# 	offer_set_id: offer_set
# )

# offer_set_show = OfferSetShow.create(
# 	dealer:        dealer,
# 	user_agent:    'Mozilla/5.0 (Linux; Android 5.0)',
# 	user_platform: 'android',
# 	country_name:  Country.find_by(name: 'ALL'),
# 	country:       Country.find_by(name: 'ALL'),
# 	country_code:  Country.find_by(abbr: 'ALL'),
# 	campaign:      campaign,
# 	offer:         Offer.find_by(id: 1),
# 	offer_set:     offer_set
# )

# offer_target = OfferTarget.create(
# 	offer:       Offer.find_by(id: 1),
# 	description: Offer.find_by(id: 1),
# )

# payment = Payment.create(
# 	payment_type: 'request',
# 	paid:         true
# )

# payment_request = PaymentRequest.create(
# 	dealer:       dealer,
# 	confirmed:    dealer_balance.confirmed,
# 	payment_type: payment.payment_type,
# 	wallet:       wallet,
# 	status:       'cancelled'
# )

# postback = Postback.create(
# 	postback_url: 'http://www.google.com',
# 	dealer:       dealer,
# 	campaign:     campaign,
# 	offer:        Offer.find_by(id: 1),
# 	country_name: Country.find_by(name: 'ALL'),
# 	country_code: Country.find_by(abbr: 'ALL'),
# 	datetime:     Time.now.strftime('%H:%M %d.%m.%Y'),
# 	country:      Country.find_by(name: 'ALL'),
# 	offer_name:   Offer.find_by(id: 1)
# )

# request_log = RequestLog.create(
# 	campaign:       campaign,
# 	offer:          Offer.find_by(id: 1),
# 	datetime:       postback.datetime,
# 	currency:       'USD',
# 	dealer:         dealer,
# 	country:        Country.find_by(name: 'ALL'),
# 	seller_name:    Seller.find_by(id: 1)
# )
Dealer.all.each { |dealer| dashboard = Dashboard.wrap_cached_regions( dealer.id ); $redis.set( "#{ dealer.id }_dashboard", dashboard.to_json ) }
