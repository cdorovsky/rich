class SupportMailer < ActionMailer::Base
  default from: 'front_page@richpays.com'

  def send_ticket( ticket )
    @ticket = OpenStruct.new( ticket )
    mail to: 'support@richpays.com', subject: 'Новый тикет с главный страницы.'
  end

  def send_answer_notification( ticket )
    @ticket = ticket
    @dealer = ticket.dealer
    if @dealer.notification_settings.ticket_email
      mail from: 'support@richpays.com', to: @dealer.email, subject: 'RichPays.com — Новый ответ на тикет.'
    end
  end

  def universal_notification( opts={} ) #email, subject, message
    @email =  opts[ :email ]
    @message = opts[ :message ]
    @subject = opts[ :subject ]
    mail from: 'support@richpays.com', to: @email, subject: @subject
  end

  def offers_yml( email )
    d = {};
    Offer.enabled.each do | o | 
    	d[ o.id ] = o.attributes.except( 'description', 'created_at', 'updated_at' )
    end
    offers_path = "#{ Rails.root }/tmp/offers.yml"
    File.open( offers_path, 'w') { | f | f.write d.to_yaml }
    attachments[ 'offers.yml' ] = File.read( offers_path )
    mail from: 'support@richpays.com', to: email, subject: 'Offers.yml'
  end

end
