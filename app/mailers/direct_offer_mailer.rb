class DirectOfferMailer < ActionMailer::Base
  default from: 'noreply@richpays.com'

  def self.mass_moderation_message( relation, params={} )
    dealers = Dealer.where( id: relation.pluck(:dealer_id) )
    relation.group_by( &:dealer_id ).each do | dealer_id, direct_offers |
      offers_params = direct_offers.map do | direct_offer |
        {
          id: direct_offer.offer_id,
          name: direct_offer.offer_name,
          cost: direct_offer.cost
        }
      end
      email_params = {
        dealer: dealers.find( dealer_id ),
        reason: params[ :reason ],
        approved: params[ :approved ],
        offers: offers_params
      }
      moderation_message( email_params ).deliver
    end.all?
  end

  def moderation_message( params={} )
    params.each do | key, value |
      instance_variable_set( "@#{ key }", value )
    end
    set_subject
    mail to: @dealer.email,
         subject: @subject,
         blocking_condition: @dealer.notification_settings.direct_offer_email
  end

  def set_subject
    many = @offers.many?
    @subject = "[RichPays] - Оффер#{ 'ы' if many } (ID: #{ @offers.map { |o| o[:id] }.join(', ') }) #{ 'не' unless @approved } активирован#{ 'ы' if many } для вашего аккаунта."
  end
end
