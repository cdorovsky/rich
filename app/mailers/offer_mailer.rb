class OfferMailer < ActionMailer::Base
  default from: 'noreply@richpays.com'

  def instant_offer_disabling( params = {} )
    @offer        = params[ :offer ]
    @dealer       = params[ :dealer ]
    @campaign_ids = params[ :campaign_ids ]
    mail to: @dealer.email,
          subject: "[RichPays] — Оффер (ID: #{ @offer.id }) остановился",
          blocking_condition: @dealer.notification_settings.offer_email
  end

  def plan_offer_disabling( params = {} )
    @offer        = params[ :offer ]
    @dealer       = params[ :dealer ]
    @campaign_ids = params[ :campaign_ids ]
    mail to: @dealer.email,
          subject: "[RichPays] — Оффер (ID: #{ @offer.id }) останавливается",
          blocking_condition: @dealer.notification_settings.offer_email
  end

  def offer_enable( params = {} )
    @offer        = params[ :offer ]
    @dealer       = params[ :dealer ]
    @campaign_ids = params[ :campaign_ids ]
    mail to: @dealer.email,
          subject: "[RichPays] — Оффер (ID: #{ @offer.id }) снова активен",
          blocking_condition: @dealer.notification_settings.offer_email
  end

  def daily_limit_offer_expiring( params = {} )
    @offer       = params[ :offer ]
    @dealer      = params[ :dealer ]
    @campaign_ids = params[ :campaign_ids ]
    mail from: 'support@richpays.com',
          to: @dealer.email,
          subject: "[RichPays] — Оффер (ID: #{ @offer.id }) скоро исчерпает дневной лимит установок",
          blocking_condition: @dealer.notification_settings.offer_email
  end

  def daily_limit_offer_exceeded( params = {} )
    @offer       = params[ :offer ]
    @dealer      = params[ :dealer ]
    @campaign_ids = params[ :campaign_ids ]
    mail from: 'support@richpays.com',
          to: @dealer.email,
          subject: "[RichPays] — Оффер (ID: #{ @offer.id }) исчерпал дневной лимит установок",
          blocking_condition: @dealer.notification_settings.offer_email
  end

  def mail( headers={}, &block )
    if headers[:blocking_condition]
      super
    else
      false
    end
  end
end
