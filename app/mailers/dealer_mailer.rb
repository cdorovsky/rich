class DealerMailer < Devise::Mailer
  default from: 'noreply@richpays.com'

  def reset_password_instructions( record, token, opts={} )
    opts[ :subject ] = 'RichPays.com — Восстановление Пароля'
    super
  end

  def successful_registration( dealer )
    @dealer = dealer
    mail to: @dealer.email, subject: 'RichPays.com — Добро Пожаловать!'
  end

  def custom_email( email, subject, body )
    @body = body
    mail to: email, subject: subject
  end
end
