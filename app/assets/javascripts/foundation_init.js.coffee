$ ->
  jQuery(document).foundation 
    tooltip:
      selector: ".has-tip"
      additional_inheritable_classes: []
      tooltip_class: ".tooltip"
      touch_close_text: "tap to close"
      disable_for_touch: false
      tip_template: (selector, content) ->
        _template = "<span data-selector=\"" + selector + "\" class=\""
        _template += Foundation.libs.tooltip.settings.tooltip_class.substring(1) 
        _template += "\">" + content + "<span class=\"nub\"></span></span>"
        _template
