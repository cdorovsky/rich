@Rich.module 'Entities', ( Entities, App, Backbone, Marionette, $, _ ) ->

  class Entities.SidebarNotifications extends Entities.Model
    url: '/sidebar'

  class Entities.Sidebar extends Entities.Model

  class Entities.SidebarCollection extends Entities.Collection
    model: Entities.Sidebar

  API = 
    getSidebarNotes: ->
      notes = new Entities.SidebarNotifications
      notes.fetch()
      notes

    getSidebar: ->
      new Entities.SidebarCollection [
        { name: 'Панель',     url: '/#/dashboard',        klass: 'dashboard', icon: 'icon-tb-panel'   }
        { name: 'Статистика', url: '/#/stats',            klass: 'stats', icon: 'icon-tb-stat'        }
        { name: 'Офферы',     url: '/#/offers',           klass: 'offers', icon: 'icon-st-cpa'        }
        { name: 'Кампании',   url: '/#/campaigns',        klass: 'campaigns', icon: 'icon-tb-company' }
        { name: 'Финансы',    url: '/#/money/wallets',    klass: 'money', icon: 'icon-tb-finance'     }
        # { name: 'Новости',    url: '/#/news',             klass: 'news', icon: 'icon-tb-news'         }
        { name: 'Рефералы',   url: '/#/referals',         klass: 'referals', icon: 'icon-tb-star'     }
        { name: 'FAQ',        url: '/#/faq',              klass: 'faq', icon: 'icon-tb-faq'           }
        { name: 'Поддержка',  url: '/#/tickets',          klass: 'tickets', icon: 'icon-tb-support'   }
        { name: 'Настройки',  url: '/#/settings/profile', klass: 'settings', icon: 'icon-tb-tool'     }
      ]

  App.reqres.setHandler 'sidebar:entities', ->
    API.getSidebar()

  App.reqres.setHandler 'sidebar:notes', ->
    API.getSidebarNotes()
