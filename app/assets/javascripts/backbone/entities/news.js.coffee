@Rich.module 'Entities', ( Entities, App, Backbone, Marionette, $, _ ) ->
  
  class Entities.News extends Entities.Model
    urlRoot: -> Routes.news_index_path()

  class Entities.NewsCollection extends Entities.Collection
    model: Entities.News
    url: -> Routes.news_index_path()

  API =
    getNews: ( cb )  ->
      news = new Entities.NewsCollection
      news.fetch
        success: ->
          cb news

    getNewsItem: ( id ) ->
      news_item = new Entities.News
        id: id
      news_item.fetch()
      news_item

  App.reqres.setHandler 'news:entities',  ( cb ) ->
    API.getNews cb

  App.reqres.setHandler 'news:entity', ( id ) ->
    API.getNewsItem id
