@Rich.module 'Entities', ( Entities, App, Backbone, Marionette, $, _ ) ->
  
  class Entities.DashAction extends Entities.Model
    urlRoot: -> Routes.dashboard_actions_path()

  class Entities.DashActionCollection extends Entities.Collection
    model: Entities.DashAction
    url: -> Routes.dashboard_actions_path()

    assignCountries: ->
      countries = App.countries
      App.execute 'when:fetched', countries, =>
        _.each @models, ( model ) ->
          country = _.findWhere( countries.get( 'countries' ), { id: model.get 'country_id' } )
          if country
            model.set( 'country_assigned', true )
            model.set( 'country_image_url', country.image_url )

  API =
    getDashActionEntities: ( cb ) ->
      action = new Entities.DashActionCollection
      action.fetch
        success: ->
          action.assignCountries()
          cb action

    getDashActionEntity: ( id ) ->
      action = new Entities.DashAction
        id: id
      action.fetch()
      action

  App.reqres.setHandler 'dash:actions:entities', ( cb ) ->
    API.getDashActionEntities cb

  App.reqres.setHandler 'dash:actions:entity', ( id ) ->
    API.getDashActionEntity id


