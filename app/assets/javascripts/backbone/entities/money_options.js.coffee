@Rich.module 'Entities', ( Entities, App, Backbone, Marionette, $, _ ) ->
  
  class Entities.MoneyOptions extends Entities.Model
    urlRoot: -> Routes.money_options_path()

  API =
    getOptions: ->
      options = new Entities.MoneyOptions
      options.fetch
        reset: true
      options

  App.reqres.setHandler 'get:money_options:entity', ->
    API.getOptions()