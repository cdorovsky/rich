@Rich.module 'Entities', ( Entities, App, Backbone, Marionette, $, _ ) ->

  class Entities.Balance extends Entities.Model
    urlRoot: -> Routes.dealer_balance_index_path()

  API =
    getBalanceEntity: ->
      balance = new Entities.Balance
      balance.fetch
        reset: true
      balance

  App.reqres.setHandler 'balance:entitiy', ->
    API.getBalanceEntity()
