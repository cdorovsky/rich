@Rich.module 'Entities', ( Entities, App, Backbone, Marionette, $, _ ) ->
  
  class Entities.Referals extends Entities.Model
    urlRoot: -> '/referrers'

  class Entities.ReferalsCollection extends Entities.Collection
    model: Entities.Referals
    url: -> '/referrers'

  API =
    getReferals: ( cb )  ->
      refs = new Entities.ReferalsCollection
      refs.fetch
        success: ->
          newrefs = new Entities.ReferalsCollection refs.sortBy('revenue').reverse()
          cb newrefs

    getReferal: ( id ) ->
      ref = new Entities.Referals
        id: id
      ref.fetch()
      ref

  App.reqres.setHandler 'referals:entities',  ( cb ) ->
    API.getReferals cb

  App.reqres.setHandler 'referals:entity', ( id ) ->
    API.getReferal id