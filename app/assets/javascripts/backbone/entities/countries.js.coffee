@Rich.module 'Entities', ( Entities, App, Backbone, Marionette, $, _ ) ->

  class Entities.Countries extends Entities.Model
    url: -> Routes.countries_path()

  API =
    getCountries: ->
      c = new Entities.Countries
      c.fetch()
      c

    getCountriesEntities: ( cb ) ->
      countries = new Entities.Countries
      countries.fetch
        success: ->
          cb countries

  App.reqres.setHandler 'get:countries', ->
    API.getCountries()

  App.reqres.setHandler 'countries:entities', ( cb ) ->
    API.getCountriesEntities cb
