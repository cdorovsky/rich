@Rich.module 'Entities', ( Entities, App, Backbone, Marionette, $, _ ) ->

  class Entities.Requests_history extends Entities.Model
    urlRoot: -> Routes.requests_history_index_path()

  class Entities.Requests_historyCollection extends Entities.Collection
    model: Entities.Requests_history
    url: -> Routes.requests_history_index_path()

  class Entities.MoneyHistory extends Entities.Model

    url: '/requests_history'

    specificCollection: ( attr ) ->
      ary = @get( attr )
      coll = new Entities.Collection ary
      for_sum = coll.map ( m ) ->
        ar_cond = ( attr == 'admin_requests' )
        pr_cond = ( attr == 'payment_requests' && m.get('status') == 'completed' )
        if ar_cond || pr_cond
          m.get 'value'
        else
          0
      sum_v = _.reduce for_sum, ((s,e)->s+e), 0
      sum = new Entities.Model
        sum: sum_v
      {
        collection: coll,
        model: sum
      }

  API =
    getRequests_history: ( cb ) ->
      requests_history = new Entities.Requests_historyCollection
      requests_history.fetch
        success: ->
          ary_of_req_money = _.map requests_history.models, ( m ) ->
            if m.get( 'status' ) == 'completed'
              m.get 'value'
            else
              0
          sum = _.reduce ary_of_req_money, ( ( s, el ) ->
            s + el
          ), 0
          requests_history.sum = new Entities.Model
            sum: sum
          cb requests_history

    getRequest_history: ( id ) ->
      request_history = new Entities.Requests_history
        id: id
      request_history.fetch()
      request_history

  App.reqres.setHandler 'requests_history:entities', ( cb ) ->
    API.getRequests_history cb

  App.reqres.setHandler 'request_history:entity', ( id ) ->
    API.getRequest_history id
