@Rich.module 'Entities', ( Entities, App, Backbone, Marionette, $, _ ) ->

  class Entities.Dashboard extends Entities.Model
    url: '/dashboard'

    balance: ->
      new Entities.Balance( @get( 'dealer_balance' ) )

    graph: ->
      new Entities.DashGraph( @get( 'graph' ) )

    dealer: ->
      dealerString = @get 'current_dealer'
      dealerJSON = JSON.parse( dealerString )
      new Entities.CurrentDealer( dealerJSON )

    conversions: ->
      conversions = @get 'conversions'
      conversionsJSON = JSON.parse( conversions )
      new Entities.DashActionCollection( conversionsJSON )

    campaigns: ->
      camps = @get 'campaigns'
      campsJSON = JSON.parse( camps )
      new Entities.DashCampaignCollection( campsJSON )

    wallets: ->
      wls = @get 'wallets'
      wlsJSON = JSON.parse( wls )
      new Entities.ConversionsCollection( wlsJSON )

    notes: ->
      notes = @get 'notifications'
      notesJSON = JSON.parse( notes )
      new Entities.NotesCollection( notesJSON )

  API =
    getDashboard: ->
      d = new Entities.Dashboard
      d.fetch()
      d

  App.reqres.setHandler 'get:dashboard', ->
    API.getDashboard()
