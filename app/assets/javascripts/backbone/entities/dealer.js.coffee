@Rich.module 'Entities', ( Entities, App, Backbone, Marionette, $, _ ) ->

  class Entities.Dealer extends Entities.Model

  class Entities.CurrentDealer extends Entities.Model
    url: '/current_dealer'

  class Entities.DealerCollection extends Entities.Collection
    model: Entities.Dealer

  API =
    setCurrentDealer: ( currentDealer ) ->
      new Entities.Dealer currentDealer

    getCurrentDealer: ->
      dealer = new Entities.CurrentDealer
      dealer.fetch()
      dealer

  App.reqres.setHandler 'set:current:dealer', ( currentDealer ) ->
    API.getCurrentDealer()
