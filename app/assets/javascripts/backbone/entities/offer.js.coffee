@Rich.module 'Entities', ( Entities, App, Backbone, Marionette, $, _ ) ->

  class Entities.Offer extends Entities.Model
    urlRoot: -> Routes.offers_path()

    initialize: ->
      @computedFields = new Backbone.ComputedFields this

    computed:
        readable_os:
          depends: [ 'apps_os' ]
          get: ( fields ) ->
            if fields.apps_os == 'android'
              'Android'
            else
              'iOS'
        readable_os_version:
          depends: [ 'os_version' ]
          get: ( fields ) ->
            if !_.isEmpty( fields.os_version ) && _.isString( fields.os_version )
              fields.os_version

  class Entities.OffersCollection extends Entities.PageableCollection
    model: Entities.Offer
    url: -> Routes.offers_path()

    prepare_for_direct_offers: ( direct_offers ) ->
      direct_offers.each ( direct_offer ) =>
        if @findWhere( { id: direct_offer.get('offer_id') } )
          extendable_attributes = { is_set: true, direct_offer_id: direct_offer.get('id') }
          _.each ['hashid', 'traffic_sources', 'approved', 'postback', 'postback_url', 'postback_action', 'offer_id'], (attr) ->
            extendable_attributes[attr] = direct_offer.get(attr)
          @findWhere( { id: direct_offer.get('offer_id') } ).set(extendable_attributes)

    prepare_for_offer_set: ( campaign ) ->
      @pager_query = campaign.pager_query
      if @pager_query
        @state = _.extend @state, @pager_query
      @fetch
        data: campaign.search_query
        success: =>
          raw_offer_sets = campaign.get 'offer_sets'
          offer_sets = new App.Entities.OfferSetsCollection raw_offer_sets
          set_offer_ids = offer_sets.pluck 'offer_id'
          offer_ids = @pluck 'id'
          set_offers = _.intersection set_offer_ids, offer_ids
          _.each set_offers, ( id ) =>
            @findWhere( { id: id } ).set
              offer_is_set: true,
              offer_set_id: offer_sets.findWhere( { offer_id: id } ).get( 'id' )
          @

  API =
    getOfferEntities: ( cb, adult ) ->
      offers = new Entities.OffersCollection
      offers.fetch
        success: ->
          cb offers

    getOffer: ( id ) ->
      offer = new Entities.Offer
        id: id
      offer.fetch()
      offer

  App.reqres.setHandler 'offer:entities', ( cb, adult ) ->
    API.getOfferEntities cb

  App.reqres.setHandler 'offer:entity', ( id ) ->
    API.getOffer id
