@Rich.module 'Entities', ( Entities, App, Backbone, Marionette, $, _ ) ->

  class Entities.DashGraph extends Entities.Model
    url: 'stats/dashboard_graph'

  class Entities.Stats extends Entities.Model
    initialize: ->
      @computedFields = new Backbone.ComputedFields this

    toGraphData: ->
      if @get( 'no_grouping' )
        [ @unixTimestamp(), @get( 'money_total' ) ]

    unixTimestamp: ->
      date = @get 'triggered_at'
      unixTime = Date.parse( date )
      Math.ceil(unixTime/1000)*1000+10800000

    computed:
        conversion_sum:
            depends: ['conversion_app_install', 'conversion_subscribe']
            get: ( fields ) ->
              cs = fields.conversion_app_install
              cs
        epc:
            depends: [
              'money_total',
              'uniq_visitor'
              ],
            get: ( fields ) ->
                epc = ( fields.money_total / fields.uniq_visitor ).toFixed( 4 )
                unless _.isFinite epc
                  epc = '0.0000'
                epc

        cr:
            depends: [
              'uniq_visitor',
              'conversion_app_install',
              'conversion_subscribe'
            ],
            get: ( fields ) ->
                ai = fields.conversion_app_install || 0
                sc = fields.conversion_subscribe || 0
                cr = ( ( ai + sc ) / fields.uniq_visitor ) * 100
                unless _.isFinite cr
                  cr = '0.00%'
                else
                  cr = cr.toFixed( 2 ) + '%'
                cr
        created:
            depends: ['created_at', 'monthly', 'hourly', 'daily']
            get: ( fields ) ->
              if fields.created_at
                date = fields.created_at.split( ' ' )[0]
                if fields.hourly
                  created = fields.created_at
                else
                  created = date
              else
                ''

  class Entities.StatsCollection extends Entities.Collection
    model: Entities.Stats
    url: -> Routes.table_dealer_stats_path()

    noGrouping: ->
      if @models[0]
        @models[0].get 'no_grouping'
      else
        false

    grouping: ->
      unless @models.length < 1
        if @models[0].get( 'offer_grouping' )
          'offer_grouping'
        else if @models[0].get( 'campaign_grouping' )
          'campaign_grouping'
        else
          'no_grouping'
      else
        'no_grouping'

    graphData: ->
      unless @grouping() == 'no_grouping'
        result = @models.map ( model ) ->
          id = model.get( 'campaign_id' ) || model.get( 'offer_id' )
          name = model.get( 'grouping_name' )
          if ( id && name )
            label = "[#{id}] #{ model.get( 'grouping_name' ) }"
          else
            if model.get( 'campaign_grouping' )
              label = "Без Кампании"
            else
              label = "Оффер"
          [ label, model.get( 'money_total' ) ]
        _.last(_.sortBy( result, (el) -> el[1] ), 10)
      else
        ary = @models.map ( model ) ->
          model.toGraphData()
        _.compact( ary.reverse() )

    numberTotal: ( key ) ->
      sum = 0
      for model in @models
        if typeof model.attributes[ key ] == 'number'
          sum += model.attributes[ key ]
      sum

    sumModel: =>
      resultModel = new Entities.Stats
        uniq_js_visitor: @numberTotal('uniq_js_visitor')
        uniq_visitor: @numberTotal('uniq_visitor')
        uniq_traf_back: @numberTotal('uniq_traf_back')
        conversion_app_install: @numberTotal('conversion_app_install')
        conversion_declined: @numberTotal('conversion_declined')
        money_total: @numberTotal('money_total')
        money_from_rebill: @numberTotal('money_from_rebill')
        money_from_buyout: @numberTotal('money_from_buyout')
        money_declined: @numberTotal('money_declined')
      if @models[0]
        resultModel.set 'common', @models[0].get 'common'
        resultModel.set 'apps_advert', @models[0].get 'apps_advert'
        resultModel.set 'no_grouping', @models[0].get 'no_grouping'
        resultModel.set 'campaign_grouping', @models[0].get 'campaign_grouping'
        resultModel.set 'offer_grouping', @models[0].get 'offer_grouping'
      else
        resultModel.set 'no_grouping', true
      resultModel

  API =
    getStatsEntities: ( cb ) ->
      stats = new Entities.StatsCollection
      stats.fetch
        success: ->
          cb stats

    getDashGraph: ->
      graph_data = new Entities.DashGraph
      graph_data.fetch()
      graph_data

  App.reqres.setHandler 'stats:entities', ( cb ) ->
    API.getStatsEntities cb

  App.reqres.setHandler 'get:dash:graph:data', ->
    API.getDashGraph()
