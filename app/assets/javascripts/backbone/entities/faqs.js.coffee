@Rich.module 'Entities', ( Entities, App, Backbone, Marionette, $, _ ) ->

  class Entities.Faqs extends Entities.Model

  class Entities.FaqsCollection extends Entities.Collection
    model: Entities.Faqs
    url: -> Routes.faqs_path()

  API =
    getFaqs: ->
      faqs = new Entities.FaqsCollection
      faqs.fetch
        reset: true
      faqs

    getFaq: ( id ) ->
      faq = new Entities.Faqs
        id: id
      faq.fetch()
      faq

  App.reqres.setHandler 'faqs:entities', ->
    API.getFaqs()
