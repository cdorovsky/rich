@Rich.module 'Entities', ( Entities, App, Backbone, Marionette, $, _ ) ->

  class Entities.Weblanding extends Entities.Model

  class Entities.Weblandings extends Entities.Collection
    model: Entities.Weblanding
    url: -> "/campaigns/#{ @id }/weblanding"

  class Entities.Campaign extends Entities.Model
    urlRoot: -> Routes.campaigns_path()

    initialize: ->
      @computedFields = new Backbone.ComputedFields this
      @search_query = {}
      @pager_query = {}

    computed:
        target_codes:
            depends: ['offers', 'redirect_codes']
            get: ( fields ) ->
              target_offers = _.where( fields.offers, target: true )
              target_offers_names  = _.pluck( target_offers, 'name' )
              if target_offers_names.length > 0
                target_codes = _.filter fields.redirect_codes, ( code ) ->
                  target_offers_names.indexOf( code.offer_name ) > -1
                target_codes
              else
                false

    defaults:
      disable_weblandings: false

    fetchCountries: ->
      App.request 'countries:entities', ( countries ) =>
        @set 'countries_hash', countries.get 'countries'

    addOfferSet: ( offer_set ) ->
      @attributes.offer_sets.push offer_set.attributes

    removeOfferSet: ( offer_set_id ) ->
      osets = @get 'offer_sets'
      newOfferSets = _.without osets, _.findWhere( osets, { id: offer_set_id } )
      @attributes.offer_sets = newOfferSets

    fetchOffers: ->
      @search_query.adult ||= @adultScope()
      offers = new Entities.OffersCollection
      offers.prepare_for_offer_set @
      offers

    adultScope: ->
      if @get 'adult'
        [ 'adult', 'adult_common' ]
      else
        [ 'adult_common', 'common' ]


  class Entities.CampaignsCollection extends Entities.Collection
    model: Entities.Campaign
    url: -> Routes.campaigns_path()

    scope: ( scope ) ->
      if scope == 'archived'
        models = @where
          archived: true
      else if scope == 'enabled'
        models = @where
          archived: false
      else
        models = @models

      new Entities.CampaignsCollection models

  API =
    getWeblandings: ( id ) ->
      wls = new Entities.Weblandings
      wls.id = id
      wls.fetch()
      wls

    getCampaignEntities: ( cb ) ->
      campaigns = new Entities.CampaignsCollection
      campaigns.fetch
        success: ->
          cb campaigns

    getCampaign: ( id ) ->
      campaign = new Entities.Campaign
        id: id
      campaign.fetch()
      campaign.fetchCountries()
      campaign

    newCampaign: ->
      new Entities.Campaign

  App.reqres.setHandler 'weblandings:entities', ( id ) ->
    API.getWeblandings id

  App.reqres.setHandler 'campaign:entities', ( cb ) ->
    API.getCampaignEntities cb

  App.reqres.setHandler 'campaign:entity', ( id ) ->
    API.getCampaign id

  App.reqres.setHandler 'new:campaign:entity', ->
    API.newCampaign()
