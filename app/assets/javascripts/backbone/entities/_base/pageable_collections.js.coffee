@Rich.module 'Entities', ( Entities, App, Backbone, Marionette, $, _ ) ->

  class Entities.PageableCollection extends Backbone.PageableCollection
    pagerModel: ->
      modelParams =
        current_page: @state.currentPage,
        total_pages:  @state.totalPages,
        page_size: @state.pageSize,
        total_entries: @state.totalRecords
      new Rich.Entities.Model modelParams
