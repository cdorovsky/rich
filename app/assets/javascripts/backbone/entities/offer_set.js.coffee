@Rich.module 'Entities', ( Entities, App, Backbone, Marionette, $, _ ) ->

  class Entities.OfferSet extends Entities.Model
    urlRoot: -> Routes.offer_sets_path()

  class Entities.OfferSetsCollection extends Entities.Collection
    model: Entities.OfferSet
    url: -> Routes.offer_sets_path()

  API =

    getOfferSet: ( id ) ->
      offer_set = new Entities.OfferSet
        id: id
      offer_set.fetch()
      offer_set

    newOfferSet: ->
      new Entities.OfferSet

  App.reqres.setHandler 'offer_set:entity', ( id ) ->
    API.getOfferSet id

  App.reqres.setHandler 'new:offer_set:entity', ->
    API.newOfferSet()