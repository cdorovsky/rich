@Rich.module 'Entities', ( Entities, App, Backbone, Marionette, $, _ ) ->

  class Entities.DashCampaign extends Entities.Model
    urlRoot: -> Routes.dashboard_campaigns_path()

    initialize: ->
      @computedFields = new Backbone.ComputedFields this

    computed:
      offers_plural:
        depends: [ 'offers_count' ]
        get: ( fields ) ->
            offer_suffix = ( fields.offers_count ).toString().slice( -2 )
            if offer_suffix == '1'
              offers_plural = 'оффер'
            else if [ '11', '12', '13' ].indexOf( offer_suffix ) != -1
              offers_plural = 'офферов'
            else if '234'.indexOf( offer_suffix ) != -1
              offers_plural = 'оффера'
            else
              offers_plural = 'офферов'
            offers_plural
      countries_plural:
        depends: [ 'countries_count' ]
        get: ( fields ) ->
            countries_suffix = ( fields.countries_count ).toString().slice( -2 )
            if countries_suffix == '1'
              countries_plural = 'страна'
            else if [ '11', '12', '13' ].indexOf( countries_suffix ) != -1
              countries_plural = 'стран'
            else if "234".indexOf( countries_suffix ) != -1
              countries_plural = 'страны'
            else
              countries_plural = 'стран'
            countries_plural

  class Entities.DashCampaignCollection extends Entities.Collection
    model: Entities.DashCampaign
    url: -> Routes.dashboard_campaigns_path()

    sortByRevenue: ->
      sorted = @sortBy( ( camp ) -> - camp.get 'monthly_income' )
      new Entities.DashCampaignCollection( sorted )

  API =
    getDashCampaignEntities: ( cb ) ->
      camp = new Entities.DashCampaignCollection
      camp.fetch
        success: ->
          cb camp

    getDashCampaignEntity: ( id ) ->
      camp = new Entities.DashCampaign
        id: id
      camp.fetch()
      camp

  App.reqres.setHandler 'dash:campaigns:entities', ( cb ) ->
    API.getDashCampaignEntities cb

  App.reqres.setHandler 'dash:campaigns:entity', ( id ) ->
    API.getDashCampaignEntity id