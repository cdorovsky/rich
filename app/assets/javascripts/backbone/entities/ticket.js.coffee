@Rich.module 'Entities', ( Entities, App, Backbone, Marionette, $, _ ) ->
  
  class Entities.Ticket extends Entities.Model
    urlRoot: -> Routes.tickets_path()

  class Entities.TicketsCollection extends Entities.Collection
    model: Entities.Ticket
    url: -> Routes.tickets_path()

  class Entities.TicketMessage extends Entities.Model
    urlRoot: -> Routes.ticket_messages_path()

  class Entities.TicketMessagesCollection extends  Entities.Collection
    model: Entities.TicketMessage
    url: -> "tickets/:ticket/ticket_messages"

  API =
    getTicketEntities: ( cb ) ->
      tickets = new Entities.TicketsCollection
      tickets.fetch
        success: ->
          sorted = new Entities.TicketsCollection tickets.sortBy( 'read', 'updated_at' )
          cb sorted

    getTicket: ( id ) ->
      ticket = new Entities.Ticket
        id: id
      ticket.fetch()
      ticket

    newTicket: ->
      new Entities.Ticket

    getTicketMessagesEntities: ( ticket_id ) ->
      ticketMessages = new Entities.TicketMessagesCollection
      ticket_id: ticket_id
      ticketMessages.fetch()
      ticketMessages

    getTicketMessage: ( id ) ->
      ticketMessage = new Entities.TicketMessage
        id: id
      ticketMessages.fetch()
      ticketMessages    

  App.reqres.setHandler 'ticket:entities', ( cb ) ->
    API.getTicketEntities cb

  App.reqres.setHandler 'ticket:entity', ( id ) ->
    API.getTicket id

  App.reqres.setHandler 'new:ticket:entity', ->
    API.newTicket()

  App.reqres.setHandler 'ticket:messages:entities', ( ticket_id ) ->
    API.getTicketMessagesEntities ticket_id

  App.reqres.setHandler 'ticket:message:entity', ( id ) ->
    API.getTicketMessage id