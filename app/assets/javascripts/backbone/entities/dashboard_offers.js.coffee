@Rich.module 'Entities', ( Entities, App, Backbone, Marionette, $, _ ) ->
  
  class Entities.DashOffer extends Entities.Model
    urlRoot: -> Routes.dashboard_offers_path()

  class Entities.DashOffersCollection extends Entities.Collection
    model: Entities.DashOffer
    url: -> Routes.dashboard_offers_path()

  API =
    getDashOffersEntities: ( cb ) ->
      offers = new Entities.DashOffersCollection
      offers.fetch
        success: ->
          cb offers

    getDashOffer: ( id ) ->
      offer = new Entities.DashOffer
        id: id
      offer.fetch()
      offer

  App.reqres.setHandler 'dash:offer:entities', ( cb ) ->
    API.getDashOffersEntities cb

  App.reqres.setHandler 'dash:offer:entity', ( id ) ->
    API.getDashOffer id