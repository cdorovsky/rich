@Rich.module 'Entities', ( Entities, App, Backbone, Marionette, $, _ ) ->

  class Entities.Request extends Entities.Model
    urlRoot: -> Routes.dashboard_create_request_path()

  API =
    newRequest: ->
      new Entities.Request

  App.reqres.setHandler 'new:request:entity', ->
    API.newRequest()
