@Rich.module 'Entities', ( Entities, App, Backbone, Marionette, $, _ ) ->
  
  class Entities.Subscribe extends Entities.Model
    url: 'stats/subscribes'

  API =
    getSubscribe: ->
      subscribe = new Entities.Subscribe
      subscribe.fetch()
      subscribe

  App.reqres.setHandler 'subscribe:entity', ->
    API.getSubscribe()
