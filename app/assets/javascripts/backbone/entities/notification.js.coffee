@Rich.module 'Entities', ( Entities, App, Backbone, Marionette, $, _ ) ->

  class Entities.Notification extends Entities.Model

  API =
    getNotification: ( msg, klass ) ->
      new Entities.Notification
        msg: msg
        klass: klass

  App.reqres.setHandler 'notification:entity', ( msg, klass ) ->
    API.getNotification( msg, klass )
