@Rich.module 'Entities', ( Entities, App, Backbone, Marionette, $, _ ) ->

  class Entities.Conversion extends Entities.Model
    urlRoot: 'stats/conversions'

  class Entities.ConversionsCollection extends Entities.PageableCollection
    model: Entities.Conversion
    url: 'stats/conversions'

    initialize: ->
      @queryParams.totalRevenue = 'total_revenue'

    pagerModel: ->
      modelParams =
        current_page: @state.currentPage,
        total_pages:  @state.totalPages,
        page_size: @state.pageSize,
        total_entries: @state.totalRecords,
        total_revenue: @state.totalRevenue
      new Rich.Entities.Model modelParams

    sumRevenues: ->
      s = _.map  @models, ( m ) ->
            m.get 'dealer_revenue' || 0
      s = _.reduce s, ( ( s, el ) ->
        s + el
      ), 0
      s.toFixed 2

  API =
    getConversionEntities: ( cb ) ->
      conversions = new Entities.ConversionsCollection
      conversions.fetch
        success: ->
          cb conversions

    getConversion: ( id ) ->
      conversion = new Entities.Conversion
        id: id
      conversion.fetch()
      conversion

  App.reqres.setHandler 'conversion:entities', ( cb ) ->
    API.getConversionEntities cb

  App.reqres.setHandler 'conversion:entity', ( id ) ->
    API.getConversion id
