@Rich.module 'Entities', ( Entities, App, Backbone, Marionette, $, _ ) ->
  
  class Entities.Wallet extends Entities.Model
    urlRoot: -> Routes.wallets_path()


  class Entities.WalletsCollection extends Entities.Collection
    model: Entities.Wallet
    url: -> Routes.wallets_path()

  API =
    getWalletEntities: ( cb ) ->
      wallets = new Entities.WalletsCollection
      wallets.fetch
        success: ->
          cb wallets

    getWallet: ( id ) ->
      wallet = new Entities.Wallet
        id: id
      wallet.fetch()
      wallet

    newWallet: ->
      new Entities.Wallet

  App.reqres.setHandler 'wallet:entities', ( cb ) ->
    API.getWalletEntities cb

  App.reqres.setHandler 'wallet:entity', ( id ) ->
    API.getWallet id

  App.reqres.setHandler 'new:wallet:entity', ->
    API.newWallet()
