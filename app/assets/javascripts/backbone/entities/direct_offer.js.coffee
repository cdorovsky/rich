@Rich.module 'Entities', ( Entities, App, Backbone, Marionette, $, _ ) ->

  class Entities.DirectOffer extends Entities.Model
    urlRoot: -> '/direct_offers'

  class Entities.DirectOffersCollection extends Entities.Collection
    url: '/direct_offers'
    model: Entities.DirectOffer

  API =
    getDealers: ( cb ) ->
      d = new Entities.DirectOffersCollection
      d.fetch
        success: ->
          cb d

    getDealer: ( id ) ->
      dealer = new Entities.DirectOffer
        id: id
      dealer.fetch()
      dealer

  App.reqres.setHandler 'direct:offer:entity', ( id ) ->
    API.getDealer id

  App.reqres.setHandler 'direct:offers:entities', ( cb ) ->
    API.getDealers cb
