@Rich.module 'Entities', ( Entities, App, Backbone, Marionette, $, _ ) ->

  class Entities.LightIndex extends Entities.Model
    url: '/campaigns_light'

  API =
    getLightIndexEntities: ( cb ) ->
      campaignsOffers = new Entities.LightIndex
      campaignsOffers.fetch
        success: ->
          cb campaignsOffers

  App.reqres.setHandler 'light:index:entities', ( cb ) ->
    API.getLightIndexEntities cb
