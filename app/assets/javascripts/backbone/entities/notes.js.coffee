@Rich.module 'Entities', ( Entities, App, Backbone, Marionette, $, _ ) ->
  
  class Entities.Notes extends Entities.Model
    urlRoot: -> Routes.nofications_path()

  class Entities.NotesCollection extends Entities.Collection
    model: Entities.Notes
    url: -> Routes.notifications_path()

  API =
    getNotesEntities: ( cb ) ->
      notes = new Entities.NotesCollection
      notes.fetch
        success: ->
          cb notes

    getNote: ( id ) ->
      note = new Entities.Notes
        id: id
      notes.fetch()
      notes

  App.reqres.setHandler 'notes:entities', ( cb ) ->
    API.getNotesEntities cb

  App.reqres.setHandler 'notes:entity', ( id ) ->
    API.getNote id
