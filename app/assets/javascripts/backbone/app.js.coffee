@Rich = do ( Backbone, Marionette ) ->

  App = new Marionette.Application

  App.on 'initialize:before', ( options ) ->
    @environment = options.environment
    @dashboard = App.request 'get:dashboard'
    @currentDealer = App.request 'set:current:dealer', options.currentDealer
    @countries = App.request 'get:countries'
    App.execute 'when:fetched', @countries, =>
      App.reqres.setHandler 'get:countries', ->
        App.countries

  App.reqres.setHandler 'get:current:dealer', ->
    App.currentDealer

  App.addRegions
    topbarRegion:  '#topbar-region'
    mainRegion:    '#main-region'
    sidebarRegion: '#sidebar-region'
    modalRegion:   '#modal-region'
    alertRegion:   '#alert-region'

  App.rootRoute = Routes.dashboard_index_path()

  App.addInitializer ->
    App.module( 'TopbarApp' ).start()
    App.module( 'SidebarApp' ).start()

  App.reqres.setHandler 'default:region', ->
    App.mainRegion

  App.commands.setHandler 'register:instance', ( instance, id ) ->
    App.register instance, id if App.environment is 'development'

  App.commands.setHandler 'unregister:instance', ( instance, id ) ->
    App.unregister instance, id if App.environment is 'development'

  App.on 'initialize:after', ->
    @startHistory()
    @navigate( @rootRoute, trigger: true ) unless @getCurrentRoute()

  App
