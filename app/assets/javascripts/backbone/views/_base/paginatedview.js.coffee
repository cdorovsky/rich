@Rich.module 'Views', ( Views, App, Backbone, Marionette, $, _ ) ->

  class Views.Paginator extends Marionette.Layout
    template: 'paginator/paginator'

    regions:
      collectionRegion:  '#collection-region'
      topPagerRegion:    '#top-pager-region'
      bottomPagerRegion: '#bottom-pager-region'

    events:
      'click a.page' : 'changePage'
      'click a.page-size' : 'changePageSize'

    onRender: ->
      @renderPages()

    renderPages: ->
      pagesView = new Views.Pages
        model: @model
      bpagesView = new Views.Pages
        model: @model
      @topPagerRegion.show pagesView
      @bottomPagerRegion.show bpagesView

    changePageSize: ( e ) ->
      page_size = $ e.currentTarget
        .data 'per-page'
      unless page_size == @model.get 'page_size'
        @model.set 'new_page_size', page_size
        @trigger 'page:resized', @model

    changePage: ( e ) ->
      target = $ e.currentTarget
      unless target.parent( 'li' ).hasClass 'unavailable'
        @model.set 'requested_page', target.data 'page'
        @trigger 'page:selected', @model

  class Views.Pages extends Marionette.ItemView
    template: 'paginator/pages'
