@Rich.module 'CampaignsApp.List', ( List, App, Backbone, Marionette, $, _ ) ->

  class List.Layout extends App.Views.Layout
    template: 'campaigns/list/templates/list_layout'

    regions:
      campaignsRegion: '#campaigns-region'
      controlsRegion: '#controls-region'

  class List.Campaign extends App.Views.ItemView
    template: 'campaigns/list/templates/_campaign'
    tagName: 'tr'

    attributes:
      'style' : 'border: none'

    modelEvents:
      'change' : 'render'

    events:
      'click .edit-campaign' : 'launchEdit'
      'click .expand-collapse': 'expandCollapse'
      'click .on-off': 'onOff'
      'click .promo' : ( e ) -> e.stopPropagation()
      'click a.offer' : 'viewOffer'
      'click a.archive' : 'archivate'

    archivate: ( e ) ->
      e.stopPropagation()
      @confirmMsg = ->
        unless @model.get 'archived'
          'Добавить кампанию в архив?'
        else
          'Вернуть кампанию из архива?'
      if confirm @confirmMsg()
        @trigger 'archivate', @model

    onBeforeRender: ->
      offer_suffix = @model.attributes.offers_count.toString().slice(-2)
      country_suffix = @model.attributes.countries_count.toString().slice(-2)

      if offer_suffix == '1'
        offers_plural = 'оффер'
      else if [ '11', '12', '13' ].indexOf(offer_suffix) != -1
        offers_plural = 'офферов'
      else if '234'.indexOf(offer_suffix) != -1
        offers_plural = 'оффера'
      else
        offers_plural = 'офферов'

      if country_suffix == '1'
        country_plural = 'страна'
      else if [ '11', '12', '13' ].indexOf(country_suffix) != -1
        country_plural = 'стран'
      else if "234".indexOf(country_suffix) != -1
        country_plural = 'страны'
      else
        country_plural = 'стран'

      @model.set 'countries_plural', country_plural
      @model.set 'offers_plural', offers_plural

    viewOffer: ( e ) ->
      offer_id = e.currentTarget
        .getAttribute 'data-id'
      @trigger 'view:offer', offer_id

    launchEdit: ( e ) ->
      unless @model.get 'archived'
        e.stopPropagation()
        @trigger 'ticket:edit:clicked', @model

    onOff: ( e ) ->
      unless @model.get 'archived'
        e.stopPropagation()
        @trigger 'enable:switch', @model

    expandCollapse: ( e ) ->
      unless @model.get 'archived'
        titleDiv = $ @el
          .find '.title-company'
        moreInfo = $ @el
          .find '.more-info'
        titleDiv
          .toggleClass 'active'
        if titleDiv.hasClass 'active'
          @trigger 'more:info:requested', @model
        moreInfo
          .toggleClass 'hide'

  class List.Empty extends App.Views.ItemView
    template: 'campaigns/list/templates/_empty'
    tagName: 'tr'
    attributes:
      'width' : '100%'

  class List.CampaignMoreInfo extends App.Views.ItemView
    template: 'campaigns/list/templates/_campaign_more_info'
    tagName: 'td'
    attributes:
      colspan: '6'

  class List.Campaigns extends App.Views.CompositeView
    template: 'campaigns/list/templates/_campaigns'
    itemViewEventPrefix: 'childview'
    itemView: List.Campaign
    emptyView: List.Empty
    itemViewContainer: 'tbody'

    onRender: ->
      $ @el
        .foundation()

  class List.Controls extends App.Views.ItemView
    template: 'campaigns/list/templates/_controls'

    events:
      'change .campaign-scope' : -> @trigger 'scope:set', @collection
