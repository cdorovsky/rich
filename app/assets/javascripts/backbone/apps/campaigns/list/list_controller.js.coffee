@Rich.module 'CampaignsApp.List', ( List, App, Backbone, Marionette, $, _ ) ->

  List.Controller =

    listCampaigns: ->
      App.request 'campaign:entities', ( campaigns ) =>

        @layout = @getLayoutView()

        @layout.on 'show', =>
          @showCampaigns campaigns.scope( 'enabled' )
          @showButtons campaigns

        App.mainRegion.show @layout

    showCampaigns: ( campaigns ) ->
      campaignsView = @getCampaignsView campaigns

      campaignsView.on 'childview:view:offer', ( campaign , offer_id ) =>
        offer = App.request 'offer:entity', offer_id
        App.execute 'when:fetched', offer, =>
          App.modalRegion.reset()
          offerView = new Rich.CampaignsApp.SetOffer.OfferView
            model: offer
          $ '#modal-wrapper'
            .removeClass 'small large'
              .addClass 'medium'
                .foundation 'reveal', 'open'
          App.modalRegion.show offerView
          $ offerView.el
            .foundation()

      campaignsView.on 'childview:archivate', ( child, campaign ) =>
        changedArchFlag = !child.model.get 'archived'
        child.model.save
          archived: changedArchFlag

      campaignsView.on 'childview:more:info:requested', ( child, campaign ) =>
        campaign = App.request 'campaign:entity', campaign.id
        App.execute 'when:fetched', campaign, =>
          moreInfoView = @getMoreInfoView campaign
          $ child.el
            .find '.more-info'
              .html moreInfoView.el

      campaignsView.on 'childview:campaign:edit:clicked', ( child, campaign ) ->
        App.vent.trigger 'campaign:edit:clicked', campaign

      campaignsView.on 'childview:enable:switch', ( child, campaign ) ->
        if campaign.get 'enabled'
          child.model.save {enabled: false},
            success: ->
              App.request 'notification:launch', 'Кампания отключена!', 'secondary'
        else
          child.model.save {enabled: true},
            success: ->
              App.request 'notification:launch', 'Кампания включена!'

      @layout.campaignsRegion.show campaignsView

    showButtons: ( campaigns ) ->
      controlsView = @getControlsView campaigns

      controlsView.on 'scope:set', ( campaigns ) =>
        scope = $ controlsView.el
          .find 'select.campaign-scope'
            .val()
        @showCampaigns campaigns.scope scope
      @layout.controlsRegion.show controlsView

    getCampaignsView: ( campaigns ) ->
      new List.Campaigns
        collection: campaigns

    getMoreInfoView: ( campaign ) ->
      v = new List.CampaignMoreInfo
        model: campaign
      v.render()
      v

    getControlsView: ( campaigns ) ->
      new List.Controls
        collection: campaigns

    getLayoutView: ->
      new List.Layout
