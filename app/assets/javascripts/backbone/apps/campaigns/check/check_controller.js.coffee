@Rich.module 'CampaignsApp.Check', ( Check, App, Backbone, Marionette, $, _ ) ->

  Check.Controller =

    check: ( id, campaign ) ->
      campaign or= App.request 'campaign:entity', id

      campaign.on 'updated', ->
        App.vent.trigger 'campaign:index', campaign

      App.execute 'when:fetched', campaign, =>

        if campaign.attributes.offer_sets.length == 0
          App.request 'notification:launch', 'Чтобы продолжить - необходимо выбрать как минимум один оффер', 'warning'
          alert 'Чтобы продолжить - необходимо выбрать как минимум один оффер'
          App.vent.trigger 'campaign:set_offer', campaign

        @layout = @getLayoutView campaign

        @layout.on 'show', =>
          @formRegion campaign

        App.mainRegion.show @layout

    formRegion: ( campaign ) ->

      checkView = @getCheckView campaign

      checkView.on 'form:cancel', ->
        App.vent.trigger 'campaign:publish', campaign

      formView = App.request 'form:wrapper', checkView,
        footer: false

      @layout.formRegion.show formView

    getLayoutView: ( campaign ) ->
      new Check.Layout
        model: campaign

    getCheckView: ( campaign ) ->
      new Check.Campaign
        model: campaign
