@Rich.module 'CampaignsApp.Check', ( Check, App, Backbone, Marionette, $, _ ) ->

  class Check.Layout extends App.Views.Layout
    template: 'campaigns/check/templates/check_layout'

    regions:
      formRegion: '#form-region'

  class Check.Campaign extends App.Views.Layout
    template: 'campaigns/check/templates/check'
