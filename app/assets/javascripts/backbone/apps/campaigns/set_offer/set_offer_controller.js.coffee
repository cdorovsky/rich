@Rich.module 'CampaignsApp.SetOffer', ( SetOffer, App, Backbone, Marionette, $, _ ) ->

  SetOffer.Controller =

    setOffer: ( id, campaign ) ->
      @campaign = App.request 'campaign:entity', id

      App.execute 'when:fetched', @campaign, =>

        @layout = @getLayoutView @campaign

        @layout.on 'show', =>
          @buildFilter @campaign
          @buildTable @campaign

        @layout.on 'hooked:proceed', ( id ) =>
          _campaign = App.request 'campaign:entity', id
          App.vent.trigger 'campaign:publish', _campaign

        App.mainRegion.show @layout

    buildFilter: ( campaign ) ->
      @filterView = @getFilterView campaign

      @filterView.on 'show', =>
        @buildTable @campaign

      @filterView.on 'apply:filter', ( campaign ) =>
        @buildTable campaign

      @layout.filterRegion.show @filterView

    buildTable: ( campaign ) ->
      offers = campaign.fetchOffers()
      App.execute 'when:fetched', offers, =>
        @buildPaginator offers

    buildPaginator: ( offers ) ->
      @paginator = @getPaginatorView offers.pagerModel()

      @table = @getOffersView offers

      @table.on 'childview:view:offer', ( offer ) =>
        App.modalRegion.reset()
        id = offer.model.get 'id'
        offer = App.request 'offer:entity', id
        App.execute 'when:fetched', offer, =>
          offerView = new SetOffer.OfferView
            model: offer
          $ '#modal-wrapper'
            .removeClass 'small large'
              .addClass 'medium'
          App.modalRegion.show offerView
          $ offerView.el
            .foundation()

      @table.on 'childview:set:offer', ( child ) =>
        offerSet = new App.Entities.OfferSet
          campaign_id: @campaign.get 'id'
          offer_id: child.model.get 'id'
        offerSet.save {},
          success: ( model, response ) =>
            child.model.set
              offer_is_set: true
              offer_set_id: response.id
            @campaign.addOfferSet offerSet
            child.render()
            App.request 'notification:launch', 'Оффер "' + child.model.get( 'name' ) + '" добавлен!', 'success'
          error: ( model, response ) =>
            App.request 'notification:launch', 'Ошибка' + response, 'error'

      @table.on 'childview:unset:offer', ( child ) =>
        if confirm 'Вы действительно хотите убрать оффер?'
          offer_set_id = child.model.get 'offer_set_id'
          offerSet = App.request 'offer_set:entity', offer_set_id
          offerSet.set 'removed', true
          offerSet.save()
          child.model.set
            offer_is_set: false
            offer_set_id: null
          @campaign.removeOfferSet offer_set_id
          child.render()
          App.request 'notification:launch', 'Оффер "' + child.model.get( 'name' ) + '" исключен!', 'secondary'
        else
          child.render()
          false

      @paginator.on 'show', =>
        @paginator.collectionRegion.show @table

        @paginator.on 'page:selected page:resized', ( pager ) =>

          requestedPage = pager.get 'requested_page'
          requestedPage ||= 1

          new_page_size = pager.get 'new_page_size'

          if new_page_size
            @campaign.pager_query.pageSize = new_page_size
            pager.unset 'new_page_size'

          @campaign.pager_query.currentPage = requestedPage
          @table.collection.prepare_for_offer_set @campaign
          @paginator.model = @table.collection.pagerModel()
          @paginator.renderPages()

      @layout.tableRegion.show @paginator

    getFilterView: ( campaign ) ->
      new SetOffer.Filter
        model: campaign

    getOffersView: ( offers ) ->
      new SetOffer.Offers
        collection: offers
        sort: false

    getPaginatorView: ( pager ) ->
      new App.Views.Paginator
        model: pager

    getLayoutView: ( campaign ) ->
      new SetOffer.Layout
        model: campaign
