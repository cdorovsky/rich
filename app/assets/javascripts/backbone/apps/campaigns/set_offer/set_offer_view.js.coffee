@Rich.module 'CampaignsApp.SetOffer', ( SetOffer, App, Backbone, Marionette, $, _ ) ->

  class SetOffer.Layout extends App.Views.Layout
    template: 'campaigns/set_offer/templates/set_offer_layout'

    regions:
      filterRegion: '#filter-region'
      tableRegion: '#offers-region'

    events:
      'click .proceed-to-next-step' : 'hookedProceed'


    hookedProceed: ->
      @trigger 'hooked:proceed', @model.get( 'id' )

  class SetOffer.Filter extends App.Views.ItemView
    template: 'campaigns/set_offer/templates/_set_offer_filter'

    events:
      'click .filter-offer'      : 'checkFilter'

    checkFilter: ->
      inps = $ '.filter'
      inps = _.map inps, ( inp ) ->
        $inp = $ inp
        [ inp.getAttribute( 'data-filter' ), $( $inp ).val() ]

      checks = $ '.filter-checkbox:checked'

      checks = _.map checks, ( check ) ->
        [ check.getAttribute( 'data-filter' ), true ]

      query = _.object inps.concat checks
      _.each query, ( val, key ) ->
        if query[key] == null || query[key].length == 0
          delete query[key]

      if query.countries
        query.countries.push 'All'

      @model.search_query = query
      @model.pager_query = {}
      @trigger 'apply:filter', @model

    onRender: ->
      $ @el
        .find 'select#countries'
          .select2
            maximumSelectionSize: 7,
            width: '100%',
            placeholder: 'Выберите страны'

      $ @el
        .foundation()

  class SetOffer.Empty extends App.Views.ItemView
    template: 'offers/list/templates/_empty'
    tagName: 'tr'

  class SetOffer.Offer extends App.Views.ItemView
    template: 'offers/list/templates/_offer'
    tagName: 'tr'

    hideButton: ->
      $ @el
        .find '.offer-button'
          .hide()
      $ @el
        .find '.loader'
          .show()

    modelEvents:
      'change' : 'render'

    events:
      'click .offer-button' : -> @hideButton()
      'click .add-offer'    : -> @trigger 'set:offer', @model
      'click .remove-offer' : -> @trigger 'unset:offer', @model
      'click .offer-info'   : -> @trigger 'view:offer', @model

  class SetOffer.Offers extends App.Views.CompositeView
    template: 'offers/list/templates/_offers'
    className: 'panel margin-b-10'
    itemView: SetOffer.Offer
    emptyView: SetOffer.Empty
    itemViewContainer: 'tbody'
    itemViewEventPrefix: 'childview'

    onRender: ->
      $ @el
        .foundation()

  class SetOffer.OfferView extends App.Views.ItemView
    template: 'offers/show/templates/_offer'
