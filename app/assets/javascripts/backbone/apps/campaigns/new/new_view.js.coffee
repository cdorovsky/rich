@Rich.module 'CampaignsApp.New', ( New, App, Backbone, Marionette, $, _ ) ->

  class New.Layout extends App.Views.Layout
    template: 'campaigns/new/new_layout'

    regions: 
      formRegion: '#form-region'

  class New.Campaign extends App.Views.Layout
    template: 'campaigns/templates/form'

    regions:
      userDomainsRegion: '#user-domains'

    events:
      'click .add-user-domain' : 'addUserDomain'
      'change #switch-postback' : 'pbSwitch'
      'click .postback' : -> @trigger 'show:postback:info'

    modelEvents:
      'change:_errors' : 'showButtons'

    showButtons: ->
      $ @el
        .find 'button'
          .show()
      $ @el
        .find '.loader'
          .hide()

    hideButtons: ->
      $ @el
        .find '.loader'
          .show()
      $ @el
        .find 'button'
          .hide()

    pbSwitch: ( e ) ->
      pbInputs = $ @el
        .find 'input.pb'

      if e.target.checked == true
        pbInputs
          .removeAttr 'disabled'
      else
        pbInputs
          .attr 'disabled', 'disabled'

    onRender: ->
      $ @el
        .find 'input.pb'
          .attr 'disabled', 'disabled'

    addUserDomain: ->
      @trigger 'add:user:domain'
      uds_count = $ '#user-domains'
        .find '.user-domain:not(.removed)'
          .length
      if uds_count == 5
        $ '.add-user-domain'
          .hide()

  class New.UserDomain extends App.Views.ItemView
    template: 'campaigns/templates/_user_domain'
    className: 'user-domain'

    events:
      'click .destroy-user-domain' : 'domainDestroy'

    domainDestroy: ->
      $ @el
        .slideUp 'fast', =>
            $ @el
              .addClass 'removed'
            $ '.add-user-domain'
              .show()
          .find '.destroy'
            .val 'true'
