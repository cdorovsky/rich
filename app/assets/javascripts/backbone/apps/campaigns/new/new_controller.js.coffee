@Rich.module 'CampaignsApp.New', ( New, App, Backbone, Marionette, $, _ ) ->

  New.Controller =

    newCampaign: ->
      @layout = @getLayoutView()
      campaign = App.request 'new:campaign:entity'
      @newView = @getNewView campaign

      @newView.on 'add:user:domain', =>
        dummy = new App.Entities.Model
          xid: window.randomInt()
        udView = @getUserDomainView dummy
        udView.render()
        $ @newView.userDomainsRegion.el
          .append udView.el

      @newView.on 'show:postback:info', =>
        postbackView = @getPostbackView()
        $ '#modal-wrapper'
          .removeClass 'small medium'
            .addClass 'large'
        App.modalRegion.show postbackView


      formView = App.request 'form:wrapper', @newView, footer: false

      formView.on 'show', =>
        $ document
          .foundation()

      App.mainRegion.show @layout

      @layout.formRegion.show formView

      formView.on 'form:submit', =>
        @newView.hideButtons()

      formView.on 'form:cancel', ->
        App.vent.trigger 'campaign:index', campaign

      campaign.on 'created', ->
        App.request 'notification:launch', 'Кампания создана!', 'success'
        App.vent.trigger 'campaign:set_offer', campaign

    getLayoutView: ->
      new New.Layout

    getNewView: ( campaign ) ->
      new New.Campaign
        model: campaign

    getUserDomainView: ( user_domain ) ->
      new New.UserDomain
        model: user_domain

    getPostbackView: ->
      new Rich.CampaignsApp.Edit.Postback
