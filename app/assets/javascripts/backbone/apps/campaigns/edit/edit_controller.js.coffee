@Rich.module 'CampaignsApp.Edit', ( Edit, App, Backbone, Marionette, $, _ ) ->

  Edit.Controller =

    editCampaign: ( id, campaign ) ->
      campaign or= App.request 'campaign:entity', id

      campaign.on 'updated', ->
        App.vent.trigger 'campaign:set_offer', campaign

      App.execute 'when:fetched', campaign, =>

        @layout = @getLayoutView campaign

        @layout.on 'show', =>
          @formRegion campaign

        App.mainRegion.show @layout

    userDomains: ( campaign ) ->
      user_domains = campaign.get 'user_domains'
      for user_domain, index in user_domains
        user_domain = new App.Entities.Model
          id: user_domain.id
          url: user_domain.url
          xid: index
        udView = @getUserDomainView user_domain
        udView.render()
        $ @editView.userDomainsRegion.el
          .append udView.el

    formRegion: ( campaign ) ->

      @editView = @getEditView campaign

      @editView.on 'add:user:domain', =>
        dummy = new App.Entities.Model
          xid: window.randomInt()
        udView = @getUserDomainView dummy
        udView.render()
        $ @editView.userDomainsRegion.el
          .append udView.el

      @editView.on 'show', =>
        @userDomains campaign

      @editView.on 'form:cancel', ->
        App.vent.trigger 'campaign:index', campaign

      formView = App.request 'form:wrapper', @editView,
        footer: false

      formView.on 'show', =>
        $ document
          .foundation()

      @layout.formRegion.show formView

      campaign.on 'updated', ->
        App.vent.trigger 'campaign:created', campaign

      @editView.on 'show:postback:info', =>
        postbackView = @getPostbackView()
        $ '#modal-wrapper'
          .removeClass 'small medium'
            .addClass 'large'
        App.modalRegion.show postbackView

    getLayoutView: ( campaign ) ->
      new Edit.Layout
        model: campaign

    getEditView: ( campaign ) ->
      new Edit.Campaign
        model: campaign

    getPostbackView: ->
      new Edit.Postback

    getUserDomainView: ( user_domain ) ->
      new Edit.UserDomain
        model: user_domain
