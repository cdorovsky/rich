@Rich.module 'CampaignsApp.Edit', ( Edit, App, Backbone, Marionette, $, _ ) ->

  class Edit.Layout extends App.Views.Layout
    template: 'campaigns/edit/edit_layout'

    regions: 
      formRegion: '#form-region'

  class Edit.Campaign extends App.Views.Layout
    template: 'campaigns/templates/form'
    regions:
      userDomainsRegion: '#user-domains'

    events:
      'click .postback' : -> @trigger 'show:postback:info'
      'click .add-user-domain' : 'addUserDomain'
      'change #switch-postback' : 'pbSwitch'

    pbSwitch: ( e ) ->
      pbInputs = $ @el
        .find 'input.pb'

      if e.target.checked == true
        pbInputs
          .removeAttr 'disabled'
      else
        pbInputs
          .attr 'disabled', 'disabled'

    onRender: ->
      pbInputs = $ @el
        .find 'input.pb'
      if @model.get( 'sent_postback' ) == false
        pbInputs
          .attr 'disabled', 'disabled'
      else
        pbInputs
          .removeAttr 'disabled'

    addUserDomain: ->
      @trigger 'add:user:domain'
      uds_count = $ '#user-domains'
        .find '.user-domain:not(.removed)'
          .length
      if uds_count == 5
        $ '.add-user-domain'
          .hide()

  class Edit.Postback extends App.Views.ItemView
    template: 'campaigns/edit/templates/_postback'

  class Edit.UserDomain extends App.Views.ItemView
    template: 'campaigns/templates/_user_domain'
    className: 'user-domain'

    events:
      'click .destroy-user-domain' : 'domainDestroy'

    onRender: ->
      if @model.get 'id'
        $ @el
          .find '.small-11'
            .removeClass 'small-11'
              .addClass 'small-12'
                .find 'input[type="url"]'
                  .attr 'disabled', 'disabled'
        $ @el
          .find '.small-1'
            .hide()

    domainDestroy: ->
      $ @el
        .slideUp 'fast', =>
            $ @el
              .addClass 'removed'
            $ '.add-user-domain'
              .show()
          .find '.destroy'
            .val 'true'
