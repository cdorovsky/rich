@Rich.module 'CampaignsApp.Publish', ( Publish, App, Backbone, Marionette, $, _ ) ->

  class Publish.Layout extends App.Views.Layout
    template: 'campaigns/publish/templates/publish_layout'

    regions:
      formRegion: '#form-region'
      weblandingRegion: '#weblanding-promo'

  class Publish.Campaign extends App.Views.Layout
    template: 'campaigns/publish/templates/publish'

    regions:
      textareaRegion: '#textarea-region'

    events:
      'click .tab-title' : 'setTab'
      'click .links-howto' : -> @trigger 'links:howto'
      'click .iframe-howto' : -> @trigger 'iframe:howto'
      'click .iframe-settings' : -> @trigger 'iframe:settings'
      'click .redirect-js-settings' : -> @trigger 'redirect:js:settings'
      'click .redirect-js-howto' : -> @trigger 'redirect:js:howto'
      'change .redirect-codes'    : 'changeLink'
      'change .target-codes'    : 'targetLink'

    changeLink: ( e ) ->
      link = $ e.currentTarget
        .val()
      $ @el
        .find '.offer-url'
          .val link

    targetLink: ( e ) ->
      link = $ e.currentTarget
        .val()
      $ @el
        .find '.target-offer-url'
          .val link

    setTab: ( e ) ->
      tab = $( e.currentTarget )
      tab.siblings().removeClass('active')
      tab.addClass('active')
      contentDivClass = tab.data('title')
      $('.'+contentDivClass).removeClass('hide').addClass('active').siblings().addClass('hide').removeClass('active')

  class Publish.Textarea extends App.Views.ItemView
    template: 'campaigns/publish/templates/textarea'
    tagName: 'textarea'
    attributes:
        'rows'    : '9'

  class Publish.LinksHowTo extends App.Views.ItemView
    template: 'campaigns/publish/templates/links_howto'

  class Publish.RedirectJsHowTo extends App.Views.ItemView
    template: 'campaigns/publish/templates/redirect_howto'

    onRender: ->
      @openModal()

    openModal: ->
      $ '#modal-wrapper'
        .foundation 'reveal', 'open'

  class Publish.RedirectJsSettings extends Publish.RedirectJsHowTo
    template: 'campaigns/publish/templates/redirect_settings'

    onRender: ->
      countrySel = $ @el
        .find '#restricted-countries'

      countrySel
        .select2
          maximumSelectionSize: 7,
          width: '100%',
          placeholder: 'Выберите страны'

      countrySel
        .select2 'val', @model.get 'restricted_countries'

      @openModal()

  class Publish.EmptyWl extends App.Views.ItemView
    template: 'campaigns/publish/templates/_empty_wls'
    tagName: 'li'

  class Publish.Weblanding extends App.Views.ItemView
    template: 'campaigns/publish/templates/_landing'
    tagName: 'li'
    events:
      'click .view-offer' : -> @viewOffer()

    viewOffer: ->
      @trigger 'view:offer', @model

  class Publish.Weblandings extends App.Views.CompositeView
    template: 'campaigns/publish/templates/landings'
    itemView: Publish.Weblanding
    itemViewContainer: '#landings'
    emptyView: Publish.EmptyWl
