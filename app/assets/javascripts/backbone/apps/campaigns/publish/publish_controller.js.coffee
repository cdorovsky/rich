@Rich.module 'CampaignsApp.Publish', ( Publish, App, Backbone, Marionette, $, _ ) ->

  Publish.Controller =

    publish: ( id, campaign, hide_controls ) ->
      campaign = App.request 'campaign:entity', id

      App.execute 'when:fetched', campaign, =>
        campaign.set 'hide_controls', hide_controls
        if campaign.attributes.offer_sets.length == 0
          App.request 'notification:launch', 'Чтобы продолжить - необходимо выбрать как минимум один оффер', 'warning'
          alert 'Чтобы продолжить - необходимо выбрать как минимум один оффер'
          App.vent.trigger 'campaign:set_offer', campaign

        @layout = @getLayoutView campaign

        @layout.on 'show', =>
          @formRegion campaign

        App.mainRegion.show @layout

    formRegion: ( campaign ) ->

      @publishView = @getPublishView campaign

      @publishView.on 'show', =>
        $ '#modal-wrapper'
          .removeClass 'small large'
            .addClass 'medium'
        @textareaRegion campaign
        if campaign.get 'has_landings'
          @buildLandings campaign.get 'id'

      @publishView.on 'form:cancel', ->
        App.vent.trigger 'campaign:set_offer', campaign

      @publishView.on 'form:submit', ->
        App.vent.trigger 'campaign:check', campaign

      @publishView.on 'links:howto', ->
        view = new Publish.LinksHowTo
        App.modalRegion.show view

      @publishView.on 'redirect:js:howto', ->
        ht_view = new Publish.RedirectJsHowTo
        App.modalRegion.show ht_view

      @publishView.on 'redirect:js:settings', ->
        set_view = new Publish.RedirectJsSettings
          model: campaign
        formView = App.request 'form:wrapper', set_view, footer: false
        formView.on 'form:submit', =>
          App.request 'notification:launch', 'Настройки сохранены.', 'info'
          $ '#modal-wrapper'
            .foundation 'reveal', 'close'

        App.modalRegion.show formView

      formView = App.request 'form:wrapper', @publishView,
        footer: false

      @layout.formRegion.show formView

    buildLandings: ( campaign_id ) ->
      landings = App.request 'weblandings:entities', campaign_id
      App.execute 'when:fetched', landings, =>
        wlsView = @getWeblandsView landings
        @layout.weblandingRegion.show wlsView

        wlsView.on 'childview:view:offer', ( wl ) =>
          App.modalRegion.reset()
          offer = App.request 'offer:entity', wl.model.get 'offer_id'
          App.execute 'when:fetched', offer, =>
            offerView = new Rich.CampaignsApp.SetOffer.OfferView
              model: offer
            App.modalRegion.show offerView
            $ offerView.el
              .foundation()
            $ '#modal-wrapper'
              .removeClass 'small large'
                .addClass 'medium'
                  .foundation 'reveal', 'open'


    textareaRegion: ( campaign ) ->
      textarea = new Publish.Textarea({model: campaign})
      @publishView.textareaRegion.show textarea

    getWeblandsView: ( landings ) ->
      new Publish.Weblandings
        collection: landings

    getLayoutView: ( campaign ) ->
      new Publish.Layout
        model: campaign

    getPublishView: ( campaign ) ->
      new Publish.Campaign
        model: campaign
