@Rich.module 'CampaignsApp', ( CampaignsApp, App, Backbone, Marionette, $, _ ) ->

  class CampaignsApp.Router extends Marionette.AppRouter
    appRoutes:
      'campaigns'               : 'indexCampaigns'
      'campaigns/new'           : 'newCampaign'
      'campaigns/:id/edit'      : 'editCampaign'
      'campaigns/:id/set_offer' : 'setOffer'
      'campaigns/:id/publish'   : 'publishCampaign'
      'campaigns/:id/check'     : 'checkCampaign'
      'campaigns/:id/promo'     : 'campaignPromo'

  API =
    indexCampaigns: ->
      CampaignsApp.List.Controller.listCampaigns()

    newCampaign: ->
      CampaignsApp.New.Controller.newCampaign()

    editCampaign: ( id ) ->
      CampaignsApp.Edit.Controller.editCampaign id

    setOffer: ( id ) ->
      CampaignsApp.SetOffer.Controller.setOffer id

    publishCampaign: ( id ) ->
      CampaignsApp.Publish.Controller.publish id

    checkCampaign: ( id ) ->
      CampaignsApp.Check.Controller.check id

    campaignPromo: ( id, options ) ->
      CampaignsApp.Promo.Controller.promo id, options

  App.vent.on 'campaign:edit:clicked campaign:edit', ( campaign ) ->
    App.navigate Routes.edit_campaign_path campaign.id
    API.editCampaign campaign.id, campaign

  App.vent.on 'campaign:set_offer', ( campaign ) ->
    App.navigate "campaigns/#{ campaign.id }/set_offer"
    API.setOffer campaign.id, campaign

  App.vent.on 'campaign:publish', ( campaign ) ->
    App.navigate "campaigns/#{ campaign.id }/publish"
    API.publishCampaign campaign.id, campaign

  App.vent.on 'campaign:check', ( campaign ) ->
    App.navigate "campaigns/#{ campaign.id }/check"
    API.checkCampaign campaign.id, campaign

  App.vent.on 'campaign:promo', ( campaign ) ->
    App.navigate "campaigns/#{ campaign.id }/promo"
    API.campaignPromo campaign.id, campaign

  App.vent.on 'campaign:index', ( campaign ) ->
    App.navigate Routes.campaigns_path()
    API.indexCampaigns()

  App.addInitializer ->
    r = new CampaignsApp.Router
      controller: API
    r.on 'route', ->
      window.setTitle 'Кампании'
    r
