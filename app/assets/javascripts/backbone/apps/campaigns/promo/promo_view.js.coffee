@Rich.module 'CampaignsApp.Promo', ( Promo, App, Backbone, Marionette, $, _ ) ->

  class Promo.Layout extends App.Views.Layout
    template: 'campaigns/promo/templates/promo_layout'

    regions:
      'textareaRegion' : '#textarea-region'
      'bannerRegion'   : '#banner-region'

    events:
      'click .redirect-howto' : -> @trigger 'redirect:howto'
      'click .banner-howto'   : -> @trigger 'banner:howto'
      'change .offer-link'    : 'changeLink'

    changeLink: ( e ) ->
      link = $ e.currentTarget
        .val()
      $ @el
        .find '.offer-url'
          .val link

  class Promo.Textarea extends App.Views.ItemView
    template: 'campaigns/promo/templates/textarea'
    tagName: 'textarea'
    attributes:
      'rows' : '7'

  class Promo.BannerChooser extends App.Views.ItemView
    template: 'campaigns/promo/templates/banner_chooser'

    events:
      'change .offer-select' : 'changeBanner'

    changeBanner: ( e ) ->
      _banner_url = $( e.currentTarget ).val()
      $( '.offer-banner' ).attr('src', _banner_url)
