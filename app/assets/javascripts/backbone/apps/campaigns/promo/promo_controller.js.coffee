@Rich.module 'CampaignsApp.Promo', ( Promo, App, Backbone, Marionette, $, _ ) ->

  Promo.Controller =

    promo: ( id, campaign ) ->
      hide_controls = true
      Rich.CampaignsApp.Publish.Controller.publish( id, campaign, hide_controls )
