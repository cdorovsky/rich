@Rich.module "NotificationsApp.Notify", ( Notify, App, Backbone, Marionette, $, _ ) ->

  Notify.Controller =

    notify: ( msg, klass ) ->
      notification = App.request "notification:entity", msg, klass
      notificationView = @getNotificationView notification
      notificationView.render()
      @showAlertRegion()
      $ App.alertRegion.el
        .prepend notificationView.el

    showAlertRegion: ->
      $ App.alertRegion.el
        .removeClass 'hide'

    getNotificationView:  ( notification ) ->
      new Notify.Notification
        model: notification
