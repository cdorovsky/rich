@Rich.module 'NotificationsApp.Notify', ( Notify, App, Backbone, Marionette, $, _ ) ->

  class Notify.Notification extends App.Views.ItemView
    template: 'notifications/notify/templates/notification'

    className: 'alert-box'

    events:
      'click .close' : -> $( @el ).remove()

    initialize: ->
      $ @el
        .addClass @model.get 'klass'
      $( @el ).delay(3000).fadeOut 400, -> $(this).remove()
      alertRegion = $ App.alertRegion.el
      if alertRegion.children().length == 0
        alertRegion.addClass 'hide'
