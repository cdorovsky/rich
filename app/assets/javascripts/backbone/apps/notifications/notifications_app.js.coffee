@Rich.module 'NotificationsApp', ( NotificationsApp, App, Backbone, Marionette, $, _ ) ->

  API =
    notify: ( msg, klass ) ->
      NotificationsApp.Notify.Controller.notify( msg, klass )

  App.reqres.setHandler 'notification:launch', ( msg, klass ) ->
     API.notify msg, klass
