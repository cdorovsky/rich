@Rich.module 'NewsApp.Show', ( Show, App, Backbone, Marionette, $, _ ) ->

  class Show.Layout extends App.Views.Layout
    template: 'news/show/templates/show_layout'

    regions:
      titleRegion:  '#top-region'
      newsRegion:   '#news-region'

  class Show.Title extends App.Views.ItemView
    template: 'news/show/templates/_title'

  class Show.ItemNews extends App.Views.ItemView
    template: 'news/show/templates/_item_news'
