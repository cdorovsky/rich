@Rich.module 'NewsApp.Show', ( Show, App, Backbone, Marionette, $, _ ) ->

  Show.Controller =

    show: ( id )  ->
      news_item = App.request 'news:entity', id

      App.execute 'when:fetched', news_item, =>

        @layout = @getLayoutView()

        @layout.on 'show', =>
          @titleRegion news_item
          @newsRegion news_item

        App.mainRegion.show @layout

    titleRegion: ( news_item ) ->
      titleView = @getTitleView news_item
      @layout.titleRegion.show titleView

    newsRegion: ( news_item ) ->
      newsView = @getNewsView news_item
      @layout.newsRegion.show newsView

    getNewsView: ( news_item ) ->
      new Show.ItemNews
        model: news_item

    getTitleView: ->
      new Show.Title
    
    getLayoutView: ->
      new Show.Layout
