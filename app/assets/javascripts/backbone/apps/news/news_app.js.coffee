@Rich.module 'NewsApp', ( NewsApp, App, Backbone, Marionette, $, _ ) ->

  class NewsApp.Router extends Marionette.AppRouter
    appRoutes:
      'news'     : 'list'
      'news/:id' : 'show'

  API =
    list: ->
      NewsApp.List.Controller.listNews()

    show: ( id ) ->
      NewsApp.Show.Controller.show id
    
  App.addInitializer ->
    r = new NewsApp.Router
      controller: API
    r.on 'route', ->
      window.setTitle 'Новости'
    r
