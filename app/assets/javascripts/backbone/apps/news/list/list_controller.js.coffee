@Rich.module 'NewsApp.List', ( List, App, Backbone, Marionette, $, _ ) ->

  List.Controller =

    listNews: ->
      App.request 'news:entities', ( news ) =>

        @layout = @getLayoutView()

        @layout.on 'show', =>
          @titleRegion news
          @newsRegion news

        App.mainRegion.show @layout

    titleRegion: ( news ) ->
      titleView = @getTitleView news
      @layout.titleRegion.show titleView

    newsRegion: ( news ) ->
      newsView = @getNewsView news
      @layout.newsRegion.show newsView

    getNewsView: ( news ) ->
      new List.News
        collection: news

    getTitleView: ->
      new List.Title
    
    getLayoutView: ->
      new List.Layout
