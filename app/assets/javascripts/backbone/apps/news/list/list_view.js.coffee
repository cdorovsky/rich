@Rich.module 'NewsApp.List', ( List, App, Backbone, Marionette, $, _ ) ->

  class List.Layout extends App.Views.Layout
    template: 'news/list/templates/list_layout'

    regions:
      titleRegion:  '#top-region'
      newsRegion:   '#news-region'

  class List.Title extends App.Views.ItemView
    template: 'news/list/templates/_title'

  class List.Empty extends App.Views.ItemView
    template: 'news/list/templates/_empty'

  class List.ItemNews extends App.Views.ItemView
    template: 'news/list/templates/_item_news'
    tagName: 'li'

  class List.News extends App.Views.CompositeView
    template: 'news/list/templates/_list_news'
    itemView: List.ItemNews
    emptyView: List.Empty
    itemViewContainer: 'ul'
