@Rich.module 'DashboardApp.List', ( List, App, Backbone, Marionette, $, _ ) ->

  List.Controller =

    listDashboard: ->
      @dashboard = App.dashboard
      @dashboard.fetch()
      App.execute 'when:fetched', @dashboard, =>
        @dealer = App.request('get:current:dealer')
        @layout = @getLayoutView()

        @layout.on 'show', =>
          @incomeRegion()
          @notesRegion()
          @campaignRegion()
          @actionRegion()
          @contactRegion()

        App.mainRegion.show @layout

    contactRegion: ->
      @contactView = @getContactView()

      @contactView.on 'show', =>
        @showSupport()

      @layout.contactRegion.show @contactView

    showSupport: ->
      ticket = App.request 'new:ticket:entity'
      supportView = @getSupportView ticket

      formView = App.request 'form:wrapper', supportView,
        footer: false

      formView.on 'form:submit', =>
        App.request 'notification:launch', 'Тикет создан!', 'info'

      @contactView.supportRegion.show formView

    actionRegion: ->
      conversions = @dashboard.conversions()
      conversions.assignCountries()
      actionView = @getActionView conversions
      @layout.actionRegion.show actionView

    campaignRegion: ->
      campaigns = @dashboard.campaigns().sortByRevenue()
      campaignView = @getCampaignView campaigns

      campaignView.on 'childview:promo:requested', ( campaign ) ->
        campaign.model.set 'from_dash', true
        App.vent.trigger 'campaign:promo', campaign.model

      @layout.campaignRegion.show campaignView

    notesRegion: ->
      notesView = @getNotesView @dashboard.notes()
      @layout.notesRegion.show notesView

    incomeRegion: ->
      balance = @dashboard.balance()
      @incomeView = @getIncomeView()
      @incomeView.on 'show', =>
        dash_graph_data = @dashboard.graph()
        graphView = new List.Graph
          model: dash_graph_data

        graphView.on 'show', ->
          @renderDashGraph()

        @incomeView.graphViewRegion.show graphView

        @showMoney balance
        @showRequest balance

      @layout.incomeRegion.show @incomeView

    showRequest: ( balance ) ->
      wallets = @dashboard.wallets()
      wallets = _.map wallets.models, ( wallet ) ->
        'id'  : wallet.get( 'id' ),
        'name': wallet.get( 'name' )

      for_checkout_without_requested = balance.get( 'for_checkout' ) - balance.get( 'requested' )
      max_value = Math.max( 0, for_checkout_without_requested )

      payment_request = App.request 'new:request:entity'
      payment_request.set wallets: wallets, max_value: max_value

      @requestView = @getRequestView payment_request

      @reqFormView = App.request 'form:wrapper', @requestView, footer: false

      @requestView.on 'create:request', =>
        hooked_form_data = Backbone.Syphon.serialize @reqFormView
        unless @reqFormView.model.get( 'max_value' ) < parseInt( hooked_form_data.payment_request_value )
          @reqFormView.model.save hooked_form_data,
            success: ( model, response ) =>
              App.request 'notification:launch', 'Выплата заказана!', 'action'
              $('#Cash').find('.close_modal').click() #ya ebal takoi modal %) todo: refactor to use global modal region
              new_dash = new App.Entities.Dashboard( response )
              @showMoney new_dash.balance()
              @showRequest new_dash.balance()
        else
          @reqFormView.model.set '_errors', { payment_request_value: ['Запрошенная сумма превышает допустимую!'] }

      @incomeView.requestRegion.show @reqFormView

    showMoney: ( balance ) ->
      App.execute 'when:fetched', balance, =>
        @moneyView = @getMoneyView balance
        @incomeView.moneyRegion.show @moneyView

    getRequestView: ( payment_request ) ->
      new List.Request
        model: payment_request

    getSupportView: ( ticket ) ->
      new List.Support
        model: ticket

    getContactView: ->
      new List.Contact

    getActionView: ( actions ) ->
      new List.Actions
        collection: actions

    getCampaignView: ( campaigns ) ->
      new List.Campaigns
        collection: campaigns

    getNotesView: ( notes ) ->
      new List.Notes
        collection: notes

    getMoneyView: ( balance ) ->
      new List.Money
        model: balance

    getIncomeView: ->
      new List.Income

    getLayoutView: ->
      new List.Layout
