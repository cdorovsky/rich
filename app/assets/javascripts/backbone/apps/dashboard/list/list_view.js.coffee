@Rich.module 'DashboardApp.List', ( List, App, Backbone, Marionette, $, _ ) ->

  class List.Layout extends App.Views.Layout
    template: 'dashboard/list/templates/_layout'

    regions:
      incomeRegion:   '#income-region'
      notesRegion:    '#notes-region'
      campaignRegion: '#campaign-region'
      actionRegion:   '#action-region'
      contactRegion:  '#contact-region'

  class List.Income extends App.Views.Layout
    template: 'dashboard/list/templates/income_layout'

    regions:
      graphViewRegion: '#dash-graph-view-region'
      moneyRegion:     '#money-region'
      requestRegion:   '#request-region'

  class List.Graph extends App.Views.Layout
    template: 'dashboard/list/templates/_graphs'

    regions:
      graphRegion: '#dash-graph-region'

    events:
      'change select.dashboard-graph-scope' : 'renderDashGraph'

    renderDashGraph: ->
      scope = $ @el
        .find 'select.dashboard-graph-scope'
          .val()
      series = @model.get scope
      dashboard_chartRender series, '#dash-graph-region'

  class List.Money extends App.Views.ItemView
    template: 'dashboard/list/templates/money'

    modelEvents:
      'change' : 'render'
      'fetch'  : 'render'

  class List.Request extends App.Views.ItemView
    template: 'dashboard/list/templates/new_request'

    events:
      'click .close_modal'  : 'closeModal'
      'click .save-request' : -> @trigger 'create:request'

    closeModal: ->
      $ '#Cash'
        .foundation 'reveal', 'close'

  class List.Support extends App.Views.ItemView
    template: 'dashboard/list/templates/support'
    events:
      'click .close_modal' : 'closeModal'

    closeModal: ->
      $ '#Support'
        .foundation 'reveal', 'close'


  class List.Note extends App.Views.ItemView
    template: 'dashboard/list/templates/note'
    tagName: 'li'

  class List.EmptyNote extends App.Views.ItemView
    template: 'dashboard/list/templates/_empty_note'

  class List.Notes extends App.Views.CompositeView
    template: 'dashboard/list/templates/notes'
    itemView: List.Note
    emptyView: List.EmptyNote
    itemViewContainer: 'ul.events-list'

  class List.Campaign extends App.Views.ItemView
    template: 'dashboard/list/templates/campaign'
    tagName: 'tr'

    events:
      'click .get-promo' : -> @trigger 'promo:requested', @model

  class List.EmptyCampaign extends App.Views.ItemView
    template: 'dashboard/list/templates/_empty_campaign'
    tagName: 'tr'

  class List.Campaigns extends App.Views.CompositeView
    template: 'dashboard/list/templates/campaigns'
    itemView: List.Campaign
    emptyView: List.EmptyCampaign
    itemViewContainer: 'tbody'

  class List.Action extends App.Views.ItemView
    template: 'dashboard/list/templates/transaction'
    tagName: 'tr'

    modelEvents:
      'change' : 'render'

  class List.EmptyAction extends App.Views.ItemView
    template: 'dashboard/list/templates/_empty_transaction'
    tagName: 'tr'

   class List.Actions extends App.Views.CompositeView
    template: 'dashboard/list/templates/transactions'
    itemView: List.Action
    emptyView: List.EmptyAction
    itemViewContainer: 'tbody'

   class List.Contact extends App.Views.Layout
    template: 'dashboard/list/templates/contact'
    className: 'panel'

    regions:
      supportRegion: '#support-region'
