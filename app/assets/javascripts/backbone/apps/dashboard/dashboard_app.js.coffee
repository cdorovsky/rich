@Rich.module 'DashboardApp', ( DashboardApp, App, Backbone, Marionette, $, _ ) ->

  class DashboardApp.Router extends Marionette.AppRouter
    appRoutes:
      'dashboard'  : 'list'

  API =
    list: ->
      DashboardApp.List.Controller.listDashboard()
    
    showSingleOffer: ( id ) ->
      DashboardApp.List.Controller.showSingleOffer id
  
  App.vent.on 'show:single:offer', (id) ->
    API.showSingleOffer id

  App.addInitializer ->
    r = new DashboardApp.Router
      controller: API
    r.on 'route', ->
      window.setTitle 'Панель'
    r
