@Rich.module 'StatsApp', ( StatsApp, App, Backbone, Marionette, $, _ ) ->

  class StatsApp.Router extends Marionette.AppRouter
    appRoutes:
      'stats': 'indexStats'
      'stats/conversions' : 'indexConversions'

  API =
    indexStats: ->
      StatsApp.List.Controller.listStats()

    indexConversions: ->
      StatsApp.Conversions.Controller.indexConversions()

  App.addInitializer ->
    r = new StatsApp.Router
      controller: API
    r.on 'route', ->
      window.setTitle 'Статистика'
    r
