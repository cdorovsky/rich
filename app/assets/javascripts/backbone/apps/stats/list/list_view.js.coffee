@Rich.module 'StatsApp.List', ( List, App, Backbone, Marionette, $, _ ) ->

  class List.Layout extends App.Views.Layout
    template: 'stats/list/templates/list_layout'
    tagName: 'form'

    regions:
      datepickerRegion:   '#datepicker-region'
      graphLinesRegion:   '#graph-lines-region'
      graphPieRegion:     '#graph-pie-region'
      typeselectorRegion: '#typeselector-region'
      tablesRegion:       '#tables-region'

    events:
      'click .filter-button'                  : 'dateRangeFilter'
      'change [name="date_scope"]'       : 'dateRangeFilter'
      'change [name="grouping"]'          : 'dateRangeFilter'
      'change .custom-datescope'           : 'noDatescope'

    setDatepickers: ( statParams ) ->
      dates = @dateScopeToDate statParams.date_scope
      $( 'input#date_from' ).val( dates.start.split( '-' ).reverse().join( '.' ) )
      $( 'input#date_to' ).val( dates.end.split( '-' ).reverse().join( '.' ) )

    dateScopeToDate: ( date_scope ) ->
      date = new Date
      date_end = date.getFullYear() + '-' + ( '0' + ( date.getMonth() + 1 ) ).slice(-2) + '-' + ( '0' + date.getDate() ).slice(-2)
      days_count = {
        day: 0,
        week: 7,
        month: 30,
        quarter: 90,
        year: 365
      }[ date_scope ]
      date.setDate( date.getDate() - days_count )
      date_start = date.getFullYear() + '-' + ( '0' + ( date.getMonth() + 1 ) ).slice(-2) + '-' + ( '0' + date.getDate() ).slice(-2)
      {
        start: date_start,
        end: date_end
      }

    paramsHash: ->
       statParams = Backbone.Syphon.serialize @
       unless statParams.date_scope == ''
         delete statParams.date_start
         delete statParams.date_end
         @setDatepickers statParams
       else
         delete statParams.date_scope
         statParams.date_start = statParams.date_start.split( '.' ).reverse().join( '-' )
         statParams.date_end = statParams.date_end.split( '.' ).reverse().join( '-' )
       statParams

    noDatescope: ->
      $ @el
        .find '.custom-datescope'
          .prop 'checked', true

    hideRangeFilterButton: ->
      $ @el
        .find '.filter-button'
          .hide()
      $ @el
        .find '.loader'
          .show()

    showRangeFilterButton: ->
      $ @el
        .find '.loader'
          .hide()
      $ @el
        .find '.filter-button'
          .show()

    dateRangeFilter: ->
      @hideRangeFilterButton()
      @triggerMethod 'filter:set'

  class List.SumStat extends App.Views.ItemView
    template: 'stats/list/templates/_sum_stat'
    tagName: 'tr'

  class List.Stat extends App.Views.ItemView
    template: 'stats/list/templates/_stat'
    tagName: 'tr'
    onBeforeRender: ->
      @model.attributes.offer_kind = $( 'li.active' ).data( 'filter' )

  class List.Empty extends App.Views.ItemView
    template: 'stats/list/templates/_empty'
    tagName: 'tr'

  class List.Stats extends App.Views.CompositeView
    tagName: 'table'
    className: 'shadow text-center'
    template: 'stats/list/templates/_stats'
    itemViewEventPrefix: 'childview'
    itemView: List.Stat
    emptyView: List.Empty
    itemViewContainer: 'tbody'

    onRender: ->
      $ @el
        .find 'thead'
          .foundation()

  class List.Graph extends App.Views.ItemView
    template: 'stats/list/templates/_graphs'
