@Rich.module 'StatsApp.List', ( List, App, Backbone, Marionette, $, _ ) ->

  List.Controller =

    listStats: ->
      @layout = @getLayoutView()
      @layout.on 'show', =>
        $( 'input#date_from' ).fdatepicker( format: 'dd.mm.yyyy' ).on('changeDate', (ev) => @layout.noDatescope()  )
        $( 'input#date_to' ).fdatepicker( format: 'dd.mm.yyyy' ).on('changeDate', (ev) => @layout.noDatescope()  )
      @layout.on 'filter:set show', =>
        @fetchStats()
      App.mainRegion.show @layout

    fetchStats: ->
      @stats = new App.Entities.StatsCollection
      @stats.fetch
        data: @layout.paramsHash()
        success: =>
          @showStats()
          chartRender( @stats.graphData(), @stats.grouping() )
          @layout.showRangeFilterButton()

    showStats: ->
      statsView = @getStatsView @stats
      @layout.tablesRegion.show statsView
      sumView = @getSumStatView @stats.sumModel()
      $ 'tfoot.sum'
        .html sumView.el

    getStatsView: ->
      gmodel = new App.Entities.Model( grouping: @stats.grouping() )
      new List.Stats
        collection: @stats
        model: gmodel

    getSumStatView: ( sums ) ->
      v = new List.SumStat
        model: sums
      v.render()
      v

    getLayoutView: ->
      new List.Layout
