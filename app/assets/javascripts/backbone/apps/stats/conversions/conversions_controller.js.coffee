@Rich
  .module 'StatsApp.Conversions', (
    Conversions, App, Backbone, Marionette, $, _
  ) ->

  Conversions.Controller =
    indexConversions: ->
      @layout = @getLayoutView()

      @layout.on 'show', =>
        @buildSidebar()
        App.request 'light:index:entities', ( index ) =>
          App.request 'countries:entities', ( countries ) =>
            index.set 'countries' , countries.get('countries')
            @buildFilter index

      App
        .mainRegion
          .show @layout

    buildSidebar: ->
      @sidebar = @getSidebarView()
      @layout
        .sidebarRegion
          .show @sidebar

    buildFilter: ( index ) ->
      @filter = @getFilterView index
      @filter.on 'show', =>
        _.each [ 'input.date-from', 'input.date-to' ], ( inp ) ->
          $ inp
            .fdatepicker
              format: 'dd.mm.yyyy'

      @filter.on 'filter show', =>
        @filter.hideButton()
        conversions = new App.Entities.ConversionsCollection
        conversions.queryParams = _.extend conversions.queryParams, @filter.getQuery()
        conversions.fetch()
        App.execute 'when:fetched', conversions, =>
          @filter.showButton()
          @buildTable conversions

      @layout
        .filterRegion
          .show @filter

    addResults: ( conversions ) ->
      resultEl =  '<ul class="inline-list">'
      resultEl +=   "<li>Показано: #{ conversions.models.length } из #{ conversions.pagerModel().get 'total_entries' } </li>"
      resultEl +=   "<li>Доход: $#{ conversions.pagerModel().get( 'total_revenue' ).toFixed( 2 ) }</li>"
      resultEl += '</ul>'
      $ '.found'
        .html resultEl

    buildTable: ( conversions ) ->
      @paginator = @getPaginatorView conversions
      @paginator.on 'show', =>
        @table = @getTableView conversions
        @paginator
          .collectionRegion
            .show @table
        @addResults conversions
      @layout
        .tableRegion
          .show @paginator

      @paginator.on 'page:selected page:resized', ( pager ) =>
        requestedPage = pager.get 'requested_page'
        if requestedPage
          @table.collection.getPage requestedPage
          @table.collection.fetch()
        if pager.get 'new_page_size'
          pager.set 'page_size', pager.get 'new_page_size'
          pager.unset 'new_page_size'
          @table.collection.state.currentPage = 1
          @table.collection.state.pageSize = pager.get( 'page_size' )
          @table.collection.fetch
            success: =>
              @addResults @table.collection
        @paginator.model = @table.collection.pagerModel()
        @paginator.renderPages()

    getFilterView: ( lightIndex ) ->
      new Conversions.Filter
        model: lightIndex

    getTableView: ( conversions ) ->
      new Conversions.Table
        collection: conversions

    getPaginatorView: ( conversions ) ->
      new App.Views.Paginator
        model: conversions.pagerModel()

    getSidebarView: ->
      new Conversions.Sidebar

    getLayoutView: ->
      new Conversions.Layout
