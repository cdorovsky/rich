@Rich
  .module 'StatsApp.Conversions', (
    Conversions, App, Backbone, Marionette, $, _
  ) ->

  class Conversions.Layout extends App.Views.Layout
    template: 'stats/conversions/templates/conversions_layout'
    regions:
      sidebarRegion: '#stats-sidebar'
      tableRegion:   '#conversions-table'
      filterRegion:  '#conversions-filter'

  class Conversions.Sidebar extends App.Views.ItemView
    template: 'stats/conversions/templates/_conversions_sidebar'

  class Conversions.Filter extends App.Views.ItemView
    template: 'stats/conversions/templates/_conversions_filter'
    tagName: 'form'

    getQuery: ->
      q = Backbone.Syphon.serialize @
      _.each q, (v, k) ->
        if !v
          delete q[k]
      q

    hideButton: ->
      $ @el
        .find '.filter'
          .hide()
      $ @el
        .find '.loader'
          .show()

    showButton: ->
      $ @el
        .find '.loader'
          .hide()
      $ @el
        .find '.filter'
          .show()

    modelEvents:
      'change' : 'render'

    events:
      'click .filter' : -> @trigger 'filter'

    onRender: ->
      $ @el
        .find 'select.country-list'
          .select2
            maximumSelectionSize: 7,
            width: '100%',
            placeholder: 'Выберите страны'
      $ @el
        .find 'select.offer-select'
          .select2
            maximumSelectionSize: 7,
            width: '100%',
            placeholder: 'Выберите офферы'

  class Conversions.Conversion extends App.Views.ItemView
    template: 'stats/conversions/templates/_conversion'
    tagName: 'tr'

  class Conversions.Empty extends Conversions.Conversion
    template: 'stats/conversions/templates/_empty_conversion'

  class Conversions.Table extends App.Views.CompositeView
    template: 'stats/conversions/templates/_conversions_table'
    tagName: 'table'
    itemViewEventPrefix: 'childview'
    className: 'shadow text-center margin-b-10'
    itemView: Conversions.Conversion
    emptyView: Conversions.Empty
    itemViewContainer: 'tbody'
