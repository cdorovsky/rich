@Rich.module 'ReferalsApp', ( ReferalsApp, App, Backbone, Marionette, $, _ ) ->

  class ReferalsApp.Router extends Marionette.AppRouter
    appRoutes:
      'referals' : 'list'

  API =
    list: ->
      ReferalsApp.List.Controller.listReferals()
    
  App.addInitializer ->
    r = new ReferalsApp.Router
      controller: API
    r.on 'route', ->
      window.setTitle 'Рефералы'
    r
