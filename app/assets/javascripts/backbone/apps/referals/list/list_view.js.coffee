@Rich.module 'ReferalsApp.List', ( List, App, Backbone, Marionette, $, _ ) ->

  class List.Layout extends App.Views.Layout
    template: 'referals/list/templates/list_layout'

    regions:
      leftRegion:  '#left-region'
      rightRegion: '#right-region'

  class List.Info extends App.Views.ItemView
    template: 'referals/list/templates/_info'

  class List.Empty extends App.Views.ItemView
    template: 'referals/list/templates/_empty'
    tagName: 'tr'

  class List.Ref extends App.Views.ItemView
    template: 'referals/list/templates/_ref'
    tagName: 'tr'

  class List.Refs extends App.Views.CompositeView
    template: 'referals/list/templates/_referals'
    itemView: List.Ref
    emptyView: List.Empty
    itemViewContainer: 'table'
