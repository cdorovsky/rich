@Rich.module 'ReferalsApp.List', ( List, App, Backbone, Marionette, $, _ ) ->

  List.Controller =

    listReferals: ->
      App.request 'referals:entities', ( refs ) =>

        @layout = @getLayoutView()

        @layout.on 'show', =>
          @leftRegion refs
          @rightRegion refs

        App.mainRegion.show @layout

    leftRegion: ( refs ) ->
      refsView = @getRefsView refs
      @layout.leftRegion.show refsView

    rightRegion: ( refs ) ->
      currentDealer = App.request 'get:current:dealer'
      App.execute 'when:fetched', currentDealer, =>
        infoView = @getInfoView currentDealer
        @layout.rightRegion.show infoView

    getRefsView: ( refs ) ->
      new List.Refs
        collection: refs

    getInfoView: ( currentDealer ) ->
      new List.Info
        model: currentDealer
    
    getLayoutView: ->
      new List.Layout
