@Rich.module "FaqApp", (FaqApp, App, Backbone, Marionette, $, _) ->

  class FaqApp.Router extends Marionette.AppRouter
    appRoutes:
      "faq"  : "list"

  API =
    list: ->
      FaqApp.List.Controller.listFaq()

  App.addInitializer ->
    r = new FaqApp.Router
      controller: API
    r.on 'route', ->
      window.setTitle 'FAQ'
    r
