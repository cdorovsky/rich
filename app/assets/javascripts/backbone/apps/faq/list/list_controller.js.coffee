@Rich.module "FaqApp.List", (List, App, Backbone, Marionette, $, _) ->

  List.Controller =

    listFaq: ->
      faqs = App.request 'faqs:entities'
    
      @layout = @getLayoutView()
    
      @layout.on 'show', =>
        @titleRegion faqs
        @faqRegion faqs
    
      App.mainRegion.show @layout
    
    titleRegion: ( faqs ) ->
      titleView = @getTitleView faqs
      @layout.titleRegion.show titleView
    
    faqRegion: (faqs)  ->
      faqView = @getFaqView faqs
      @layout.faqRegion.show faqView
    
    getFaqView: (faqs)  ->
      new List.Faqs
    
    getTitleView: ->
      new List.Title
    
    getLayoutView: ->
      new List.Layout
