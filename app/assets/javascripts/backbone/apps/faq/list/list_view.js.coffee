@Rich.module "FaqApp.List", (List, App, Backbone, Marionette, $, _) ->

  class List.Layout extends App.Views.Layout
    template: 'faq/list/templates/list_layout'

    regions:
      titleRegion: '#title-region'
      faqRegion:   '#faq-region'

  class List.Title extends App.Views.ItemView
    template: 'faq/list/templates/_title'

#  class List.Empty extends App.Views.ItemView
#    template: 'faq/list/templates/_empty'

#  class List.Faq extends App.Views.ItemView
#    template: 'faq/list/templates/_faq'
#
#    events:
#     'click .toggle' : 'showHidden'
#
#    showHidden: ( e ) ->
#      klass = '.answer' + e.currentTarget.id
#      $(klass).toggleClass('hide')  

  class List.Faqs extends App.Views.ItemView
    template: 'faq/list/templates/_list_faqs'

    events:
      'click .toggle' : 'showHidden'

    showHidden: ( e ) ->
      klass = '.panel' + e.currentTarget.id
      $(klass).toggleClass('hide')
