@Rich.module 'SettingsApp.Accounts', ( Accounts, App, Backbone, Marionette, $, _ ) ->

  class Accounts.Layout extends App.Views.Layout
    template: 'settings/accounts/templates/layout'

    regions: 
      accountsRegion: '#accounts-region'
