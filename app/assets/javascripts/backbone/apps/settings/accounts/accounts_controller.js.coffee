@Rich.module 'SettingsApp.Accounts', ( Accounts, App, Backbone, Marionette, $, _ ) ->

  Accounts.Controller =

    accounts: ->
      @layoutWrapper = new Accounts.Layout

      @layoutWrapper.on 'show', =>
        @wallets()

      App.mainRegion.show @layoutWrapper



    wallets: ->
      App.request 'wallet:entities', ( wallets ) =>

        @layout = @getLayoutView()
        @layout.on 'show', =>
          @showMoney wallets
          @showEdit()
          @showNew()

        @layoutWrapper.accountsRegion.show @layout

    showMoney: ( wallets ) ->
      @walletsView = @getWalletsView wallets
      @layout.moneyRegion.show @walletsView

    showNew: ->
      wallet = App.request 'new:wallet:entity'
      $('#New').foundation('reveal', 'close')

      @newLayout = @getNewLayoutView wallet

      @newLayout.on 'show', =>
        @formNewRegion wallet

      @layout.newRegion.show @newLayout

    formNewRegion: ( wallet ) ->
      newView = @getNewView wallet

      formView = App.request 'form:wrapper', newView,
        footer: false

      formView.on 'form:submit', =>
        @accounts()

      @newLayout.formRegion.show formView

    showEdit: ( id ) ->
      wallet = App.request 'wallet:entity', id
      $('#Edit').foundation('reveal', 'close')

      App.execute 'when:fetched', wallet, =>
        @editLayout = @getEditLayoutView wallet

        @editLayout.on 'show', =>
          @formEditRegion wallet

        @layout.editRegion.show @editLayout

    formEditRegion: ( wallet ) ->
      editView = @getEditView wallet

      formView = App.request 'form:wrapper', editView,
        footer: false

      formView.on 'form:submit', =>
        @listWallets()

      @editLayout.formRegion.show formView

    getNewLayoutView: ( wallet ) ->
      new App.MoneyApp.List.FormNewLayout
        model: wallet

    getNewView: ( wallet ) ->
      new App.MoneyApp.List.New
        model: wallet

    getEditLayoutView: ( wallet ) ->
      new App.MoneyApp.List.FormEditLayout
        model: wallet

    getEditView: ( wallet ) ->
      new App.MoneyApp.List.Edit
        model: wallet

    getWalletsView: ( wallets ) ->
      new App.MoneyApp.List.Wallets
        collection: wallets

    getButtonsView: ( wallets ) ->
      new App.MoneyApp.List.Buttons
        collection: wallets

    getModalNewView: ->
      new App.MoneyApp.List.NewModal

    getLayoutView: ->
      new App.MoneyApp.List.Layout
        template: 'settings/accounts/templates/sublayout'
        regions:
          editRegion: '#edit-region'

    
