@Rich.module 'SettingsApp.Notifications', ( Notifications, App, Backbone, Marionette, $, _ ) ->

  Notifications.Controller =

    notifications: ->

      dealer = App.request 'get:current:dealer'

      dealer.on 'updated', ->
        App.request 'notification:launch', 'Информация обновлена.', 'success'

      App.execute 'when:fetched', dealer, =>
        @layout = new Notifications.Layout

        @layout.on 'show', =>
          @formRegion dealer

        App.mainRegion.show @layout

    formRegion: ( dealer ) ->

      settingsView = @getSettingsView dealer

      formView = App.request 'form:wrapper', settingsView,
        footer: false

      @layout.formRegion.show formView

    getSettingsView: ( dealer ) ->
      new Notifications.Settings
        model: dealer
