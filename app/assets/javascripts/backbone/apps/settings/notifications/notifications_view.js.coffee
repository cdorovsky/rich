@Rich.module 'SettingsApp.Notifications', ( Notifications, App, Backbone, Marionette, $, _ ) ->

  class Notifications.Layout extends App.Views.Layout
    template: 'settings/notifications/templates/layout'

    regions: 
      formRegion: '#form-region'

  class Notifications.Settings extends App.Views.Layout
    template: 'settings/notifications/templates/settings'

