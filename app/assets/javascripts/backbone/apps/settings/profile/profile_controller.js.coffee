@Rich.module 'SettingsApp.Profile', ( Profile, App, Backbone, Marionette, $, _ ) ->

  Profile.Controller =

    profile: ->

      dealer = App.request 'get:current:dealer'

      dealer.on 'updated', ->
        App.request 'notification:launch', 'Информация обновлена.', 'success'
        $ App.modalRegion.el
          .foundation 'reveal', 'close'

      App.execute 'when:fetched', dealer, =>
        @layout = new Profile.Layout

        @layout.on 'show', =>
          @formRegion dealer

        App.mainRegion.show @layout

    formRegion: ( dealer ) ->

      dealerView = @getDealerView dealer

      dealerView.on 'number:change', =>
        App.vent.trigger 'set:phone:action'

      formView = App.request 'form:wrapper', dealerView,
        footer: false

      @layout.formRegion.show formView

    phoneChange: ->
      if $( Rich.mainRegion.el ).html() == ''
        App.vent.trigger 'go:to:settings'
        App.navigate 'settings/profile/phone'


      dealer = App.request 'get:current:dealer'
      App.execute 'when:fetched', dealer, =>
        numberChangeView = @getNumberChangeView dealer

        dealer.on 'updated', ->
          $ '#modal-wrapper'
            .foundation 'reveal', 'close'
          App.vent.trigger 'go:to:settings'

        numberChangeView.on 'sms:requested', =>
          dealerPhone = dealer.get 'phone'
          unless dealerPhone
            newPhone = $ '.new-phone'
              .val()
            smsRequestParams = '?new_phone=' + newPhone
          else
            smsRequestParams = ''

          smsRequest = $.get '/settings/phone_update.json' + smsRequestParams
          smsRequest
            .done ->
              $ numberChangeView.el
                .find '.phone-update'
                  .removeAttr 'readonly'

        @numberForm = App.request 'form:wrapper', numberChangeView,
          footer: false

        @numberForm.on 'show', =>
          $ '#modal-wrapper'
            .foundation 'reveal', 'open'

        $ '#modal-wrapper'
          .removeClass 'medium large'
            .addClass 'small'

        App
          .modalRegion
            .show @numberForm


    getNumberChangeView: ( dealer ) ->
      new Profile.NumberChange
        model: dealer

    getDealerView: ( dealer ) ->
      new Profile.DealerFields
        model: dealer
