@Rich.module 'SettingsApp.Profile', ( Profile, App, Backbone, Marionette, $, _ ) ->

  class Profile.Layout extends App.Views.Layout
    template: 'settings/profile/templates/layout'

    regions:
      formRegion: '#form-region'

  class Profile.NumberChange extends App.Views.ItemView
    template: 'settings/profile/templates/_number_change'
    events:
      'click .request-confirmation' : 'requestConfirmation'
      'change .country'             : 'setCountryCode'

    setCountryCode: ( e ) ->
      countryCode = e.currentTarget.value
      $ '.country-code'
        .text '+' + countryCode
      phoneMask =
        '371' : '9999-9999'
        '38'  : '(999) 999-99-99'
        '7'   : '(999) 999-99-99'
        '370' : '(999) 99-999'
        '375' : '(99) 999-99-99'
        '372' : '(99) 999-99-99'
        '358' : '999-99-99'
        '49'  : '(999) 9999-9999'
        '48'  : '(999) 999-99-99'
        '992' : '(99) 999-99-99'
        '972' : '(999) 999-99-99'
        '998' : '(99) 999-99-99'
        '996' : '(99) 999-99-99'
      $ @el
        .find '.new-phone-dummy'
          .mask phoneMask[countryCode]
            .removeAttr 'disabled'
              .attr 'placeholder', 'Формат: ' + phoneMask[countryCode]

    requestConfirmation: ->
      el = $ @el
      partialPhone = el
        .find '.new-phone-dummy'
          .val()
      countryCode = el
        .find '.country'
          .val()
      formattedPhone = '' + countryCode + partialPhone
      el
        .find '.new-phone'
          .val formattedPhone.replace /[^0-9]+/g, ''
      el
        .find '.request-button-container'
          .slideUp()
      el
        .find '.new-phone-dummy'
          .attr 'disabled', 'disabled'
      el
        .find '.confirm-code'
          .slideDown()
      el
        .find '.country'
          .attr 'disabled', 'disabled'
      @trigger 'sms:requested'

  class Profile.DealerFields extends App.Views.ItemView
    template: 'settings/profile/templates/_dealer'
    events:
      'click .change-phone' : -> @trigger 'number:change'

    modelEvents:
      'change' : -> @model.fetch()
