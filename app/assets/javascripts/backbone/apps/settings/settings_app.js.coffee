@Rich.module 'SettingsApp', ( SettingsApp, App, Backbone, Marionette, $, _ ) ->

  class SettingsApp.Router extends Marionette.AppRouter
    appRoutes:
      'settings/profile'       : 'profile'
      'settings/accounts'      : 'accounts'
      'settings/password'      : 'password'
      'settings/profile/phone' : 'phone'
      'settings/notifications' : 'notifications'

  API =
    
    profile: ->
      SettingsApp.Profile.Controller.profile()

    phone: ->
      SettingsApp.Profile.Controller.phoneChange()

    accounts: ->
      SettingsApp.Accounts.Controller.accounts()

    password: ->
      SettingsApp.Password.Controller.password()

    notifications: ->
      SettingsApp.Notifications.Controller.notifications()

  App.vent.on 'set:phone:action', ->
    App.navigate 'settings/profile/phone'
    API.phone()

  App.vent.on 'go:to:settings', ->
    App.navigate 'settings/profile'
    API.profile()

  App.addInitializer ->
    r = new SettingsApp.Router
      controller: API
    r.on 'route', ->
      window.setTitle 'Настройки'
    r
