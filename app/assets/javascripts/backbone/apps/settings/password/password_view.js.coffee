@Rich.module 'SettingsApp.Password', ( Password, App, Backbone, Marionette, $, _ ) ->

  class Password.Layout extends App.Views.Layout
    template: 'settings/password/templates/layout'

    regions: 
      formRegion: '#form-region'

  class Password.Fields extends App.Views.Layout
    template: 'settings/password/templates/_fields'

    onRender: ->
      $ @el
        .find 'input'
          .click ( e ) ->
            $ e.currentTarget
              .val ''