@Rich.module 'SettingsApp.Password', ( Password, App, Backbone, Marionette, $, _ ) ->

  Password.Controller =

    password: ->
      dealer = App.request 'get:current:dealer'

      dealer.on 'updated', ->
        App.request 'notification:launch', 'Информация обновлена.', 'success'

      App.execute 'when:fetched', dealer, =>
        @layout = new Password.Layout

        @layout.on 'show', =>
          @formRegion dealer

        App.mainRegion.show @layout

    formRegion: ( dealer ) ->

      fieldsView = @getFieldsView dealer

      formView = App.request 'form:wrapper', fieldsView,
        footer: false

      @layout.formRegion.show formView

    getFieldsView: ( dealer ) ->
      new Password.Fields
        model: dealer
