@Rich.module 'MoneyApp', (MoneyApp, App, Backbone, Marionette, $, _) ->

  class MoneyApp.Router extends Marionette.AppRouter
    appRoutes:
      'money/wallets'           :  'list'
      'money/history'           :  'listHistory'
      #'money/output'            :  'moneyOutput'

  API =
    list: ->
      MoneyApp.List.Controller.listWallets()

    newWallet: ->
      MoneyApp.List.Controller.showNew()

    editWallet: ( id ) ->
      MoneyApp.List.Controller.showEdit id

    #moneyOutput: ->
    #  MoneyApp.Output.Controller.moneyOutput()

    listHistory: ->
      MoneyApp.History.Controller.listHistory()

  App.vent.on 'money_options:created money_options:updated money_options:cancelled', ( wallet ) ->
    App.navigate '#/money/wallets'
    API.list()

  App.vent.on 'edit:wallet', (id) ->
    API.editWallet id

  App.vent.on 'new:wallet', ->
    API.editWallet()

  App.vent.on 'request:history', ->
    API.listHistory()

  App.addInitializer ->
    r = new MoneyApp.Router
      controller: API
    r.on 'route', ->
      window.setTitle 'Финансы'
    r
