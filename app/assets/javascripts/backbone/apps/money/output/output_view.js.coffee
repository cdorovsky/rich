@Rich.module 'MoneyApp.Output', ( Output, App, Backbone, Marionette, $, _ ) ->

  class Output.Layout extends App.Views.Layout
    template: 'money/output/templates/layout'

    regions:
      optionsRegion:  '#options-region'
      buttonRegion:   '#button-region'

  class Output.Options extends App.Views.Layout
    template: 'money/output/templates/options_layout'

    regions:
      formRegion: '#form-region'

  class Output.OptionsForm extends App.Views.ItemView
    template: 'money/output/templates/options_form'

    events:
      'click .chose' : 'changeFormParts'
      'change #switch-auto-payment' : 'toggleAutoPaymentForm'

    toggleAutoPaymentForm: ->
      $( '.auto-payment' ).toggleClass 'active'

    changeFormParts: ( e ) ->
      if $( e.currentTarget ).hasClass('value')
        $('.valued').removeClass('hide')
        $('.timed').addClass('hide')
      if $( e.currentTarget ).hasClass('time')
        $('.timed').removeClass('hide')
        $('.valued').addClass('hide')

  class Output.Buttons extends App.Views.ItemView
    template: 'money/output/templates/buttons'
