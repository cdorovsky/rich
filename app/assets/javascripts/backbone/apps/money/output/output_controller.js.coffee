@Rich.module 'MoneyApp.Output', ( Output, App, Backbone, Marionette, $, _ ) ->

  Output.Controller =

    moneyOutput: ->

      @layout = @getLayoutView()
      @layout.on 'show', =>
        dealer = App.request 'get:current:dealer'
        App.execute 'when:fetched', dealer, =>
          unless dealer.get('phone')
            App.vent.trigger 'set:phone:action'
        @showOptions()
        @showButtons()

      App.mainRegion.show @layout

    showOptions: ->

      money_options = App.request 'get:money_options:entity'

      money_options.on 'updated', ->
        App.request 'notification:launch', 'Настройки вывода средств обновлены', 'success'
        App.vent.trigger 'money_options:updated', money_options
      
      App.execute 'when:fetched', money_options, =>

        @optionsLayout = @getOptionsView money_options
 
        @optionsLayout.on 'show', =>
          @formRegion money_options

        @layout.optionsRegion.show @optionsLayout

    formRegion: ( money_options ) ->
      optionsFormView = @getOptionsFormView money_options

      optionsFormView.on 'form:cancel', ->
        App.vent.trigger 'money_options:cancelled', money_options

      formView = App.request 'form:wrapper', optionsFormView,
        footer: false

      @optionsLayout.formRegion.show formView

    showButtons: ->
      buttonsView = @getButtonsView()
      @layout.buttonRegion.show buttonsView

    getLayoutView: ->
      new Output.Layout

    getOptionsView: ( money_options ) ->
      new Output.Options
        model: money_options

    getOptionsFormView: ( money_options ) ->
      new Output.OptionsForm
        model: money_options

    getButtonsView: ->
      new Output.Buttons
