@Rich.module 'MoneyApp.List', (List, App, Backbone, Marionette, $, _) ->

  class List.Layout extends App.Views.Layout
    template: 'money/list/templates/list_layout'

    regions:
      moneyRegion:  '#money-region'
      buttonRegion: '#button-region'
      editRegion:   '#edit-region'
      newRegion:    '#new-region'

  class List.FormNewLayout extends App.Views.Layout
    template: 'money/list/templates/new_layout'

    regions:
      formRegion: '#form-region'

  class List.New extends App.Views.ItemView
    template: 'money/list/templates/form_wallet'
    initialize: ->
      @model.attributes.isNew = true
    events:
      'click .close_modal' : 'closeModal'
      'change .kind'       : 'checkVal'

    checkVal: ->
      val = $ @el
        .find 'select'
          .prop 'value'
      @model.set('kind', val)
      @render()

    closeModal: ->
      $('#New').foundation('reveal', 'close')

  class List.FormEditLayout extends App.Views.Layout
    template: 'money/list/templates/edit_layout'

    regions:
      formRegion: '#form-region'

  class List.Edit extends App.Views.ItemView
    template: 'money/list/templates/form_wallet'
    events:
      'click  .close_modal' : 'closeModal'
      'change select' : 'checkVal'

    checkVal: ->
      val = $ @el
        .find 'select'
          .prop 'value'
      @model.set('kind', val)
      @render()

    closeModal: ->
      $('#Edit').foundation('reveal', 'close')

  class List.Buttons extends App.Views.ItemView
    template: 'money/list/templates/buttons'

  class List.Empty extends App.Views.ItemView
    template: 'money/list/templates/_empty'

  class List.Wallet extends App.Views.ItemView
    template: 'money/list/templates/wallet'
    tagName: 'li'

    events:
      'click .edit_wallet' : 'showEdit'

    showEdit: ( e ) ->
      id = e.currentTarget.id
      App.vent.trigger 'edit:wallet', id

  class List.Wallets extends App.Views.CompositeView
    template: 'money/list/templates/list_wallets'
    itemView: List.Wallet
    emptyView: List.Empty
    itemViewContainer: 'ul.money_list'

    events:
      'click a.create' : -> @trigger 'create:wallet'
