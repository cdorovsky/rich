@Rich.module 'MoneyApp.List', (List, App, Backbone, Marionette, $, _) ->

  List.Controller =

    listWallets: ->
      App.request 'wallet:entities', ( wallets ) =>

        @layout = @getLayoutView()
        @layout.on 'show', =>
          @showMoney wallets
          @showButtons wallets
          @showEdit()

        App.mainRegion.show @layout

    showMoney: ( wallets ) ->
      @walletsView = @getWalletsView wallets
      @walletsView.on 'create:wallet', =>
        @showNew wallets
      @layout.moneyRegion.show @walletsView

    showButtons: ( wallets ) ->
      buttonsView = @getButtonsView wallets
      @layout.buttonRegion.show buttonsView

    showNew: ( wallets ) ->
      dealer = App.request 'get:current:dealer'
      App.execute 'when:fetched', dealer, =>
        if wallets.length < dealer.get('webmoney_wallets_count')
          wallet = App.request 'new:wallet:entity'
          $('#New').foundation('reveal', 'close')

          @newLayout = @getNewLayoutView wallet

          @newLayout.on 'show', =>
            $('#New').foundation('reveal', 'open')

            @formNewRegion wallet

          @layout.newRegion.show @newLayout
        else
          alert 'Добавление нового WMZ кошелька не доступно. Обратитесь к вашему менеджеру.'

    formNewRegion: ( wallet ) ->
      newView = @getNewView wallet

      formView = App.request 'form:wrapper', newView,
        footer: false

      wallet.on 'created', ->
        $( '.close_modal' ).trigger 'click'
        App.request 'notification:launch', 'Кошелек создан!', 'success'
        App.vent.trigger 'money_options:created'

      @newLayout.formRegion.show formView

    showEdit: ( id ) ->
      wallet = App.request 'wallet:entity', id
      $('#Edit').foundation('reveal', 'close')

      App.execute 'when:fetched', wallet, =>
        @editLayout = @getEditLayoutView wallet

        @editLayout.on 'show', =>
          @formEditRegion wallet

        App.modalRegion.show @editLayout

    formEditRegion: ( wallet ) ->
      editView = @getEditView wallet

      formView = App.request 'form:wrapper', editView,
        footer: false

      wallet.on 'updated', ->
        $( '.close_modal' ).trigger 'click'
        App.request 'notification:launch', 'Кошелек обновлен!', 'success'
        App.vent.trigger 'money_options:updated'

      @editLayout.formRegion.show formView

    getNewLayoutView: ( wallet ) ->
      new List.FormNewLayout
        model: wallet

    getNewView: ( wallet ) ->
      new List.New
        model: wallet

    getEditLayoutView: ( wallet ) ->
      new List.FormEditLayout
        model: wallet

    getEditView: ( wallet ) ->
      new List.Edit
        model: wallet

    getWalletsView: ( wallets ) ->
      new List.Wallets
        collection: wallets

    getButtonsView: ( wallets ) ->
      new List.Buttons
        collection: wallets

    getModalNewView: ->
      new List.NewModal

    getLayoutView: ->
      new List.Layout

    
