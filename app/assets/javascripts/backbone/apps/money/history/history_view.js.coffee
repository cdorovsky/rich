@Rich.module 'MoneyApp.History', ( History, App, Backbone, Marionette, $, _ ) ->

  class History.Layout extends App.Views.Layout
    template: 'money/history/templates/layout'

    regions:
      paymentRegion:   '#money-region'
      adminRegion: '#admin-region'
      buttonRegion:  '#button-region'
      prSumRegion: '.pr-tfoot'
      arSumRegion: '.ar-tfoot'

  class History.Buttons extends App.Views.ItemView
    template: 'money/history/templates/buttons'

  class History.PaymentSumView extends App.Views.ItemView
    template: 'money/history/templates/_sum'
    tagName: 'tr'

  class History.AdminSumView extends App.Views.ItemView
    template: 'money/history/templates/_a_sum'
    tagName: 'tr'

  class History.PREmpty extends App.Views.ItemView
    template: 'money/history/templates/_pr_empty'

  class History.AREmpty extends App.Views.ItemView
    template: 'money/history/templates/_ar_empty'

  class History.PR extends App.Views.ItemView
    template: 'money/history/templates/_payment_request'
    tagName: 'tr'

  class History.AR extends History.PR
    template: 'money/history/templates/_admin_request'

  class History.PaymentRequests extends App.Views.CompositeView
    template: 'money/history/templates/payment_requests'
    itemView: History.PR
    emptyView: History.PREmpty
    itemViewContainer: 'table'

  class History.AdminRequests extends History.PaymentRequests
    template: 'money/history/templates/admin_requests'
    itemView: History.AR
    emptyView: History.AREmpty
    itemViewContainer: 'table'
