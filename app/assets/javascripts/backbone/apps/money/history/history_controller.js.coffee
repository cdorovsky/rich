@Rich.module 'MoneyApp.History', ( History, App, Backbone, Marionette, $, _ ) ->

  History.Controller =

    listHistory: ->
      @history = new App.Entities.MoneyHistory
      @history.fetch
        success: =>
          @paymentRequests = @history.specificCollection('payment_requests')
          @adminRequests = @history.specificCollection('admin_requests')
          @layout = @getLayoutView()
          @layout.on 'show', =>
            @showButtons()
            @showPRList()
            @showPRSum()
            @showARList()

          App.mainRegion.show @layout

    showButtons: ->
      @layout.buttonRegion.show @getButtonsView()

    showPRList: ->
      lv = @getPRLView @paymentRequests.collection
      @layout.paymentRegion.show lv

    showPRSum: ->
      sv = @getPRSView @paymentRequests.model
      @layout.prSumRegion.show sv

    showARList: ->
      lv = @getARLView @adminRequests.collection
      @layout.adminRegion.show lv

    showARSum: ->
      sv = @getARSView @adminRequests.model
      @layout.arSumRegion.show sv

    getPRLView: ( coll ) ->
      new History.PaymentRequests
        collection: coll

    getPRSView: ( model ) ->
      new History.PaymentSumView
        model: model

    getARLView: ( coll ) ->
      new History.AdminRequests
        collection: coll

    getARSView: ( model ) ->
      new History.AdminSumView
        model: model

    getButtonsView: ->
      new History.Buttons

    getLayoutView: ->
      new History.Layout

    
