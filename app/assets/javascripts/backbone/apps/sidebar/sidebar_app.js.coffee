@Rich.module "SidebarApp", (SidebarApp, App, Backbone, Marionette, $, _) ->
  @startWithParent = false

  API =
    listSidebar: ->
      SidebarApp.List.Controller.listSidebar()

  SidebarApp.on "start", ->
    API.listSidebar()

  App.reqres.setHandler "rebuild:sidebar", ->
    API.listSidebar()
