@Rich.module "SidebarApp.List", ( List, App, Backbone, Marionette, $, _ ) ->

  List.Controller =

    listSidebar: ->
      @nav_links = App.request "sidebar:entities"
      @notes = App.request 'sidebar:notes'
      App.execute 'when:fetched', @notes, =>
        @nav_links.findWhere( klass: 'tickets' )
          .set( 'notes', @notes.get( 'unread_tickets_count' ) )

        sidebarView = @getSidebarView @nav_links
        App.sidebarRegion.show sidebarView

    getSidebarView:  ( nav_links ) ->
      new List.Sidebars
        collection: nav_links
