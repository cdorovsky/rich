@Rich.module 'TopbarApp.Show', ( Show, App, Backbone, Marionette, $, _ ) ->

  class Show.Topbar extends App.Views.Layout
    template: 'topbar/show/templates/show_topbar'

    modelEvents:
      'change' : 'render'

    regions:
      welcomeRegion: '#welcome-wrapper'

    tagName:   'nav'
    className: 'top-bar'
    attributes:
      'data-topbar' : ''

  class Show.Welcome extends App.Views.ItemView
    template: 'topbar/show/templates/welcome'
