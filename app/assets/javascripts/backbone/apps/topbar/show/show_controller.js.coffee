@Rich.module 'TopbarApp.Show', ( Show, App, Backbone, Marionette, $, _ ) ->

  Show.Controller =

    showTopbar: ->
      currentDealer = App.request 'get:current:dealer'
      @topbarView = @getTopbarView currentDealer

      App.topbarRegion.show @topbarView

      App.execute 'when:fetched', currentDealer, =>
        signInCount = currentDealer.get 'sign_in_count'
        if signInCount == 1
          welcomeView = @getWelcomeView currentDealer
          welcomeView.on 'welcome:closed', =>
            $ '#welcome-wrapper'
              .remove()
            currentDealer.save
              sign_in_count: 2

          welcomeView.on 'show', =>
            $ '#welcome-wrapper' 
              .foundation 'reveal', 'open'

          @topbarView.welcomeRegion.show welcomeView
          $ '.close-welcome-modal'
            .click ->
              $ '#welcome-wrapper'
                .foundation 'reveal', 'close'
              welcomeView.trigger 'welcome:closed'
        else
          $ '#welcome-wrapper'
            .remove()
 
    getWelcomeView: ( currentDealer ) ->
      new Show.Welcome
        model: currentDealer

    getTopbarView: ( currentDealer ) ->
      new Show.Topbar
        model: currentDealer
