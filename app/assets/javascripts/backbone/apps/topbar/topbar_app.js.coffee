@Rich.module "TopbarApp", (TopbarApp, App, Backbone, Marionette, $, _) ->
  @startWithParent = false

  API =
    showTopbar: ->
      TopbarApp.Show.Controller.showTopbar()

  TopbarApp.on "start", ->
    API.showTopbar()
