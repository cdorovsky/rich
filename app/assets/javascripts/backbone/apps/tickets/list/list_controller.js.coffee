@Rich.module 'TicketsApp.List', ( List, App, Backbone, Marionette, $, _ ) ->

  List.Controller =

    listTickets: ->
      App.request 'ticket:entities', ( tickets ) =>

        @layout = @getLayoutView()

        @layout.on 'show', =>
          @showTickets tickets
          @showNew tickets

        App.mainRegion.show @layout

    showTickets: ( tickets ) ->
      ticketsView = @getTicketsView tickets

      ticketsView.on 'childview:ticket:edit:clicked', ( child, ticket ) ->
        App.vent.trigger 'ticket:edit:clicked', ticket

      @layout.ticketsRegion.show ticketsView

    showNew: ->
      ticket = App.request 'new:ticket:entity'
      $('#New').foundation( 'reveal', 'close' )

      @newLayout = @getNewLayoutView ticket

      @newLayout.on 'show', =>
        @formNewRegion ticket

      @layout.newRegion.show @newLayout

    formNewRegion: ( ticket ) ->
      newView = @getNewView ticket

      formView = App.request 'form:wrapper', newView,
        footer: false

      formView.on 'form:submit', =>
        App.request 'notification:launch', 'Тикет создан!', 'info'
        @listTickets()

      @newLayout.formRegion.show formView

    getNewLayoutView: ( ticket ) ->
      new List.FormNewLayout
        model: ticket

    getNewView: ( ticket ) ->
      new List.New
        model: ticket

    getTicketsView: ( tickets ) ->
      new List.Tickets
        collection: tickets

    getButtonsView: ( tickets ) ->
      new List.Buttons
        collection: tickets

    getLayoutView: ->
      new List.Layout
