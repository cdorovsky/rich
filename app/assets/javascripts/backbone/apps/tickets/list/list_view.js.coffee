@Rich.module 'TicketsApp.List', ( List, App, Backbone, Marionette, $, _ ) ->

  class List.Layout extends App.Views.Layout
    template: 'tickets/list/templates/list_layout'

    regions:
      ticketsRegion: '#tickets-region'
      newRegion: '#new-region'

  class List.Ticket extends App.Views.ItemView
    template: 'tickets/list/templates/_ticket'
    tagName: 'tr'

    events:
      'click .edit-ticket' : -> @trigger 'ticket:edit:clicked', @model

  class List.Empty extends App.Views.ItemView
    template: 'tickets/list/templates/_empty'
    tagName: 'tr'

  class List.Tickets extends App.Views.CompositeView
    template: 'tickets/list/templates/_tickets'
    itemViewEventPrefix: 'childview'
    itemView: List.Ticket
    emptyView: List.Empty
    itemViewContainer: 'table'

  class List.Ticket extends App.Views.ItemView
    template: 'tickets/new/new_ticket'

  class List.FormNewLayout extends App.Views.Layout
    template: 'tickets/list/templates/new_layout'

    regions:
      formRegion: '#form-region'

  class List.New extends App.Views.ItemView
    template: 'tickets/list/templates/new_ticket'
    events:
      'click .close_modal' : 'closeModal'
      'click .submit'      : 'form:submit'

    closeModal: ->
      $('#New').foundation( 'reveal', 'close' )