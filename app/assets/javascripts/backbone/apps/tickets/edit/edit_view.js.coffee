@Rich.module 'TicketsApp.Edit', ( Edit, App, Backbone, Marionette, $, _ ) ->

  class Edit.Layout extends App.Views.Layout
    template: 'tickets/edit/templates/edit_layout'

    regions:
      formRegion: '#form-region'
      chatRegion: '#support-region'

  class Edit.Ticket extends App.Views.ItemView
    template: 'tickets/edit/templates/edit_ticket'

    events:
      'click .update-ticket' : -> @trigger 'ticket:update', @model
      'click .submit-ticket' : -> @trigger 'message:submit', @model

  class Edit.Chat extends App.Views.ItemView
    template: 'tickets/edit/templates/_chat'
    modelEvents:
      'change' : 'reRender'

    reRender: ->
      @model.fetch()
      @render()
