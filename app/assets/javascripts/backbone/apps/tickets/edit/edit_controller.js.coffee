@Rich.module 'TicketsApp.Edit', ( Edit, App, Backbone, Marionette, $, _ ) ->

  Edit.Controller =

    edit: ( id, ticket ) ->
      ticket or= App.request 'ticket:entity', id

      App.execute 'when:fetched', ticket, =>
        App.request 'rebuild:sidebar'
        @layout = @getLayoutView ticket

        @layout.on 'show', =>
          @formRegion ticket
          @getChat ticket

        App.mainRegion.show @layout

    getChat: ( ticket ) ->
      chatV = @getChatView ticket
      @layout.chatRegion.show chatV

    formRegion: ( ticket ) ->
      editView = @getEditView ticket

      editView.on 'form:cancel', ->
        App.vent.trigger 'ticket:cancelled', ticket

      @formView = App.request 'form:wrapper', editView,
        footer: false

      editView.on 'ticket:update', ( ticket ) =>
        ticket.fetch()
        @getChat ticket

      editView.on 'message:submit', ( ticket ) =>
        unless ticket.get 'closed'
          textarea = $ @formView.el
            .find 'textarea'
          unless textarea.val().length < 1
            @formView.trigger 'form:submit', ticket
            textarea.val ''
        else
          alert 'Тикет закрыт, создайте новый, чтобы задать вопрос.'

      @layout.formRegion.show @formView

    getChatView: ( ticket ) ->
      new Edit.Chat
        model: ticket

    getLayoutView: ( ticket ) ->
      new Edit.Layout
        model: ticket

    getEditView: ( ticket ) ->
      new Edit.Ticket
        model: ticket
