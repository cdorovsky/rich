@Rich.module 'TicketsApp', ( TicketsApp, App, Backbone, Marionette, $, _ ) ->

  class TicketsApp.Router extends Marionette.AppRouter
    appRoutes:
      'tickets'          : 'indexTickets'
      'tickets/:id/edit' : 'edit'

  API = 
    indexTickets: ->
      TicketsApp.List.Controller.listTickets()

    edit: ( id ) ->
      TicketsApp.Edit.Controller.edit id

  App.vent.on 'ticket:edit:clicked', ( ticket ) ->
    App.navigate Routes.edit_ticket_path ticket.id
    API.edit ticket.id, ticket

  App.vent.on 'ticket:cancelled ticket:updated ticket:created', ( ticket ) ->
    App.navigate Routes.tickets_path()
    API.indexTickets()

  App.addInitializer ->
    r = new TicketsApp.Router
      controller: API
    r.on 'route', ->
      window.setTitle 'Поддержка'
    r
