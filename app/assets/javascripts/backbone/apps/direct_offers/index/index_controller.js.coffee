@Rich.module 'DirectOffersApp.Index', ( Index, App, Backbone, Marionette, $, _ ) ->

  Index.Controller =

    index: ( id, offers ) ->
      @direct_offers = new App.Entities.DirectOffersCollection
      @direct_offers.fetch
        success: =>
          @layout = @getLayoutView()
          @layout.on 'show', =>
            @buildPostBack()
            @buildCollection()
            @buildFilter()
          App.mainRegion.show @layout

    buildPostBack: ->
      @postbackView = @getPostbackView()
      @postbackView.on 'return', ->
        @viewRequest @savedRequestView
      @layout.postbackRegion.show @postbackView

    buildCollection: ( query={} ) ->
      @offers = new App.Entities.OffersCollection
      @offers.fetch
        data: query
        success: =>
          @offers.prepare_for_direct_offers( @direct_offers )
          @buildTable()

    buildFilter: ->
      countries = App.request 'get:countries'
      @filterView = @getFilterView countries
      @filterView.on 'apply:filter', ( query ) =>
        if query.activated
          query.id = _.map @direct_offers.where({approved: true}), (i) ->
            i.get('offer_id')
          delete query["activated"]
        @buildCollection( query )

      @layout.filterRegion.show @filterView

    viewRequest: ( req ) ->
      requestView = @getDirectOfferRequest req.model
      requestView.on 'show', =>
        linkClip = new ZeroClipboard( $('.copy-link') )
        targetLinkClip = new ZeroClipboard( $('.copy-link-target') )
        linkClip.on 'aftercopy', ( e ) ->
          App.request 'notification:launch', 'Ссылка скопирована!', 'info'
        targetLinkClip.on 'aftercopy', ( e ) ->
          App.request 'notification:launch', 'Ссылка скопирована!', 'info'

      App.modalRegion.show requestView
      requestView.on 'get:instruction', =>
        @savedRequestView = requestView
        $ document
          .on 'closed.fndtn.reveal', '#postback-wrapper', (e) =>
            @viewRequest @savedRequestView
            @layout.postbackRegion.show @postbackView
        $ '#postback-wrapper'
          .foundation 'reveal', 'open'

    buildTable: ->
      @paginator = @getPaginatorView @offers.pagerModel()
      @table = @getOffersView @offers
      @table.on 'childview:open:request', ( model ) =>
        @viewRequest model
      @table.on 'childview:view:offer', ( offer ) =>
        App.modalRegion.reset()
        id = offer.model.get 'id'
        offer = App.request 'offer:entity', id
        App.execute 'when:fetched', offer, =>
          offerView = new Index.OfferView
            model: offer
          $ '#modal-wrapper'
            .removeClass 'small large'
              .addClass 'medium'
          App.modalRegion.show offerView
          $ offerView.el
            .foundation()
      @table.on 'childview:add:direct:offer', ( child ) =>
        @direct_offers.add child.toAdd
      @paginator.on 'show', =>
        @paginator.collectionRegion.show @table
        @paginator.on 'page:selected page:resized', ( pager ) =>
          requestedPage = pager.get 'requested_page'
          requestedPage ||= 1
          new_page_size = pager.get 'new_page_size'
          if new_page_size
            @offers.state.pageSize = new_page_size
            pager.unset 'new_page_size'
          @offers.state.currentPage = requestedPage
          query = @filterView.getQuery()
          if query.activated
            query.id = _.map @direct_offers.where({approved: true}), (i) ->
              i.get('offer_id')
            delete query["activated"]
          _.extend @offers.queryParams, query
          @offers.fetch
            success: =>
              @offers.prepare_for_direct_offers( @direct_offers )
              @buildTable()
      @layout.tableRegion.show @paginator

    getPostbackView: ->
      new App.CampaignsApp.Edit.Postback

    getDirectOfferRequest: ( d_o ) ->
      new Index.Request
        model: d_o

    getFilterView: ( countries ) ->
      new Index.Filter
        model: countries

    getOffersView: ( offers ) ->
      new Index.Offers
        collection: offers
        sort: false

    getPaginatorView: ( pager ) ->
      new App.Views.Paginator
        model: pager

    getLayoutView: ( direct_offers ) ->
      new Index.Layout
        model: direct_offers
