@Rich.module 'DirectOffersApp.Index', ( Index, App, Backbone, Marionette, $, _ ) ->

  class Index.Layout extends App.Views.Layout
    template: 'direct_offers/index/templates/layout'

    regions:
      filterRegion: '#filter-region'
      tableRegion: '#offers-region'
      postbackRegion: '#postback-region'

  class Index.Request extends App.Views.ItemView
    template: 'direct_offers/index/templates/_direct_offer_modal'
    tagName: 'form'

    events:
      'submit' : 'actConditionally'
      'click .get-instruction' : 'getInstruction'
      'change #switch-postback' : 'postbackFormToggle'

    postbackFormToggle: ( e ) ->
      sw = $ e.currentTarget
      toggleables = $ @el
        .find '.toggleable'
      description = $ @el
        .find '.description'
      if sw.prop('checked') == false
        @fadeSwitch( toggleables, description )
        @actConditionally( e )
      else
        @fadeSwitch( description, toggleables )

    onRender: ->
      modalClass = if @model.get('approved') then 'large' else 'small'
      $ '#modal-wrapper'
        .removeClass 'small medium large'
          .addClass modalClass
            .foundation 'reveal', 'open'

    fadeSwitch:  ( fadable, showable ) ->
      fadable.fadeOut 'fast', =>
        showable.fadeIn 'fast'

    getInstruction: ->
      $ '#modal-wrapper'
        .foundation 'reveal', 'close'
      @trigger 'get:instruction', @

    actConditionally: ( e ) ->
      e.preventDefault()
      e.stopPropagation()
      attrs = Backbone.Syphon.serialize @
      persisted = @model.get('hashid')
      if persisted
        @model.url = '/direct_offers/' + @model.get('hashid')
      else
        @model.url = '/direct_offers'
        @model.unset('id')
      if !persisted && attrs.traffic_sources.length < 5
        App.request 'notification:launch', 'Укажите источники трафика!', 'warning'
      else
        @model.save attrs,
          success: =>
            if persisted && @model.get('approved')
              msg = 'Настройки сохранены'
            else if !persisted && @model.get('approved')
              msg = 'Оффер добавлен!'
            else
              msg = 'Заявка подана'
            if msg
              App.request 'notification:launch', msg, 'success'
            offer_id = @model.get('offer_id')
            @model.set('id', offer_id)
            @model.url = '/offers/' + offer_id
            @render()

  class Index.PostBack extends App.Views.ItemView
    template: 'direct_offers/index/templates/_postback'

    events:
      'click .return' : -> @trigger 'return'

  class Index.Filter extends App.Views.ItemView
    template: 'direct_offers/index/templates/_index_filter'
    tagName: 'form'

    modelEvents:
      'change' : 'render'

    events:
      'click .filter-offer' : 'checkFilter'

    getQuery: ->
      query = Backbone.Syphon.serialize @
      _.each query, (v, k) ->
        if !v
          delete query[k]
      if query.countries
        query.countries.push 'All'
      query

    checkFilter: ->
      @trigger 'apply:filter', @getQuery()

    onRender: ->
      $ @el
        .find 'select#countries'
          .select2
            maximumSelectionSize: 7,
            width: '100%',
            placeholder: 'Выберите страны'
      $ @el
        .foundation()

  class Index.Empty extends App.Views.ItemView
    template: 'offers/list/templates/_empty'
    tagName: 'tr'

  class Index.Offer extends App.Views.ItemView
    template: 'offers/list/templates/_d_offer'
    tagName: 'tr'
    modelEvents:
      'change' : 'render'
      'save'   : 'render'
    events:
      'click .request'         : -> @trigger 'open:request', @model
      'click .offer-info'      : -> @trigger 'view:offer', @model
      #'click .confirm-request' : 'confirmRequest'

    requestForm: ( e ) ->
      btn = $ e.currentTarget
      btn.parent('.request-direct-offer').fadeOut 'fast', => @showForm()

    showForm: ->
      $ @el
        .find '.direct-offer-form'
          .fadeIn 'fast', => $( @el ).find( '.sources' ).focus()

  class Index.Offers extends App.Views.CompositeView
    template: 'offers/list/templates/_d_offers'
    className: 'panel margin-b-10'
    itemView: Index.Offer
    emptyView: Index.Empty
    itemViewContainer: 'tbody'
    itemViewEventPrefix: 'childview'

    onRender: ->
      $ @el
        .foundation()

  class Index.OfferView extends App.Views.ItemView
    template: 'offers/show/templates/_offer'
