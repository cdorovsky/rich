@Rich.module 'DirectOffersApp', ( DirectOffersApp, App, Backbone, Marionette, $, _ ) ->

  class DirectOffersApp.Router extends Marionette.AppRouter
    appRoutes:
      'offers' : 'index'

  API =
    index: ->
      DirectOffersApp.Index.Controller.index()

  App.addInitializer ->
    r = new DirectOffersApp.Router
      controller: API
    r.on 'route', ->
      window.setTitle 'Офферы'
    r
