$(function() {
  function install() {
    $('.overlay-install').fadeIn();
      $(".install-block").addClass('active');
      $('html, body').animate({
        scrollTop: $(".install-block").offset().top
      }, 1000);
      return false;
  }
  $('.btn.install').click(function() {
    install();
  });
  $('.heading').emoji();
  $('.overlay-install').click(function() {
    $(this).fadeOut();
    $('.install-block').removeClass('active');
    return false;
  });
  var formDisabler = function(btnClass, inputClass, texts){
    var btn = $(btnClass);
    btn.html(btn.html().replace(texts[0], texts[1]));
    $(inputClass).attr('disabled','disabled');
    $('button').attr('disabled', 'disabled');
  };
  var postPhone = function(selector, url, formTexts){
    form = $(selector);
    form.submit(function(e){
      var inp = form.find('.phone');
      if (inp.val().length < 8){
        inp.attr('placeholder', 'Номер введен неверно');
      } else {
        $.ajax({
          type: 'POST',
          url: $('form.link-form').attr('action'),
          datatype: 'json',
          data: {
            phone: inp.val()
          },
          success: function(){
            formDisabler('.retreive-link', '.phone', formTexts);
            $('#window-sms-install').modal();
          }
        })};
      return false;
    });
  };
  if($('form').hasClass('link-form')){
    postPhone('.link-form', '/send-link', ['Получить ссылку по SMS', 'Отправлено'])
  } else {
    postPhone('.unsubscribe-form', '', ['Отписаться', 'Успешно'])
  };
  $('input.phone').click(function(){
    var that = $(this);
    if (that.val().length == 0){
    that.val(that.data('phone-code'));
  }});
  function prepareCookie(){
    if (typeof($.cookie('timer')) == 'undefined' || $.cookie('timer') == '00:00' ){
      $.cookie('timer', '37:35');
    }
    $('#my_timer').html($.cookie('timer'));
  }
  prepareCookie();
  function startTimer() {
    var my_timer = document.getElementById("my_timer");
    var time = my_timer.innerHTML;
    var arr = time.split(":");
    var m = arr[0];
    var s = arr[1];
    if (s == 0) {
      if (m == 0) {
          install();
          return;
        h--;
        m = 60;
        if (h < 10) h = "0" + h;
      }
      m--;
      if (m < 10) m = "0" + m;
      s = 59;
    }
    else s--;
    if (s < 10) s = "0" + s;
    document.getElementById("my_timer").innerHTML = m+":"+s;
    $.cookie('timer', m+":"+s);
    setTimeout(startTimer, 1000);
  }
  startTimer();
});
