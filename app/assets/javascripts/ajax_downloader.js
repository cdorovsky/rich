(function($, undefined) {
  $.download = function(url, key, data){
      var form = $('<form></form>').attr('action', url).attr('method', 'post');
      form.append($("<input></input>").attr('type', 'hidden').attr('name', key).attr('value', data));
      form.appendTo('body').submit().remove();
  };
})( jQuery );