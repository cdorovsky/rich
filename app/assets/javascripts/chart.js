function chartRender( graphData, grouping ) {
  var seriesOptions = [],
    yAxisOptions = [],
    seriesCounter = 0,
    colors = ['#01a1ec'],
    data = graphData,
    grouping = grouping
    if (data.length < 1) {
      setPlaceholder()
    } else if (grouping == 'no_grouping') {
      seriesOptions[0] = { name: 'apps_advert', data: data };
      seriesOptions[0].type = 'areaspline'
      seriesOptions[0].fillColor = { 
        linearGradient : { x1: 0, y1: 0, x2: 0, y2: 1 },
        stops : [ [0, "#01a1ec"], [1, "rgba(255,255,255,0)"] ], 
      }
      createRevenueChart();
    } else {
      createGroupingChart();
    }

  function setPlaceholder() {
    var pn, placeHolderNode;
    pn =   '<div class="rating-blank">'
    pn +=   '<img src="../assets/blank_stats_graph.png" alt="">'
    pn +=   '<span>Статистики пока нет, но как только вы получите доход она появится</span>'
    pn += '</div>'
    placeHolderNode = pn;
    $('#graph-lines-region').html(placeHolderNode);
  }

  function createRevenueChart() {

    $('#graph-lines-region').highcharts('StockChart', {

      loading: {
          labelStyle: { color: 'white' },
          style: { backgroundColor: 'gray' }
      },

      lang: {
        loading: 'Загрузка...'
      },
      rangeSelector: { enabled: false },
      navigator: { enabled: false },
      scrollbar: { enabled: false },
      plotOptions: {
        line: { color: '#01a1ec' },
        series: {
          color: '#01a1ec',
          marker: {
            states: { hover: { radiusPlus: 25 } },
            enabled: false
          }
        }
      },
      yAxis: {
        title: {
          style: { "color": '#01a1ec' },
          text: 'Доход ($)'
        },
        labels: {
          align: 'right',
          format: '{value} $',
          y: 0,
          formatter: function() {
            return this.value + '$';
          }
        },
        offset: 10,
        plotLines: [{
          value: 0,
          width: 2,
          color: 'silver'
        }],
        min: 0
      },
      tooltip: {
        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
        valueDecimals: 2
      },
      series: seriesOptions
    });
  }
  function createGroupingChart() {
    $('#graph-lines-region').highcharts({
        chart: { type: 'column' },
        title: { text: '' },
        xAxis: {
            type: 'category',
            labels: {
                padding: 0,
                rotation: -45,
                style: { fontSize: '10px', fontFamily: 'Verdana, sans-serif' },
                formatter: function(){
                    if (this.value.length > 20){ return this.value.substr(0,20) + "..."; } else { return this.value; }
                }
            },
        },
        plotOptions: {
          column: { color: '#01a1ec' }
        },
        yAxis: { min: 0, title: { text: 'Доход, $' } },
        legend: { enabled: false },
        tooltip: { pointFormat: 'Доход за выбранный период: <b>$ {point.y:.2f}</b>' },
        series: [{
            name: 'Population',
            data: graphData,
            dataLabels: {
                enabled: true,
                rotation: 0,
                color: '#333333',
                align: 'center',
                format: '$ {point.y:.2f}',
                y: -1,
                style: {
                    textShadow: '1px 1px 3px #eeeeee',
                    fontSize: '10px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
  }
}