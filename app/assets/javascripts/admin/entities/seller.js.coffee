@RichAdmin
  .module 'Entities', (
    Entities, App, Backbone, Marionette, $, _
    ) ->

  class Entities.Seller extends Entities.Model
    urlRoot: -> '/admin/sellers'

  class Entities.SellersCollection extends Entities.Collection
    model: Entities.Seller
    url: -> '/admin/sellers'

    toList: ->
      list = @models.map ( seller ) =>
        {
          id: seller.get 'id'
          seller_name: seller.get 'seller_name'
        }
      list

  API =
    getSellerEntities: ( cb ) ->
      sellers = new Entities.SellersCollection
      sellers.fetch
        success: ->
          cb sellers

    getSeller: ( id ) ->
      seller = new Entities.Seller
        id: id
      seller.fetch()
      seller

    newSeller: ->
      new Entities.Seller

  App.reqres.setHandler 'sellers:entities', ( cb ) ->
    API.getSellerEntities cb

  App.reqres.setHandler 'seller:entity', ( id ) ->
    API.getSeller id

  App.reqres.setHandler 'new:seller:entity', ->
    API.newSeller()
