@RichAdmin.module 'Entities', ( Entities, App, Backbone, Marionette, $, _ ) ->

  class Entities.Countries extends Entities.Model
    url: -> Routes.countries_path()

  API =
    getCountriesEntities: ( cb ) ->
      countries = new Entities.Countries
      countries.fetch
        success: ->
          cb countries

  App.reqres.setHandler 'countries:entities', ( cb ) ->
    API.getCountriesEntities cb
