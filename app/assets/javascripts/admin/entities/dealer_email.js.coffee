@RichAdmin.module 'Entities', ( Entities, App, Backbone, Marionette, $, _ ) ->

  class Entities.DealersScope extends Entities.Model
    url: '/admin/dealers/get_scope'

  class Entities.DealerEmail extends Entities.Model
    urlRoot: '/admin/dealer_mailings'

  class Entities.DealerMailing extends Entities.Model
    urlRoot: -> '/admin/dealer_mailings'

  class Entities.DealerMailingsCollection extends Entities.Collection
    model: Entities.DealerMailing
    url: -> '/admin/dealer_mailings'

  API =
    getMailerEntities: ( cb ) ->
      mailings = new Entities.DealerMailingsCollection
      mailings.fetch
        success: ->
          cb mailings

    getMailing: ( id ) ->
      mailing = new Entities.DealerMailing
        id: id
      mailing.fetch()
      mailing

  App.reqres.setHandler 'get:dealer:mailings', ( cb ) ->
    API.getMailerEntities cb

  App.reqres.setHandler 'get:dealer:mailing', ( id ) ->
    API.getMailing id
