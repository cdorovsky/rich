@RichAdmin.module 'Entities', ( Entities, App, Backbone, Marionette, $, _ ) ->

  class Entities.DirectOffer extends Entities.Model
    urlRoot: -> '/admin/direct_offers'

  class Entities.DirectOffersCollection extends Entities.Collection
    model: Entities.DirectOffer
    url: -> '/admin/direct_offers'

  class Entities.DirectOfferModeration extends Entities.Model
    urlRoot: -> '/admin/direct_offers'
