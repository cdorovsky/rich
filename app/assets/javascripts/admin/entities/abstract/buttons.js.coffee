@RichAdmin.module 'Entities', ( Entities, App, Backbone, Marionette, $, _ ) ->

  class Entities.Button extends Entities.Model

  class Entities.ButtonsCollection extends Entities.Collection
    model: Entities.Button

  API =
    getFormButtons: ( buttons, model ) ->
      buttons = @getDefaultButtons buttons, model
      array = []
      array.push { type: 'cancel',  className: 'secondary expand', text: buttons.cancel } unless buttons.cancel is false
      array.push { type: 'primary', className: 'success expand',   text: buttons.primary } unless buttons.primary is false

      buttonCollection = new Entities.ButtonsCollection array
      buttonCollection

    getDefaultButtons: ( buttons, model ) ->
      _.defaults buttons, 
        primary: if model.isNew() then 'Создать' else 'Сохранить'
        cancel: 'Отмена'



  App.reqres.setHandler 'form:button:entities', ( buttons = {}, model ) ->
    API.getFormButtons buttons, model
