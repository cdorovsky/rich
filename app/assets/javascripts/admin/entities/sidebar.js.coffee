@RichAdmin.module 'Entities', ( Entities, App, Backbone, Marionette, $, _ ) ->

  class Entities.Sidebar extends Entities.Model

  class Entities.SidebarCollection extends Entities.Collection
    model: Entities.Sidebar

  API = 
    getSidebar: ->
      new Entities.SidebarCollection [
        { name: 'Панель',     url: '#/admin',   klass: 'dashboard'       }
        { name: 'Партнеры',   url: '#/dealers', klass: 'partners'        }
        { name: 'Офферы',     url: '#/offers',  klass: 'offers'          }
        { name: 'Директ Офферы', url: '#/direct_offers',  klass: 'direct_offers'}
        { name: 'Статистика', url: '#/stats',   klass: ''                }
        { name: 'Выплаты',    url: '#/payments',klass: ''                }
        { name: 'Поддержка',  url: '#/tickets', klass: 'support tickets' }
        { name: 'Админка [old]', url: 'admin/offers', klass: ''          }
      ]

  App.reqres.setHandler 'sidebar:entities', ->
    API.getSidebar()
