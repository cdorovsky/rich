@RichAdmin
  .module 'Entities', (
    Entities, App, Backbone, Marionette, $, _
    ) ->

  class Entities.Screenshot extends Entities.Model
    urlRoot: -> '/admin/offer_screenshots'


  API =
    createScreen: ( obj ) ->
      new Entities.Screenshot obj

  App.reqres.setHandler 'create:screenshot:entity', ( obj ) ->
    API.createScreen obj
