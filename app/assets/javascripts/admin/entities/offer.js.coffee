@RichAdmin
  .module 'Entities', (
    Entities, App, Backbone, Marionette, $, _
    ) ->
  class Entities.Offer extends Entities.Model
    urlRoot: -> '/admin/offers'

    beforeSave: ( data, options ) ->
      data.offer.features_list = data.offer.features_list.split '\n'
      data.offer.dialog_translations = @serializeTranslations( data.offer.dialog_translations )

    serializeTranslations: ( translations ) ->
      result = {}
      _.each translations, ( translation, key ) ->
        _.each key.split( ', ' ), ( abbr ) ->
          result[abbr] = translation
      result

    readableTranslations: ->
      translations = @get 'dialog_translations'
      if translations
        pairs = _.pairs translations
        mapped = _.map pairs, ( pair ) ->
          {
            abbr: pair[0],
            dialog: pair[1]
          }
        values = _.values translations
        grouped = _.map values, ( dialog ) ->
          {
            abbr: _.pluck( _.where( mapped, { dialog: dialog } ), 'abbr' ).join( ', ' )
            dialog: dialog
          }
        uniq_grouped = _.uniq( grouped, ( item ) -> item.dialog )
        uniq_grouped

  class Entities.CachedOffersCollection extends Entities.Collection
    url: '/admin/cached_offers'
    model: Entities.Offer

    forSelectBox: ->
      attributes =
        offers: @map ( offer ) ->
          id: offer.get 'id'
          name: offer.get 'name'
      new Entities.Model attributes

  class Entities.OffersCollection extends Entities.Collection
    model: Entities.Offer
    url: -> '/admin/offers'

  API =
    getCachedOfferEntities: ( cb ) ->
      offers = new Entities.CachedOffersCollection
      offers.fetch
        success: ->
          cb offers

    getOfferEntities: ( cb ) ->
      offers = new Entities.OffersCollection
      offers.fetch
        success: ->
          cb offers

    getOffer: ( id ) ->
      offer = new Entities.Offer
        id: id
      offer.fetch()
      offer

    newOffer: ->
      offer = new Entities.Offer
      offer.set 'offer_screenshots', []
      offer

  App.reqres.setHandler 'offers:entities', ( cb ) ->
    API.getOfferEntities cb

  App.reqres.setHandler 'offer:entity', ( id ) ->
    API.getOffer id

  App.reqres.setHandler 'new:offer:entity', ->
    API.newOffer()
