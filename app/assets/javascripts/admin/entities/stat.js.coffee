@RichAdmin.module 'Entities', ( Entities, App, Backbone, Marionette, $, _ ) ->

  class Entities.CachedDealers extends Entities.Collection
    url: '/admin/cached_dealers'

    forSelectBox: ->
      @models.map ( model ) ->
        {
          id: model.get( 'id' )
          email: model.get( 'email' )
        }

  class Entities.TopCRCollection extends Entities.Collection
    url: '/admin/dealer_stats/top_cr'

  class Entities.RawTimingStats extends Entities.Collection
    url: '/admin/timing_stats'

    toGraphStats: ->
      attributes =
        categories: @graphCategories()
        partners: @pluck( 'daily_revenue' )
        sellers: @pluck( 'seller_revenue' )
      new Entities.GraphStats attributes

    graphCategories: ->
      @pluck "created_at"

    toCountersTable: ->
      days_weekly = moment().day()
      days_monthly = moment().date()
      attributes =
        partners_weekly: @sumOfLastItems( 'daily_revenue', days_weekly )
        sellers_weekly: @sumOfLastItems( 'seller_revenue', days_weekly )
        partners_monthly: @sumOfLastItems( 'daily_revenue', days_monthly )
        sellers_monthly: @sumOfLastItems( 'seller_revenue', days_monthly )
      new Entities.Model attributes

    sumOfLastItems: ( attr=[0], itemCount=1 ) ->
      if attr.length == 0
        attr = [0]
      _.reduce( _.last( @pluck( attr ), itemCount ), ( s, e ) -> s + e )

  class Entities.GraphStats extends Entities.Model
    timeScopeToDaysCount: ( timeScope ) ->
      today = moment()
      previousMonth = moment().subtract( 1, 'months' )
      diffs =
        current_week: today.day()
        previous_week: 7
        current_month: today.date()
        previous_month: previousMonth.daysInMonth()
      offsets =
        current_week: 0
        previous_week: today.day()
        current_month: 0
        previous_month: today.date()
      resObj =
        diffs: diffs[ timeScope ]
        offsets: offsets[ timeScope ]

    toDaysDiff: ( start, end ) ->
      moment()
        .range start, end
          .diff 'days'

    cropWithOffset: ( ary, days ) ->
      _.first _.last( ary, days.diffs+days.offsets ), days.diffs

    graphSeries: ( timeScope ) ->
      [
        {
          name: 'Партнеры'
          data: @cropWithOffset( @get( 'partners' ), @timeScopeToDaysCount( timeScope ) )
        },
        {
          name: 'Рекламодатели'
          data: @cropWithOffset( @get( 'sellers' ), @timeScopeToDaysCount( timeScope ) )
        }
      ]

    graphCategories: ( timeScope ) ->
      @cropWithOffset( @get( 'categories' ), @timeScopeToDaysCount( timeScope ) )

  class Entities.RawDealerStats extends Entities.PageableCollection
    url: '/admin/dealer_stats'

    toDealerStats: ( dealers ) ->
      mapped = @map ( stat ) ->
        dealer = dealers.findWhere( id: stat.get( 'id' ) )
        stat.set 'dealer_email', dealer.get( 'email' )
        new Entities.DealerStat stat.attributes
      sorted = _.sortBy( mapped, ( ag ) -> ag.get( 'daily_revenue' ) ).reverse()
      @models = sorted
      @

  class Entities.DealerStat extends Entities.Model

  class Entities.DealerStats extends Entities.PageableCollection
    model: Entities.DealerStat

  API =
    getCachedDealers: ->
      d = new Entities.CachedDealers
      d.fetch()
      d

    getRawDealerStatEntities: ( cb ) ->
      stats = new Entities.RawDealerStats

      stats.fetch
        success: ->
          cb stats

  App.reqres.setHandler 'admin:dealer:stats:collection', ( cb ) ->
    API.getRawDealerStatEntities cb
