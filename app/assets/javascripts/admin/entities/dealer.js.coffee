@RichAdmin.module 'Entities', ( Entities, App, Backbone, Marionette, $, _ ) ->

  class Entities.CurrentDealer extends Entities.Model
    url: '/current_dealer'

  class Entities.Dealer extends Entities.Model
    urlRoot: -> '/admin/dealers'

    balanceModel: ->
      model = new Entities.Model
      model.url = "/admin/dealers/#{ @get 'id' }/change_balance"
      model.set 'dealer_id', @get 'id'
      model.set 'balance', @get 'balance'
      model.isNew = -> true
      model

  class Entities.DealersList extends Entities.Model
    url: '/admin/dealers_list'

  class Entities.DealersCollection extends Entities.PageableCollection
    url: '/admin/dealers'
    model: Entities.Dealer

  class Entities.Request extends Entities.Model

  class Entities.DealerPaymentRequests extends Entities.PageableCollection
    url: -> '/admin/requests_for_dealer'
    model: Entities.Request

  API =
    setCurrentDealer: ( currentDealer ) ->
      new Entities.Dealer currentDealer

    getCurrentDealer: ->
      dealer = new Entities.CurrentDealer
      dealer.fetch()
      dealer

    getDealers: ( cb ) ->
      d = new Entities.DealersCollection
      d.fetch
        success: ->
          cb d

    getDealer: ( id ) ->
      dealer = new Entities.Dealer
        id: id
      dealer.fetch()
      dealer

    getPaymentRequests: ( id ) ->
      requests = new Entities.DealerPaymentRequests
      requests.queryParams.dealer_id = id
      requests.fetch()
      requests


  App.reqres.setHandler 'dealer:entity', ( id ) ->
    API.getDealer id

  App.reqres.setHandler 'dealer:requests', ( id ) ->
    API.getPaymentRequests id

  App.reqres.setHandler 'dealers:entities', ( cb ) ->
    API.getDealers cb

  App.reqres.setHandler 'set:current:dealer', ( currentDealer ) ->
    API.getCurrentDealer()
