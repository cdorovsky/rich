@RichAdmin.module 'Entities', ( Entities, App, Backbone, Marionette, $, _ ) ->

  class Entities.PaymentRequest extends Entities.Model
    urlRoot: -> '/admin/payment_requests'

  class Entities.PaymentRequests extends Entities.Collection
    model: Entities.PaymentRequest
    url: -> '/admin/payment_requests'
