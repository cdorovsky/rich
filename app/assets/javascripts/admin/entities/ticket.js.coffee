@RichAdmin
  .module 'Entities', (
    Entities, App, Backbone, Marionette, $, _
    ) ->

  class Entities.Ticket extends Entities.Model
    urlRoot: -> '/admin/tickets'

    closeTicket: ->
      @save
        closed: true

  class Entities.TicketsCollection extends Entities.Collection
    model: Entities.Ticket
    url: -> '/admin/tickets'

  API =
    getTicketEntities: ( cb, query ) ->
      tickets = new Entities.TicketsCollection
      tickets.fetch
        success: ->
          cb tickets

    getTicket: ( id ) ->
      ticket = new Entities.Ticket
        id: id
      ticket.fetch()
      ticket

  App.reqres.setHandler 'tickets:entities', ( cb ) ->
    API.getTicketEntities cb

  App.reqres.setHandler 'ticket:entity', ( id ) ->
    API.getTicket id

