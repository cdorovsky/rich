@RichAdmin.module 'Entities', (Entities, App, Backbone, Marionette, $, _) ->

  class Entities.Payment extends Entities.Model
    urlRoot: -> '/admin/payments'

    requests: ->
      new Entities.Collection @get('payment_requests')

  class Entities.Payments extends Entities.Collection
    model: Entities.Payment
    url: -> '/admin/payments'

  API =
    getPaymentEntities: (cb) ->
      p = new Entities.Payments
      p.fetch
        success: ->
          cb p

    getPaymentEntity: (id) ->
      p = new Entities.Payment
        id: id
      p.fetch()
      p

    newPayment: ->
      new Entities.Payment

  App.reqres.setHandler 'payment:entities', (cb) ->
    API.getPaymentEntities cb

  App.reqres.setHandler 'payment:entity', (id) ->
    API.getPaymentEntity id

  App.reqres.setHandler 'new:payment:entity', ->
    API.newPayment()
