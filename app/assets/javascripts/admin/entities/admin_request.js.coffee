@RichAdmin.module 'Entities', ( Entities, App, Backbone, Marionette, $, _ ) ->

  class Entities.AdminRequest extends Entities.Model
    urlRoot: -> '/admin/admin_requests'

    beforeSave: ->
      val = @get('value')
      if _.isNumber(val) && val!=0
        alert 'Введите сумму для изменения баланса!'
