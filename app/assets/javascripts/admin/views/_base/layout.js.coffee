@RichAdmin.module 'Views', ( Views, App, Backbone, Marionette, $, _ ) ->

	class Views.Layout extends Marionette.Layout

    preloader: ->
      _buf =  '<div class="circleIG">'
      _buf +=   '<div class="circleIG_1 circleG"></div>'
      _buf +=   '<div class="circleIG_2 circleG"></div>'
      _buf +=   '<div class="circleIG_3 circleG"></div>'
      _buf += '</div>'
      _buf

    setPreloader: ( region ) ->
      $ region.el
        .html @preloader()

    onRender: ->
      document.body.scrollTop = 0
