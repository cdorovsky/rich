do (Backbone) ->
  Backbone.Syphon.InputReaders.register 'checkbox', (el) ->
    return el.val()

  Backbone.Syphon.KeyAssignmentValidators.register "checkbox", ($el, key, value) ->
    return $el.prop "checked"
