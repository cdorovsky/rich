@RichAdmin
  .module 'TopbarApp.Show', ( 
      Show, App, Backbone, Marionette, $, _
  ) ->

  Show.Controller =

    showTopbar: ->
      currentDealer = App.request 'get:current:dealer'
      topbarView = @getTopbarView currentDealer
      App
        .topbarRegion
          .show topbarView

    getTopbarView: ( currentDealer ) ->
      new Show.Topbar
        model: currentDealer
