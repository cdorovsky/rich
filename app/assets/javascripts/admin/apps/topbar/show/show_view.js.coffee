@RichAdmin.module 'TopbarApp.Show', ( Show, App, Backbone, Marionette, $, _ ) ->

  class Show.Topbar extends App.Views.ItemView
    template: 'topbar/show/templates/show_topbar'

    modelEvents:
      'change' : 'render'

    tagName:   'nav'
    className: 'top-bar'
    attributes:
      'data-topbar' : ''
