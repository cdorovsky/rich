@RichAdmin
  .module 'OffersApp.Show', (
    Show, App, Backbone, Marionette, $, _
    ) ->

  Show.Controller =

    show: ( id ) ->
      offer = App.request 'offer:entity', id
      App.execute 'when:fetched', offer, =>
        offerView = @getOfferShowView offer
        $ '#modal-wrapper'
          .removeClass 'small'
            .foundation 'reveal', 'open'
        $ document
          .on 'closed.fndtn.reveal', '#modal-wrapper', ->
            App.vent.trigger 'offers:index'
        App.modalRegion.show offerView

    getOfferShowView: ( offer ) ->
      new Show.Offer
        model: offer
