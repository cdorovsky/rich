@RichAdmin
  .module 'OffersApp.Show', (
      Show, App, Backbone, Marionette, $, _
    ) ->

  class Show.Offer extends App.Views.Layout
    template: 'offers/show/templates/offer'
