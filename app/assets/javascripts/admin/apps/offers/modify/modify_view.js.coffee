@RichAdmin
  .module 'OffersApp.Modify', (
      Modify, App, Backbone, Marionette, $, _
    ) ->

  class Modify.Fields extends App.Views.Layout
    template: 'offers/modify/templates/form_offer'

    regions:
      screenshotsRegion: '#screenshots-region'
      headingsRegion: '#headings-region'

    events:
      'change select.seller-id' : -> @trigger 'seller:change'
      'click .affilate-offer'   : -> @trigger 'affilate:offer'
      'change .offer-tariff'    : -> @trigger 'set:tariffes'
      'change #switch-disabler' : 'toggleHideOfferDisabler'
      'click .submit'           : 'confirmSubmit'
      'click .add-heading'      : -> @trigger 'add:heading'
      'click a.add-dialog-translation' : 'addDialogTranslation'

    onRender: ->
      translations = @model.readableTranslations()
      _.each translations, ( translation ) =>
        model = new App.Entities.Model( translation )
        @addDialogTranslation model

    addDialogTranslation: ( dialogTr ) ->
      appendTranslationRegion = $ @el
        .find '#dialog-translations'
      appendTranslationRegion
        .find '#no-translations'
          .remove()
      view = new Modify.DialogTranslation
      if typeof( dialogTr.attributes ) == 'object'
        view.model = dialogTr
      view.render()
      appendTranslationRegion.append view.el

    toggleHideOfferDisabler: ->
      $ '.offer_disabler'
        .toggleClass 'hide'

    confirmSubmit: ->
      @trigger 'confirm:submit'

  class Modify.DialogTranslation extends App.Views.ItemView
    template: 'offers/modify/templates/_dialog_translation'

    events:
      'change .dialog-country' : 'changeCountry'
      'click .remove-translation' : 'removeTranslation'

    removeTranslation: ->
      if $( '#dialog-translations' ).children().length < 2
        $( '#dialog-translations' ).append( "<input id='no-translations' type='hidden' name='offer[remove_dialog_translations]' value='true'>" )
      @remove()

    changeCountry: ->
      country_abbr = $ @el
        .find '.dialog-country'
          .val()
      $ @el
        .find 'textarea'
          .prop 'name', "offer[dialog_translations][#{ country_abbr }]"

  class Modify.Heading extends App.Views.ItemView
    template: 'offers/modify/templates/_heading_fields'
    events:
      'click .delete-heading' : -> @trigger 'delete:heading', @

  class Modify.Screenshot extends App.Views.ItemView
    template: 'offers/modify/templates/_screenshot'
    tagName: 'li'
    events:
      'click .delete-screenshot': 'deleteScreenshot'

    deleteScreenshot: ->
      if confirm 'Вы уверены, что хотите удалить скриншот?'
        @model.destroy()
        @$el.hide()

  class Modify.OfferTarget extends App.Views.ItemView
    template: 'offers/modify/templates/_target_fields'
    className: 'offer-target padding-b-20'

    events:
      'click .delete-target' : -> @trigger 'delete:target', @
