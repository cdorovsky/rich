@RichAdmin
  .module 'OffersApp.Modify', (
    Modify, App, Backbone, Marionette, $, _
    ) ->

  Modify.Controller =

    modify: ( id ) ->

      if id
        offer = App.request 'offer:entity', id
        App.execute 'when:fetched', offer, =>
          @buildView offer
      else
        offer = App.request 'new:offer:entity'
        App.request 'sellers:entities', ( sellers ) =>
          sellers = _.map sellers.models, ( s ) ->
            id:     s.get 'id'
            name:   s.get 'seller_name'
            kind:   s.get 'kind'
            prefix: s.get 'prefix'
          offer.set 'sellers', sellers
          @buildView offer

    buildView: ( offer ) ->

      offer.on 'created updated', ->
        App.vent.trigger 'offers:index'

      @fields = @getOfferView offer

      @form = App.request 'form:wrapper',
        @fields,
        footer: false

      @fields.on 'show', =>
        countries = new RichAdmin.Entities.Countries
        countries.fetch()
        App.execute 'when:fetched', countries, =>
          countries = countries.attributes.countries.map (country) ->
            country.name
          _.each [ '.country-list', '.restricted-country-list' ], ( field ) ->
            $ field
              .textcomplete [
                words: countries
                match: /\b(\w{2,})$/
                search: ( term, callback ) ->
                  callback $.map @words, ( word ) ->
                    if word.indexOf( term ) is 0
                      word
                    else
                      null
                index: 1
                replace: (word) ->
                    word + ', '
                placement: 'top'
              ]

        screenshots = @fields.model.get 'offer_screenshots'
        for screenshot in screenshots
          screenshot = App.request 'create:screenshot:entity', screenshot
          @buildScrView screenshot


        dropzoneOpts =
          maxfile: 1
        iconUpdr = new Dropzone '.dropzone', dropzoneOpts
        iconUpdr.on 'success', ( file, responseText ) ->
          $ '.offer-icon'
            .attr 'src', responseText.file.url
          $ '.icon-id'
            .val responseText.id

        scrUpdr = new Dropzone '.dropzone-scr', {}
        scrUpdr.on 'success', ( file, responseText ) =>
          @fields.model.attributes.offer_screenshots.push responseText
          screenshot = App.request 'create:screenshot:entity', responseText
          @buildScrView screenshot

        targets = offer.get 'offer_targets'
        _.each targets, ( target, index ) ->
          Modify
            .Controller
              .buildTarget index, target

        headings = offer.get 'landing_headings'
        _.each headings, ( head, index ) =>
          @buildHeading index, head

      @fields.on 'set:tariffes seller:change', =>
        select = $ 'select.seller-id'
          .find 'option:selected'
            .data 'kind'
        $ '[name="offer[kind]"]'
          .val select
        fetcher = $ '.offer-fetcher'
        tariffInp = $ '.offer-tariff'
        tariffInp.val tariffInp.val().replace /,/g, '.'
        dealerTariff = 0.8 * parseFloat tariffInp.val()
        if select == 'apps_advert'
          fetcher.removeClass 'hide'
          targetName = 'Установка приложения'
        unless select == ''
          $ '.destroy'
            .val 'true'
          $ '.offer-target'
            .hide()
          @buildTarget window.randomInt(),
            { name: targetName, price: dealerTariff.toFixed 2 }

      @fields.on 'add:heading', =>
        @buildHeading window.randomInt()

      @fields.on 'affilate:offer', =>
        network_id = $ 'select.seller-id'
          .find 'option:selected'
            .data 'prefix'
        offerId = $ '.external-id'
          .val()
        offerUrl = '/admin/affilate_offer?offer_id='
        offerUrl += offerId
        offerUrl += '&network_id='
        offerUrl += network_id
        affilate = $.get offerUrl
        affilate.done ->
          if affilate.responseJSON.status == 1
            offer = affilate.responseJSON.data.Offer
            $ '.name'
              .val offer.name
            $ '.country-list'
              .val offer.country_list
            $ '.preview-url'
              .val offer.preview_url
          else
            alert affilate.responseJSON.errorMessage
        affilate.fail ->
          alert 'Fetching failed!'

      @fields.on 'confirm:submit', =>
        if confirm 'Вы уверены?'
          data = Backbone.Syphon.serialize @form
          if @fields.triggerMethod( 'form:submit', data ) isnt false
            model = @fields.model
            collection = @fields.collection
            @processFormSubmit data, model, collection

      App.mainRegion.show @form

    processFormSubmit: ( data, model, collection ) ->
      model.save data,
        collection: collection

    buildScrView: ( screenshot ) ->
      scrView = @getScreenshowView screenshot
      $ @fields.screenshotsRegion.el
        .append scrView.el

    buildHeading: ( lhid, head ) ->
      headObj = { lhid: lhid }
      unless typeof head == 'undefined'
        headObj = _.extend headObj, head
      headModel = new App.Entities.Model headObj
      hView = @getHeadingView headModel
      $ '#headings-region'
        .append hView.el

      hView.on 'delete:heading', ( view ) =>
        $ view.el
          .hide 'fast'
            .find '.destroy'
              .val 'true'

    buildTarget: ( otid, target ) ->
      targetObj = { otid: otid }
      unless typeof target == 'undefined'
        targetObj = _.extend targetObj, target
      targetModel = new App.Entities.Model targetObj
      tView = @getTargetView targetModel
      $ '#offer-targets'
        .append tView.el

      tView.on 'delete:target', ( view ) =>
        $ view.el
          .hide 'fast'
            .find '.destroy'
              .val 'true'

    getOfferView: ( offer ) ->
      new Modify.Fields
        model: offer

    getScreenshowView: ( screenshot ) ->
      v = new Modify.Screenshot
        model: screenshot
      v.render()
      v

    getTargetView: ( target ) ->
      v = new Modify.OfferTarget
        model: target
      v.render()
      v

    getHeadingView: ( head ) ->
      v = new Modify.Heading
        model: head
      v.render()
      v
