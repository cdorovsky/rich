@RichAdmin.module 'OffersApp', (
    OffersApp, App, Backbone, Marionette, $, _
  ) ->

  class OffersApp.Router extends Marionette.AppRouter
    appRoutes:
      'offers'          : 'indexOffers'
      'offers/new'      : 'newOffer'
      'offers/:id/edit' : 'editOffer'
      'offers/:id'      : 'showOffer'

  API =
    indexOffers: ->
      OffersApp
        .Index
          .Controller
            .indexOffers()

    editOffer: ( id ) ->
      OffersApp
        .Modify
          .Controller
            .modify id

    showOffer: ( id ) ->
      OffersApp
        .Show
          .Controller
            .show ( id )

    newOffer: ->
      OffersApp
        .Modify
          .Controller
            .modify()

  App.vent.on 'offer:edit:clicked', ( offer ) ->
    App.navigate "offers/#{offer.id}/edit"
    API.edit ticket.id, ticket

  App.vent.on 'offers:index', ->
    App.navigate '#offers'
    API.indexOffers()

  App.addInitializer ->
    new OffersApp.Router
      controller: API
