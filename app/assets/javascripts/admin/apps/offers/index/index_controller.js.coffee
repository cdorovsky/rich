@RichAdmin
  .module 'OffersApp.Index', (
    Index, App, Backbone, Marionette, $, _
    ) ->

  Index.Controller =

    indexOffers: ->
      App.request 'offers:entities', ( offers ) =>
        countries = new App.Entities.Countries
        countries.fetch()
        App.execute 'when:fetched', countries, =>
          @countries = countries
          sellers = new App.Entities.SellersCollection
          sellers.fetch()
          App.execute 'when:fetched', sellers, =>
            @sellers = sellers
            helper_entity = new App.Entities.Model
              countries: @countries.attributes.countries
              sellers: @sellers.toList()
            @layout = @getLayoutView offers, helper_entity

            @layout.on 'show', =>
              @showOffers offers

            @layout.on 'offer:filter', ( offers ) =>
              oQid = ->
                selValue = parseInt $( 'input.offer-id' ).val()
                if isNaN selValue
                  ''
                else
                  selValue
              oQext_id = ->
                selValue = parseInt $( 'input.offer-external_id' ).val()
                if isNaN selValue
                  ''
                else
                  selValue
              oQdisabled = ->
                selValue = $ 'select.offer-state'
                  .val()
                if selValue == 'true'
                  true
                else if selValue == 'false'
                  false
                else
                  ''
              offerQuery =
                id:                  oQid()
                external_id:         oQext_id()
                disabled:            oQdisabled()
                kind:                $( 'select.offer-kind' ).val()

              Object.keys offerQuery
                .forEach (k) ->
                  if offerQuery[k] == '' || offerQuery[k] == NaN
                    delete offerQuery[k]

              if _.isEmpty offerQuery
                offers = offers.models
              else
                offers = offers.where offerQuery

              partName = $ 'input.offer-name'
                .val()

              if partName
                offers = _.reject offers, ( offer ) ->
                  indexOfResponse = offer
                    .get 'name'
                      .indexOf partName
                  indexOfResponse == -1

              countries = $ 'select.offer-countries'
                .val()

              if countries
                offers = _.reject offers, ( offer ) ->
                  country_list = offer
                    .get 'country_list'
                      .split ', '
                  _.intersection country_list, countries
                    .length == 0

              seller_id = $ 'select.offer-seller_id'
                .val()

              if seller_id
                offers = _.reject offers, ( offer ) ->
                  o_seller_id = offer.get 'seller_id'
                  parseInt( seller_id ) != o_seller_id

              offers = new App.Entities.OffersCollection offers

              @showOffers offers

            @layout.on 'mass:offer:switch', =>
              if confirm 'Вы уверены?'

                inputs = $ '.offers-action:checked'

                action = $ '.offer-action-behaviour'
                  .find 'option:selected'
                    .val()

                unless inputs.length == 0 || action.length == 0
                  offerIds = []
                  inputs.each ( index, inp ) ->
                    offerIds.push inp.name

                  switchRequest =
                    offer_ids: offerIds
                    disabled: action

                  offerRequest = $.post '/admin/mass_offer_switch', switchRequest
                  offerRequest.done =>
                    App.request 'offers:entities', ( offers ) =>
                      @showOffers offers
                  offerRequest.fail ->
                    alert 'Error!'

            App.mainRegion.show @layout

    showOffers: ( offers ) ->
      offersView = @getOffersView offers

      offersView.on 'show', =>
        $ offersView.el
          .foundation 'tooltip', {}

      @layout.offersRegion.show offersView

    getLayoutView: ( offers, helper_entity ) ->
      new Index.Layout
        collection: offers
        model: helper_entity

    getOffersView: ( offers ) ->
      new Index.Offers
        collection: offers

