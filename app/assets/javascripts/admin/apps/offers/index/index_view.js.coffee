@RichAdmin
  .module 'OffersApp.Index', (
     Index, App, Backbone, Marionette, $, _
    ) ->

  class Index.Layout extends App.Views.Layout
    template: 'offers/index/templates/index_layout'

    regions:
      offersRegion: '#offers-region'

    events:
      'click .checkall' : 'checkAll'
      'click .apply-offer-filter' : -> @trigger 'offer:filter', @collection
      'click .apply-offer-action' : -> @trigger 'mass:offer:switch'

    onRender: ->
      $ @el
        .find 'select.offer-countries'
          .select2
            maximumSelectionSize: 7,
            width: '100%',
            placeholder: 'Выберите страны'

    checkAll: ( e ) ->
      @isChecked = e.currentTarget.checked
      boxes = $ '.offers-action'
      if @isChecked
        $ '.offers-action:not(:checked)'
          .click()
          .attr 'checked', 'checked'
      else
        boxes
          .removeAttr 'checked'

  class Index.Offer extends App.Views.ItemView
    template: 'offers/index/templates/_offer'
    tagName: 'tr'

  class Index.Empty extends App.Views.ItemView
    template: 'offers/index/templates/_empty'
    tagName: 'tr'

  class Index.Offers extends App.Views.CompositeView
    template: 'offers/index/templates/offers'
    itemViewEventPrefix: 'childview'
    itemView: Index.Offer
    emptyView: Index.Empty
    itemViewContainer: 'tbody'
