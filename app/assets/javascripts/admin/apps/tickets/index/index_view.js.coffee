@RichAdmin
  .module 'TicketsApp.Index', (
     Index, App, Backbone, Marionette, $, _
    ) ->

  class Index.Layout extends App.Views.Layout
    template: 'tickets/index/templates/layout'
    regions:
      ticketsRegion: '#tickets-region'
      filterRegion: '#filter-region'

  class Index.Filter extends App.Views.ItemView
    template: 'tickets/index/templates/filter'
    className: 'row'
    events:
      'click .filter-tickets' : -> @trigger 'filter'

  class Index.Ticket extends App.Views.ItemView
    template: 'tickets/index/templates/_ticket'
    tagName: 'tr'

  class Index.Empty extends App.Views.ItemView
    template: 'tickets/index/templates/_empty'
    tagName: 'tr'

  class Index.Tickets extends App.Views.CompositeView
    template: 'tickets/index/templates/tickets'
    itemViewEventPrefix: 'childview'
    tagName: 'table'
    className: 'margin-t-20'
    itemView: Index.Ticket
    emptyView: Index.Empty
    itemViewContainer: 'tbody'
