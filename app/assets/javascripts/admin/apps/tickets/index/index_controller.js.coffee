@RichAdmin
  .module 'TicketsApp.Index', (
    Index, App, Backbone, Marionette, $, _
    ) ->

  Index.Controller =

    indexTickets: ->
      @layout = @getLayoutView()

      @layout.on 'show', =>
        @buildFilter()

      App.mainRegion.show @layout

    buildFilter: ->
      filterView = @getFilterView()
      filterView.on 'filter show', =>
        inps = $ '.filter'
        inps = _.map inps, ( inp ) ->
          $inp = $ inp
          [ inp.getAttribute( 'data-filter' ), $( $inp ).val() ]

        query = _.object inps
        _.each query, ( val, key ) ->
          if query[key] == null || query[key].length == 0
            delete query[key]

        @tickets = new App.Entities.TicketsCollection
        @tickets.fetch
          data: query
          success: =>
            @showTickets @tickets

      filterView.on 'show', =>
        $ '.date'
          .datetimepicker()

      @layout.filterRegion.show filterView


    showTickets: ( tickets ) ->
      @ticketsView = @getTicketsView tickets
      @layout.ticketsRegion.show @ticketsView

    getFilterView: ->
      new Index.Filter

    getLayoutView: ->
      new Index.Layout

    getTicketsView: ( tickets ) ->
      new Index.Tickets
        collection: tickets
