@RichAdmin.module 'TicketsApp', (
    TicketsApp, App, Backbone, Marionette, $, _
  ) ->

  class TicketsApp.Router extends Marionette.AppRouter
    appRoutes:
      'tickets'          : 'indexTickets'
      'tickets/:id'      : 'showTicket'

  API =
    indexTickets: ->
      TicketsApp
        .Index
          .Controller
            .indexTickets()

    showTicket: ( id ) ->
      TicketsApp
        .Show
          .Controller
            .show( id )

  App.vent.on 'tickets:index', ->
    App.navigate 'tickets'
    API.indexTickets()

  App.addInitializer ->
    new TicketsApp.Router
      controller: API
