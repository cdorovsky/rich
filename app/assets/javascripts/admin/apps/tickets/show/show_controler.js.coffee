@RichAdmin
  .module 'TicketsApp.Show', (
    Show, App, Backbone, Marionette, $, _
    ) ->

  Show.Controller =
    show: ( id ) ->
      @layout = @getLayoutView()

      @layout.on 'show', =>
        @ticket = App.request 'ticket:entity', ( id )

        @ticket.on 'updated', =>
          @layout.trigger 'show'

        @ticket.on 'request:updated', =>
          @ticket.fetch
            success: =>
              @buildChat @ticket

        @ticket.on 'ticket:close', =>
          @ticket.closeTicket()

        App.execute 'when:fetched', @ticket, =>
          @ticketView = @getTicketView @ticket

          @form = App.request 'form:wrapper',
            @ticketView,
            footer: false

          @form.on 'show', =>
            @buildChat @ticket

          @form.on 'form:submit', =>
            @chatView.model.fetch()

          @layout.formRegion.show @form

      App.mainRegion.show @layout

    buildChat: ( ticket ) ->
      @chatView = @getChatView ticket

      @layout.chatRegion.show @chatView

    getChatView: ( ticket ) ->
      new Show.Chat
        model: ticket

    getLayoutView: ->
      new Show.Layout

    getTicketView: ( ticket ) ->
      new Show.Ticket
        model: ticket
