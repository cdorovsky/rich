@RichAdmin
  .module 'TicketsApp.Show', (
     Show, App, Backbone, Marionette, $, _
    ) ->

  class Show.Layout extends App.Views.Layout
    template: 'tickets/show/templates/show_layout'

    regions:
      formRegion: '#form-region'
      chatRegion: '#chat-region'

  class Show.Ticket extends App.Views.ItemView
    template: 'tickets/show/templates/ticket'
    events:
      'click a.update-ticket' : 'updTicket'
      'click a.close-ticket'  : 'closeTicket'

    closeTicket: ->
      @model.trigger 'ticket:close'

    updTicket: ->
      @model.trigger 'request:updated'

  class Show.Chat extends App.Views.ItemView
    template: 'tickets/show/templates/_chat'
    modelEvents:
      'change' : 'reRender'
      'fetch'  : 'render'

    reRender: ->
      @model.fetch()
