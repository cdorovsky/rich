@RichAdmin.module 'PaymentsApp', ( PaymentsApp, App, Backbone, Marionette, $, _ ) ->

  class PaymentsApp.Router extends Marionette.AppRouter
    appRoutes:
      'payments'    : 'indexPayments'
      'payments/new': 'newPayment'
      'payments/:id': 'showPayments'

  API =
    indexPayments: ->
      PaymentsApp.Index.Controller.indexPayments()

    newPayment: ->
      PaymentsApp.New.Controller.new()

    showPayments: ( id ) ->
      PaymentsApp.Show.Controller.show id

  App.vent.on 'payment:show', ( id ) ->
    App.navigate "#payments/#{id}"
    API.showPayments id

  App.vent.on 'payment:index', ->
    App.navigate "#payments"
    API.indexPayments()

  App.addInitializer ->
    new PaymentsApp.Router
      controller: API
