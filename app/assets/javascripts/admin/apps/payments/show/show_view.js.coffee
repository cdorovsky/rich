@RichAdmin.module 'PaymentsApp.Show', ( Show, App, Backbone, Marionette, $, _ ) ->

  class Show.Layout extends App.Views.Layout
    template: 'payments/show/templates/layout'
    tagName: 'form'
    regions:
      buttonsRegion: '.buttons-region'
      requestsRegion: '.requests-region'

    events:
      'click button' : 'updatePayment'

    updatePayment: (e) ->
      e.preventDefault()
      e.stopPropagation()
      idshash = Backbone.Syphon.serialize @
      _.each idshash, (v, k) ->
        if !v
          delete idshash[k]
      ids = _.keys idshash

      type = $ e.currentTarget
        .prop 'name'

      result =
        payment_action: type
        request_ids: ids
      @trigger 'update:payment', result

  class Show.Buttons extends App.Views.ItemView
    template: 'payments/show/templates/buttons'

  class Show.PaymentRequest extends App.Views.ItemView
    template: 'payments/show/templates/_request'
    tagName: 'tr'

  class Show.NoRequest extends Show.PaymentRequest
    template: 'payments/show/templates/_no_request'

  class Show.PaymentRequests extends App.Views.CompositeView
    template: 'payments/show/templates/requests'
    tagName: 'table'
    itemView: Show.PaymentRequest
    emptyView: Show.NoRequest
    itemViewContainer: 'tbody'
    events:
      'change .checkall' : 'checkAll'

    checkAll: ->
      val = $(@el).find('.checkall').prop('checked')
      $(@el).find('input').prop('checked', val)