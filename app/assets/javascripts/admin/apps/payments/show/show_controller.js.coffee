@RichAdmin.module 'PaymentsApp.Show', ( Show, App, Backbone, Marionette, $, _ ) ->

  Show.Controller =

    show: ( id ) ->
      @payment = new App.Entities.Payment
        id: id
      @layout = new Show.Layout
      @layout.on 'show', =>
        @payment.fetch()
        App.execute 'when:fetched', @payment, =>
          @buildViews()
      @layout.on 'update:payment', ( query ) =>
        if query.payment_action == 'table'
          if query.request_ids.length > 0
            $.download "/admin/payments/#{@payment.get('id')}/as_csv", 'request_ids', query.request_ids
          else
            alert 'Выберите хотя бы одну транзакцию!'
        else
          @payment.attributes = _.extend query, { id: @payment.get('id') }
          @payment.save {},
            success: =>
              @payment.fetch
                success: =>
                  @buildViews()
      App.mainRegion.show @layout

    buildViews: ->
      @buttons = @getButtons @payment
      @layout.buttonsRegion.show @buttons
      @payment_requests = @payment.requests()
      @prs = @getPaymentRequests @payment_requests
      @layout.requestsRegion.show @prs

    getButtons: ( payment ) ->
      new Show.Buttons
        model: payment

    getPaymentRequests: ( payment_requests ) ->
      new Show.PaymentRequests
        collection: payment_requests
