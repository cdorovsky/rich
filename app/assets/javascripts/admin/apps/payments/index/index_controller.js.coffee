@RichAdmin.module 'PaymentsApp.Index', ( Index, App, Backbone, Marionette, $, _ ) ->

  Index.Controller =

    indexPayments: ->
      @layout = new Index.Layout
      @layout.on 'show', =>
        @filter = new Index.Filter
        @filter.on 'filter', ( query ) =>
          @payments.fetch
            data: query
        @layout.filterRegion.show @filter
        @payments = new App.Entities.Payments
        @payments.fetch
          success: =>
            @table = @getTable @payments
            @layout.tableRegion.show @table
      App.mainRegion.show @layout

    getTable: ( collection ) ->
      new Index.Table
        collection: collection
