@RichAdmin.module 'PaymentsApp.Index', ( Index, App, Backbone, Marionette, $, _ ) ->

  class Index.Layout extends App.Views.Layout
    template: 'payments/index/templates/layout'
    regions:
      filterRegion:  '.filter-region'
      tableRegion: '.payments-region'

  class Index.Payment extends App.Views.ItemView
    template: 'payments/index/templates/_payment'
    tagName: 'tr'

  class Index.NoPayment extends Index.Payment
    template: 'payments/index/templates/_no_payment'

  class Index.Table extends App.Views.CompositeView
    template: 'payments/index/templates/table'
    className: 'small-12 columns'
    itemViewContainer: 'tbody'
    itemView: Index.Payment
    emptyView: Index.NoPayment

  class Index.Filter extends App.Views.ItemView
    template: 'payments/index/templates/filter'
    tagName: 'form'
    events:
      'submit' : 'applyFilter'

    onRender: ->
      _.each [ '.date_start', '.date_end' ], ( inp ) =>
        $ @el
          .find inp
            .fdatepicker()

    applyFilter: ( e ) ->
      e.preventDefault()
      e.stopPropagation()
      query = Backbone.Syphon.serialize @
      _.each query, (v, k) ->
        if !v
          delete query[k]
        else if ['date_start', 'date_end'].indexOf(k) > -1
          query[k] = query[k].split('.').reverse().join('-')
      @trigger 'filter', query
