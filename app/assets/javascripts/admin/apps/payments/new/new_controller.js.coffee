@RichAdmin.module 'PaymentsApp.New', ( New, App, Backbone, Marionette, $, _ ) ->

  New.Controller =

    new: ->
      @layout = new New.Layout
      @layout.on 'show', =>
        @requestsQuery =
          status: 'new'
        @filter = new New.Filter
          model: new App.Entities.Model
        @filter.on 'show', =>
          @requests = new App.Entities.PaymentRequests
          @buildTable()
        @filter.on 'filter', (query) =>
          @requestsQuery = query
          @buildTable()
        @layout.filterRegion.show @filter
      @layout.on 'create:payment', (ids) =>
        @payment = new App.Entities.Payment
        @payment.save { request_ids: ids },
          success: =>
            App.vent.trigger 'payment:show', @payment.get('id')
          error: =>
            alert 'error!11'
            window.location.reload()
      App.mainRegion.show @layout

    buildTable: ->
      @requests.fetch
        data: @requestsQuery
        success: =>
          @table = @getTable @requests
          @layout.tableRegion.show @table

    getTable: (reqs) ->
      new App.PaymentsApp.Show.PaymentRequests
        collection: reqs
