@RichAdmin.module 'PaymentsApp.New', ( New, App, Backbone, Marionette, $, _ ) ->

  class New.Layout extends App.Views.Layout
    template: 'payments/new/templates/layout'
    regions:
      tableRegion: '.table-region'
      filterRegion: '.filter-region'

    events:
      'click .create-payment' : 'createPayment'

    createPayment: ->
      selected = $ '.payment-request:checked'
      mapped = _.map selected, ( inp ) ->
        inp.getAttribute 'name'
      if mapped.length > 0
        @trigger 'create:payment', mapped
      else
        alert 'Выберите хотя бы один запрос на выплату!'

  class New.Filter extends App.Views.ItemView
    template: 'payments/new/templates/filter'

    onRender: ->
      _.each [ '.date_start', '.date_end' ], ( inp ) =>
        $ @el
          .find inp
            .fdatepicker()

    tagName: 'form'
    events:
      'click .filter' : 'applyFilter'

    applyFilter: ->
      query = Backbone.Syphon.serialize @
      _.each query, (v, k) ->
        if !v
          delete query[k]
        else if ['date_start', 'date_end'].indexOf(k) > -1
          query[k] = query[k].split('.').reverse().join('-')
      @trigger 'filter', query
