@RichAdmin.module "SidebarApp.List", ( List, App, Backbone, Marionette, $, _ ) ->

  List.Controller =

    listSidebar: ->
      nav_links = App.request "sidebar:entities" 
      sidebarView = @getSidebarView nav_links
      App.sidebarRegion.show sidebarView

    getSidebarView:  ( nav_links ) ->
       new List.Sidebars
        collection: nav_links
