@RichAdmin.module 'SidebarApp.List', ( List, App, Backbone, Marionette, $, _ ) ->

  class List.Sidebar extends App.Views.ItemView
    template: 'sidebar/list/templates/_item'
    tagName: 'li'

  class List.Sidebars extends App.Views.CompositeView
    template: 'sidebar/list/templates/sidebars'
    itemView: List.Sidebar
    itemViewContainer: 'ul'
    className: 'sidebar-menu'
    events:
      'click a' : 'setHighlight'

    setHighlight: ( e ) ->
      $ @el
        .find 'li'
          .removeClass 'active'
      $ e.currentTarget
        .parent 'li'
          .addClass 'active'

    beforeRender: ->
      $ @el
        .find 'li'
          .addClass 'active'
          .removeClass 'active'
    onRender: ->
      subAppName = window.location.hash.split( '/' )[1] || 'dashboard'
      $ @el
        .find 'li'
          .removeClass 'active'
          .find '.'+ subAppName
            .parent 'li' 
              .addClass 'active'

      $ @el
        .find 'a.finance'
          .parent 'li'
            .append '<li class="divider margin-t-10"></li>'
