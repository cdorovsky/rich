@RichAdmin.module 'DirectOffersApp', ( DirectOffersApp, App, Backbone, Marionette, $, _ ) ->

  class DirectOffersApp.Router extends Marionette.AppRouter
    appRoutes:
      'direct_offers' : 'index'

  API =
    index: ->
      App.DirectOffersApp
        .Index
          .Controller
            .index()

  App.vent.on 'direct:offers:index', ->
    App.navigate 'direct_offers'
    API.index()

  App.addInitializer ->
    new DirectOffersApp.Router
      controller: API
