@RichAdmin.module 'DirectOffersApp.Index', ( Index, App, Backbone, Marionette, $, _ ) ->

  class Index.Layout extends App.Views.Layout
    template: 'direct_offers/index/templates/layout'
    regions:
      filterRegion: '.filter-region'
      tableRegion: '.table-region'
      confirmRegion: '.confirm-region'
      moderationRegion: '.moderation-region'

  class Index.Filter extends App.Views.ItemView
    template: 'direct_offers/index/templates/filter'
    tagName: 'form'

    modelEvents:
      'change' : 'render'

    events:
      'submit form' : 'filter'
      'click a.filter' : 'filterReqs'

    filterReqs: (e) ->
      e.preventDefault()
      e.stopPropagation()
      @trigger 'filter'

    searchQuery: ->
      q = Backbone.Syphon.serialize @
      _.each q, (v, k) ->
        if !v
          delete q[k]
      q

    onRender: ->
      unless @model.get('dealers')
        @dealers = new App.Entities.CachedDealers
        @dealers.fetch
          success: =>
            @model.set 'dealers', @dealers.forSelectBox()
      unless @model.get('offers')
        @offers = new App.Entities.CachedOffersCollection
        @offers.fetch
          success: =>
            @model.set 'offers', @offers.forSelectBox().get('offers')
      @select2init()

    select2init: ->
      _.each ['.dealer-id', '.offer-id'], ( sel ) =>
        $ @el
          .find sel
            .select2
              placeholder: "Выберите " + if sel == '.offer-id' then "офферы" else "дилеров"
              width: '100%'

  class Index.ModerationModal extends App.Views.ItemView
    template: 'direct_offers/index/templates/moderation_modal'
    tagName: 'form'
    events:
      'click .moderation-launch' : 'moderation'

    moderation: ->
      query = Backbone.Syphon.serialize @
      @model.save query,
        success: =>
          alert 'Заявки промодерированы!'
          $ '.moderation-modal'
            .foundation 'reveal', 'close'
          App.vent.trigger 'direct:offers:index'

  class Index.DirectOffer extends App.Views.ItemView
    template: 'direct_offers/index/templates/_direct_offer'
    tagName: 'tr'

    modelEvents:
      'change' : 'render'

  class Index.EmptyDirectOffer extends App.Views.ItemView
    template: 'direct_offers/index/templates/_empty_direct_offer'
    tagName: 'tr'

  class Index.Table extends App.Views.CompositeView
    template: 'direct_offers/index/templates/table'
    itemView: Index.DirectOffer
    emptyView: Index.EmptyDirectOffer
    tagName: 'form'
    itemViewContainer: 'tbody'

    moderationInit: ->
      if _.isArray @getQuery().direct_offer_ids
        @model.set 'direct_offer_ids', @getQuery().direct_offer_ids
        @trigger 'init:moderation'
      else
        alert 'Выберите хотя бы одну заявку!'

    getQuery: ->
      query = Backbone.Syphon.serialize @
      query.direct_offer_ids = _.keys query.direct_offer_ids
      query

    checkAll: ->
      val = $(@el).find('.checkall').prop('checked')
      $(@el).find('input').prop('checked', val)

    events:
      'change .checkall' : 'checkAll'
      'click .moderation-init' : 'moderationInit'
      'click .moderation-launch' : 'moderation'

    modelEvents:
      'change' : 'render'
