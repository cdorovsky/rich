@RichAdmin.module 'DirectOffersApp.Index', ( Index, App, Backbone, Marionette, $, _ ) ->

  Index.Controller =

    index: ->
      @layout = @getLayoutView()
      @layout.on 'show', =>
        @filter = @getFilterView()
        @filter.on 'show', =>
          @direct_offers = new App.Entities.DirectOffersCollection
          @table = @getTableView @direct_offers
          @table.on 'init:moderation', =>
            @modalView = @getModerationView()
            @layout.moderationRegion.show @modalView
            $ @layout.el
              .find '.moderation-modal'
                .foundation 'reveal', 'open'
          @layout.tableRegion.show @table
          @buildTable()
        @filter.on 'filter', =>
          @buildTable()
        @layout.filterRegion.show @filter
      App.mainRegion.show @layout

    buildTable: ->
      query = @filter.searchQuery()
      @table.collection.fetch
        data: query
        success: =>
          @table.render()

    getLayoutView: ->
      new Index.Layout

    getFilterView: ->
      model = new App.Entities.Model
      new Index.Filter
        model: model

    getModerationView: ->
      new Index.ModerationModal
        model: @table.model

    getTableView: ->
      @moderation_model = new App.Entities.DirectOffer
      new Index.Table
        collection: @direct_offers
        model: @moderation_model
