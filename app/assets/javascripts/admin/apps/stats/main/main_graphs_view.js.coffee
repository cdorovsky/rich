@RichAdmin
  .module 'StatsApp.Main', (
     Main, App, Backbone, Marionette, $, _
    ) ->

  class Main.Graph extends App.Views.ItemView
    template: 'stats/main/templates/base_graph'
    graphRegion: ->
      $ @el
        .find '.graph-region'

  class Main.ProfitsGraph extends Main.Graph
    template: 'stats/main/templates/profits_graph'
    events:
      'change select' : 'changeDateScope'

    changeDateScope: ->
      @renderChat @currentDateScope()

    currentDateScope: ->
      $ @el
        .find 'select'
          .val()

    renderChat: ( dateScope='current_week' ) ->
      categories = @model.graphCategories( dateScope )
      series = @model.graphSeries( dateScope )
      rotation = if categories.length > 7
        90
      else
        0
      @graphRegion().highcharts
        chart:
          type: 'line'
        title:
          text: 'Доход по датам'
        xAxis:
          categories: categories
          labels:
            rotation: rotation
            style:
              fontSize: '11px'
              fontFamily: 'Verdana, sans-serif'
        yAxis: 
          title: text: 'Оборот ($)'
          min: 0
        plotOptions: line:
          dataLabels: 
            enabled: true
            color: '#FFFFFF'
            style:
              textShadow: '0px 0px 3px #000000'
          enableMouseTracking: true
        series: series

    onRender: ->
      @renderChat 'current_week'
