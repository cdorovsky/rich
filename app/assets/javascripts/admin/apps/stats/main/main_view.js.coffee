@RichAdmin
  .module 'StatsApp.Main', (
     Main, App, Backbone, Marionette, $, _
    ) ->

  class Main.Layout extends App.Views.Layout
    template: 'stats/main/templates/main_layout'
    regions:
      profitsGraphRegion: '.profits-graph-region'
      crTableRegion: '.cr-table-region'
      dealerStatsRegion: '.dealer-stats-region'
      dealerStatsFormRegion: '.dealer-stats-form-region'
      revenueCountersRegion: '.revenue-counters-region'

  class Main.CRTable extends App.Views.ItemView
    template: 'stats/main/templates/cr_table'
    itemViewContainer: 'tbody'
    tagName: 'table'
    itemView: Main.TopCrItem
    emptyView: Main.TopCrEmpty

  class Main.TopCrItem extends App.Views.ItemView
    template: 'stats/main/templates/_top_cr_row'
    tagName: 'tr'

  class Main.TopCrEmpty extends Main.TopCrItem
    template: 'stats/main/templates/_top_cr_empty'


  class Main.DealerStatsForm extends App.Views.ItemView
    template: 'stats/main/templates/dealer_stats_form'
    className: '.row'
    events:
      'click a.filter' : 'filterStats'

    filterStats: ->
      @trigger 'filter:stats'

    onRender: ->
      $ @el
        .find 'select.offer_id'
          .select2
            width: '100%',
            placeholder: 'Выберите офферы'


  class Main.DealerStat extends App.Views.ItemView
    template: 'stats/main/templates/_dealer_stat'
    tagName: 'tr'

  class Main.EmptyDealerStat extends Main.DealerStat
    template: 'stats/main/templates/_empty_dealer_stat'

  class Main.DealerStatsTable extends App.Views.CompositeView
    template: 'stats/main/templates/dealer_stats'
    itemViewContainer: 'tbody'
    tagName: 'table'
    itemView: Main.DealerStat
    emptyView: Main.EmptyDealerStat

  class Main.RevenueCountersView extends App.Views.ItemView
    template: 'stats/main/templates/revenue_counters'
