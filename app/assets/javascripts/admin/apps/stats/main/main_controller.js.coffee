@RichAdmin
  .module 'StatsApp.Main', (
    Main, App, Backbone, Marionette, $, _
    ) ->

  Main.Controller =

    mainStats: ->
      dealers = new App.Entities.CachedDealers
      dealers.fetch()
      App.execute 'when:fetched', dealers, =>
        @dealers = dealers
        @layout = @getMainLayout()
        @layout.on 'show', =>
          @showGraphs()
          @showDealerStats()
        App.mainRegion.show @layout

    showDealerStats: ->
      offers = new App.Entities.CachedOffersCollection
      offers.fetch()
      App.execute 'when:fetched', offers, =>
        dealerStatsFormView = @getDealerStatsForm offers.forSelectBox()
        @form = App.request 'form:wrapper', dealerStatsFormView,
          footer: false
          focusFirstInput: false
        dealerStatsFormView.on 'filter:stats', =>
          @dealerStatsView.collection.state.currentPage = 1
          params = @fetchFilterParams()
          @buildDealerStatsTable params
        @form.on 'show', =>
          _.each [ '.date_start', '.date_end' ], ( inp ) =>
            $ @form.el
              .find inp
                .fdatepicker()
        @layout.dealerStatsFormRegion.show @form
        @buildDealerStatsTable()

    fetchFilterParams: ->
      params = Backbone.Syphon.serialize @form
      _.each params, (v, k) ->
        if !v
          delete params[k]
        else if ['date_start', 'date_end'].indexOf(k) > -1
          params[k] = params[k].split('.').reverse().join('-')
      params

    buildDealerStatsTable: ( params={} ) ->
      raw_aggregates = new App.Entities.RawDealerStats
      _.extend raw_aggregates.queryParams, params
      raw_aggregates.fetch
        success: =>
          @dealerStats = raw_aggregates.toDealerStats( @dealers )
          @paginator = @getPaginatorView @dealerStats
          @dealerStatsView = @getDealerStatsTable @dealerStats
          @paginator.on 'show', =>
            @paginator
              .collectionRegion
                .show @dealerStatsView
          @paginator.on 'page:selected page:resized', ( pager ) =>
            requestedPage = pager.get 'requested_page'
            if requestedPage
              @dealerStatsView.collection.state.currentPage = requestedPage
            if pager.get 'new_page_size'
              pager.set 'page_size', pager.get 'new_page_size'
              pager.unset 'new_page_size'
              @dealerStatsView.collection.state.currentPage = 1
              @dealerStatsView.collection.state.pageSize = pager.get( 'page_size' )
            @dealerStatsView.collection.fetch
              success: =>
                @dealerStatsView.collection = @dealerStatsView.collection.toDealerStats( @dealers )
                @dealerStatsView.render()
                @paginator.model = @dealerStatsView.collection.pagerModel()
                @paginator.renderPages()
          @layout.dealerStatsRegion.show @paginator

    showGraphs: ->
      raw_timing_data = new App.Entities.RawTimingStats
      raw_timing_data.fetch()
      App.execute 'when:fetched', raw_timing_data, =>
        @showProfitsGraph raw_timing_data
        @showCRTable()
        @showRevenueCounters( raw_timing_data.toCountersTable() )

    showProfitsGraph: ( raw_timing_data ) ->
      graphView = @getProfitsGraph raw_timing_data.toGraphStats()
      @layout.profitsGraphRegion.show graphView

    showCRTable: ->
      crRows = new App.Entities.TopCRCollection
      crRows.fetch()
      App.execute 'when:fetched', crRows, =>
        rows = crRows.map ( row ) ->
          row.attributes
        attrs =
          rows: rows
        topCrModel = new App.Entities.Model attrs
        topCrView = @getCRTable topCrModel
        @layout.crTableRegion.show topCrView

    showRevenueCounters:  ( countersData ) ->
      countersView = @getRevenueCountersView countersData
      @layout.revenueCountersRegion.show countersView

    getMainLayout: ->
      new Main.Layout

    getProfitsGraph: ( graphStats ) ->
      new Main.ProfitsGraph
        model: graphStats

    getCRTable: ( crRows ) ->
      new Main.CRTable
        model: crRows

    getRevenueCountersView: ( countersData ) ->
      new Main.RevenueCountersView
        model: countersData

    getDealerStatsForm: ( model ) ->
      new Main.DealerStatsForm
        model: model

    getDealerStatsTable: ( aggregates ) ->
      new Main.DealerStatsTable
        collection: aggregates

    getPaginatorView: ( aggregates ) ->
      new App.Views.Paginator
        model: aggregates.pagerModel()
