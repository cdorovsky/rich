@RichAdmin.module 'StatsApp', (
    StatsApp, App, Backbone, Marionette, $, _
  ) ->

  class StatsApp.Router extends Marionette.AppRouter
    appRoutes:
      'stats' : 'mainStats'

  API =
    mainStats: ->
      StatsApp.Main.Controller.mainStats()

  App.vent.on 'main:stats', ->
    App.navigate '#/stats'
    API.mainStats()

  App.addInitializer ->
    new StatsApp.Router
      controller: API
