 @RichAdmin.module 'DealersApp.Mailer', ( Mailer, App, Backbone, Marionette, $, _ ) ->

  Mailer.Controller =

    showMailer: ( id ) ->
      if id
        @originDealerMailing = new RichAdmin.Entities.DealerMailing
          id: id
        @originDealerMailing.fetch()
      else
        @originDealerMailing = false
      @layout = @getLayout()
      @layout.on 'show', =>
        @buildForm()
      App.mainRegion.show @layout

    buildForm: ->
      dealersList = new RichAdmin.Entities.DealersList
      dealersList.fetch()
      App.execute 'when:fetched', dealersList, =>
        @addressView = @getAdrView dealersList
        @addressView.on 'submit:message', =>
          dealer_email_hash = Backbone.Syphon.serialize( @form )
          editor_hash = @editorView.getBody()
          # underscore.js cannot into deep hash merge
          dealer_email_hash.dealer_email.body = editor_hash.body
          dealer_email_hash.deliver_after_save = editor_hash.deliver_after_save
          @email = new RichAdmin.Entities.DealerMailing dealer_email_hash
          unless @originDealerMailing
            delete @email.attributes.dealer_email.id
          @email.save {},
            success: =>
              @originDealerMailing = @email
              App.vent.trigger 'dealers:mailer'

        @addressView.on 'send:scope:request', ( query ) =>
          scopeRequest = new RichAdmin.Entities.DealersScope
          scopeRequest.fetch
            data: query
          App.execute 'when:fetched', scopeRequest, =>
            @addressView.select2Dealers scopeRequest.get( 'dealer_ids' )

        @addressView.on 'append:offer:template', ( offer_id ) =>
          @disableCKFilter()
          offer = new RichAdmin.Entities.Offer
            id: offer_id
          offer.fetch()
          App.execute 'when:fetched', offer, =>
            layout = @getAppendableLayout()
            item = new Mailer.OfferTemplateView
              model: offer
            layout.renderAppendableView item

        @form = App.request 'form:wrapper', @addressView,
          footer: false
        @layout.addressRegion.show @form
        @buildEditor()

    disableCKFilter: ->
      CKEDITOR.instances.messageBody.filter.disable()

    buildEditor: ->
      @editorView = @getEditorView()
      @editorView.on 'show', =>
        CKEDITOR.replaceAll()
        if @originDealerMailing
          @populateData( @originDealerMailing )
        else
          CKEDITOR.instances.messageBody.setData ''

      @layout.editorRegion.show @editorView

    populateData: ( mailing ) ->
      _.each ['id', 'name', 'subject'], ( field ) =>
        $ @addressView.el
          .find ".#{ field }"
            .val mailing.get( field )
      @addressView.select2Dealers mailing.get( 'dealer_ids' )
      CKEDITOR.on 'instanceReady', ( evt ) =>
        mailingBody = @originDealerMailing.get 'body'
        CKEDITOR.instances.messageBody.filter.disable()
        CKEDITOR.instances.messageBody.setData mailingBody

    getAppendableLayout: ->
      new Mailer.BaseTemplateView

    getLayout: ->
      new Mailer.Layout

    getAdrView: ( dealersList ) ->
      new Mailer.AddressView
        model: dealersList

    getEditorView: ->
      new Mailer.EditorView
