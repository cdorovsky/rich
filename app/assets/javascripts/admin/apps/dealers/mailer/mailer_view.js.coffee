@RichAdmin.module 'DealersApp.Mailer', (
  Mailer, App, Backbone, Marionette, $, _
) ->

  class Mailer.Layout extends App.Views.Layout
    template: 'dealers/mailer/templates/layout'
    regions:
      addressRegion: '#address-form'
      editorRegion: '#editor-form'

  class Mailer.AddressView extends App.Views.ItemView
    template: 'dealers/mailer/templates/address'

    onRender: ->
      $ @el
        .find 'select.dealer_ids'
          .select2
            width: '100%',
            placeholder: 'Выберите дилеров'

    events:
      'click a.send'            : 'initSave'
      'click a.select-scope'    : 'scopeSelect'
      'change .scoper'          : 'updScoper'
      'click a.append-template' : 'appendTemplate'

    appendTemplate: ->
      offer_id = $( @el ).find( '.offer-id' ).val()
      @trigger 'append:offer:template', offer_id

    updScoper: ->
      scoper = $( @el ).find('.scoper')
      scopeParam = $( @el ).find( '.additional-params' )
      button = $( @el ).find( '.select-scope' )
      scopeParam.val('')
      placeholders = @scoperPlaceholders()
      unless scoper.val() == ''
        placeholder = placeholders[ scoper.val() ]
        unless typeof( placeholder ) == 'undefined'
          scopeParam.attr('placeholder', placeholder )
          scopeParam.parent().removeClass( 'hide' )
          button.parent().removeClass( 'hide' )
        else
          scopeParam.parent().addClass( 'hide' )
          button.parent().removeClass( 'hide' )
      else
        button.parent().addClass( 'hide' )

    scoperPlaceholders: ->
      with_recent_revenues: 'Количество дней'
      by_offer_id: 'ID оффера'
      by_offer_id_with_traffic: 'ID оффера'
      except_ids: 'ID дилеров не попадающих в рассылку'
      by_dealer_ids: 'Массив ID дилеров'

    dealersQuery: ->
      {
        scope_method: $( @el ).find( '.scoper' ).val(),
        scope_param: $( @el ).find( '.additional-params' ).val().split(', ')
      }


    scopeSelect: ->
      query = @dealersQuery()
      @trigger 'send:scope:request', query

    select2Dealers: ( dealer_ids ) ->
      $( '.dealer_ids' ).select2( 'val', dealer_ids )

    initSave: ->
      @trigger 'submit:message'

  class Mailer.EditorView extends App.Views.ItemView
    template: 'dealers/mailer/templates/editor'

    getBody: ->
      {
        deliver_after_save: $( @el ).find( '.confirm_deliver' ).is( ':checked' ),
        body: CKEDITOR.instances.messageBody.getData()
      }

  class Mailer.BaseTemplateView extends App.Views.Layout
    template: 'dealers/mailer/templates/base_appendable'

    regions:
      wrapperRegion: '.append-wrapper'

    renderAppendableView: ( view ) ->
      view.render()
      @appendToEditor view

    appendToEditor: ( view ) ->
      CKEDITOR.instances.messageBody.filter.disable()
      appendableItem = view.el.innerHTML
      beforeData = CKEDITOR.instances.messageBody.getData()
      CKEDITOR.instances.messageBody.setData( beforeData + appendableItem )

  class Mailer.OfferTemplateView extends App.Views.ItemView
    template: 'dealers/mailer/templates/offer_appendable'
