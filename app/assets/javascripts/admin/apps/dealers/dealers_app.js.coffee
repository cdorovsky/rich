@RichAdmin.module 'DealersApp', (
    DealersApp, App, Backbone, Marionette, $, _
  ) ->

  class DealersApp.Router extends Marionette.AppRouter
    appRoutes:
      'dealers'              : 'indexDealers'
      'dealers/:id/edit'     : 'editDealer'
      'dealers/:id'          : 'showDealer'
      'dealers_mailer/new'   : 'universalMailer'
      'dealers_mailer/index' : 'listEmail'
      'dealers_mailer/:id'   : 'editEmail'

  API =
    indexDealers: ->
      DealersApp
        .Index
          .Controller
            .indexDealers()

    editDealer: ( id ) ->
      DealersApp
        .Edit
          .Controller
            .editDealer id

    showDealer: ( id ) ->
      DealersApp
        .Show
          .Controller
            .showDealer id

    universalMailer: ->
      DealersApp.Mailer.Controller.showMailer()

    listEmail: ->
      DealersApp.ListEmail.Controller.listEmail()

    editEmail: ( id ) ->
      DealersApp.Mailer.Controller.showMailer id

  App.vent.on 'dealer:edit', ( dealer ) ->
    App.navigate "#dealers/#{dealer.id}/edit"
    API.editDealer dealer

  App.vent.on 'dealer:show', ( dealer ) ->
    App.navigate "#dealers/#{dealer.id}"
    API.showDealer dealer

  App.vent.on 'dealers:index', ->
    App.navigate '#dealers'
    API.indexDealers()

  App.vent.on 'dealers:mailer', ->
    App.navigate '#/dealers_mailer/index'
    API.listEmail()

  App.addInitializer ->
    new DealersApp.Router
      controller: API
