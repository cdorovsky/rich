@RichAdmin.module 'DealersApp.Index', ( Index, App, Backbone, Marionette, $, _ ) ->

  Index.Controller =

    indexDealers: ->
      @layout = @getLayout()

      @layout.on 'show', =>
        _.each [ 'input.date-from', 'input.date-to' ], ( inp ) ->
          $ inp
            .fdatepicker
              format: 'dd.mm.yyyy'

      @layout.on 'dealer:filter show', =>
        inps = $ '.filter'
        query = _.object _.map inps, ( inp ) ->
          [ inp.getAttribute( 'data-filter' ), $( inp ).val() ]
        _.each query, ( val, key ) ->
          if query[key] == null || query[key].length == 0
            delete query[key]
        dealers = new App.Entities.DealersCollection
        dealers.queryParams = _.extend dealers.queryParams, query
        dealers.fetch()
        App.execute 'when:fetched', dealers, =>
          @buildDealers dealers

      App.mainRegion.show @layout

    buildDealers: ( dealers ) ->
      @paginator = @getPaginatorView dealers
      @paginator.on 'show', =>
        @dealersView = @getDealersView dealers
        @paginator
          .collectionRegion
            .show @dealersView
      @layout
        .dealersRegion
          .show @paginator

      @paginator.on 'page:selected page:resized', ( pager ) =>
        requestedPage = pager.get 'requested_page'
        if requestedPage
          @dealersView.collection.getPage requestedPage
          @dealersView.collection.fetch
        if pager.get 'new_page_size'
          pager.set 'page_size', pager.get 'new_page_size'
          pager.unset 'new_page_size'
          @dealersView.collection.state.currentPage = 1
          @dealersView.collection.state.pageSize = pager.get( 'page_size' )
          @dealersView.collection.fetch()
        @paginator.model = @dealersView.collection.pagerModel()
        @paginator.renderPages()

      @layout.dealersRegion.show @paginator

    getDealersView: ( dealers ) ->
      new Index.Dealers
        collection: dealers

    getPaginatorView: ( dealers ) ->
      new App.Views.Paginator
        model: dealers.pagerModel()

    getLayout: ->
      new Index.Layout
