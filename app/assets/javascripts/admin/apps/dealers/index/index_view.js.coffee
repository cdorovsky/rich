@RichAdmin
  .module 'DealersApp.Index', (
     Index, App, Backbone, Marionette, $, _
    ) ->

  class Index.Layout extends App.Views.Layout
    template: 'dealers/index/templates/index_layout'
    regions:
      dealersRegion: '#index-region'
    events:
      'click a.filter-dealers' : 'filterDealers'

    filterDealers: ->
      @trigger 'dealer:filter'

  class Index.Dealer extends App.Views.ItemView
    template: 'dealers/index/templates/_dealer'
    tagName: 'tr'

  class Index.NoDealer extends App.Views.ItemView
    template: 'dealers/index/templates/_no_dealer'
    tagName: 'tr'

  class Index.Dealers extends App.Views.CompositeView
    template: 'dealers/index/templates/dealers_list'
    itemView: Index.Dealer
    emptyView: Index.NoDealer
    itemViewContainer: 'tbody'
