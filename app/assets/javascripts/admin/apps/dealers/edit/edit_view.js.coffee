@RichAdmin.module 'DealersApp.Edit', ( Edit, App, Backbone, Marionette, $, _ ) ->

  class Edit.Dealer extends App.Views.Layout
    template: 'dealers/edit/templates/dealer'

    regions:
      incDecRegion: '#incdec-region'

    modelEvents:
      'change' : 'render'

    onRender: ->
      wrp = $ document
        .find '#modal-wrapper'
      wrp.removeClass 'small'
      unless wrp.hasClass 'open'
        wrp
          .foundation 'reveal', 'open'

  class Edit.IncDec extends App.Views.ItemView
    template: 'dealers/edit/templates/_incdec'

    modelEvents:
      'change' : 'render'
