@RichAdmin.module 'DealersApp.Edit', ( Edit, App, Backbone, Marionette, $, _ ) ->

  Edit.Controller =

    editDealer: ( id ) ->
      dealer = App.request 'dealer:entity', id
      App.execute 'when:fetched', dealer, =>
        @dealerView = @getDealerView dealer

        @dealerView.on 'render', =>
          @adminRequest = new App.Entities.AdminRequest
          @adminRequest.set( 'dealer_id', dealer.get('id') ).set( 'dealer_balance', dealer.get('balance') )
          @adminRequest.on 'created', =>
            alert 'Баланс успешно изменен!'
            dealer.fetch()

          idView = @getIncDecView @adminRequest
          idForm = App.request 'form:wrapper', idView,
            footer: false
            focusFirstInput: false
          @dealerView.incDecRegion.show idForm

        @form = App.request 'form:wrapper', @dealerView,
          footer: false

        App.modalRegion.show @form

    getIncDecView: ( adminRequest ) ->
      new Edit.IncDec
        model: adminRequest

    getDealerView: ( dealer ) ->
      new Edit.Dealer
        model: dealer
