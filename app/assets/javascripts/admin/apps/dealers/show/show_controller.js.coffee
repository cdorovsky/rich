@RichAdmin
  .module 'DealersApp.Show', (
     Show, App, Backbone, Marionette, $, _
    ) ->

  Show.Controller =

    showDealer: ( id ) ->
      dealer = App.request 'dealer:entity', id
      App.execute 'when:fetched', dealer, =>
        requests = App.request 'dealer:requests', dealer.get 'id'
        App.execute 'when:fetched', requests, =>
          @dealerView = @getDealerView dealer

          @dealerView.on 'show', =>
            @paginator = @getPaginatorView requests

            @paginator.on 'show', =>

              @requestsView = @getRequestsView requests
              @paginator
                .collectionRegion
                  .show @requestsView

            @paginator.on 'page:selected page:resized', ( pager ) =>
              requestedPage = pager.get 'requested_page'
              if requestedPage
                @requestsView.collection.getPage requestedPage
              if pager.get 'new_page_size'
                pager.set 'page_size', pager.get 'new_page_size'
                pager.unset 'new_page_size'
                @requestsView.collection.state.currentPage = 1
                @requestsView.collection.state.pageSize = pager.get( 'page_size' )
                @requestsView.collection.fetch()
              @paginator.model = @requestsView.collection.pagerModel()
              @paginator.renderPages()

            @dealerView.historyRegion.show @paginator

          App.modalRegion.show @dealerView

    getDealerView: ( dealer ) ->
      new Show.Dealer
        model: dealer

    getRequestsView: ( requests ) ->
      new Show.History
        collection: requests

    getPaginatorView: ( requests ) ->
      new App.Views.Paginator
        model: requests.pagerModel()
