@RichAdmin
  .module 'DealersApp.Show', (
    Show, App, Backbone, Marionette, $, _
    ) ->

  class Show.Dealer extends App.Views.Layout
    template: 'dealers/show/templates/dealer'

    regions:
      historyRegion: '#money-history'

    events:
      'click a.close-modal'  : 'closeModal'
      'click a.send-message' : 'sendMessage'

    onRender: ->
      wrp = $ document
        .find '#modal-wrapper'
      wrp.removeClass 'small'
      unless wrp.hasClass 'open'
        wrp
          .foundation 'reveal', 'open'

    sendMessage: ->
      alert 'to be developed'

    closeModal: ->
      $ document
        .find '#modal-wrapper'
          .foundation 'reveal', 'close'

  class Show.Request extends App.Views.ItemView
    template: 'dealers/show/templates/_request'
    tagName: 'tr'

  class Show.NoReq extends App.Views.ItemView
    template: 'dealers/show/templates/_no_requests'
    tagName: 'tr'

  class Show.History extends App.Views.CompositeView
    template: 'dealers/show/templates/request_history'
    tagName: 'table'
    itemView: Show.Request
    emptyView: Show.NoReq
    itemViewContainer: 'tbody'
