@RichAdmin.module 'DealersApp.ListEmail', ( ListEmail, App, Backbone, Marionette, $, _ ) ->

  ListEmail.Controller =

    listEmail: ->
      @layout = @getLayout()
      @layout.on 'show', =>
        @buildList()
      App.mainRegion.show @layout

    buildList: ->
      App.request 'get:dealer:mailings', ( emails ) =>
        view = @getIndexView emails
        @layout.indexRegion.show view

    getLayout: ->
      new ListEmail.Layout

    getIndexView: ( emails ) ->
      new ListEmail.IndexView
        collection: emails
