@RichAdmin.module 'DealersApp.ListEmail', ( ListEmail, App, Backbone, Marionette, $, _ ) ->

    class ListEmail.Layout extends App.Views.Layout
      template: 'dealers/list_email/templates/layout'
      regions:
        indexRegion: '#index-region'

    class ListEmail.Delivery extends App.Views.ItemView
      template: 'dealers/list_email/templates/delivery'
      tagName: 'tr'

    class ListEmail.EmptyDelivery extends ListEmail.Delivery
      template: 'dealers/list_email/templates/empty_delivery'

    class ListEmail.IndexView extends App.Views.CompositeView
      template: 'dealers/list_email/templates/emails'
      itemView: ListEmail.Delivery
      emptyView: ListEmail.EmptyDelivery
      itemViewContainer: 'tbody'
      tagName: 'table'
