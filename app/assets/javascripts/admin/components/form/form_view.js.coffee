@RichAdmin.module 'Components.Form', ( Form, App, Backbone, Marionette, $, _ ) ->

  class Form.FormWrapper extends App.Views.Layout
    template: 'form/form'

    tagName: 'form'
    attributes: ->
      'data-type': @getFormDataType()

    regions:
      formContentRegion: '#form-content-region'

    triggers:
      'submit'                           : 'form:submit'
      'click [data-form-button="cancel"]': 'form:cancel'

    modelEvents:
      'change:_errors' : 'changeErrors'

    initialize: ->
      @setInstancePropertiesFor 'config', 'buttons'

    serializeData: ->
      footer: @config.footer
      buttons: @.buttons?.toJSON() ? false

    onShow: ->
      _.defer =>
        @focusFirstInput() if @config.focusFirstInput

    focusFirstInput: ->
      @$( ':input:visible:enabled:first' ).focus()

    getFormDataType: ->
      if @model.isNew() then 'new' else 'edit'

    changeErrors: ( model, errors, options ) ->
      if @config.errors
        if _.isEmpty errors then @removeErrors() else @addErrors errors

    removeErrors: ->
      @$( '.error' ).removeClass( 'error' ).find( 'h6' ).remove()

    addErrors: ( errors = {} ) ->
      for name, array of errors
        @addError name, array[0]

    addError: ( name, error ) ->
      el = @$( "[name='#{ name }']" )
      msg = $( '<h6 class="alert-color">' ).text error
      el.after( msg ).closest( '.row' ).addClass 'error'
