$ ->
  #Left menu
  $(".left-toggle1").click ->
    if $(this).hasClass("active")
      $(this).removeClass "active"
      $("body").removeClass "left-open-menu"
    else
      $(this).addClass "active"
      $("body").addClass "left-open-menu"
    false

  #Show|hide company info table
  $(".title-company").click ->
    id = $(this).attr("href")
    $(this).toggleClass "active"
    $(this).closest("tr").toggleClass "no-border"
    $(id + ".content-company").slideToggle()
    if $(this).hasClass("active")
      $(this).find(".icon use").attr "xlink:href", "#icon-caret-down"
    else
      $(this).find(".icon use").attr "xlink:href", "#icon-caret-right"
    false

  #Show|hide best-list Index page
  $(".best-list").click ->
    if $(this).hasClass("active")
      $(this).removeClass "active"
      $(this).find("a.show-hide").text "Развернуть"
    else
      $(this).addClass "active"
      $(this).find("a.show-hide").text "Свернуть"
    false

  #Show|hide best-list Index page
  $(".1best-list .show-hide").click ->
    if $(this).parent().hasClass("active")
      $(this).removeClass "active"
      $(this).parent().removeClass "active"
    else
      $(this).addClass "active"
      $(this).parent().addClass "active"
    false

  $ 'a.i-forget'
    .click ( e ) ->
      $ '.login-form'
        .slideUp()
      $ '.forget-form'
        .slideDown()

  $ 'a.i-remember'
    .click ( e ) ->
      $ '.forget-form'
        .slideUp()
      $ '.login-form'
        .slideDown()


  $ '.restore-password'
    .on "ajax:success", (e, data, status, xhr) ->
      if data.success
        $ 'form.restore-password'
          .find 'input'
            .removeClass 'err'
            .val 'Успешно. Проверьте почту.'
      else
        $ 'form.restore-password'
          .find 'input'
            .addClass 'err'
            .val data.dealer
            .focus ( e ) ->
              @el = $ e.currentTarget
              if @el.hasClass 'err'
                @el
                  .removeClass 'err'
                  .val ''

    .on "ajax:error", (e, xhr, status, error) ->
      alert error

  $ '#jms-slideshow'
    .jmslideshow()

  $ '.become'
    .click ( e ) ->
      e.preventDefault()
      e.stopPropagation()

      $ 'html, body'
        .animate
          scrollTop: '0px', 1000

      $ '#agreement'
        .foundation 'reveal', 'close'

      reg_form = $ 'a.register'
      unless reg_form.hasClass 'open'
        reg_form.click()

  _.each [ '.jms-arrows-next', '.jms-arrows-prev', 'nav.jms-dots > span' ], ( arrow ) ->
    $ arrow
      .click ( e ) ->
        if navigator.userAgent.match(/firefox/i)
          $('.step:not(.active)').hide()
        else
          $('.step:not(.active)').fadeOut 'fast'
        $('.step.active').fadeIn()
        #if $( '.step.active' ).hasClass 'jmstep2'
        #  $ '.jmstep1'
        #    .hide()
        #else
        #  $ '.jmstep1'
        #    .show 'fast'


  $ 'form.welcome_form'
    .bind 'ajax:success', ( e, data, status, xhr ) ->
      if data.success
        $ 'form#new_dealer'
          .find 'input'
            .removeClass 'err'
        location.reload()
      else
        _.each data.dealer, ( msg, name ) ->
          inp = $ '#' + name.split('.').slice(-1)[0]
          inp
            .addClass 'err'
            .val msg
            .attr 'type', 'text'
            .focus ( e ) ->
              @el = $ e.currentTarget
              if @el.hasClass 'err'
                @el
                  .removeClass 'err'
                  .val ''
              if @el.is '.password'
                @el
                  .attr 'type', 'password'

  jQuery.fn.swap = (b) ->
    b = jQuery(b)[0]
    a = this[0]
    a2 = a.cloneNode(true)
    b2 = b.cloneNode(true)
    stack = this
    a.parentNode.replaceChild b2, a
    b.parentNode.replaceChild a2, b
    stack[0] = a2
    @pushStack stack

  $(".drop-contacts .parent-contact").click ->
    $(".drop-contacts ul").show()
    $(".drop-contacts").addClass "active"
    return

  $(document).on "focus", ".drop-contacts ul input", ->
    $(".drop-contacts ul").hide()
    $(".drop-contacts").removeClass "active"
    $(this).parent().swap ".drop-contacts .parent-contact div"
    $(".drop-contacts .parent-contact input").focus()
    return

  $(document).click (event) ->
    return if $(event.target).closest(".drop-contacts").length
    $(".drop-contacts.active ul").hide()
    $(".drop-contacts").removeClass "active"
    $ '.contact'
      .focus (e) ->
        if $( e.target ).hasClass 'err'
          $ document
            .find 'input.contact'
              .removeClass 'err'
                .val ''

  $ '.next-slide'
    .click ( e ) ->
      e.preventDefault()
      arw = $ e.currentTarget
      $ 'html, body'
        .animate
          scrollTop: arw.offset().top, 1000

  $ 'a.exec'
    .click ( e ) ->
      $ e.currentTarget
        .parent 'li'
          .find ':input:visible:enabled:first'
            .focus()

  $(document).foundation 'reveal',
    opened: ( e ) ->
        $ e.target
          .find 'input'
            .first()
              .focus()

  regParam = $.parseParams(window.location.search)['?reg']
  unless typeof( regParam ) == 'undefined'
    $ '#reg-params'
      .val regParam
    reg_form = $ 'a.register'
    unless reg_form.hasClass 'open'
      reg_form.click()
