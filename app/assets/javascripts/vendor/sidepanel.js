$(function() {
  $('.left-toggle').click(function(){
      if($(this).hasClass('active')){
        $(this).removeClass('active');
        $('body').removeClass('left-open-menu');
      }
      else{
        $(this).addClass('active');
        $('body').addClass('left-open-menu');
      }
      return false;
    });
});