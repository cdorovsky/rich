window.randomInt = ->
  Math.floor(
    Math.random() * (200000 - 100000) + 100000
  )

window.setTitle = ( page_name ) ->
  window.document.title = "RichPays | #{ page_name }"
