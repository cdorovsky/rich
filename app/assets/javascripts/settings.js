$(function() {
  //Left menu
  $('.left-toggle1').click(function(){
      if($(this).hasClass('active')){
        $(this).removeClass('active');
        $('body').removeClass('left-open-menu');
      }
      else{
        $(this).addClass('active');
        $('body').addClass('left-open-menu');
      }
      return false;
    });

  //Show|hide company info table
  $('.title-company').click(function(){
    var id = $(this).attr('href');
    $(this).toggleClass('active');
    $(this).closest('tr').toggleClass('no-border');
    $(id+'.content-company').slideToggle();

    if($(this).hasClass('active')){
      $(this).find('.icon use').attr('xlink:href','#icon-caret-down');
    }
    else{
      $(this).find('.icon use').attr('xlink:href','#icon-caret-right');
    }
    return false;
  });

  //Show|hide best-list Index page
  $('.best-list').click(function(){
    if($(this).hasClass('active')){
      $(this).removeClass('active');
      $(this).find('a.show-hide').text('Развернуть');
    }
    else{
      $(this).addClass('active');
      $(this).find('a.show-hide').text('Свернуть');
    }
    return false;
  });

  //Show|hide best-list Index page
  $('.1best-list .show-hide').click(function(){
    if($(this).parent().hasClass('active')){
      $(this).removeClass('active');
      $(this).parent().removeClass('active');
    }
    else{
      $(this).addClass('active');
      $(this).parent().addClass('active');
    }
    return false;
  });

  //Card
  $('.card .view .button').each(function(){
    $(this).click(function(){
      $(this).closest('.view').fadeOut(function(){
        $(this).closest('.card').find('.edit').fadeIn();
      });
    });
  });

  $('.card .edit .button').each(function(){
    $(this).click(function(){
      $(this).closest('.card').find('.msg').fadeIn();
      $(this).closest('.card').addClass('msg-active');
    });
  });

  $('.card .msg .button').each(function(){
    $(this).click(function(){
      $(this).closest('.msg').fadeOut();
    });
  });

  $('#switch-auto-payment').each(function(){
    if ($(this).prop('checked')){
      $('.auto-payment').removeClass('active');
    }
    else{
      $('.auto-payment').addClass('active');
    }
  });


   $('.switch-auto-payment').click(function(){
      if ($('#switch-auto-payment').prop('checked')){
        $('.auto-payment').addClass('active');
      }
      else{
       $('.auto-payment').removeClass('active');
      }
  });
  //Check tabs
  $('.check-tab label').each(function(){
    $(this).click(function(){
      var id = '#'+$(this).find('input').attr('value');
      $(id+'.content').parent('.tabs-content').find('>.content').removeClass('active');
      $(id+'.content').addClass('active');
    });
  });

  //Show|hide pass
  $('.show-hide-pass .switch').click(function(){
    if ($(this).hasClass('active')){
      $(this).removeClass('active');
      $(this).next('input').attr('type','password');
      $(this).find('.icon use').attr('xlink:href','#icon-view-hide');
    }
    else{
      $(this).addClass('active');
      $(this).next('input').attr('type','text');
      $(this).find('.icon use').attr('xlink:href','#icon-view');
    }
  });

  //Reset input
  $('.reset').click(function(){
    $(this).next('input').val('');
  });

  //Reset input
  $('input.err').click(function(){
    $(this).removeClass('err');
    $(this).val('');
  });

  //Scroll-table auto-height
  function scroll_height(){
    $('.scroll-table').each(function(){
      var win_height = $(window).height(),
      this_wrap = $(this).closest('.scroll-wrapper');
      this_wrap.height(win_height-300);
      var scroll_height = this_wrap.height()-250;
      $(this).height(scroll_height);
    });
  }scroll_height();

  $(window).resize(function(){
    scroll_height();
  });

  $('.q-link li').hover(function(){
    var id = $(this).attr('class');
    $('.q-content q').hide().removeClass('active');
    $('.q-content #'+id).css('display','block').addClass('active');
  });
});
