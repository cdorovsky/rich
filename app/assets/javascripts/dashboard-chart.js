function dashboard_chartRender( series, selector ) {
	var seriesOptions = [],
		yAxisOptions = [],
		seriesCounter = 0,
		names = [ 'common' ],
		colors = ["#7cb5ec", "#434348", "#90ed7d", "#f7a35c", "#8085e9", "#f15c80", "#e4d354", "#8085e8", "#8d4653", "#91e8e1"]

		seriesOptions[0] = {
			name: 'Общий доход',
			data: series
		};

		seriesOptions[0].type = 'areaspline'
		seriesOptions[0].fillColor = {
                linearGradient : { x1: 0, y1: 0, x2: 0, y2: 1 },
                stops : [
                    [0, "#01a1ec"],
                    [1, "rgba(255,255,255,0)"]
								],
              }
       if (series.length > 0) {
			createDashChart( selector );
		} else {
			setPlaceholder();
		}

	function translate( name ) {
    if ( name == 'apps_advert' ){
      return 'Приложения'
    } else if ( name == 'common' ) {
      return 'Общий доход'
    } else if ( name == 'cpa' ) {
      return 'CPA (Плата за действие)'
    } else if ( name == 'wapclick' ) {
      return 'Wapclick'
    }
  }

  function setPlaceholder() {
    var pn, placeHolderNode;
    pn =   '<div class="rating-blank">'
    pn +=   '<img src="../assets/blank_dash_graph.png" alt="">'
    pn +=   '<span>Статистики пока нет, но как только вы получите доход она появится</span>'
    pn += '</div>'
    placeHolderNode = pn;
    $('#dash-graph-region').html(placeHolderNode);
  }

	function createDashChart( selector ) {

		$( selector ).highcharts('StockChart', {

			chart: { height: 270 },
	    rangeSelector: { enabled: false },
	    navigator: { enabled: false },
	    scrollbar: { enabled: false },
	    type: 'areaspline',
	    plotOptions: {
	    	line: {
	    		color: '#01a1ec'
	    	},
	    	series: {
	    		color: '#01a1ec',
		    	marker: {
		    		states: {
		    			hover: { radiusPlus: 25 }
		    		},
		    		enabled: false
		    	}
		    }
		  },
	    yAxis: {
	    	title: {
	    		style: { "color": '#01a1ec' },
	    		text: 'Доход ($)'
	    	},
	    	labels: {
	    		align: 'right',
	    		format: '$ {value}',
	    		y: 0,
	    		formatter: function() {
	    			return '$' + this.value;
	    		}
	    	},
	    	offset: 10,
	    	plotLines: [{
	    		value: 0,
	    		width: 2,
	    		color: 'silver'
	    	}],
	    	min: 0
	    },

	    tooltip: {
	    	pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>$ {point.y}</b><br/>',
	    	valueDecimals: 2
	    },

	    series: seriesOptions
		});
	}

}
