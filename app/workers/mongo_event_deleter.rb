class MongoEventDeleter
  include Sidekiq::Worker

  def perform( ids )
    MongoEventContainer.in( id: ids ).delete_all
  end

end
