class DirectOffersReloader
  include Sidekiq::Worker

  def perform( ids )
  	DirectOffer.where( id: ids ).each do |direct_offer|
  	  direct_offer.save
  	  direct_offer.load_to_rediska
  	end
  end

end
