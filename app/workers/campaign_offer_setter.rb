class CampaignOfferSetter
  include Sidekiq::Worker
  sidekiq_options :queue => :campaign_offer_setter

  def perform( campaign_id, offer_ids )
    Campaign.find( campaign_id ).set_offers!( offer_ids )
  end

end
