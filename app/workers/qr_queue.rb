class QrQueue

  @queue = :offer_set_qr_queue

  def self.perform( offer_set_id )
    OfferSet.find_and_create_qr_code( offer_set_id )
  end

end
