class OfferSetsReloader
  include Sidekiq::Worker

  def perform( ids )
  	OfferSet.where( id: ids ).each { |os| os.save }
  end

end
