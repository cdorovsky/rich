class OfferDisablerHandler
  include Sidekiq::Worker

  def perform( fact, dealer_id, offer_id, campaign_ids )
  	dealer = Dealer.find( dealer_id )
  	offer = Offer.find( offer_id )
    #SmsOfferNotification.send_by_type( type: fact.to_sym, dealer: dealer, offer: offer ) unless dealer.phone.blank?
    case fact.to_sym
    when :plan
      OfferMailer.plan_offer_disabling( dealer: dealer, offer: offer, campaign_ids: campaign_ids ).deliver
    when :instant
      OfferMailer.instant_offer_disabling( dealer: dealer, offer: offer, campaign_ids: campaign_ids ).deliver
    when :enable
      OfferMailer.offer_enable( dealer: dealer, offer: offer, campaign_ids: campaign_ids ).deliver
    end
  end

end
