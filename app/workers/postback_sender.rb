require 'sneakers'
require 'active_record'
require 'newrelic_rpm'
require 'new_relic/agent/method_tracer'

class PostbackSender
  include Sneakers::Worker
  include ::NewRelic::Agent::Instrumentation::ControllerInstrumentation
  include ::NewRelic::Agent::MethodTracer
  from_queue 'postback_sender'

  def work( msg )
    NewRelic::Agent.set_transaction_name("custom/postback_send")
    env = Rails.env || 'development'
    configuration = YAML::load(IO.read('config/database.yml'))
    begin
      ActiveRecord::Base.establish_connection( ENV['DATABASE_URL'] )
    rescue =>e
      Rails.logger.info e
      ActiveRecord::Base.establish_connection( ENV['DATABASE_URL'] )
    end
    dealer_id = msg.gsub( 'send_postbacks_for_dealer_id_', '' ).to_i
    if dealer_id > 0
      Postback.where( sended: false, dealer_id: dealer_id ).find_each do | postback |
        if postback.ready_to_send_postback?
          postback.find_and_relate_conversion!
          postback.send_notification
          postback.sended = true
          postback.save!
        end
      end
    end
    ActiveRecord::Base.connection.disconnect!
    ack!
  end

  add_transaction_tracer :work, :category => :task
  add_method_tracer :work, 'Custom/cash_update'

end