class SilentRedisReloader
  include Sidekiq::Worker

  def perform
    Rediska.load_countries
    Rediska.load_platform 
  end

end
