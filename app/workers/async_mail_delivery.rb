class AsyncMailDelivery
  include Sidekiq::Worker

  def perform( dealer_ids, subject, body )
    dealer_emails = Dealer.where( id: dealer_ids ).pluck( :email )
    dealer_emails.each do | email |
      DealerMailer.custom_email( email, subject, body ).deliver
    end
  end

end
