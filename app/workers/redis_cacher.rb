class RedisCacher
  include Sidekiq::Worker

  def perform( dealer_id )
    dashboard = Dashboard.wrap_cached_regions( dealer_id )
    $redis.set( "#{ dealer_id }_dashboard", dashboard.to_json )
    Precacher.replenish_stats_cache_for_dealer( dealer_id )
  end

end
