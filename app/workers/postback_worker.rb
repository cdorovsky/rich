class PostbackWorker

  include Sidekiq::Worker

  def perform
    Postback.to_send.find_each do | postback |
      if postback.ready_to_send_postback?
        postback.find_and_relate_conversion!
        postback.send_notification
        postback.sended = true
        postback.save!
      end
    end
  end

end
