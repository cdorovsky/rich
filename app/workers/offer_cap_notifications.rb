require 'sneakers'
require 'active_record'
require 'newrelic_rpm'
require 'new_relic/agent/method_tracer'

class OfferCapNotifications
  include Sneakers::Worker
  include ::NewRelic::Agent::Instrumentation::ControllerInstrumentation
  include ::NewRelic::Agent::MethodTracer
  from_queue 'offer_cap_notifications'

  def work( msg )
    NewRelic::Agent.set_transaction_name("custom/cash_update")
    env = Rails.env || 'development'
    configuration = YAML::load(IO.read('config/database.yml'))
    ActiveRecord::Base.establish_connection( ENV['DATABASE_URL'] )
    dealer_id = msg.gsub( 'update_dealer_id_', '' ).to_i
    Precacher.replenish_stats_cache_for_dealer( dealer_id ) if dealer_id > 0
    ack!
  end

  add_transaction_tracer :work, :category => :task
  add_method_tracer :work, 'Custom/cash_update'

end