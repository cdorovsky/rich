class MongoImportWorker
  include Sidekiq::Worker

  def perform( id )
    MongoEventContainer.find( id ).import_to_pg
  end

end
