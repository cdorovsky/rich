class OfferCapLimitNotification
  include Sidekiq::Worker

  def perform( fact, dealer_id, offer_id, campaign_ids )

  	dealer = Dealer.find( dealer_id )
  	offer = Offer.find( offer_id )

    case fact.to_sym
      when :email_expiring
        OfferMailer.daily_limit_offer_expiring( dealer: dealer, offer: offer, campaign_ids: campaign_ids ).deliver
      when :email_exceeded
        OfferMailer.daily_limit_offer_exceeded( dealer: dealer, offer: offer, campaign_ids: campaign_ids ).deliver
      when :sms
        SmsOfferNotification.send_offer_limit_notification( offer: offer, dealer: dealer )
    end
  end
end
