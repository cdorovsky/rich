class AdminStatsCacher
  include Sidekiq::Worker

  def perform
    Admin::Stats.reload_all
  end

end
