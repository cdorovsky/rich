json.extract! @dealer, :id, :email, :webmoney_wallets_count
json.balance @dealer.dealer_balance.confirmed.round(2)
json.currency @dealer.dealer_balance.user_currency
