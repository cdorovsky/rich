  json.extract! requests_history, :id, :status
  json.value requests_history.confirmed.to_i
  json.date requests_history.updated_at.strftime( '%F' )
  json.wallet @wallet
