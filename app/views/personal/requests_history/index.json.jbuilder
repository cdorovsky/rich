json.payment_requests @payment_requests do | payment_request |
  json.extract! payment_request, :id, :status
  json.value payment_request.confirmed
  json.date payment_request.updated_at.strftime( '%F' )
  json.wallet_name payment_request.wallet.try( :name )
end

json.admin_requests @admin_requests do | ar |
  json.extract! ar, :id, :value, :description
  json.created_at ar.created_at.strftime( '%F' )
end
