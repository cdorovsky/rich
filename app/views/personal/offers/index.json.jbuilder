json.array! [0,1] do |index|
  if index == 0
    json.total_pages @offers.total_pages
    json.total_entries @offers.total_entries
    json.current_page  @offers.current_page.to_i
    json.per_page @offers.per_page
  else
    json.array!( @offers ) do | offer |
      json.extract! offer,
               :id,
               :name,
               :country_list,
               :kind,
               :adult,
               :weblanding_ready,
               :featured,
               :adult,
               :motivated,
               :redirect,
               :preview_url
      json.rules simple_format( offer.rules )
      json.dealer_revenue ( offer.tariff * current_dealer.tariff.dealer_percent ).round(2)
      if offer.offer_icon
        json.icon_url offer.offer_icon.file_url
      elsif offer.icon
        json.icon_url offer.icon.url
      else
        json.icon_url ''
      end
    end
  end
end
