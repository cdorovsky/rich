json.array! [0,1] do |index|
  if index == 0
    json.total_pages   @conversions.total_pages
    json.total_entries @conversions.total_entries
    json.current_page  @conversions.current_page.to_i
    json.per_page      @conversions.per_page
    json.total_revenue @conversions.sum( :dealer_revenue )
  else
    json.array!( @conversions ) do | conversion |
      json.extract! conversion,
                      :offer_id,
                      :offer_name,
                      :dealer_revenue,
                      :source_url,
                      :campaign_name,
                      :kind,
                      :country,
                      :country_code,
                      :conversion_reason,
                      :sub_id,
                      :sub_id2,
                      :sub_id3
      json.id conversion.crypted_id
      if conversion.triggered_at
        json.created_at conversion.triggered_at.strftime( '%d.%m.%Y %H:%M:%S' )
      else
        json.created_at conversion.created_at.strftime( '%d.%m.%Y %H:%M:%S' )
      end
    end
  end
end
