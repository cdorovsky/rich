@stats ||= data
json.array!( @stats ) do | stat |
  json.extract! stat, *( stat.attributes.keys.map! { |attr_name| attr_name.to_sym } )
  json.created_at stat.try( :triggered_at ).try( :strftime, '%d.%m.%Y %H:00' )
  json.grouping_name     stat.grouping_name
  json.campaign_name  stat.grouping_name
end
