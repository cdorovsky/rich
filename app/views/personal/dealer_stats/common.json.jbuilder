json.array!( @stats ) do | stat |
  json.extract! stat, :id, :current, :hourly, :monthly, :dayly, :visitor, :uniq_visitor, :js_visitor, uniq_js_visitor, :traffic_back, :download, :money_total, :money_waiting, :money_confirmed, :money_declined, :subscribe, :unsubscribe, :rebill
  json.created_at stat.created_at.strftime( '%d.%m.%Y' )
  json.time_created stat.created_at.strftime( '%H:%M' )
  json.offer_name Offer.find(stat.offer_id).name if stat.offer_id
  json.campaign_name Campaign.find(stat.campaign_id).name if stat.campaign_id
  epc = !stat.uniq_visitor.zero? ? ( stat.money_total / stat.uniq_visitor ).round(2) : 0.00
  cr = !stat.uniq_visitor.zero? ? ( ( stat.download + stat.subscribe_wapclick ) / stat.uniq_visitor * 100 ) : 0
  json.cr cr.to_s
  json.epc epc
end
