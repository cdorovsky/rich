wapclick = @wapclick_stats.pluck( :money_total ).sum
apps_advert = @apps_advert_stats.pluck( :money_total ).sum
#cpa = @cpa_stats.pluck( :money_total ).sum
total = wapclick + apps_advert #+ cpa

unless total.zero?
  response = [ 'WapClick', ( wapclick / total ) * 100 ],
             [ 'Приложения', ( apps_advert / total ) * 100 ]
  #            [ 'CPA', ( cpa / total ) * 100 ]
  json.array! response
end
