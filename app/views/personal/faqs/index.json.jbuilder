json.array!( @faqs ) do | faq |
  json.extract! faq, :id, :question, :answer
  json.answer News.convert_content_to_html( faq[ :answer ] )
  json.date faq.updated_at.strftime( '%F' )
  json.time faq.updated_at.strftime( '%H:%M' )
  json.url faqs_url(faq, format: :json)
end
