  json.extract! @faq, :id, :question, :answer
  json.answer @answer
  json.date @faq.updated_at.strftime( '%F' )
  json.time @faq.updated_at.strftime( '%H:%M' )
