json.array!( @news ) do |news|
  json.extract! news, :id, :title
  json.short_content News.convert_content_to_html( news[ :short_content ] )
  json.content News.convert_content_to_html( news[ :content ] )
  json.date news.updated_at.strftime( '%d.%m' )
  json.full_date news.updated_at.strftime( '%Y.%d.%m' )
  json.url news_url( news, format: :json )
end
