json.extract! @news_item, :id, :title
json.short_content News.convert_content_to_html( @news_item[ :short_content ] )
json.content News.convert_content_to_html( @news_item[ :content ] )
json.date @news_item.updated_at.strftime( '%d.%m' )
json.full_date @news_item.updated_at.strftime( '%Y.%d.%m' )