@notes ||= notes
json.array!( @notes ) do | note |
  json.extract! note, :id, :title, :text
  json.date note.updated_at.strftime( '%d.%m' )
end
