json.extract! @offer, :id, :name, :weekly_cr, :weekly_epc, :banner_url
json.icon_url @offer.icon.url
json.icon_thumb_url @offer.icon.thumb.url
json.short_name @offer.name.split(' ').first(2).join(' ')
json.countries @countries