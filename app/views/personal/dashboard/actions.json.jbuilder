conversions ||= @conversions
json.array!( conversions ) do | conversion |
  json.extract! conversion, :offer_name, :campaign_name, :kind, :dealer_revenue, :country_id, :country_code, :country_name
  json.created_at conversion.created_at.strftime( '%d.%m.%y %H:%M' )
end
