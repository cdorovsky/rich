json.array!(@offers) do | offer |
  json.extract! offer, :id, :name, :weekly_cr, :weekly_epc, :banner_url
  json.icon_url offer.icon.url
  json.icon_thumb_url offer.icon.thumb.url
  json.countries @countries[ "#{offer.id}" ]
  json.url dashboard_offers_url( offer, format: :json)
end
