campaigns ||= current_dealer.campaigns.actual.order( 'updated_at DESC' ).limit( 5 )
json.array!( campaigns ) do | campaign |
  json.extract! campaign, :id, :name, :countries_count, :monthly_income, :offers_count
end
