json.array!(@ticket_messages) do |message|
  json.extract! message, :id, :dealer_id, :ticket_id, :number, :message_text, :ownership
end
