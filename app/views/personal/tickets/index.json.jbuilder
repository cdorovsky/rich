json.array!( @tickets ) do | t |
  json.extract! t, :id, :title, :message_counter, :last_answered, :dealer_id, :closed, :read
  json.updated_at t.updated_at.strftime( '%d.%m.%Y' )
  json.url ticket_url( t, format: :json )
end
