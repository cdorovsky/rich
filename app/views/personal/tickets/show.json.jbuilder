json.extract! @ticket, :id, :title, :message_counter, :last_answered, :dealer_id, :closed, :ticket_messages
json.created_at @ticket.created_at.strftime( '%H:%M %d.%m.%Y' )
json.updated_at @ticket.updated_at.strftime( '%d.%m.%Y' )
