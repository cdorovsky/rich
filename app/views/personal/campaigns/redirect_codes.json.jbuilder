json.array!(@redirect_codes) do |code|
  json.extract! code, :code, :offer_id, :offer_name
end
