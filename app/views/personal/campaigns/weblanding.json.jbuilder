json.array!( @offer_sets ) do | offer_set |
  json.extract! offer_set, :offer_id
  json.offer_name offer_set.offer.landing_name
  json.offer_icon offer_set.offer.offer_icon.file_url
  json.domain offer_set.offer.sms_domain
  json.redirect_code offer_set.id
  json.apps_os offer_set.offer.apps_os.titleize
  json.postfix offer_set.offer.webland_url_postfix
end
