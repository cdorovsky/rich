json.extract! @campaign,
                        :id,
                        :dealer_id,
                        :name,
                        :adult,
                        :offer_sets,
                        :trafficback_url,
                        :m_trafficback_url,
                        :broken_offer_behavior,
                        :megafon_buyout,
                        :beeline_buyout,
                        :mts_buyout,
                        :arbitrage,
                        :user_domains,
                        :wapclick_ready,
                        :postback_url,
                        :sent_postback,
                        :request_kind,
                        :countries,
                        :disable_weblandings
json.offers @campaign.offers.enabled
json.campaign_limiter @campaign.campaign_limiter.serialized_json
json.redirect_codes @campaign.redirect_codes_for_links
json.has_landings @campaign.has_landings?
json.created_at @campaign.created_at.strftime( '%H:%M %d.%m.%Y' )
json.redirect_code @campaign.hash_code
json.offer_sets_ids Hash[ @campaign.offer_sets.map{ | os | [ os.offer_id, os.id ] } ]
json.country_list Country.order( 'id ASC' ).pluck( :name )
json.restricted_countries ['1', '2', '3'] # массив с айди запрещенных стран
