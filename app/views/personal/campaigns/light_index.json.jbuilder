json.campaigns ( @campaigns ) do | campaign |
  json.extract! campaign,
                :id,
                :name
  json.url campaign_url( campaign.id )
end

json.offers @offers
