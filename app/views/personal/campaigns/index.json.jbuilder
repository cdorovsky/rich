json.array!( @campaigns ) do | campaign |
  json.extract! campaign,
                :id,
                :dealer_id,
                :name,
                :enabled,
                :adult,
                :countries_count,
                :archived
  json.offers_count campaign.offers.enabled.count
end
