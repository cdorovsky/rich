@dealer ||= dealer
json.extract! @dealer, 
  :id,
  :name,
  :email,
  :phone,
  :created_at,
  :skype,
  :icq,
  :jabber,
  :sign_in_count,
  :webmoney_wallets_count,
  :referrer_rate,
  :referal_link,
  :notification_settings
json.created_at @dealer.created_at.strftime( '%d.%m.%Y' )
json.balance @dealer.dealer_balance.confirmed.round(2)
