json.array! @direct_offers do | d_o |
  json.partial! 'direct_offer', direct_offer: d_o
end
