json.array!( @refs ) do | ref |
  json.extract! ref, :id, :email
  json.revenue ref.referrer_increments.sum( :confirmed )
end
