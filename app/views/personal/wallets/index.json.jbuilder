@wallets ||= wallets
json.array!(@wallets) do |wallet|
  json.extract! wallet, :id, :name, :kind, :number, :receiver, :receiver_address, :bank_name, :bank_address, :bank_city, :bank_country, :account_number_iban, :swift_code, :details
end
