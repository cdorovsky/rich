json.extract! @offer,
                    :id,
                    :seller_id,
                    :name,
                    :preview_url,
                    :description,
                    :kind,
                    :country_list,
                    :weblanding_ready,
                    :apps_os

adultness = if @offer.adult == 'common'
     'disallowed'
else
     'allowed'
end
json.adult adultness

revenue = if @offer.offer_targets.present?
     @offer.offer_targets.first.price
else
     @offer.dealer_revenue
end
json.revenue revenue
