json.array!( @offers ) do | offer |
  json.extract! offer,
           :id,
           :name,
           :country_list,
           :kind,
           :weblanding_ready
  adultness = if offer.adult == 'common'
    'disallowed'
  else
    'allowed'
  end
  json.adult adultness
  revenue = if offer.offer_targets.present?
    offer.offer_targets.first.price
  else
    offer.dealer_revenue
  end
  json.revenue revenue
end
