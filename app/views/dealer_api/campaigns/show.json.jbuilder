json.extract! @campaign,
                        :id,
                        :dealer_id,
                        :name,
                        :trafficback_url,
                        :m_trafficback_url,
                        :arbitrage,
                        :postback_url,
                        :sent_postback,
                        :request_kind,
                        :disable_weblandings
adultness = if @campaign.adult == 'common'
  'disallowed'
else
  'allowed'
end
json.adult adultness
json.countries @campaign.countries.pluck( :name ).join( ', ' )
json.set_offer_ids @campaign.offers.pluck( :id )
json.common_redirect_code 'http://xeclick.com/c/' + @campaign.hash_code
offer_redirect_codes = @campaign.offer_sets.map do | offer_set | 
    {
      url: 'http://xeclick.com/c/' + offer_set.id.to_s,
      offer_id: offer_set.offer_id,
      offer_name: offer_set.offer_name 
    }
end
json.offer_redirect_codes 
