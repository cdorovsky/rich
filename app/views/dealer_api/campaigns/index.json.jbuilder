json.array!( @campaigns ) do | campaign |
  json.extract! campaign,
                :id,
                :dealer_id,
                :name,
                :enabled,
                :created_at,
                :archived,
                :arbitrage
  adultness = if campaign.adult == 'common'
       'disallowed'
  else
       'allowed'
  end
  json.adult adultness
  json.countries campaign.countries.pluck( :name ).uniq.join( ', ' )
  json.offers_count campaign.offers.enabled.count
  json.set_offer_ids campaign.offers.pluck( :id )
end
