setCookie = (cookieName, cookieValue, nDays) ->
  today = new Date()
  expire = new Date()
  nDays = 1  if not nDays? or nDays is 0
  expire.setTime today.getTime() + 3600000 * 24 * nDays
  document.cookie = cookieName + "=" + escape(cookieValue) + ";expires=" + expire.toGMTString()
  return

url = document.URL
id_check = /[?&]buyer_id=([^&]+)/i
match = id_check.exec(url)
if match?
  buyer_id = match[1]
  setCookie('buyer_id', buyer_id, 7)
