# Popup css
css =   "div.overdiv { 
            filter: alpha(opacity=75);
            -moz-opacity: .75;
            opacity: .75;
            background-color: #c0c0c0;
            position: absolute;
            top: 0px;
            left: 0px;
            width: 100%; height: 100%; 
        }
        div.square { 
            position: absolute;
            top: 200px;
            left: 200px;
            background-color: Menu;
            border: #f9f9f9;
            height: 1200px;
            width: 1000px; }
        div.square div.msg { 
            color: #3e6bc2;
            font-size: 15px;
            padding: 15px; 
        }"

head  = document.head or document.getElementsByTagName("head")[0]
style = document.createElement("style")
style.type = "text/css"

if style.styleSheet
  style.styleSheet.cssText = css
else
  style.appendChild document.createTextNode(css)
head.appendChild style


# Popup class
myPop = ->
  @square = null
  @overdiv = null

  @popOut = (msg_content) ->
    @overdiv = document.createElement("div")
    @overdiv.className = "overdiv"
    @square = document.createElement("div")
    @square.className = "square"
    @square.Code = this
    msg = document.createElement("div")
    msg.className = "msg"
    msg.appendChild msg_content
    @square.appendChild msg
    closebtn = document.createElement("button")
    closebtn.onclick = ->
      @parentNode.Code.popIn()
      return
    closebtn.innerHTML = "Close"
    @square.appendChild closebtn
    document.body.appendChild @overdiv
    document.body.appendChild @square
    return

  @popIn = ->
    if @square?
      document.body.removeChild @square
      @square = null
    if @overdiv?
      document.body.removeChild @overdiv
      @overdiv = null
    return

  return

# Create popup with Seller's iFrame
createIFrame = (url) ->
  iframe = document.createElement("iframe")
  iframe.setAttribute "src", url
  iframe.style.width  = 1000 + "px"
  iframe.style.height = 1200 + "px"

  pop = new myPop()
  pop.popOut iframe
  return

# Business logic
urls = "<%=@url_list.join(',')%>".split(',')
for url in urls
    createIFrame url
