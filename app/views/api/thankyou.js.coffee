getCookie = (name) ->
  value = "; " + document.cookie
  parts = value.split("; " + name + "=")
  parts.pop().split(";").shift()  if parts.length is 2


loadScript = (url) ->
  head = document.getElementsByTagName("head")[0]
  script = document.createElement("script")
  script.type = "text/javascript"
  script.src = url
  head.appendChild script
  return

buyer_id = getCookie('buyer_id')
if buyer_id
  loadScript('http://api.<%=@domain%>/confirm/' + buyer_id + '.js')
