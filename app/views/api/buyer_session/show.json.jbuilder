json.id           @buyer_request.offer_set_id
json.enabled      @buyer_request.redis_offer[ :enabled ]
json.redirect_url @buyer_request.redirect_url
json.message      @buyer_request.message
json.img_url      @buyer_request.img_url
