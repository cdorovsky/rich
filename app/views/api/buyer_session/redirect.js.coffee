if navigator.userAgent.match(/(android|midp|j2me|symbian|series 60|symbos|windows mobile|windows ce|ppc|smartphone|blackberry|mtk|bada|mobile|windows phone|iphone|ipad)/i)!=null
  <%- if @offer_alertable %>
    if confirm "<%= @js_dialogue %>"
      window.location.replace "<%= raw @forward_url %>"
  <%- else %>
    window.location.replace "<%= raw @forward_url %>"
  <%- end %>
