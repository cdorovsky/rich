json.array! @offers + @disabled_offers do | offer |
  json.extract! offer,
    :id,
    :name,
    :country_list,
    :kind,
    :disabled,
    :our_profit,
    :partners_profit,
    :tariff,
    :external_id,
    :weblanding_ready
    json.clicks 'TBD'
    json.conversions 'TBD'
    json.seller_id offer.seller_id
    json.seller_name offer.seller.seller_name
end