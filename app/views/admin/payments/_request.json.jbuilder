json.( request, :id, :dealer_id, :confirmed, :payment_type, :status )
json.wallet do
  json.number request.wallet.number
  json.kind request.wallet.kind
end
json.dealer_email request.dealer.email
json.created_at request.created_at.strftime( '%d.%m.%Y %H:%M:%S' )
