json.array! @payments do | payment |
  json.extract! payment, :id, :paid
  json.created_at payment.created_at.strftime('%d.%m.%Y %H:%M')
  json.dealers_count PaymentRequest.where( payment_id: payment.id ).distinct.count( :dealer_id )
  json.total_payout_value payment.payment_requests.sum( :confirmed )
end
