json.extract! @payment, :id, :paid, :payment_requests
json.payment_requests @payment.payment_requests, partial: 'admin/payments/request', as: :request
json.created_at @payment.created_at.strftime( '%H:%M:%S %d.%m.%Y' )
