json.array! @dealer_mailings do | dealer_mailing |
  json.extract! dealer_mailing,
    :id,
    :name
  json.created_at dealer_mailing.created_at.strftime( '%d.%m.%Y %H:%M' )
end
