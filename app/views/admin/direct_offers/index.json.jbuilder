json.array! @direct_offers do | d_o |
  json.extract! d_o, :id, :dealer_id, :offer_id, :approved, :offer_name, :traffic_sources, :created_at
end
