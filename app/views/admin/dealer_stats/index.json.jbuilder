@aggregates ||= data
json.array! [0,1] do |index|
  if index == 0
    json.total_pages @aggregates.total_pages
    json.total_entries @aggregates.total_entries
    json.current_page  @aggregates.current_page.to_i
    json.per_page @aggregates.per_page
  else
    json.array! @aggregates do | aggregate |
      json.extract! aggregate,
      :daily_install_count,
      :daily_visitor_count,
      :conversion_rate,
      :seller_revenue,
      :daily_revenue,
      :id
    end
  end
end
