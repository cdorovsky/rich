@aggregates ||= data
json.array! @aggregates do | aggregate |
  json.extract! aggregate, :dealer_id, :conversion_rate, :seller_revenue, :daily_revenue
  json.dealer_email aggregate.dealer.email
end