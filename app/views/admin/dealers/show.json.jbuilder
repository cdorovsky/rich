json.extract! @dealer,
              :id,
              :email,
              :name,
              :blocked,
              :skype,
              :icq,
              :jabber,
              :phone,
              :wallets,
              :tariff_id
json.tariffs Tariff.all
json.balance @dealer.dealer_balance.confirmed.round( 2 )
json.tickets_count @dealer.tickets.count