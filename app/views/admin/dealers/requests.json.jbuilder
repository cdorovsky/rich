json.array! [0,1] do |index|
  if index == 0
    json.total_pages @payment_requests.total_pages
    json.total_entries @payment_requests.total_entries
    json.current_page  @payment_requests.current_page.to_i
    json.per_page @payment_requests.per_page
  else
    json.array! @payment_requests do | req |
      json.extract! req, :id, :confirmed, :status
      json.wallet_number req.wallet.number
      json.created_at req.created_at.strftime( '%d.%m.%y' )
    end
  end
end