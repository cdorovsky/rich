json.array! [0,1] do |index|
  if index == 0
    json.total_pages @dealers.total_pages
    json.total_entries @dealers.total_entries
    json.current_page  @dealers.current_page.to_i
    json.per_page @dealers.per_page
  else
    json.array! @dealers do | dealer |
      json.extract! dealer,
        :id,
        :email,
        :blocked
      json.balance dealer.dealer_balance.confirmed.round( 2 )
      json.created_at dealer.created_at.strftime( '%d.%m.%y' )
    end
  end
end