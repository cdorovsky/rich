json.array!(@tickets) do |ticket|
  json.extract! ticket,
    :id,
    :title,
    :dealer_id,
    :closed,
    :last_answered
  json.dealer_email ticket.dealer.email
  json.created_at ticket.updated_at.strftime( '%d.%m.%Y %H:%M' )
  json.updated_at ticket.updated_at.strftime( '%d.%m.%Y %H:%M' )
end
