json.extract! @ticket, :id, :title, :dealer_id, :closed, :message_counter, :dealer
json.dealer_email @ticket.dealer.email
json.dealer_name @ticket.dealer.name
json.ticket_messages @ticket.ticket_messages
json.updated_at @ticket.updated_at.strftime( '%d.%m.%Y %H:%M:%S' )
json.created_at @ticket.created_at.strftime( '%d.%m.%Y %H:%M:%S' )
