@aggregates ||= data
json.array! @aggregates do | aggregate |
  json.extract! aggregate,
    :daily_install_count,
    :daily_visitor_count,
    :conversion_rate,
    :seller_revenue,
    :daily_revenue
  json.created_at aggregate.created_at.in_time_zone.strftime('%d.%m.%y')
end
