module BuyerSessionHelper

  def self.session_expired( session )
    session[ :rich_session ] and session[ :rich_session ][ :expires ] <= Time.zone.now
  end

  def self.find_session( session )
    BuyerSession.find_by( session_key: session[ :rich_session ][ :session_key ] ) if session[ :rich_session ]
  end

  def self.init_campaign_limiters( session, buyer_request )
    campaign_id = buyer_request.campaign_id
    add_new_campaign_limiter( session, campaign_id ) unless campaign_limiter_exists( session, campaign_id )
  end

  def self.campaign_limiter_exists( session, campaign_id )
    session[ :rich_session ][ :js_limits ] and js_counters_from_cookie( session ).keys.include?( campaign_id.to_s )
  end

  def self.js_counters_from_cookie( session )
    js_counters_from_redis = JSON.parse( session[ :rich_session ][ :js_limits ] ).with_indifferent_access
  end

  def self.add_new_campaign_limiter( session, campaign_id )
    limiters    = Campaign.find( campaign_id ).campaign_limiter
    show_limit  = limiters.show_limits
    day_limit   = limiters.days
    hour_limit  = limiters.hours
    js_counters = Hash.new
    js_counters[ campaign_id.to_s ] = { counter: 0,
                                        show_limit: show_limit,
                                        day_limit: day_limit,
                                        hour_limit: hour_limit }
    session[ :rich_session ][ :js_limits ] = js_counters.to_json
  end

  def self.update_js_counter( session, buyer_request )
    campaign_id           = buyer_request.campaign_id
    js_counters_to_cookie = js_counters_from_cookie( session )
    js_counters_to_cookie[ campaign_id.to_s ][ :counter ] = js_counters_to_cookie[ campaign_id.to_s ][ :counter ].next
    session[ :rich_session ][ :js_limits ] = js_counters_to_cookie.to_json
    session
  end

  def self.check_js_campaign_limiters( session, campaign_id )
    campaign_limiters = js_counters_from_cookie( session )[ campaign_id.to_s ]
    show_limit_check  = campaign_limiters[ :counter ] < campaign_limiters[ :show_limit ]
    day_check         = CampaignLimiter.day_check( Time.zone.now.wday, campaign_limiters[ :day_limit ] )
    hour_check        = CampaignLimiter.hour_check( Time.zone.now.hour, campaign_limiters[ :hour_limit ] )
    show_limit_check and day_check and hour_check
  end

end
