module PaymentsHelper

  def self.to_created_at( params={} )
    hashed = params.to_hash
    date_start_str = hashed.delete( "date_start" ) || '1970-1-1'
    date_end_str   = hashed.delete( "date_end"   ) || 1.day.from_now.to_date.to_s
    date_start = Date.parse( date_start_str )
    date_end = Date.parse( date_end_str )
    hashed[ :created_at ] = date_start..date_end
    ::ActionController::Parameters.new( hashed )
  end

  def to_created_at( params={} )
    PaymentsHelper.to_created_at( params )
  end

end
