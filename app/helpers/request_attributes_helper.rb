module RequestAttributesHelper

  def self.buyer_request
    { code: :code, sub_id: :sub_id, sub_id2: :sub_id2, sub_id3: :sub_id3 }
  end

  def self.billing8
    { user_params: :buyer_request_id, status: :subscription_action, phone: :phone,
      subscription_id: :external_sub_id, id: :external_offer_id }
  end

  def self.onlypay
    { mydata: :buyer_request_id, mt_action: :subscription_action, abonent: :phone,
      subscription_id: :external_sub_id, service_id: :external_offer_id }
  end

  def self.plasticmedia
    { serviceSubscriptionId: :buyer_request_id, action_type: :subscription_action,
      phone: :phone, subscription_id: :external_sub_id, service_id: :external_offer_id }
  end

  def self.shemrock
    { pt_id: :buyer_request_id, status: :subscription_action }
  end

  def self.performance
    { aff_sub: :buyer_request_id, offer_id: :external_offer_id,
      transaction_id: :transaction_id, payout: :external_revenue }
  end

  def self.appflood
    { aff_sub: :buyer_request_id, offer_id: :external_offer_id,
      transaction_id: :transaction_id, payout: :external_revenue }
  end

  def self.iconpeak
    { aff_sub: :buyer_request_id, offer_id: :external_offer_id,
      transaction_id: :transaction_id, payout: :external_revenue }
  end

  def self.everyads
    { aff_sub: :buyer_request_id, offer_id: :external_offer_id,
      transaction_id: :transaction_id, payout: :external_revenue }
  end

  def self.mobvista
    { aff_sub: :buyer_request_id, offer_id: :external_offer_id,
      transaction_id: :transaction_id, payout: :external_revenue }
  end

  def self.unileadnetwork
    { aff_sub: :buyer_request_id, offer_id: :external_offer_id,
      transaction_id: :transaction_id, payout: :external_revenue }
  end

  def self.yeahmobi
    { aff_sub: :buyer_request_id, offer_id: :external_offer_id,
      transaction_id: :transaction_id, payout: :external_revenue }
  end

  def self.mobvistadirect
    { aff_sub: :buyer_request_id }
  end

  def self.matomy
    { postback: :buyer_request_id, offerid: :external_offer_id, county_name: :countryname }
  end

  def self.glispa
    { placement: :buyer_request_id }
  end

  def self.appflooddirect
    { aff_sub: :buyer_request_id }
  end

  def self.domobmedia
    { aff_sub: :buyer_request_id, offer_id: :external_offer_id,
      transaction_id: :transaction_id, payout: :external_revenue }
  end

  def self.youmi
    { aff_sub: :buyer_request_id }
  end

  def self.yeahmobidirect
    { aff_sub: :buyer_request_id }
  end

  def self.highcpi
    { aff_sub: :buyer_request_id }
  end

  def self.appsflyer
    { aff_sub: :buyer_request_id }
  end

  def self.wmadv
    { aff_sub: :buyer_request_id }
  end

  def self.solidclix
    { aff_sub: :buyer_request_id }
  end

  def self.solidclix
    { sid: :buyer_request_id }
  end

  def self.params_for( fact, params )
    clear_params = {}
    permitted_params = send fact
    permitted_params.each do |request_param, model_param|
      clear_params[ model_param ] = params[ request_param ] if params[ request_param ]
    end
    clear_params
  end 

end
