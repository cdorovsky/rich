module AdminAuthHelper

  def self.is_an_admin?( current_dealer )
    current_dealer && current_dealer.is_an_admin
  end

	def self.session_actual?( session )
		session[ :admin ] == true && session[ :admin_expiration_date ] && session[ :admin_expiration_date ] >= Time.zone.now
	end

  def self.build_session( session )
    random_digits = SmsAuth.random_digits
    session[ :admin_auth_code ] = random_digits
  end

  def self.update_admin_session( session )
    session[ :admin ] = true
    session[ :admin_expiration_date ] = Time.zone.now + 7.days
  end

  def self.clear_session( session )
    session.delete :admin
    session.delete :admin_ip
    session.delete :admin_auth_code
    session.delete :admin_expiration_date
  end

end
