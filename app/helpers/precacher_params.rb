module PrecacherParams

  def self.stats_params
    stats_with_dates_and_groupings
  end

  def self.base_stat_params
    { 
      "format" => "json",
      "controller" => "personal/dealer_stats",
      "action" => "table"
    }
  end

  def self.stats_with_groupings
    stat_groups = %w[ no_grouping offer_grouping campaign_grouping ]
    stat_groups.map! do | group |
      base_stat_params.merge( { 'grouping' => group } )
    end
    stat_groups
  end

  def self.stats_with_dates_and_groupings
    dates_groups = %w[ day week month quarter year ]
    dates_groups.map! do | group |
      stats_with_groupings.map { |g| g.merge( { 'date_scope' => group } ) }
    end
    dates_groups.flatten
  end

  def self.dealers_index
    {
      "controller" => "admin/dealers",
      "action" => "index",
      "format" => "json"
    }.deep_symbolize_keys
  end

  def self.offers_index
    {
      "controller" => "admin/offers",
      "action" => "index",
      "format" => "json"
    }.deep_symbolize_keys
  end

  def self.top_cr
    {
      "controller" => "admin/dealer_stats",
      "action" => "top_cr",
      "format" => "json"
    }.deep_symbolize_keys
  end

  def self.admin_stats_index
    {
      "controller" => "admin/dealer_stats",
      "action" => "index",
      "format" => "json",
      "page" => "1",
      "per_page" => "25"
    }.deep_symbolize_keys
  end

end
