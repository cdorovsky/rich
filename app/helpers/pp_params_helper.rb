module PpParamsHelper

  def self.turn_binary_hash_to_int( time_boolean_hash )
    time_boolean_hash.values.map{ | time_boolean_hash_element | time_boolean_hash_element ? 1 : 0 }.join.to_i( 2 )
  end

end
