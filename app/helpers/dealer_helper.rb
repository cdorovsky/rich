module DealerHelper
  def self.generate_ids_array( params )
    fetch_method = case params[ :scope_method ]
    when 'with_recent_revenues'
      days_count = params[ :scope_param ].first
      DailyAggregate.where( 'daily_revenue > (?)', 0 ).where( created_at: eval( "#{ days_count }.days.ago" )..Time.now ).pluck( :dealer_id ).uniq
    when 'by_offer_id'
      Campaign.where( id: OfferSet.where( offer_id: params[ :scope_param ] ).pluck( :campaign_id ).uniq.compact ).pluck( :dealer_id ).uniq
    when 'by_offer_id_with_traffic'
      DailyAggregate.where( offer_id: params[ :scope_param ] ).pluck( :dealer_id ).uniq
    when 'except_ids'
      Dealer.where( 'id NOT in (?)', params[ :scope_param ] ).pluck( :id )
    when 'only_active'
      Dealer.where( blocked: false ).pluck( :id )
    when 'by_dealer_ids'
      Dealer.where( id: params[ :scope_param ] ).pluck( :id )
    when 'all'
      Dealer.limit( 3000 ).pluck( :id )
    end
  end
end