module LandingHelper
  def self.sms_action( opts={} )
    request = opts[ :request ]
    code = opts[ :code ] + '/'
    landing_domain( request ) + "#{ code }send-link"
  end

  def self.landing_domain( request )
    subdomain = ( request.subdomain + '.' ) if request.subdomain.present?
    domain = request.domain + '/'
    "http://#{ subdomain }#{ domain }"
  end
end