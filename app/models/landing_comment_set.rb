class LandingCommentSet < ActiveRecord::Base
  belongs_to :offer
  belongs_to :offer_comment
end
