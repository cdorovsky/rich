class Clicker

  attr_accessor :id, :uniq

  def initialize( params = {} )
    redis_offer = offer_from_redis( params[ :id ] )
    params[ :dealer_id ] = redis_offer[ :dealer_id ]
    params[ :campaign_id ] = redis_offer[ :campaign_id ]
    params[ :offer_id ] = redis_offer[ :offer_id ]
    if params[ :kind ] == 'push'
      MongoEventContainer.js_push( params )
    else
      MongoEventContainer.clicker( params )
    end
  end

  def offer_from_redis( id )
    JSON.parse( $redis.get( "offer_set_#{ id }" ) ).with_indifferent_access
  end

end
