class MoneyOptions < ActiveRecord::Base

  belongs_to :dealer, dependent: :destroy
  belongs_to :wallet

  def self.auto_output
    MoneyOptions.where( enabled: true ).each do |options|
      dealer = Dealer.find( options.dealer_id )
      if dealer.has_valid_phone?
        value = options.dealer.dealer_balance.confirmed - options.dealer.dealer_balance.requested
        if options.pay_type == 'value' and value >= options.pay_value
          PaymentRequest.new_payment_request( dealer, options.pay_value, 'auto' )
        end
        if options.pay_type == 'time' and options.pay_day == Time.zone.now.wday and value > 1
          PaymentRequest.new_payment_request( dealer, value, 'auto' )
        end
      end
    end
  end
end
