class BuyerRequest

  BUYER_REQUEST_SALT = 'god love peace'

  attr_accessor :redirect_url, :message, :img_url, :redis_offer, :uniq_id, :request_params, :offer_set_id, :locale, :target

  def initialize( request, params= {} )
    @offer_set_id = params[ :id ]
    @redis_offer = offer_from_redis( params[ :id ] )
    @request_information = RequestInformation.new( request )
    @locale = @request_information.locale
    @request_params  = {
      referer:       @request_information.referer,
      ip:            @request_information.ip,
      country:       @request_information.country,
      user_agent:    @request_information.user_agent,
      user_platform: @request_information.user_platform
    }
    @message = RequestInformation.message_l10n( @redis_offer[:message], @locale )
    @target  =  params[ :action ] == 'target_show'
    if @target
      if possible_to_handle( params[ :id ] )
        @uniq_id = MongoEventContainer.buyer_request( request: @request_params, redis: @redis_offer, web_params: params )
        @redirect_url = generate_redirect_url( @uniq_id, @redis_offer )
        if request.try( :format ) == 'text/html'
          Clicker.new( params )
        end
      else
        @uniq_id = BSON::ObjectId.new.to_s
        @redirect_url = @redis_offer[ :preview_url ] || 'about:blank'
      end
    else
      if possible_to_handle( params[ :id ] )
        params[ :no_count ] = true if request.format == 'json'
        @uniq_id = MongoEventContainer.buyer_request( request: @request_params, redis: @redis_offer, web_params: params )
        @redirect_url = generate_redirect_url( @uniq_id, @redis_offer )
        if request.try( :format ) == 'text/html'
          Clicker.new( params )
        end
      else
        @uniq_id = BSON::ObjectId.new.to_s
        @redirect_url = choose_tb_url
        if request.format == 'text/html'
          active_offer_set_id = @redis_offer[ :enabled ] ? [ @offer_set_id ] : []
          tb_reason = OffersFilter.tb_reason( @request_params[ :user_platform ], @request_params[ :country ], active_offer_set_id )
          TrafficBack.new( :offer_set, dealer_id: @redis_offer[ :dealer_id ], campaign_id: @redis_offer[ :campaign_id ], offer_id: @redis_offer[ :offer_id ], offer_set_id: @offer_set_id, referer: @request_params[ :referer ], reason: tb_reason )
        end
      end
    end
  end

  def offer_from_redis( id )
    JSON.parse( $redis.get( "offer_set_#{ id }" ) ).with_indifferent_access
  end

  def generate_redirect_url( id, params = {} )
    provider_url_generator = 'Providers::AppsAdvert::' + params[ :seller_name ] + '::Sender.redirect( id, params )'
    target_url = eval( provider_url_generator )
    target_url
  end

  def possible_to_handle( id )
    offer_enabled = @redis_offer[ :enabled ]
    possible_offers = OffersFilter.possible_offers( @request_params[ :user_platform ], @request_params[ :country ] )
    offer_enabled & ( possible_offers.include? id.to_i )
  end

  def choose_tb_url
    mobile = /Mobile|iP(hone|od|ad)|Android|BlackBerry|IEMobile/
    if mobile.match @request_params[ :user_agent ]
      @redis_offer[ :m_tb_url ]
    else
      @redis_offer[ :tb_url ]
    end
  end

end
