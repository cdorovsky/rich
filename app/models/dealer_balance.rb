class DealerBalance < ActiveRecord::Base

  belongs_to :dealer
  has_many :payment_requests
  has_many :dealer_increments

  before_save :correct_for_checkout

  scope :unpaid, -> { where( 'confirmed > 0' ) }

  def self.trigger( seller_request, options = {} )
    incr = DealerIncrement.create( seller_request.for_balance( options ) )
    DealerIncrement.commit_to_balance( incr.dealer.id, incr.confirmed )
  end

  def payment_request_trigger( payment_request )
    incr = DealerIncrement.create( payment_request.for_balance( self ) )
    commit_transaction( incr )
  end

  def admin_request_trigger( admin_request )
    incr = DealerIncrement.create( admin_request.for_balance( self ) )
    commit_transaction( incr )
  end

  def commit_transaction( dealer_increment )
    increment! :confirmed, by = dealer_increment.confirmed
  end

  def confirm_waiting( value )
    current_confirm_value = confirmed
    current_waiting_value = waiting
    new_confirmed_value = current_confirmed_value + value
    new_waiting_value = current_waiting_value - value
    update_attribute( :confirmed, new_confirm_value )
    update_attribute( :waiting, new_waiting_value )
  end

  def correct_for_checkout
    self.for_checkout = 0 if self.for_checkout < 0
  end

  def set_for_checkout_manually( for_checkout )
    self.update( for_checkout: for_checkout )
    dashboard = Dashboard.wrap_cached_regions( dealer_id )
    $redis.set( "#{ dealer_id }_dashboard", dashboard.to_json )
    Precacher.replenish_stats_cache_for_dealer( dealer_id )    
  end

end
