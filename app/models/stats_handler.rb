class StatsHandler

  def self.save_visitor_stats( fact, buyer_request )
    save_showed_offers( buyer_request ) if fact == :visitor
    save_dealer_stat( fact, buyer_request )
  end

  def self.save_showed_offers( buyer_request )
    SessionShowedOffer.find_or_create_by( buyer_session: buyer_request.buyer_session,
                                          campaign:      buyer_request.campaign,
                                          dealer:        buyer_request.dealer,
                                          offer:         buyer_request.offer ) do |showed_offer|
      showed_offer.show_counter += 1
    end	 
  end

  def self.save_dealer_stat( reason, buyer_request )
    visitor_uniqness = buyer_request.is_uniq?
    offer_id         = buyer_request.offer.id 
    campaign_id      = buyer_request.campaign_id
    dealer_id        = buyer_request.dealer_id
    current          = Time.zone.now.to_i
    Resque.enqueue( RealTimeBuyerRequest,
                    dealer_id,
    	              reason, 
    	              campaign_id, 
    	              offer_id, 
    	              visitor_uniqness,
                    current )
  end

  def self.save_seller_request_stats( reason, seller_request )
    offer_id         = seller_request.offer.id 
    campaign_id      = seller_request.campaign_id
    dealer_id        = seller_request.dealer_id
    Resque.enqueue( StatTrigger, 
                    dealer_id,
                    reason, 
                    campaign_id, 
                    offer_id,
                    true )
  end

end
