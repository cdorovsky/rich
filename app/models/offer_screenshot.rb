class OfferScreenshot < ActiveRecord::Base
  mount_uploader :file, ScreenshotUploader
  belongs_to :offer


  def self.remove_unwanted_records
    where( offer_id: nil ).delete_all
  end

end
