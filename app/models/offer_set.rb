class OfferSet < ActiveRecord::Base
  as_enum :lending_type, :redirect => 0 #:iframe => 0, :popup => 1, :banner => 2

  belongs_to :campaign
  belongs_to :offer
  belongs_to :dealer
  belongs_to :seller
  mount_uploader :qr_link, QrCodeUploader

  serialize :message, Hash

  scope :with_weblanding, -> { joins( :offer ).where( offers: { weblanding_ready: true } ) }
  scope :active,          -> { where( enabled: true  ) }
  scope :not_removed,     -> { where( removed: false ) }
  scope :removed,         -> { where( removed: true  ) }
  scope :available,       -> { where( enabled: true, removed: false ) }

  validates_presence_of   :offer_id
  validates_presence_of   :campaign_id

  before_save    :set_cost, :set_ability, :add_offer_set_data
  after_create   :save_campaign, :enqueue_qr_code, :load_to_all_redis_groups, :set_hashid

  before_destroy :delete_from_redis
  after_destroy  :save_campaign
  after_save     :load_to_rediska_offer_set

  def set_hashid
    update_column( :hashid, counted_hashid )
  end

  def counted_hashid
    self.class.hashids_instance.encode campaign_id, offer_id
  end

  def self.hashids_instance
    Hashids.new 'some salt for offer set', 8
  end

  def enabled?
    !offer.disabled
  end

  def add_offer_set_data
    self.enabled     = enabled? && attached?
    self.dealer_id   = campaign.dealer_id
    self.tb_url      = campaign.trafficback_url
    self.m_tb_url    = campaign.m_trafficback_url
    self.external_id = offer.external_id
    self.seller_id   = offer.seller_id
    self.offer_name  = offer.name
    self.seller_name = offer.seller.provider_system_name.capitalize
    self.message     = offer.fetch_dialogs
    self.seller_url  = offer.url
    self.preview_url = offer.preview_url
    self.apps_os     = offer.apps_os
    true
  end

  def attached?
    !removed
  end

  def enqueue_qr_code
    if Rails.env.production?
      create_qr_code if self.offer.weblanding_ready
    end
  end

  def self.offer_available_for?( buyer_request )
    if !buyer_request.campaign.enabled
      TrafficBack.trigger( 'campaign_disabled', buyer_request: buyer_request )
      false
    elsif buyer_request.campaign.dealer.blocked
      TrafficBack.trigger( 'dealer_blocked', buyer_request: buyer_request )
      false
    elsif buyer_request.offer.system?
      TrafficBack.trigger( 'no_offer', buyer_request: buyer_request )
      false
    else
      true
    end
  end

  def self.find_and_create_qr_code( offer_set_id )
    OfferSet.find( offer_set_id )
    offer_set.try( :create_qr_code )
  end

  def self.hourly_offer_set_reload
    not_removed.find_in_batches do | offer_set_batch |
      OfferSetsReloader.perform_async( offer_set_batch.map(&:id) )
    end
  end

  def create_qr_code
    unless Rails.env.test?
    api_server_domain = 'http://apst.me/'
    qr_code = RQRCode::QRCode.new( api_server_domain + id.to_s, size: 3, level: :l )
    qr_image = qr_code.to_img
    self.qr_link = qr_image.resize( 150, 150 ).to_file
    self.save
    end
  end

  def sms_link
    offer.sms_domain + redirect_code.code + '/' + offer.webland_url_postfix
  end

  def offer_url
    offer.url
  end

  def save_campaign
    campaign.save
  end

  def set_cost
    offer_price = offer.tariff
    dealer_percent = campaign.dealer.tariff.dealer_percent
    self.cost = ( offer_price * dealer_percent ).round( 2 )
  end

  def set_ability
    self.enabled = enabled? && attached?
    true # haha
  end

  def load_to_all_redis_groups
    load_to_rediska_offer_set
    load_to_country_groups
    load_to_campaign_group
    load_to_platform_groups
  end

  def load_to_campaign_group
    offer_sets = campaign.offer_sets.active.order( 'cost DESC' ).pluck( :id )
    tb_url     = campaign.trafficback_url
    m_tb_url   = campaign.m_trafficback_url
    result = { offers:      offer_sets,
               hours:       campaign.campaign_limiter.hours,
               days:        campaign.campaign_limiter.days,
               show_limits: campaign.campaign_limiter.show_limits,
               tb_url:      tb_url,
               m_tb_url:    m_tb_url }.to_json
    $redis.set( "redirect_code_#{ campaign.hash_code }", result )
  end

  def load_to_platform_groups
    if offer.apps_os == 'android'
      $redis.set 'android_offers',             Offer.android.map{ |offer| offer.offer_sets.pluck( :id ) }.flatten.to_json
      $redis.set 'android_support_offers',     Offer.android.support.map{ |offer| offer.offer_sets.pluck( :id ) }.flatten.to_json
    end
    if ['ipad', 'ios' ].include?( offer.apps_os )
      $redis.set 'ipad_offers',                Offer.ipad.map{ |offer| offer.offer_sets.pluck( :id ) }.flatten.to_json
      $redis.set 'ipad_support_offers',        Offer.ipad.support.map{ |offer| offer.offer_sets.pluck( :id ) }.flatten.to_json
    end
    if ['ipod+iphone', 'ios' ].include?( offer.apps_os )
      $redis.set 'ipod_iphone_offers',         Offer.ipod_iphone.map{ |offer| offer.offer_sets.pluck( :id ) }.flatten.to_json
      $redis.set 'ipod_iphone_support_offers', Offer.ipod_iphone.support.map{ |offer| offer.offer_sets.pluck( :id ) }.flatten.to_json
    end
    if offer.apps_os == 'mobile'
      $redis.set 'mobile_offers',             Offer.mobile.map{ |offer| offer.offer_sets.pluck( :id ) }.flatten.to_json
      $redis.set 'mobile_support_offers',     Offer.mobile.support.map{ |offer| offer.offer_sets.pluck( :id ) }.flatten.to_json
    end
    #$redis.set 'pc_offers',                  [].to_json
  end

  def load_to_country_groups
    offer.countries.each do |country|
      $redis.set "country_offers_#{ country.abbr }",        country.offers.map{ |offer| offer.offer_sets.pluck( :id ) }.flatten.to_json
      $redis.set "country_support_offers_#{ country.abbr}", country.offers.support.map{ |offer| offer.offer_sets.pluck( :id ) }.flatten.to_json
    end
  end

  def load_to_rediska_offer_set
    result = Hash.new
    result[ :enabled ]     = enabled
    result[ :external_id ] = external_id
    result[ :dealer_id ]   = dealer_id
    result[ :tb_url ]      = tb_url
    result[ :m_tb_url ]    = m_tb_url
    result[ :offer_id ]    = offer_id
    result[ :campaign_id ] = campaign_id
    result[ :seller_name ] = seller_name
    result[ :message ]     = message
    result[ :seller_url ]  = seller_url
    result[ :preview_url ] = preview_url
    result[ :apps_os ]     = apps_os
    $redis.set("offer_set_#{ id }", result.to_json)
  end

  def delete_from_redis
    $redis.del( "offer_set_#{ id }" )
  end

end
