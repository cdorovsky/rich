class OfferDisabler < ActiveRecord::Base

  belongs_to :offer

  scope :enabled,  -> { where( offer_disabled: false ) }
  scope :disabled, -> { where( offer_disabled: true ) }

  def instant_disable
    update( offer_disabled: true, disabling_time: nil )
    if instant_note_sent_time.blank?
      disabling_notifications( :instant, offer )
      update( instant_note_sent_time: Time.zone.now )
    end
  end

  def plan_disable_notification
    update( disabling_time: offer.disabling_time )
    if plan_note_sent_time.blank?
      disabling_notifications( :plan, offer )
      update( plan_note_sent_time: Time.zone.now )
    end
  end

  def enable
    update( offer_disabled: false, disabling_time: nil, plan_note_sent_time: nil, instant_note_sent_time: nil )
    disabling_notifications( :enable, offer )
  end

  def disabling_notifications( fact, offer )
    if %i{ enable instant plan }.include?(fact.to_sym)
      campaign_dealers = offer.campaigns.actual.pluck( :dealer_id )
      direct_offer_dealers = DirectOffer.where(offer_id: offer.id, approved: true).pluck(:dealer_id)
      dealers_list = campaign_dealers | direct_offer_dealers
      Dealer.where( id: dealers_list, blocked: false ).find_each do | dealer |
        campaign_ids = offer.offer_sets.where( dealer_id: dealer.id ).pluck( :campaign_id )
        sent_all_notes( fact, dealer, offer, campaign_ids )
      end
    end
  end

  def sent_all_notes( fact, dealer, offer, campaign_ids )
    case fact.to_sym
    when :plan
      Notification.plan_offer_disable( dealer: dealer, offer: offer )
    when :instant
      Notification.instant_offer_disable( dealer: dealer, offer: offer )
    when :enable
      Notification.offer_enable( dealer: dealer, offer: offer )
    end
    OfferDisablerHandler.perform_async( fact, dealer.id, offer.id, campaign_ids )
  end

end
