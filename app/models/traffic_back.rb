class TrafficBack

  def initialize( type, params = {} )
    case type
    when :campaign
      mongo_params = RedirectCode.decode( params[ :id ] )
      mongo_params[ :referer ] = params[ :referer ]
      mongo_params[ :reason ]  = params[ :reason ]
      MongoEventContainer.traffic_back( mongo_params )
    when :offer_set
      MongoEventContainer.traffic_back( params )
    end
  end

end
