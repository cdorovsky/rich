class SellerBalance < ActiveRecord::Base
  
  belongs_to :seller

  def increase( value, type )
    current_value = send( "#{ type }" ) || 0
    new_value = current_value + value
    update_attributes( type => new_value ) 
    save
  end

  def decrease( value, type )
    current_value = send( "#{ type }" ) || 0
    new_value = current_value - value
    update_attributes( type => new_value )
  end

  def confirm_waiting( value )
    current_confirm_value = confirmed
    current_waiting_value = waiting
    new_confirmed_value = current_confirmed_value + value
    new_waiting_value = current_waiting_value - value
    update_attribute( :confirmed, new_confirm_value )
    update_attribute( :waiting, new_waiting_value )
  end

end
