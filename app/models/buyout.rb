class Buyout

  attr_accessor :subscribe

  def initialize( subscribe )
    @subscribe = subscribe
  end

  def process_buyout
    @subscribe.active ? buyout_subscribe : send_rebill_to_dealer
  end

  def buyout_subscribe
    @subscribe.buyoutable = false
    @subscribe.buyouted   = true
    trigger_buyot_stats_for_dealer
    @subscribe.dealer     = Dealer.subscribe_buyer
    @subscribe.campaign   = find_or_create_buyout_campaign
    @subscribe.save
    trigger_buyot_stats_for_buyout_dealer
  end

  def send_rebill_to_dealer
    @subscribe.buyoutable = false
    @subscribe.save
    DealerStat.trigger( :rebill, campaign: @subscribe.campaign, offer: @subscribe.offer )
    DealerStat.trigger( :unsubscribe, campaign: @subscribe.campaign, offer: @subscribe.offer )
    Conversion.trigger( :rebill, campaign: @subscribe.campaign, offer: @subscribe.offer, country: Country.find_by( name: @subscribe.country ) )
    @subscribe.dealer.increase_balance( @subscribe.offer.dealer_revenue ) 
  end

  def trigger_buyot_stats_for_dealer
    DealerStat.trigger( :buyout, campaign: @subscribe.campaign, offer: @subscribe.offer )
    Conversion.trigger( :buyout, campaign: @subscribe.campaign, offer: @subscribe.offer, country: Country.find_by( name: @subscribe.country ) )
    @subscribe.dealer.increase_balance( @subscribe.buyout_price )
  end

  def trigger_buyot_stats_for_buyout_dealer
    DealerStat.trigger( :rebill, campaign: @subscribe.campaign, offer: @subscribe.offer )
    Conversion.trigger( :rebill, campaign: @subscribe.campaign, offer: @subscribe.offer, country: Country.find_by( name: @subscribe.country ) )
    Dealer.subscribe_buyer.increase_balance( @subscribe.offer.dealer_revenue ) 
  end

  def find_or_create_buyout_campaign
    old_campaign = @subscribe.campaign 
    new_campaign = Campaign.campaign_for_buyout( old_campaign )
    new_campaign
  end

end
