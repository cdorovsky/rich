module Pagination
  extend ActiveSupport::Concern

  included do

    self.per_page = 50

  end

  module ClassMethods

    def parametrize_for_pagination( params={} )

      params[ :per_page ] = params[ :per_page ].to_i.zero? ? 50 : [ params[ :per_page ].to_i, 100 ].min
      params[ :page ] = params[ :page ].to_i.zero? ? 1 : params[ :page ].to_i
      params

    end

  end

end
