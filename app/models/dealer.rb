class Dealer < ActiveRecord::Base

  include Pagination

  SALT = 'imma dealer'

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  belongs_to :tariff
  has_many :campaigns
  has_many :user_domains, through: :campaigns
  has_many :offer_sets
  has_many :tickets, dependent: :destroy
  has_many :ticket_messages
  has_many :subscribes
  has_many :rebills
  has_many :dealer_increments
  has_many :payments
  has_many :payment_requests
  has_many :wallets
  has_many :dealer_stats
  has_many :request_logs
  has_many :traffic_backs
  has_many :conversions
  has_many :postbacks
  has_many :notifications
  has_many :app_installations
  has_many :buyer_requests
  has_many :seller_requests
  has_many :admin_requests
  has_many :session_showed_offers
  has_many :daily_aggregates
  has_many :buyer_sessions
  has_many :tb_referrers
  has_many :direct_offers
  has_one :dealer_balance
  has_one :money_options
  has_one :notification_settings
  has_one :sidebar_notifications
  has_one :referrer, class_name: 'Dealer', foreign_key: :id, primary_key: :referrer_id
  has_many :referals, class_name: 'Dealer', primary_key: :id, foreign_key: :referrer_id
  has_many :referrer_increments, -> { referrer }, class_name: 'DealerIncrement', foreign_key: :referrer_id
  accepts_nested_attributes_for :wallets, allow_destroy: true
  accepts_nested_attributes_for :notification_settings
  after_create :new_balance, :new_money_options, :send_welcome_message, :create_api_key, 
               :create_notification_settings, :choose_dealer_tariff,
               :new_sidebar_notifications, :assign_referrer, :cache_dashboard

  after_create do
    update_admin_dealers_cache unless Rails.env.test?
  end

  serialize :role, Array

  validate :at_least_one_contact
  validates :name, presence: true

  def to_s
    name || "Партнер"
  end

  def new_sidebar_notifications
    SidebarNotifications.create( dealer_id: id )
  end

  def assign_referrer
    update_column :referrer_id, raw_referrer_id
  end

  def raw_referrer_id
    if reg_params
      ref_id = self.class.hashids_instance.decode( reg_params ).first
      ref_id if Dealer.exists?( ref_id )
    end
  end

  def choose_dealer_tariff
    self.tariff ||= Tariff.default_tariff
    save
  end

  def dealer_revenue( offer )
    offer_cost = offer.tariff
    if self.tariff
      ( offer_cost * tariff.dealer_percent ).round( 2 )
    else
      ( offer_cost * Tariff.default_tariff.dealer_percent ).round( 2 )
    end
  end

  def create_notification_settings
    NotificationSettings.create dealer_id: self.id
  end

  def send_welcome_message
    DealerMailer.successful_registration( self ).deliver
  end

  def new_balance
    DealerBalance.create( dealer_id: self[ :id ],
                          basic_currency: 'USD',
                          user_currency: 'USD',
                          waiting: 0,
                          confirmed: 0)
  end

  def new_money_options
    unless role == [ 'subscribe_buyer' ]
      MoneyOptions.create( dealer_id: id,
                           wallet_id: wallets.first.id )
    end
  end

  def referrers
    Dealer.where( referral_code: self.id.to_s ) # http://railscasts.com/episodes/163-self-referential-association
  end

  def increments_hash
    increments = {}
    %i( prev_month current_month yesterday today ).each do | scope |
      increments[ scope ] = {}
        %i(confirmed waiting).each do | column |
          increments[ scope ][ column ] = self.dealer_increments.send( scope ).pluck( column ).sum
        end
     end
   increments
  end

  def at_least_one_contact
    contacts = [ skype, icq, jabber ]
    if contacts.all?( &:blank? )
      %i{ skype icq jabber }.each do | contact_name |
        errors.add( contact_name, 'Укажите хотя бы один контакт!' )
      end
    end
  end

  def sms_auth
    if phone
      auth_code = SmsAuth.send( self )
      auth_code
    else
      raise 'Не задан номер телефона'
      nil
    end
  end

  def self.subscribe_buyer
    buyer = where( 'role = ?', %w( subscribe_buyer ).to_yaml ).last
    unless buyer
      buyer = new
      buyer.role     = [ 'subscribe_buyer' ]
      buyer.email    = 'subscribe_buyer@richpays.com'
      buyer.skype    = 'subscribe_buyer'
      buyer.name     = 'subscribe_buyer'
      buyer.password = 'subscribe_buyer'
      buyer.save
    end
    buyer
  end

  def has_increments( time_range )
    dealer_increments.where( created_at: time_range ).count > 0
  end

  def has_valid_phone?
    phone.present?
  end

  def active_for_authentication?
    super && !self.blocked
  end

  def self.payment_requests( params={} )
    parametrize_for_pagination( params )
    PaymentRequest
      .where( dealer_id: params[ :dealer_id ] )
      .paginate( page: params[ :page ], per_page: params[ :per_page ] )
      .order( 'id DESC' )
  end

  def self.load_collection( params={} )
    if params.keys == [ 'format', 'controller', 'action' ]
      Dealer.all
    else
      parametrize_for_pagination params
      id              = params[ :id ]
      email           = params[ :email ]
      blocked         = params[ :blocked ]
      icq             = params[ :icq ]
      phone           = params[ :phone ]
      skype           = params[ :skype ]
      jabber          = params[ :jabber ]
      start           = params[ :created_at_from ] ? params[ :created_at_from ].to_date : '01.01.2014'.to_date
      finish          = params[ :created_at_to ] ? params[ :created_at_to ].to_date : Date.tomorrow
      campaign_id     = params[ :campaign_id ]
      user_domain_url = params[ :user_domain ]
      wallet_number   = params[ :wallet ]
      page            = params[ :page ]
      per_page        = params[ :per_page ]
      reg_params     = params[ :reg_params ]
    end
    range           = start..finish

    options = {}
    options[ :id ]             = id if id
    options[ :created_at ]    = range if range
    options[ :email ]          = email if email
    options[ :phone ]          = phone if phone
    options[ :blocked ]        = blocked if blocked
    options[ :icq ]            = icq if icq
    options[ :skype ]          = skype if skype
    options[ :jabber ]         = jabber if jabber
    options[ :user_domains ]  = { url: user_domain_url } if user_domain_url
    options[ :campaigns ]     = { id: campaign_id } if campaign_id
    options[ :wallets ]       = { number: wallet_number } if wallet_number
    options[ :reg_params ]  = reg_params if reg_params

     joins( if options[ :user_domains ] then :user_domains end )
    .joins( if options[ :campaigns ]    then :campaigns    end )
    .joins( if options[ :wallets ]      then :wallets      end )
    .joins(                                  :dealer_balance   )
    .where( options )
    .paginate( page: page, per_page: per_page )
    .order( 'created_at DESC' )
    .uniq
  end

  def self.build_timescope( params={} )
    from            = params[ :date_from ] ? params[ :date_from ].to_datetime : Time.now.beginning_of_day
    to              = params[ :date_to ]   ? params[ :date_to ].to_datetime   : Time.now.end_of_day
    from..to
  end

  def collect_positive_increments( time_range )
    dealer_increments.where( :created_at => time_range ).where( 'confirmed > ?', 0 ).sum( :confirmed )
  end

  def self.cached_for( dealer_id )
    dealer = Dealer.find( dealer_id )
    Precacher.build_template( 'personal/settings/show.json.jbuilder', dealer: dealer )
  end

  def update_admin_dealers_cache
    Precacher.reload( Dealer, Dealer.new, params: PrecacherParams.dealers_index, options: { expiring: 1.day.from_now.to_i } )
  end

  def cache_dashboard
    dashboard = Dashboard.wrap_cached_regions( id )
    $redis.set( "#{ id }_dashboard", dashboard.to_json )
  end

  def create_api_key
    update_column( :api_key, SecureRandom.hex(8) )
  end

  def self.authorize_as_api_client( sign, dealer_id, timestamp )
    if [ sign, dealer_id, timestamp ].all?( &:present? )
      dealer = Dealer.find( dealer_id )
      if dealer
        secure_hashed_api_key = dealer.api_key + timestamp
        Digest::MD5.hexdigest( secure_hashed_api_key ) == sign
      end
    end
  end

  def referal_link
    'http://richpays.com/?reg=' + hashid
  end

  def hashid
    self.class.hashids_instance.encode id
  end

  def self.hashids_instance
    Hashids.new SALT, 8
  end

end
