class SmsDirectOfferNotification < SMS
  def self.mass_moderation_message( relation, params={} )
    dealers = Dealer.where( id: relation.pluck(:dealer_id) )
    relation.group_by( &:dealer_id ).each do | dealer_id, direct_offers |
      dealer = dealers.find( dealer_id )
      sms_params = {
        phone: dealer.phone,
        approved: params[ :approved ],
        offer_ids: direct_offers.map( &:offer_id ),
        blocking_condition: dealer.notification_settings.direct_offer_sms
      }
      moderation_message( sms_params )
    end.all?
  end


  def self.moderation_message( options={} )
    options[ :request_type ] = 'direct_offers_moderation'
    prepare_text options
    send_message options
  end
end
