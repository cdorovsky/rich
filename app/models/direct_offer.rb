class DirectOffer < ActiveRecord::Base

  belongs_to :dealer
  belongs_to :offer

  scope :enabled, -> { where( enabled: true ) }
  #scope :created_between, lambda { | start_date, end_date | where( 'created_at >= ? AND created_at <= ?', ( start_date || '1970-1-1' ).to_date, ( end_date || Time.now.to_s ).to_date ) }

  validates_presence_of :dealer_id, :offer_id

  before_save :add_offer_data
  after_save  :try_auto_approve
  after_destroy :unload_from_rediska

  def self.trigger( dealer, offer )
    create( dealer: dealer, offer: offer )
  end

  def add_offer_data
  	self.hashid      = RedirectCode.direct_generate( dealer, offer )
    self.external_id = offer.external_id
    self.seller_id   = offer.seller_id
    self.offer_name  = offer.name
    self.seller_name = offer.seller.provider_system_name.capitalize
    self.apps_os     = offer.apps_os
    self.cost        = ( offer.tariff * dealer.tariff.dealer_percent ).round( 2 )
  end

  def load_to_rediska
    attrs = %i( dealer_id offer_id enabled approved hashid selfmade adult cost apps_os seller_url )
    result = {}
    attrs.map! do | attr |
      result[ attr ] = send( attr )
    end
    $redis.set( "direct_offer_#{ hashid }", result.to_json )
  end

  def approve
    update_column( :approved, true )
    load_to_rediska
  end

  def try_auto_approve
    if offer.auto_approve
      update_column( :approved, true )
      load_to_rediska
    end
  end

  def unload_from_rediska
    $redis.del( "direct_offer_#{ hashid }" )
  end

  def seller_url
    id     = 'mongo_id'
    params = {}
    params[ :seller_url ]  = offer.url
    params[ :dealer_id ]   = dealer_id
    params[ :external_id ] = external_id
    params[ :apps_os ]     = apps_os
    provider_url_generator = 'Providers::AppsAdvert::' + seller_name + '::Sender.redirect( id, params )'
    target_url = eval( provider_url_generator )
    target_url
  end

  def delete_from_redis
    $redis.del( "direct_offer_#{ hashid }" )
  end

  def self.moderate!( params={} )
    rel = where( id: params[ :direct_offer_ids ] )
    DirectOfferMailer.mass_moderation_message( rel, params )
    SmsDirectOfferNotification.mass_moderation_message( rel, params )
    if params[ :approved ]
      rel.update_all( approved: true )
      where( id: params[ :direct_offer_ids ] ).each( &:load_to_rediska )
    else
      $redis.del( rel.pluck( :hashid ) )
      rel.destroy_all
    end
  end
end
