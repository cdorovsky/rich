class SellerRequest

  attr_accessor :seller_name

  def initialize( params = {} )
    seller_name = params[ :provider ]
    unless seller_name == 'glispa' and params[ :placement ] == '777'
      adapted_params = adapt( seller_name, params )
      begin
        MongoEventContainer.seller_request( adapted_params )
      rescue =>e
        Rails.logger.info e
      end
    end
  end

  def adapt( seller_name, params = {} )
    adapted_params = Hash.new
    replaces( seller_name ).each do | provider_param_name, adapted_param_name |
      adapted_params[ adapted_param_name ] = params[ provider_param_name ] if params[ provider_param_name ]
    end
    adapted_params
  end

  def replaces( seller_name )
    case seller_name
    when 'performance'
      { aff_sub: :mongo_id, offer_id: :external_offer_id, transaction_id: :transaction_id, payout: :external_revenue }
    when 'appflood'
      { aff_sub: :mongo_id, offer_id: :external_offer_id, transaction_id: :transaction_id, payout: :external_revenue }
    when 'iconpeak'
      { aff_sub: :mongo_id, offer_id: :external_offer_id, transaction_id: :transaction_id, payout: :external_revenue }
    when 'everyads'
      { aff_sub: :mongo_id, offer_id: :external_offer_id, transaction_id: :transaction_id, payout: :external_revenue }
    when 'mobvista'
      { aff_sub: :mongo_id, offer_id: :external_offer_id, transaction_id: :transaction_id, payout: :external_revenue }
    when 'unileadnetwork'
      { aff_sub: :mongo_id, offer_id: :external_offer_id, transaction_id: :transaction_id, payout: :external_revenue }
    when 'yeahmobi'
      { aff_sub: :mongo_id, offer_id: :external_offer_id, transaction_id: :transaction_id, payout: :external_revenue }
    when 'mobvistadirect'
      { aff_sub: :mongo_id}
    when 'matomy'
      { postback: :mongo_id, offerid: :external_offer_id, county_name: :countryname }
    when 'glispa'
      { sub_id: :mongo_id }
    when 'appflooddirect'
      { aff_sub: :mongo_id }
    when 'domobmedia'
      { aff_sub: :mongo_id }
    when 'youmi'
      { aff_sub: :mongo_id }
    when 'yeahmobidirect'
      { aff_sub: :mongo_id }
    when 'highcpi'
      { aff_sub: :mongo_id }
    when 'appsflyer'
      { aff_sub: :mongo_id }
    when 'wmadv'
      { aff_sub: :mongo_id }
    when 'solidclix'
      { aff_sub: :mongo_id }
    when 'voluum'
      { sid: :mongo_id }
    when 'macromac'
      { click_id: :mongo_id }
    when 'clickgen'
      { uid: :mongo_id }
    when 'ucweb'
      { aff_sub: :mongo_id }
    when 'mobio'
      { aff_sub: :mongo_id }
    end
  end

end
