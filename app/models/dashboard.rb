class Dashboard

  def self.wrap_cached_regions( dealer_id )
    {
      dealer_balance: DealerIncrement.cached_for( dealer_id ),
      conversions:    Conversion.cached_for(      dealer_id ),
      current_dealer: Dealer.cached_for(          dealer_id ),
      notifications:  Notification.cached_for(    dealer_id ),
      campaigns:      Campaign.cached_for(        dealer_id ),
      wallets:        Wallet.cached_for(          dealer_id ),
      graph:          DealerStat.cached_for(      dealer_id )
    }
  end

  def self.cached_json( dealer_id )
    dashboard = $redis.get( "#{ dealer_id }_dashboard" )
    if dashboard
      JSON.parse( dashboard ) 
    else
      {}
    end
  end

  def self.partial_update( partial_type, dealer_id )
    dashboard_hash = case partial_type
    when :campaign
      update_campaign_for_dash( dealer_id )
    when :payment_request
      update_payment_request_for_dash( dealer_id )
    end
    set_dashboard( dealer_id, dashboard_hash ) if dashboard_hash
    cached_json( dealer_id )
  end

  def self.update_campaign_for_dash( dealer_id )
    dashboard = cached_json( dealer_id )
    dashboard[ :campaigns ] = Campaign.cached_for( dealer_id )
    dashboard
  end

  def self.update_payment_request_for_dash( dealer_id )
    dashboard = cached_json( dealer_id )
    dashboard[ :dealer_balance ] = DealerIncrement.cached_for( dealer_id )
    dashboard[ :current_dealer ] = Dealer.cached_for( dealer_id )
    dashboard
  end

  def self.set_dashboard( dealer_id, dashboard_hash )
    $redis.set( "#{ dealer_id }_dashboard", dashboard_hash.to_json )
  end

  def self.get_json( dealer_id )
    dashboard_hash = cached_json( dealer_id )
    unless dashboard_hash
      dashboard_hash = wrap_cached_regions( dealer_id )
      set_dashboard( dealer_id, dashboard_hash )
    end
    dashboard_hash
  end

end
