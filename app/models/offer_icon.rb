class OfferIcon < ActiveRecord::Base
  mount_uploader :file, IconUploader
  belongs_to :offer
end
