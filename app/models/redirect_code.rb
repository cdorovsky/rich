module RedirectCode

  REDIRECT_CODE_SALT = 'richpays number one'

  class << self

    def generate( campaign )
      hashids = Hashids.new( REDIRECT_CODE_SALT )
      hashids.encode( [ campaign.dealer_id, campaign.id ] )
    end

    def decode( hashid )
      hashids = Hashids.new( REDIRECT_CODE_SALT )
      begin
        values = hashids.decode( hashid )
        { dealer_id: values[ 0 ], campaign_id: values[ 1 ] }
      rescue Hashids::InputError => e
        Rails.logger.info( e.message )
        Rails.logger.info( e.backtrace.inspect )
        {}
      end
    end

    def direct_generate( dealer, offer )
      hashids = Hashids.new( REDIRECT_CODE_SALT )
      hashids.encode( [ dealer.id, offer.id ] )
    end

    def direct_decode( hashid )
      hashids = Hashids.new( REDIRECT_CODE_SALT )
      begin
        values = hashids.decode( hashid )
        { dealer_id: values[ 0 ], offer_id: values[ 1 ] }
      rescue Hashids::InputError => e
        Rails.logger.info( e.message )
        Rails.logger.info( e.backtrace.inspect )
        {}
      end
    end  

  end

end
