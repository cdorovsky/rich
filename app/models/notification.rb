class Notification < ActiveRecord::Base

  belongs_to :dealer

  def self.plan_offer_disable( params = {} )
    offer = params[ :offer ]
    title = "Отключение оффера ID: #{ offer.id }."
    text = "Оффер ID: #{ offer.id }, название: #{ offer.name } Будет отключен: #{ ( offer.disabling_time - 4.hours ).strftime( '%F %H:%M' ) }."
    Notification.create( dealer_id: params[ :dealer ].id, title: title, text: text )
  end

  def self.instant_offer_disable( params = {} )
    offer = params[ :offer ]
    title = "Отключение оффера ID: #{ offer.id }."
    text = "Оффер ID: #{ offer.id }, название: #{ offer.name } отключен."
    Notification.create( dealer_id: params[ :dealer ].id, title: title, text: text )
  end

  def self.offer_enable( params = {} )
    offer = params[ :offer ]
    title = "Включение оффера ID: #{ offer.id }."
    text = "Оффер ID: #{ offer.id }, название: #{ offer.name } снова активен."
    Notification.create( dealer_id: params[ :dealer ].id, title: title, text: text )
  end

  def self.cached_for( dealer_id )
    notes = Notification.where( dealer_id: [ nil, dealer_id ] ).order( 'created_at DESC' ).first(5)
    Precacher.build_template( 'personal/notifications/index.json.jbuilder', notes: notes )
  end

end
