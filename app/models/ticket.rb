class Ticket < ActiveRecord::Base

  belongs_to :dealer
  has_many   :ticket_messages, dependent: :destroy
  validates :title, presence: true

  scope       :unanswered, -> { select{ |t| t.last_answered == 'dealer' } }
  scope             :open, -> { where( closed: false       ) }
  scope :unanswered_first, -> { order( 'last_answered ASC' ) }
  scope       :open_first, -> { order( 'closed ASC'        ) }
  scope        :new_first, -> { order( 'updated_at DESC'   ) }
  belongs_to :notification_settings, foreign_key: :dealer_id, primary_key: :dealer_id

  def last_support_message
    last_answer = ticket_messages.where( ownership: 'support' ).first
    last_answer.try( :message_text )
  end

  def create_message( opts={} )
    ownership = opts[ :ownership ]
    message_params = {
      dealer_id: dealer_id,
      ticket_id: id,
      message_text: message,
      number: message_counter,
      ownership: ownership
    }
    TicketMessage.create( message_params )
  end

  def self.load_collection( params )
    joins( if params[ :dealer_email ] then :dealer end )
    .where( dealer_search_params( params ) )
    .where(  title_search_params( params ) )
    .where( sanitize_load_params( params ) )
    .unanswered_first
    .open_first
    .uniq
  end

  def unread?
    !read
  end

  def read!
    change_read_state_to true
  end

  def unread!
    change_read_state_to false
  end

  def change_read_state_to( new_state )
    counter_delta = new_state ? -1 : 1
    dealer.sidebar_notifications.change_by :unread_tickets_count, counter_delta
    update_column :read, new_state
  end

  def notify_dealer
    SupportMailer.send_answer_notification( self ).deliver
    SmsSupport.send_answer_notification( self ) if dealer.phone.present?
    unread! if read
  end

  def self.find_and_read( dealer_id, id )
    ticket = self.find_by id: id, dealer_id: dealer_id
    if ticket
      ticket.read! if ticket.unread?
      ticket
    end
  end

  private

    def self.sanitize_load_params( params )
      query_keys = %i( id title closed dealer_id )

      load_params = Hash.new

      query_keys.each do | query_key |
        load_params[ query_key ] = params[ query_key ] if params[ query_key ]
      end

      load_params[ :created_at ] = parametrize_date( params ) if there_is_a_time_scope?( params )

      load_params
    end

    def self.dealer_search_params( params={} )
      if params[ :dealer_email ]
        [ 'email LIKE?', "%#{ params.delete( :dealer_email ) }%" ]
      end
    end

    def self.title_search_params( params={} )
      if params[ :title ]
        [ 'title LIKE?', "%#{ params.delete( :title ) }%" ]
      end
    end

    def self.parametrize_date( params={} )
      from = ( params[ :date_start ].try( :to_date ).try( :beginning_of_day ) || Ticket.minimum( :created_at ).beginning_of_day )
      to   = ( params[ :date_end   ].try( :to_date ).try( :end_of_day )       || Time.now.end_of_day                            )

      from..to
    end

    def self.there_is_a_time_scope?( params={} )
      [ params[ :date_start ], params[ :date_end ] ].any?( &:present? )
    end

end
