class MongoSellerRequest
  include Mongoid::Document
  include Mongoid::Timestamps

  embedded_in :mongo_stat

  field :transaction_id,       type: String

  field :external_offer_id,    type: String

  field :external_sub_id,      type: String

  field :sub_id,               type: String
  field :sub_id2,              type: String
  field :sub_id3,              type: String

  field :offer_dealer_revenue, type: Float
  field :revenue_from_request, type: Float
  field :country_id,           type: Integer
  field :offer_id,             type: Integer
  field :campaign_id,          type: Integer
  field :dealer_id,            type: Integer
  field :pg_seller_request_id, type: Integer
  field :triggered_at,         type: Integer

  def for_postback( offer, campaign )
    country = Country.find( country_id )
    { dealer_id:       dealer_id,
      campaign_id:     campaign_id,
      offer_id:        offer_id,
      offer_name:      offer.name,
      conversion_type: :install,
      datetime:        Time.zone.at( triggered_at ).strftime( '%Y-%m-%d %H:%M:%S' ),
      country:         country,
      country_name:    country.name || '',
      country_code:    country.abbr || '',
      profit_usd:      offer_dealer_revenue,
      postback_url:    campaign.postback_url,
      triggered_at_timestamp: triggered_at,
      sub_id:          sub_id,
      sub_id2:         sub_id2,
      sub_id3:         sub_id3 }
  end

  def for_conversion( campaign, offer, dealer )
    country = Country.find( country_id )
    { offer_id:       offer_id,
      campaign_id:    campaign_id,
      dealer_id:      dealer_id,
      offer_name:     offer.name,
      dealer_revenue: offer_dealer_revenue,
      campaign_name:  campaign.name,
      country:        country,
      country_name:   country.name || '',
      country_code:   country.abbr || '',
      kind:           offer.kind,
      conversion_reason: :install,
      crypted_id:     Conversion.generate_uniq_crypted_id,
      triggered_at_timestamp: triggered_at,
      sub_id:         sub_id,
      sub_id2:        sub_id2,
      sub_id3:        sub_id3,
      triggered_at:   Time.zone.at( triggered_at ) }
  end

  def for_app_installation
    { dealer_id:   dealer_id,
      offer_id:    offer_id,
      campaign_id: campaign_id,
      transaction_id: transaction_id,
      triggered_at: Time.zone.at( triggered_at )}
  end

  def for_dealer_increment( dealer )
    { triggered_at:   Time.zone.at( triggered_at ),
      dealer_balance: dealer.dealer_balance,
      dealer_id:      dealer_id,
      offer_id:       offer_id,
      campaign_id:    campaign_id,
      confirmed:      offer_dealer_revenue,
      reason_type:    'SellerRequest',
      reason_id:      pg_seller_request_id }
  end

end
