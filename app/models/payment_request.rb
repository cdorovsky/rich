class PaymentRequest < ActiveRecord::Base

  belongs_to :dealer
  belongs_to :wallet
  belongs_to :payments
  has_one    :dealer_increment, as: :reason

  scope :new_requests, -> { where( status: 'new' ) }
  scope :pending, -> { where( status: 'pending' ) }
  scope :recent, -> { where( status: [ 'new', 'pending' ] ) }
  scope :request, -> { where( payment_type: 'request' ) }
  validates :dealer_id, presence: true
  validate :dealer_has_enough_money_to_request
  validate :sum_of_requests_not_bigger_than_for_checkout
  validate :minimal_request

  after_update :update_dashboard, only: [ :complete, :cancel ]

  def self.trigger( dealer, wallet_id, requested_value )
    if dealer.has_valid_phone?
      value = requested_value.to_f
      payment_request = PaymentRequest.new( dealer: dealer,
                                            confirmed: value,
                                            payment_type: 'request',
                                            wallet_id: wallet_id )
      if payment_request.save
        dealer.dealer_balance.increment!( :requested, value )
        payment_request
      else
        false
      end
    end
  end

  def self.has_same_count?( request_ids )
    self.count == request_ids.count
  end

  def self.is_all?( attribute, value )
    self.pluck( attribute ).uniq == [ value ]
  end

  def self.payment_ready?( request_ids )
    [
      self.wallets.are_same_kind?,         self.is_all?( :payment_type, 'request' ),
      self.has_same_count?( request_ids ), self.is_all?( :status,           'new' )
    ].all?
  end

  def self.wallets
    Wallet.where id: self.pluck( :wallet_id )
  end

  def self.payment_type
    self.pluck( :payment_type ).uniq.first
  end

  def table_description
    "\"RichPays.com for #{ dealer.email } (#{ Time.zone.now.strftime( '%F' ) })\""
  end

  def complete
    request_value = confirmed
    dealer.dealer_balance.payment_request_trigger( self )
    dealer.dealer_balance.increment!( :for_checkout, -request_value )
    dealer.dealer_balance.increment!( :requested, -request_value )
    update_attribute( :status, 'completed' )
  end

  def cancel
    request_value = confirmed
    dealer.dealer_balance.increment!( :requested, -request_value )
    update_attribute( :status, 'cancelled' )
  end

  def to_csv_row
    [
      wallet.number,
      confirmed.round(2),
      table_description,
      id,
      nil
    ]
  end

  def update_dashboard
    Dashboard.partial_update( symbolized_name, dealer_id )
  end

  def for_balance( balance )
    {
      dealer_balance: balance,
      dealer:         dealer,
      confirmed:      -confirmed,
      reason:         self,
      paid:           true
    }
  end

  def dealer_has_enough_money_to_request
    if confirmed <= ( dealer.dealer_balance.for_checkout - dealer.dealer_balance.requested )
      true
    else
      errors.add( :confirmed, 'Указаная сумма превышает доступные к снятию средства' )
    end
  end

  def sum_of_requests_not_bigger_than_for_checkout
    if ( self.confirmed + PaymentRequest.where( status: 'new', dealer_id: dealer_id ).sum( :confirmed ) ).between?( 0, dealer.dealer_balance.for_checkout )
      true
    else
      errors.add( :confirmed, 'Указаная сумма превышает доступные к снятию средства' )
    end
  end

  def minimal_request
    if confirmed >= 10
      true
    else
      errors.add( :confirmed, 'Минимальный размер выплаты 10$' )
    end
  end

end
