=begin
Usage example: Precacher.load( DealerStat, current_dealer, get_params, options ) from controller's action

* get_params should be sanitized using 'strong_parameters'
* template should have fallback for variable with record(s), e.g. @stats ||= data
* you can provide two options in options hash: partial subkeys (sub_keys: [:dealer_stats, :conversions, :etc]
  should be referenced to class names) and expring time (expiring: 1.day.from_now.to_i)
=end

class Precacher < OpenStruct
  def self.load( klass, dealer, options={} )
    new( klass: klass, dealer: dealer, params: options[ :params ], options: options[ :options ] ).fetch!
  end

  def self.reload( klass, dealer, options={} )
    new( klass: klass, dealer: dealer, params: options[ :params ], options: options[ :options ] ).renew_data
  end

  def redis_hash
    Precacher.get_hash_from_redis( redis_key )
  end

  def fetch!
    if ready_for_caching
      data_is_actual ? raw_value : renew_data
    else
      template
    end
  end

  def ready_for_caching
    begin
      klass.restricted_caching_params( params ).blank?
    rescue NoMethodError
      true
    end
  end

  def sub_keys
    options.try( :[], :sub_keys )
  end

  def is_composite?
    sub_keys.present?
  end

  def expiring_time
    options.try( :[], :expiring ) || 1.day.from_now.to_i
  end

  def data_is_actual
    if present_in_redis?
      redis_hash[ 'expiring_at' ] > Time.now.to_i
    else
      false
    end
  end

  def renew_data
    set_data
    raw_value
  end

  def partial_update( key )
    full_template = raw_value
    partial_template = records_from_single_table( key, dealer, params )
    full_template[ key ] = partial_template
    $redis.set redis_key, self.class.wrapped_value( full_template )
  end

  def pg_records
    if is_composite?
      records = {}
      sub_keys.each do | sub_key |
        sub_class = self.class.subkey_to_class( sub_key )
        records[ sub_key.to_sym ] = records_from_single_table( sub_class, dealer, params )
      end
      records
    else
      records_from_single_table( klass, dealer, params )
    end
  end

  def records_from_single_table( klass, dealer, params )
    skipped_params = params.except( :controller, :action, :format )
    begin
      klass.records_for_caching( dealer, skipped_params )
    rescue NoMethodError
      klass.where( skipped_params )
    end
  end

  def template
    if is_composite?
      pg_records.map do | key, value |
        Hash[ key, Precacher.build_template( partial_template_path( key ), data: value ) ]
      end.reduce( &:merge )
    else
      Precacher.build_template( template_path, data: pg_records )
    end
  end

  def redis_key
    "dealer_#{ dealer.id }/" + Precacher.to_redis_key( params )
  end

  def partial_template_path( key='' )
    Precacher.to_template_path params, key, klass
  end

  def template_path
    Precacher.to_template_path params
  end

  def self.get_hash_from_redis( key )
    redis_string = $redis.get( key )
    redis_string ? JSON.parse( redis_string ) : {}
  end

  def self.build_template( path='', locals={} ) #path is relative to 'app/views'
    view_paths = Rails::Application::Configuration.new( Rails.root ).paths[ 'app/views' ]
    av_helper = ActionView::Base.new view_paths
    begin
      av_helper.render template: path, locals: locals
    rescue
      locals[ :data ].as_json
    end
  end

  def self.get_raw_params_hash( controller_params={} )
    controller_params.to_hash.deep_symbolize_keys
  end

  def self.to_template_path( controller_params, key='', klass=Class )
    hashed_params = get_raw_params_hash( controller_params )
    if key.present?
      "#{ klass.name.downcase }/#{ key }.#{ hashed_params[ :format ] }"
    else
      "#{ hashed_params.delete( :controller ) }/#{ hashed_params.delete( :action ) }.#{ format_to_extension( hashed_params.delete( :format ) ) }"
    end
  end

  def self.format_to_extension( format )
    formats = {
      json: 'json.jbuilder'
    }
    formats[ format.to_sym ]
  end

  def self.to_redis_key( controller_params )
    hashed_params = get_raw_params_hash( controller_params )
    params_str = "#{ hashed_params.delete(:controller ) }##{ hashed_params.delete( :action ) }.#{ hashed_params.delete( :format ) }"
    params_str << '?' + hashed_params.to_query unless hashed_params.blank?
    params_str
  end

  def present_in_redis?
    $redis.exists redis_key
  end

  def set_data
    $redis.set redis_key, wrapped_value
  end

  def raw_value
    if present_in_redis?
      wrapped = $redis.get redis_key
      JSON.parse( wrapped )[ 'data' ]
    end
  end

  def self.wrapped_value( composite )
    {
      data: composite,
      expiring_at: expiring_time
    }.to_json
  end

  def wrapped_value
    {
      data: template,
      expiring_at: expiring_time
    }.to_json
  end

  def self.replenish_cache
    dealer_ids = DealerBalance.where( 'confirmed > ?', 0 ).pluck( :dealer_id )
    cache_dealers( dealer_ids )
  end

  def self.clear_dealer_redis_data( dealer_id )
    keys = $redis.keys( "dealer_#{ dealer_id }*" )
    $redis.del( keys )
  end

  def self.replenish_stats_cache_for_dealer( dealer_id )
    dealer = Dealer.find( dealer_id )
    PrecacherParams.stats_params.each do | params |
      self.reload( DealerStat, dealer, params: params )
    end
  end

  def self.subkey_to_class( sub_key='' )
    eval( sub_key.to_s.singularize.camelize )
  end
end
