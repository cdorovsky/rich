class Tariff < ActiveRecord::Base

  has_many :dealers

  def self.default_tariff
    tariff = find_by( name: 'DefaultTariff' ) 
    unless tariff
      tariff = new
      tariff.name = 'DefaultTariff'
      tariff.dealer_percent = 0.8
      tariff.save
    end
    tariff
  end

end
