class UserDomain < ActiveRecord::Base
  belongs_to :campaign

  before_save :url_readonly
  validates :url, url: true

  def self.is_unique_collection?( user_domains )
    ( user_domains - user_domains.to_a.uniq{ |ud| ud.url } ).empty?
  end

  def url_readonly
    unless new_record?
      url_changed? ? false : true
    else
      true
    end
  end

  def check_for_pdf

    @url = URI.parse( self.url + '/message_for_oss.pdf' )

    Net::HTTP.start( @url.host, @url.port ) do | http |
      @mime_type_is_pdf = http.head( @url.request_uri )[ 'Content-Type' ].start_with?( 'application/pdf' )
    end

    if @mime_type_is_pdf
      self.update( wapclick_ready: true )
    end

    self

  end
end
