class Conversion < ActiveRecord::Base

  include Pagination

  belongs_to :dealer
  belongs_to :offer
  belongs_to :campaign
  belongs_to :buyer_request
  belongs_to :country
  has_one    :postback
  has_one    :seller_request

  validates :crypted_id, uniqueness: true

  def self.trigger( reason, context = {} ) # fact :subscribe, :rebill, :install, :buyout
    if context[ :seller_request ]
      seller_request = context[ :seller_request ]
      dealer         = seller_request.dealer
      campaign       = seller_request.campaign
      offer          = seller_request.offer
      country        = seller_request.country
      sub_id         = seller_request.sub_id
      sub_id2        = seller_request.sub_id2
      sub_id3        = seller_request.sub_id3
    else
      campaign = context[ :campaign ]
      dealer = campaign.dealer
      offer = context[ :offer ]
      country = context[ :country ]
    end
    dealer_revenue = choose_dealer_revenue( reason, dealer, offer )
    params   = {
      triggered_at:      context[ :current ] ? Time.zone.at( context[ :current ] ) : Time.zone.now,
      conversion_reason: reason.to_s,
      campaign:          campaign,
      campaign_name:     campaign.name,
      dealer:            dealer,
      offer:             offer,
      offer_name:        offer.name,
      kind:              offer.kind,
      dealer_revenue:    dealer_revenue,
      country:           country,
      country_name:      country.name || '',
      country_code:      country.abbr || '',
      sub_id:            sub_id,
      sub_id2:           sub_id2,
      sub_id3:           sub_id3
    }
    begin
      params[ :crypted_id ] = generate_uniq_crypted_id
      conversion = Conversion.new( params )
    end until Conversion.new( params ).valid?
    conversion.save
    seller_request.update_attribute( :conversion_id, conversion.id ) if seller_request
    conversion
  end

  def self.choose_dealer_revenue( reason, dealer, offer )
    case reason
    when :subscribe, :unsubscribe
      0
    when :buyout
      offer.buyout_price
    else
      dealer.dealer_revenue( offer )
    end
  end

  def self.generate_uniq_crypted_id
    (0...8).map { (('a'..'z').to_a + ('0'..'9').to_a)[rand(36)] }.join
  end

  def self.load_collection( dealer, params = {} )
    if params.keys == [ 'format', 'controller', 'action' ]
      common = true
      start = 7.days.ago.beginning_of_day
      finish = Time.zone.now.end_of_day
    else
      parametrize_for_pagination params
      conversion_id = params[ :crypted_id ]
      offer_id      = params[ :offer_id ]
      offer_name    = params[ :offer_name ]
      campaign_id   = params[ :campaign_id ]
      campaign_name = params[ :campaign_name ]
      source_url    = params[ :source_url ]
      kind          = params[ :offer_kind ]
      sub_id        = params[ :sub_id ]
      sub_id2       = params[ :sub_id2 ]
      sub_id3       = params[ :sub_id3 ]
      country_codes = params[ :country_list ].try( :uniq )
      start         = params[ :date_start ] ? Date.parse( params[ :date_start ] ).in_time_zone.beginning_of_day : '2014.01.01'.to_date
      finish        = params[ :date_end ]   ? Date.parse( params[ :date_end ] ).in_time_zone.end_of_day : Time.zone.now.end_of_day
      page          = params[ :page ]
      per_page      = params[ :per_page ]
    end
    range         = start..finish

    options = {}
    options[ :dealer_id ]     = dealer.id
    options[ :created_at ]    = range
    options[ :kind ]          = kind          if kind
    options[ :crypted_id ]    = conversion_id if conversion_id
    options[ :offer_id ]      = offer_id      if offer_id
    options[ :offer_name ]    = offer_name    if offer_name
    options[ :campaign_id ]   = campaign_id   if campaign_id
    options[ :campaign_name ] = campaign_name if campaign_name
    options[ :country_code ]  = country_codes if country_codes
    options[ :source_url ]    = source_url    if source_url
    options[ :sub_id ]        = sub_id        if sub_id
    options[ :sub_id2 ]       = sub_id2       if sub_id2
    options[ :sub_id3 ]       = sub_id3       if sub_id3
    where( options ).paginate( page: page, per_page: per_page ).order( 'created_at DESC' )
  end

  def self.cached_for( dealer_id )
    conversions = where( dealer_id: dealer_id ).order( 'created_at DESC' ).limit( 10 )
    Precacher.build_template( 'personal/dashboard/actions.json.jbuilder', conversions: conversions )
  end

end
