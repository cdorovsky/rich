class Country < ActiveRecord::Base
  has_many :offer_country_sets
  has_many :restricted_offer_country_sets
  has_many :offers, through: :offer_country_sets
  has_many :prohibited_offers, through: :restricted_offer_country_sets, source: :offer
  has_many :campaign_country_sets
  has_many :conversions
  has_many :buyer_requests
  has_many :seller_requests
  has_many :postbacks

  def self.world
  	Country.where( name: 'All' )
  end

  def self.country_list_with_count( country_list )
    if country_list.map( &:name ).include? 'All'
      [ Country.world, 248 ]
    else
      [ country_list, country_list.count ]
    end
  end

end
