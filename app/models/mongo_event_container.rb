class MongoEventContainer
  include Mongoid::Document
  include Mongoid::Timestamps

  field :dealer_id,    type: Integer
  field :triggered_at, type: Integer
  field :expired_at,   type: Integer
  field :condition,    type: String

  has_many :mongo_event_atomics

  class << self

    def actual( dealer_id )
      where( dealer_id: dealer_id, condition: 'actual' )
    end

    def trigger( dealer_id )
      if actual( dealer_id ).exists?
        container = actual( dealer_id ).first
      else
        container = create( default_trigger_params( dealer_id ) )
      end
      container
    end

    def buyer_session( params = {} )
      redirect_code = RedirectCode.decode( params[ :id ] )
      container = trigger( redirect_code[ :dealer_id ] )
      atomic_params = redirect_code.merge params
      MongoEventAtomic.visitor( container, atomic_params )
    end

    def buyer_request( params = {} )
      dealer_id = params[ :redis ][ :dealer_id ]
      container = trigger( dealer_id )
      MongoEventAtomic.offer_set( container, params )
    end

    def seller_request( params = {} )
      offer_set_show = DirectOfferRedirect.not_converted.find_by( request_id: params[ :mongo_id ] )
      offer_set_show = OfferSetShow.not_converted.find_by( mongo_id: params[ :mongo_id ] ) if offer_set_show.nil?
      offer_set_show = MongoEventAtomic.find( params[ :mongo_id ] ) if offer_set_show.nil?
      unless offer_set_show.converted
        container = trigger( offer_set_show.dealer_id )
        if offer_set_show.class.name == 'DirectOfferRedirect'
          MongoEventAtomic.direct_conversion( container, offer_set_show, params )
        else
          MongoEventAtomic.conversion( container, offer_set_show, params )
        end
      end
    end

    def traffic_back( params = {} )
      container = trigger( params[ :dealer_id ] )
      MongoEventAtomic.traffic_back( container, params )
    end

    def clicker( params = {} )
      container = trigger( params[ :dealer_id ] )
      MongoEventAtomic.clicker( container, params )
    end

    def js_push( params = {} )
      container = trigger( params[ :dealer_id ] )
      MongoEventAtomic.js_push( container, params )
    end

    def default_trigger_params( dealer_id )
      now = Time.zone.now.to_i
      {
        triggered_at: now,
        expired_at:  swimming_timeout( now ),
        dealer_id:   dealer_id,
        condition:    'actual'
      }
    end

    def swimming_timeout( now )
      now + rand( 60..180 )
    end

    def send_expired_to_sidekiq( current )
      coll = expired_actual( current )
      coll.each do | m |
        MongoImportWorker.perform_async( m.id.to_s )
      end
      coll.update_all( condition: 'importing' )
    end

    def expired_actual( current )
      where( condition: 'actual' ).lte( expired_at: current )
    end

    def delete_old
      coll = where( condition: 'to_delete' )
      coll.update_all( condition: 'deleting' )
      ids = where(  condition: 'deleting' ).pluck( :id )
      ids.map! { | id | id.to_s }
      MongoEventDeleter.perform_async( ids )
    end
  end

  def import_to_pg
    change_condition( 'importing' )
    import_offer_set_show
    import_js_push
    import_clicker
    import_conversions
    import_traffic_back
    DealerIncrement.pay_to_balance( dealer_id )
    Postback.send_to_partners( dealer_id )
    RedisCacher.perform_async(  dealer_id )
    change_condition( 'to_delete' )
  end

  def change_condition( cond )
    update( condition: cond )
  end

  def import_offer_set_show
    offer_set_shows = []
    result = {}
    mongo_event_atomics.where( event_name: 'offer_set' ).each do | atomic |
      result[ atomic.offer_set_id ] ||= Hash.new
      if atomic.extras[ :uniq ]
        result[ atomic.offer_set_id ][ :uniq ] = if result[ atomic.offer_set_id ][ :uniq ]
                                                   result[ atomic.offer_set_id ][ :uniq ] + atomic.counter
                                                 else
                                                   atomic.counter
                                                 end
      else
        result[ atomic.offer_set_id ][ :not_uniq ] = if result[ atomic.offer_set_id ][ :not_uniq ]
                                                      result[ atomic.offer_set_id ][ :not_uniq ] + atomic.counter
                                                    else
                                                      atomic.counter
                                                    end
      end
      offer_set_shows << OfferSetShow.new( atomic.for_offer_set_show )
    end
    OfferSetShow.import offer_set_shows
  end

  def import_direct_offers
    offer_set_shows = []
    result = {}
    mongo_event_atomics.where( event_name: 'direct_offer' ).each do | atomic |
      result[ atomic.direct_offer_id ] ||= Hash.new
      if atomic.extras[ :uniq ]
        result[ atomic.direct_offer_id ][ :uniq ] = if result[ atomic.direct_offer_id ][ :uniq ]
                                                   result[ atomic.direct_offer_id ][ :uniq ] + atomic.counter
                                                 else
                                                   atomic.counter
                                                 end
      else
        result[ atomic.direct_offer_id ][ :not_uniq ] = if result[ atomic.direct_offer_id ][ :not_uniq ]
                                                      result[ atomic.direct_offer_id ][ :not_uniq ] + atomic.counter
                                                    else
                                                      atomic.counter
                                                    end
      end
      direct_offers << OfferSetShow.new( atomic.for_offer_set_show )
    end
    OfferSetShow.import offer_set_shows
  end

  def import_conversions
    postbacks         = []
    conversions       = []
    app_installations = []
    dealer_increments = []
    result = {}
    uniq_mongo_ids( 'conversion' ).each do | uniq_mongo_id |
      atomic = mongo_event_atomics.find_by( "extras.mongo_id" => uniq_mongo_id )
      campaign = atomic.campaign_id ? Campaign.find( atomic.campaign_id ) : nil
      offer    = Offer.find( atomic.offer_id )
      dealer   = Dealer.find( atomic.dealer_id )
      conversions       << Conversion.new(      atomic.for_conversion( campaign, offer, dealer ) )
      postbacks         << Postback.new(        atomic.for_postback( offer, campaign ) )
      dealer_increments << DealerIncrement.new( atomic.for_dealer_increment( dealer ) )
      if atomic.offer_set_id
        result[ atomic.offer_set_id ]    = result[ atomic.offer_set_id ]    ? [ ( result[ atomic.offer_set_id ][ 0 ]    + atomic.counter ), 'offer_set' ] : [ atomic.counter, 'offer_set' ]
      else
        result[ atomic.direct_offer_id ] = result[ atomic.direct_offer_id ] ? [ ( result[ atomic.direct_offer_id ][ 0 ] + atomic.counter ), 'direct_offer' ] : [ atomic.counter, 'direct_offer' ]
      end
    end
    Conversion.import      conversions
    Postback.import        postbacks
    DealerIncrement.import dealer_increments
    result.each do | counter_base, counter |
      if counter[ 1 ] == 'offer_set'
        offer_set = OfferSet.find( counter_base )
        offer     = offer_set.offer
        campaign  = offer_set.campaign
        dealer    = offer_set.dealer
      else
        direct_offer = DirectOffer.find( counter_base )
        offer     = direct_offer.offer
        campaign  = nil
        dealer    = direct_offer.dealer
      end
      DealerStat.trigger_with_amount( :app_install, dealer: dealer, campaign: campaign, offer: offer, amount: counter[ 0 ] )
      DailyAggregate.trigger( :app_install, dealer: dealer, campaign: campaign, offer: offer, amount: counter[ 0 ] )
      offer.offer_limit.increment!( :install_count, counter[0] )
    end
  end

  def uniq_mongo_ids( event_name )
    mongo_event_atomics.where( event_name: event_name ).map{ |m| m.extras[ :mongo_id ] }.uniq
  end

  def import_js_push
    pushes = []
    result = {}
    mongo_event_atomics.where( event_name: 'js_push' ).each do | atomic |
      result[ atomic.offer_set_id ] ||= Hash.new
      if atomic.extras[ :uniq ]
        result[ atomic.offer_set_id ][ :uniq ] = if result[ atomic.offer_set_id ][ :uniq ]
                                                   result[ atomic.offer_set_id ][ :uniq ] + atomic.counter
                                                 else
                                                   atomic.counter
                                                 end
      else
        result[ atomic.offer_set_id ][ :not_uniq ] = if result[ atomic.offer_set_id ][ :not_uniq ]
                                                      result[ atomic.offer_set_id ][ :not_uniq ] + atomic.counter
                                                     else
                                                       atomic.counter
                                                     end
      end
    end
    result.each do | offer_set_id, counter |
      offer_set = OfferSet.find_by( id: offer_set_id )
      if offer_set
        DealerStat.trigger_with_amount( :uniq_js_visitor, offer: offer_set.offer, campaign: offer_set.campaign, amount: counter[ :uniq ] ) if counter[ :uniq ]
        DealerStat.trigger_with_amount( :js_visitor,      offer: offer_set.offer, campaign: offer_set.campaign, amount: counter[ :not_uniq ] ) if counter[ :not_uniq ]
      end
    end
  end

  def import_clicker
    clickers = []
    result = {}
    mongo_event_atomics.where( event_name: 'clicker' ).each do | atomic |
      result[ atomic.offer_set_id ] ||= Hash.new
      if atomic.extras[ :uniq ]
        result[ atomic.offer_set_id ][ :uniq ] = if result[ atomic.offer_set_id ][ :uniq ]
                                                   result[ atomic.offer_set_id ][ :uniq ] + atomic.counter
                                                 else
                                                   atomic.counter
                                                 end
      else
        result[ atomic.offer_set_id ][ :not_uniq ] = if result[ atomic.offer_set_id ][ :not_uniq ]
                                                      result[ atomic.offer_set_id ][ :not_uniq ] + atomic.counter
                                                    else
                                                      atomic.counter
                                                    end
      end
    end
    result.each do | offer_set_id, counter |
      offer_set = OfferSet.find_by( id: offer_set_id )
      if offer_set
        DealerStat.trigger_with_amount( :uniq_visitor, offer: offer_set.offer, campaign: offer_set.campaign, amount: counter[ :uniq ] ) if counter[ :uniq ]
        DealerStat.trigger_with_amount( :visitor,      offer: offer_set.offer, campaign: offer_set.campaign, amount: counter[ :not_uniq ] ) if counter[ :not_uniq ]
        DailyAggregate.trigger( :visitor, campaign: offer_set.campaign, offer: offer_set.offer, amount: counter[ :uniq ] ) if counter[ :uniq ]
      end
    end
  end

  def import_traffic_back
    result  = {}
    tb_refs = []
    mongo_event_atomics.where( event_name: 'traffic_back' ).each do | atomic |
      dealer   = Dealer.find( atomic.dealer_id )
      campaign = Campaign.find( atomic.campaign_id )
      referrer = atomic.extras ? atomic.extras[ :referer ] : 'N/A'
      reason   = atomic.extras ? atomic.extras[ :reason ]  : 'N/A'
      tb_refs  << TbReferrer.new( dealer: dealer, campaign: campaign, referrer: referrer, reason: reason)
      result[ atomic.campaign_id ] = result[ atomic.campaign_id ] ? ( result[ atomic.campaign_id ] + atomic.counter ) : atomic.counter
    end
    TbReferrer.import tb_refs
    result.each do | campaign_id, counter |
      campaign = Campaign.find( campaign_id )
      DealerStat.trigger_with_amount( :uniq_traf_back, campaign: campaign, amount: counter )
    end
  end

end
