class VisitorQueue

  def self.redis_key_for( fact, campaign_id, offer_id, triggered_at )
    triggered_time = Time.zone.at( triggered_at )
    year  = triggered_time.year
    month = triggered_time.month
    day   = triggered_time.day
    hour  = triggered_time.hour
    raw_key = "#{ fact }:#{ campaign_id }:#{ offer_id }:#{ year }:#{ month }:#{ day }:#{ hour }"
    'visitor_facts_' + Digest::MD5.hexdigest( raw_key )
  end

  def self.load_event_to_redis( fact, dealer_id, campaign_id, offer_id, current_timestamp )
    redis_key = redis_key_for( fact, campaign_id, offer_id, current_timestamp )
    $redis.set( redis_key, default_value( fact, dealer_id, campaign_id, offer_id, current_timestamp ) )
  end

  def self.default_value( fact, dealer_id, campaign_id, offer_id, current_timestamp )
    { fact:         fact,
      dealer_id:    dealer_id,
      campaign_id:  campaign_id,
      offer_id:     offer_id,
      counter:      1,
      triggered_at: current_timestamp,
      expired_at:   current_timestamp + expired_timeout }.to_json
  end

  def self.increment( redis_key )
    redis_value = JSON.parse($redis.get( redis_key )).with_indifferent_access
    redis_value[ :counter ] = redis_value[ :counter ].next
    $redis.set( redis_key, redis_value.to_json )
  end

  def self.check_expire
    redis_values = current_redis_values
    redis_values.each { |k,v | $redis.del( k ) }
    redis_values.each do | redis_key, fact |
      campaign = Campaign.find( fact[ :campaign_id ] )
      offer    = Offer.find( fact[ :offer_id ] )
      current  = fact[ :triggered_at ]
      amount   = fact[ :counter ]
      event_fact = fact[ :fact ].to_sym
      DealerStat.trigger_with_amount( event_fact, campaign: campaign, offer: offer, amount: amount, current: current )
      DailyAggregate.trigger( :visitor, campaign: campaign, offer: offer, amount: amount ) if fact[ :fact ] == 'uniq_visitor'
    end
  end

  def self.current_redis_values
    unsorted = $redis.keys('visitor_facts_*').map do | redis_key |
      [ redis_key, JSON.parse( $redis.get( redis_key ) ).with_indifferent_access ]
    end
    sorted = unsorted.sort! { |a,b| a[1][ :triggered_at] <=> b[1][ :triggered_at ] }
    sorted.to_h.with_indifferent_access
  end

  def self.expired_timeout
    300
  end
end
