class Admin::Stats::Timing < Admin::Stats

  def self.records_for_caching( dealer=nil, params={} )
    ar_params = prepare_for_ar( params )
    DailyAggregate.select(
      %s[
        short_date                                  as short_date,
        MAX( created_at                           ) as created_at,
        SUM( daily_aggregates.daily_install_count ) as daily_install_count,
        SUM( daily_aggregates.daily_visitor_count ) as daily_visitor_count,
        AVG( daily_aggregates.conversion_rate     ) as conversion_rate,
        SUM( daily_aggregates.seller_revenue      ) as seller_revenue,
        SUM( daily_aggregates.daily_revenue       ) as daily_revenue
      ]
    ).where( ar_params ).group( 'short_date' ).order( 'created_at ASC' )
  end

end
