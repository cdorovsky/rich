class Admin::Stats::Dealers < Admin::Stats

  def self.default_date_start
    0.days.ago.beginning_of_week.in_time_zone.to_s
  end

  def self.records_for_caching( dealer=nil, params={} )
    ar_params = prepare_for_ar( params )
    pager_params = prepare_for_pager( params )
    DailyAggregate.select(
      %s[
        SUM( daily_aggregates.daily_install_count ) as daily_install_count,
        SUM( daily_aggregates.daily_visitor_count ) as daily_visitor_count,
        MAX( daily_aggregates.conversion_rate     ) as conversion_rate,
        SUM( daily_aggregates.seller_revenue      ) as seller_revenue,
        SUM( daily_aggregates.daily_revenue       ) as daily_revenue,
           (  dealer_id                           ) as id
      ]
    ).where( ar_params )
    .group( 'dealer_id' )
    .paginate( page: params[ :page ], per_page: params[ :per_page ] )
    .order( 'daily_revenue DESC, daily_install_count DESC' )
  end

  def self.reload_cache
    Precacher.reload( self, Dealer.new, params: PrecacherParams.admin_stats_index, options: { expiring: 10.minutes.from_now.to_i } )
  end

  def self.restricted_caching_params( params={} )
    %i[ created_at date_start date_end dealer_id offer_id ] & params.keys.map(&:to_sym)
  end

  class TopCR < Admin::Stats::Dealers
    def self.records_for_caching( dealer=nil, params={} )
      ar_params = prepare_for_ar( params )
      DailyAggregate.joins( :dealer ).select(
        %s[
          MAX( daily_aggregates.conversion_rate ) as conversion_rate,
          SUM( daily_aggregates.seller_revenue  ) as seller_revenue,
          SUM( daily_aggregates.daily_revenue   ) as daily_revenue,
             (  dealer_id                       ) as dealer_id
        ]
      ).where( ar_params )
      .where( dealers: { blocked: false } )
      .group( 'dealer_id' )
      .order( 'conversion_rate DESC' ).limit(10)
    end

    def self.reload_cache
      Precacher.reload( self, Dealer.new, params: PrecacherParams.top_cr, options: { expiring: 10.minutes.from_now.to_i } )
    end

  end

end
