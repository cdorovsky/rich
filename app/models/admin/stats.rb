class Admin::Stats

  def self.default_date_start
    2.month.ago.end_of_month.in_time_zone.to_s
  end

  def self.default_date_end
    Time.zone.now.end_of_day.to_s
  end

  def self.prepare_for_ar( params={} )
    params[ :date_start ] ||= default_date_start
    params[ :date_end ]   ||= default_date_end
    start  = params[ :date_start ].try( :to_date )
    finish = params[ :date_end ].try( :to_date ).in_time_zone.end_of_day
    params[ :created_at ] = start..finish
    params.except( :controller, :action, :format, :date_start, :date_end, :page, :per_page, :total_pages, :total_entries )
  end

  def self.prepare_for_pager( params={} )
    params[ :per_page ] = params[ :per_page ].to_i.zero? ? 50 : [ params[ :per_page ].to_i, 100 ].min
    params[ :page ] = params[ :page ].to_i.zero? ? 1 : params[ :page ].to_i
    params.slice( :page, :per_page )
  end

  def self.reload_all
    Admin::Stats::Dealers::TopCR.reload_cache
    Admin::Stats::Dealers.reload_cache
  end

end
