class Wallet < ActiveRecord::Base
  belongs_to :dealer
  has_one :money_options

  validate :validate_main_ident
  validates :name, presence: true
  validates :number, presence: :true, if: :wmz?
  validates :swift_code, :account_number_iban, presence: :true, if: :bank?
  validate :wmz_wallets_quantity, on: :create

  scope :wmz,  -> { where( kind: [ 'WMZ' ] ) }
  scope :bank, -> { where( kind: [ 'Bank' ] ) }
  scope :pending, -> { where( pending: true ) }

  before_save :case_control

  def self.cached_for( dealer_id )
    wallets = where( dealer_id: dealer_id ).order('updated_at DESC')
    Precacher.build_template( 'personal/wallets/index.json.jbuilder', wallets: wallets )
  end

  def wmz?
    kind == 'WMZ'
  end

  def bank?
    kind == 'Bank'
  end

  def self.kind
    self.pluck( :kind ).uniq.first
  end

  def self.are_same_kind?
    self.pluck( :kind ).uniq.count <= 1
  end

  private

    def case_control
      if kind == 'WMZ'
        self.number.upcase!
      elsif kind == 'PayPal'
        self.number.downcase!
      end
    end

    def wallet_creation_prohibited?
      dealer && dealer.wallets.wmz.count > dealer.webmoney_wallets_count
    end

    def wmz_wallets_quantity
      if wallet_creation_prohibited?
        self.errors[ :name ] << 'Превышено допустимое число Webmoney кошельков для данного аккаунта. Обратитесь в службу поддержки.'
      end
    end

    def validate_main_ident
      case kind
      when 'WMZ'
        unless /\A[Z]\d{12}?\Z/i.match( number )
          self.errors[ :number ] << 'Номер кошелька введен неверно'
        end
      when 'PayPal'
        unless /^([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})$/i.match( number )
          self.errors[ :number ]  << 'E-mail введен неверно'
        end
      when 'Visa'
        unless /\d{16}/i.match( number )
          self.errors[ :number ] << 'Номер карты введен неверно'
        end
      when 'MasterCard'
        unless /\d{16}/i.match( number )
          self.errors[ :number ] << 'Номер карты введен неверно'
        end
      when 'Bank'
        swift_match = /(^[a-z]{6})[a-z0-9]{2}[a-z0-9]{3}/i.match( swift_code )
        swift_lenght = swift_match ? swift_match[0].length : 0
        unless swift_lenght == 11
          self.errors[ :number ] << 'Код SWIFT введен неверно'
        end
      else
        self.errors[ :number ] << 'Ошибка'
      end
    end

end
