class OfferComment < ActiveRecord::Base
  belongs_to :commenter

  scope :rand_application, -> { limit( 3 ).where( app_kind: 'application' ).order( 'RANDOM()' ) }
  scope :rand_game,        -> { limit( 3 ).where( app_kind: 'game'        ).order( 'RANDOM()' ) }

end
