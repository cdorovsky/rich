class SmsLanding < SMS

  def self.send_app_link( subscriber={}, landing_name='' ,link='' )
    @sub = subscriber
    message = { phone: @sub.phone, text: "Установите приложение #{ landing_name } перейдя по ссылке #{ link }" }
    send_message message
  end
end