require "#{ FileUtils.pwd }/lib/bunny/event"

class DirectOfferVisitor

  attr_accessor :event

  def initialize( event_json )
    @event = Event.new( event_json )
  end

end
