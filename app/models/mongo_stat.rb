class MongoStat
  include Mongoid::Document
  include Mongoid::Timestamps

  field :dealer_id,   type: Integer
  field :offer_id,    type: Integer
  field :campaign_id, type: Integer
  field :minute_number,  type: Integer

  embeds_many :mongo_seller_requests

  def self.trigger( seller_request, triggered_at )
    stat_options = seller_request.mongo_stat_options
    m_stat = find_or_create_by( stat_options )
    m_seller_request_options = seller_request.mongo_seller_request_options( m_stat, triggered_at )
    m_seller_request_options[ :pg_seller_request_id ] = m_seller_request_options.delete( :id )
    m_stat.mongo_seller_requests.push( MongoSellerRequest.new( m_seller_request_options ) )
    m_stat
  end

  def self.session_minute
    ( Time.zone.now.min / 10 )
  end

  def self.scope_minute
    scope_minute = session_minute == 0 ? 5 : session_minute - 1
  end

  def self.for_loading
    where( minute_number: scope_minute )
  end

  def self.load_to_pg( options = {} )
    collection_for_load = options[ :all ] ? all : for_loading
    dealer_ids_for_caching = []
    increment_for = {}
    collection_for_load.each do | mongo_stat |
      counter  = mongo_stat.mongo_seller_requests.count
      campaign = Campaign.find( mongo_stat.campaign_id )
      offer    = Offer.find( mongo_stat.offer_id )
      dealer   = campaign.dealer
      increment_for[ offer.id ] = increment_for[ offer.id ] ? ( increment_for[ offer.id ] += counter ) : counter
      dealer_ids_for_caching << dealer.id
      DealerStat.trigger( :app_install, campaign: campaign, offer: offer, amount: counter, triggered_at: mongo_stat.created_at.to_i )
      DailyAggregate.trigger( :app_install, campaign: campaign, offer: offer, amount: counter )
      postbacks         = []
      conversions       = []
      app_installations = []
      dealer_increments = []
      mongo_stat.mongo_seller_requests.each do |mongo_seller_request|
        conversions       << Conversion.new(      mongo_seller_request.for_conversion( campaign, offer, dealer ) )
        postbacks         << Postback.new(        mongo_seller_request.for_postback( offer, campaign ) )
        app_installations << AppInstallation.new( mongo_seller_request.for_app_installation )
        dealer_increments << DealerIncrement.new( mongo_seller_request.for_dealer_increment( dealer ) )
      end
      Postback.import        postbacks
      Conversion.import      conversions
      AppInstallation.import app_installations
      DealerIncrement.import dealer_increments
    end
    Dashboard.enqueue_dealer_results( dealer_ids_for_caching )
    DealerStat.enqueue_dealer_results( dealer_ids_for_caching )
    Offer.increment_install_counter_and_check_limit( increment_for )
  end

  def self.clear_scoped
    delete_all( minute_number: scope_minute )
  end
end
