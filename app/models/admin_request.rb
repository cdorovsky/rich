class AdminRequest < ActiveRecord::Base

  belongs_to :dealer
  has_one    :dealer_increment, as: :reason
  has_one    :dealer_balance, primary_key: :dealer_id, foreign_key: :dealer_id

  validates_presence_of :dealer_id
  validates_presence_of :value, message: 'Вы не можете начислить партнеру ничего.'
  validates_exclusion_of :value, within: 0..0, message: 'Вы не можете начислить партнеру 0 денег.'

  after_create do
    trigger unless triggered
  end

  def self.trigger( dealer, value )
  	admin_request = new( dealer: dealer, value: value, triggered: true )
  	if admin_request.save
  	   dealer.dealer_balance.admin_request_trigger( admin_request )
  	end
    admin_request
  end

  def trigger
    dealer_balance.admin_request_trigger( self )
    update_column :triggered, true
  end

  def for_balance( balance )
    {
      dealer_balance: balance,
      dealer:         dealer,
      confirmed:      value,
      reason:         self,
      paid:           true
    }
  end

end
