class Commenter < ActiveRecord::Base
  has_many :offer_comments, dependent: :destroy
  mount_uploader :avatar, ImageUploader
end
