class CampaignCountrySet < ActiveRecord::Base
  belongs_to :campaign
  belongs_to :country
end
