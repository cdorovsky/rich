class JsApiRequest < BuyerRequest

  BUYER_REQUEST_SALT = 'god love peace'

  attr_accessor :redirect_url, :message, :img_url, :redis_offer, :uniq_id, :request_info, :offer_set_id, :locale, :target

  def initialize( request_info, params={} )
    @request_info = request_info
    @offer_set_id = params[ :id ]
    @redis_offer = offer_from_redis( params[ :id ] )
    @message = RequestInformation.message_l10n( @redis_offer[:message], request_info.locale )
    if possible_to_handle( redis_offer[ :offer_id ] )
      params[ :no_count ] = true
      @uniq_id = MongoEventContainer.buyer_request( request: request_info, redis: @redis_offer, web_params: params )
      @redirect_url = generate_redirect_url( @uniq_id, @redis_offer )
    end
  end

  def offer_from_redis( id )
    JSON.parse( $redis.get( "offer_set_#{ id }" ) ).with_indifferent_access
  end

  def generate_redirect_url( id, params = {} )
    provider_url_generator = 'Providers::AppsAdvert::' + params[ :seller_name ] + '::Sender.redirect( id, params )'
    target_url = eval( provider_url_generator )
    target_url
  end

  def possible_to_handle( offer_id )
    offer_enabled = @redis_offer[ :enabled ]
    possible_offers = OffersFilter.possible_offers( @request_info.user_platform, @request_info.country )
    offer_enabled & ( possible_offers.include? offer_id.to_i )
  end

  def choose_tb_url
    mobile = /Mobile|iP(hone|od|ad)|Android|BlackBerry|IEMobile/
    if mobile.match @request_info.user_agent
      @redis_offer[ :m_tb_url ]
    else
      @redis_offer[ :tb_url ]
    end
  end

end
