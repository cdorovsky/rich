class MtsIps

  def self.load_to_redis
    ip_list.each do | ip |
      $redis.set ip, 'mts'
    end
  end

def self.ip_list
  [
    '194.54.148.8',
    '213.87.6.8',
    '84.17.224.252',
    ( 0..255 ).to_a.map {|last_oktet| "84.17.254.#{last_oktet}"},
    ( 0..255 ).to_a.map {|last_oktet| "84.17.255.#{last_oktet}"},
    ( 0..255 ).to_a.map {|third_oktet| ( 0..255 ).to_a.map {|fourth_oktet| "213.87.#{third_oktet}.#{fourth_oktet}"}},
    ( 148..151 ).to_a.map {|third_oktet| ( 0..255 ).to_a.map {|fourth_oktet| "194.54.#{third_oktet}.#{fourth_oktet}"}},
    ( 226..232 ).to_a.map {|third_oktet| ( 0..255 ).to_a.map {|fourth_oktet| "217.8.#{third_oktet}.#{fourth_oktet}"}},
  ].flatten
end

end
