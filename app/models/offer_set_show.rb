class OfferSetShow < ActiveRecord::Base
  
  belongs_to :offer
  belongs_to :dealer
  belongs_to :offer_set
  belongs_to :campaign
  belongs_to :country

  scope :not_converted, -> { where( converted: false ) }

  def self.params_for_conversion( offer_set_show )
    if offer_set_show.class == OfferSetShow
    { sub_id:  offer_set_show.sub_id,
      sub_id2: offer_set_show.sub_id2,
      sub_id3: offer_set_show.sub_id3,
      revenue: offer_set_show.dealer.dealer_revenue( offer_set_show.offer ),
      referer: offer_set_show.referer,
      country_id:   offer_set_show.country_id,
      country_name: offer_set_show.country_name,
      country_code: offer_set_show.country_code,
      ip:           offer_set_show.ip.to_s,
      user_agent:   offer_set_show.user_agent,
      user_platform: offer_set_show.user_platform }
    elsif offer_set_show.class == MongoEventAtomic
      country = Country.find_by( abbr: RequestInformation.find_country_by_ip( offer_set_show.extras[ :ip ] ) )
      dealer = Dealer.find( offer_set_show.dealer_id )
      offer = Offer.find( offer_set_show.offer_id )
      revenue = dealer.dealer_revenue( offer )
      { referer: offer_set_show.extras[ :referer ],
        sub_id:  offer_set_show.extras[ :sub_id ],
        sub_id2: offer_set_show.extras[ :sub_id2 ],
        sub_id3: offer_set_show.extras[ :sub_id3 ],
        revenue: revenue,
        country_id:   country.id,
        country_name: country.name,
        country_code: country.abbr,
        ip:            offer_set_show.extras[ :ip ],
        user_agent:    offer_set_show.extras[ :user_agent ],
        user_platform: offer_set_show.extras[ :user_platform ] }
    end
  end

  def set_converted
    update! converted: true
  end
end
