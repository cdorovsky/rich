class Campaign < ActiveRecord::Base

  require 'rack/utils'

  belongs_to :dealer
  has_many :traffic_backs
  has_many :dealer_stats
  has_many :request_logs
  has_many :subscribes
  has_many :rebills
  has_many :conversions
  has_many :postbacks
  has_many :app_installations
  has_many :campaign_country_sets
  has_many :countries, through: :campaign_country_sets
  has_many :user_domains
  has_many :buyer_requests
  has_many :dealer_increments
  has_many :session_showed_offers
  has_many :daily_aggregates
  has_many :buyer_session
  has_one :redirect_settings
  has_one :campaign_limiter
  has_many :offer_sets, -> { not_removed }, :class_name => 'OfferSet'
  has_many :removed_offer_sets, -> { removed }, :class_name => 'OfferSet'
  has_many :offers, through: :offer_sets
  has_many :tb_referrers
  accepts_nested_attributes_for :user_domains, reject_if: proc { |attributes| UrlValidator.url_valid?( attributes[ 'url' ] ) == false }
  accepts_nested_attributes_for :campaign_limiter
  validates_presence_of :name, message: 'Не может быть пустым.'
  validates :trafficback_url, url: true
  validates :m_trafficback_url, url: true
  validates :broken_offer_behavior, inclusion: { in: %w( next_offer traffic_back ) }
  validate :postback_url_params_should_be_correct
  validate :count_of_user_domains
  validate :uniqueness_of_user_domains

  before_save :set_countries
  after_save :create_settings, :prioritize_offers, :load_code_to_redis
  after_create :singletone_limiter, :create_hash_code

  scope :actual,   -> { where( archived: false ) }
  scope :archived, -> { where( archived: true  ) }

  def available?
    enabled & !archived
  end

  def prioritize_offers
    case offer_scheme.to_sym
    when :cost
      offers_sorted_by_cost = offers.sort_by { |offer| offer.tariff }.reverse
      order_number = 1
      offers_sorted_by_cost.each do |offer|
        OfferSet.find_by( campaign: self, offer: offer ).update_attribute( :priority, order_number )
        order_number += 1
      end
    end
  end

  def create_hash_code
    redirect_code = RedirectCode.generate( self )
    update_column( :hash_code, redirect_code )
  end

  def singletone_limiter
    CampaignLimiter.find_or_create_by( campaign_id: self.id )
  end

  def redirect_codes_for_links
    offer_sets.active.joins( :offer ).map { | offer_set |  { code: offer_set.id, offer_name: offer_set.offer.name } }
  end

  def create_settings
    RedirectSettings.find_or_create_by( campaign_id: self.id )
  end

  def service_id
    adult == 'adult' ? 48 : 65
  end

  def returnurl
    'about:blank'
  end

  def monthly_income
    self.dealer_stats.monthly.apps_advert.last.try( :money_total ) || 0
  end

  def offers_count
    self.offers.enabled.count
  end

  def self.campaign_for_buyout( old_campaign )
    find_or_create_by( buyout_source_id: old_campaign.id ) do | campaign |

      campaign.dealer            = Dealer.subscribe_buyer
      campaign.name              = "#{ old_campaign.dealer_id}_#{ old_campaign.id }"
      campaign.trafficback_url   = old_campaign.trafficback_url
      campaign.m_trafficback_url = old_campaign.m_trafficback_url
      campaign.user_domains      = old_campaign.user_domains
      campaign.save!
      campaign.offers            = old_campaign.offers

      old_campaign.buyout_target_id = campaign.id
      old_campaign.save!

    end
  end

  def set_countries
    country_list = offers.enabled.map { | o | o.countries }.flatten.uniq
    self.countries, self.countries_count = Country.country_list_with_count( country_list )
    self
  end

  def postback_target_url
    postback_url.split( '?' )[ 0 ]
  end

  def postback_params_list
    postback_url_params = postback_url.split( '?' )[ 1 ]
  end

  def ready_to_send_postback?
    sent_postback and !postback_url.blank?
  end

  def has_landings?
    offers.pluck(:weblanding_ready).any?
  end

  def landings_enabled?
    disable_weblandings == false
  end

  def set_offers!( offer_ids )
    self.offer_sets.where.not( offer_id: offer_ids ).update_all( removed: true )
    offer_ids.each do |oid|
      os = OfferSet.find_or_initialize_by( offer_id: oid, campaign_id: self.id, dealer_id: self.dealer_id )
      os.removed = false
      os.save
    end
  end

  def load_code_to_redis
    offer_sets = self.offer_sets.active.order( 'cost DESC' ).pluck( :id )
    tb_url     = trafficback_url
    m_tb_url   = m_trafficback_url
    result = { offers:      offer_sets,
               hours:       campaign_limiter.hours,
               days:        campaign_limiter.days,
               show_limits: campaign_limiter.show_limits,
               tb_url:      tb_url,
               m_tb_url:    m_tb_url }.to_json
    $redis.set( "redirect_code_#{ hash_code }", result)
  end

  private

  def count_of_user_domains
    if user_domains.blank?
      errors.add( :user_domains, 'Укажите хотя бы один источник трафика!' )
    elsif user_domains.size > 5
      errors.add( :user_domains, 'Лимит в 5 источников превышен!' )
    end
  end

  def uniqueness_of_user_domains
    unless UserDomain.is_unique_collection?( user_domains )
      errors.add( :user_domains, 'Источники траффика должны быть уникальными в пределах одной кампании!' )
    end
  end

  def postback_url_params_should_be_correct
    if postback_url
      strict_params = %w( datetime conversion_type conversion_code campaign_id offer_id offer_name country_code profit_usd sub_id sub_id2 sub_id3 )
      custom_params_list = Rack::Utils.parse_nested_query( postback_url.split( '?' )[ 1 ] )
      custom_params_list.each do |name, value|
        if /{\w+}/.match value
          param_name = value.gsub( /[{}]/,'' )
          errors.add(:postback_url, "Некорректные имена параметров в фигурных скобках") unless strict_params.include?( param_name )
        end
      end
    end
  end

  def self.cached_for( dealer_id )
    campaigns = where( dealer_id: dealer_id ).actual.order( 'updated_at DESC' ).limit( 10 )
    Precacher.build_template( 'personal/dashboard/campaigns.json.jbuilder', campaigns: campaigns )
  end

end
