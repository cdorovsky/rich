class DealerEmail < ActiveRecord::Base

  serialize :dealer_ids, Array
  validates_presence_of :name, :subject, :body


  def deliver
    if self.dealer_ids.count > 10
      deliver_async
    else
      deliver_sync
    end
  end

  def deliver_sync
    dealers = Dealer.where( id: self.dealer_ids )
    dealers.each do | dealer |
      email = dealer.email
      subject = self.subject
      body = self.body
      DealerMailer.custom_email( email, subject, body ).deliver
    end
  end

  def deliver_async
    ids_slices = self.dealer_ids.each_slice(10).to_a
    ids_slices.each do | dealer_ids |
      subject = self.subject
      body = self.body
      AsyncMailDelivery.perform_async( dealer_ids, subject, body )
    end
  end
end
