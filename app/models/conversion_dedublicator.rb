module ConversionDedublicator

  class << self

    def clear_all( start, finish )
      increment_ids = get_increment_dubs( start, finish )
      clear_increment_dubs( increment_ids )
      conversions_ids = get_converson_dubs( start, finish )
      clear_conversion_dubs( conversions_ids )
      Dealer.all.each { |dealer| recalculate_balance( dealer.id ) }
    end

    def get_increment_dubs( start, finish )
      all  = DealerIncrement.where( created_at: start..finish, reason_type: 'OfferSetShow' ).map{ |d| d.id }
      uniq = DealerIncrement.where( created_at: start..finish, reason_type: 'OfferSetShow' ).uniq_by{ |d| d.reason_id }.map{ |d| d.id }
      inc_dubs = all - uniq
    end

    def clear_increment_dubs( increment_ids )
      DealerIncrement.where( id: increment_ids ).delete_all
    end

    def get_converson_dubs( start, finish )
      all  = Conversion.where( created_at: start..finish ).map{ |d| d.id }
      uniq = Conversion.where( created_at: start..finish ).uniq_by{ |d| d.mongo_id }.map{ |d| d.id }
      con_dubs = all - uniq
    end

    def clear_conversion_dubs( conversions_ids )
      Conversion.where( id: conv_id ).delete_all
    end

    def recalculate_balance( dealer_id )
      new_balance = DealerIncrement.where( dealer_id: dealer_id ).sum( :confirmed ).round(2)
      DealerBalance.find_by( dealer_id: dealer_id ).update( confirmed: new_balance )
    end

  end
end