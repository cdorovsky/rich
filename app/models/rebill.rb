class Rebill < ActiveRecord::Base

  belongs_to :dealer
  belongs_to :subscribe
  belongs_to :offer
  belongs_to :campaign
  has_one    :seller_request

  validates_presence_of :subscribe_id

  #before_create :check_subscribe, :one_rebill_per_day
  after_create :update_rebilled_at

  def self.trigger( seller_request )
    if seller_request.seller_name == 'shemrock'
      sub = Subscribe.find_by( campaign: seller_request.campaign, offer: seller_request.offer, active: true )
    else
      sub = Subscribe.find_by( sub_id: seller_request.external_sub_id )
    end
    if sub
      rebill = Rebill.create(  subscribe_id:   sub.id,
                               billing:        seller_request.seller_name,
                               tariff:         seller_request.offer.tariff,
                               dealer_revenue: seller_request.offer.dealer_revenue,
                               dealer_id:      seller_request.dealer_id,
                               campaign_id:    seller_request.campaign_id,
                               offer_id:       seller_request.offer_id,
                               platform:       sub.platform,
                               country:        seller_request.country.name,
                               operator:       sub.operator )
      sub.update_rebill
      seller_request.update_attribute( :rebill_id, rebill.id )
      rebill
    else
      nil
    end
  end

  def check_subscribe
    subscribe.present?
  end

  def one_rebill_per_day
    unless subscribe.rebills.count == 0
      subscribe.rebills.last.created_at < 1.day.ago
    else
      true
    end
  end
  
  def update_rebilled_at
    unless  %w( shemrock plasticmedia ).include? billing 
      if subscribe
        subscribe.rebilled_at = Time.zone.now
        subscribe.rebill_count += 1
        subscribe.save
      end
    end
  end

  def self.handle_stats( options = {} )
    rebill = Rebill.create!( subscribe_id:   options[ :subscribe ].id,
                             billing:        options[ :subscribe ].billing,
                             phone:          options[ :subscribe ].phone,
                             tariff:         options[ :offer ].tariff,
                             dealer_revenue: options[ :offer ].dealer_revenue,
                             dealer:         options[ :subscribe ].dealer,
                             campaign:       options[ :subscribe ].campaign,
                             offer:          options[ :offer ],
                             platform:       options[ :subscribe ].platform,
                             country:        options[ :country ],
                             operator:       options[ :subscribe ].operator )
    if options[ :subscribe ].rebill_stats_triggered_immediately
      offer_id         = options[ :offer ].id 
      campaign_id      = options[ :subscribe ].campaign.id
      dealer_id        = options[ :subscribe ].dealer.id
      Resque.enqueue( StatTrigger, :rebill, dealer_id, campaign_id, offer_id, nil )
      country = Country.find_by( name: options[ :country ] )
      Conversion.trigger( :rebill, campaign: options[ :subscribe ].campaign, offer: options[ :offer ] , country: country )
      options[ :subscribe ].dealer.increase_balance( options[ :offer ].dealer_revenue ) 
      #save_request_logs( options[ :offer ].dealer_revenue, 'revenue' )
    end


  end

end
