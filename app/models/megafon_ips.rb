class MegafonIps

  def self.load_to_redis
    ip_list.each do | ip |
      $redis.set ip, 'megafon'
    end
  end

def self.ip_list
  [
    '193.201.231.2',
    '83.149.0.228',
    '83.149.24.249',
    '83.149.24.251',
    '83.149.24.252',
    '83.149.49.209',
    '83.149.49.212',
    '5.153.30.100',   
    ( 32..35 ).to_a.map {|third_oktet| ( 0..255 ).to_a.map {|fourth_oktet| "185.3.#{third_oktet}.#{fourth_oktet}"}}, 
    ( 0..255 ).to_a.map {|fourth_oktet| "217.8.185.#{fourth_oktet}"}, 
    ( 0..255 ).to_a.map {|fourth_oktet| "37.28.187.#{fourth_oktet}"},
    ( 0..255 ).to_a.map {|fourth_oktet| "37.28.185.#{fourth_oktet}"},
    ( 0..255 ).to_a.map {|fourth_oktet| "37.29.86.#{fourth_oktet}"},
    ( 0..255 ).to_a.map {|fourth_oktet| "37.29.87.#{fourth_oktet}"},
    ( 132..134 ).to_a.map {|third_oktet| ( 0..255 ).to_a.map {|fourth_oktet| "46.229.#{third_oktet}.#{fourth_oktet}"}},
    ( 140..142 ).to_a.map {|third_oktet| ( 0..255 ).to_a.map {|fourth_oktet| "46.229.#{third_oktet}.#{fourth_oktet}"}},
    ( 202..203 ).to_a.map {|third_oktet| ( 0..255 ).to_a.map {|fourth_oktet| "46.232.#{third_oktet}.#{fourth_oktet}"}},
    ( 120..123 ).to_a.map {|third_oktet| ( 0..255 ).to_a.map {|fourth_oktet| "78.25.#{third_oktet}.#{fourth_oktet}"}},
    ( 88..95 ).to_a.map {|third_oktet| ( 0..255 ).to_a.map {|fourth_oktet| "78.25.#{third_oktet}.#{fourth_oktet}"}},
    ( 192..215 ).to_a.map {|fourth_oktet| "83.149.1.#{fourth_oktet}"},
    ( 2..3 ).to_a.map {|third_oktet| ( 0..255 ).to_a.map {|fourth_oktet| "83.149.#{third_oktet}.#{fourth_oktet}"}},
    ( 0..255 ).to_a.map {|fourth_oktet| "83.149.21.#{fourth_oktet}"},
    ( 64..127 ).to_a.map {|fourth_oktet| "83.149.24.#{fourth_oktet}"},
    ( 0..255 ).to_a.map {|fourth_oktet| "83.149.25.#{fourth_oktet}"},
    ( 0..255 ).to_a.map {|fourth_oktet| "83.149.28.#{fourth_oktet}"},
    ( 128..255 ).to_a.map {|fourth_oktet| "83.149.34.#{fourth_oktet}"},
    ( 35..38 ).to_a.map {|third_oktet| ( 0..255 ).to_a.map {|fourth_oktet| "83.149.#{third_oktet}.#{fourth_oktet}"}},
    ( 0..255 ).to_a.map {|fourth_oktet| "83.149.41.#{fourth_oktet}"},
    ( 128..255 ).to_a.map {|fourth_oktet| "83.149.43.#{fourth_oktet}"},
    ( 44..47 ).to_a.map {|third_oktet| ( 0..255 ).to_a.map {|fourth_oktet| "83.149.#{third_oktet}.#{fourth_oktet}"}},
    ( 0..143 ).to_a.map {|fourth_oktet| "83.149.48.#{fourth_oktet}"},
    ( 160..175 ).to_a.map {|fourth_oktet| "83.149.48.#{fourth_oktet}"},
    ( 0..31 ).to_a.map {|fourth_oktet| "83.149.49.#{fourth_oktet}"},
    ( 52..55 ).to_a.map {|third_oktet| ( 0..255 ).to_a.map {|fourth_oktet| "83.149.#{third_oktet}.#{fourth_oktet}"}},
    ( 8..9 ).to_a.map {|third_oktet| ( 0..255 ).to_a.map {|fourth_oktet| "83.149.#{third_oktet}.#{fourth_oktet}"}},
    ( 252..255 ).to_a.map {|third_oktet| ( 0..255 ).to_a.map {|fourth_oktet| "83.169.#{third_oktet}.#{fourth_oktet}"}},
    ( 0..255 ).to_a.map {|fourth_oktet| "85.26.155.#{fourth_oktet}"},
    ( 0..255 ).to_a.map {|fourth_oktet| "85.26.164.#{fourth_oktet}"},
    ( 0..255 ).to_a.map {|fourth_oktet| "85.26.176.#{fourth_oktet}"},
    ( 0..255 ).to_a.map {|fourth_oktet| "85.26.177.#{fourth_oktet}"},
    ( 0..255 ).to_a.map {|fourth_oktet| "85.26.179.#{fourth_oktet}"},
    ( 183..186 ).to_a.map {|third_oktet| ( 0..255 ).to_a.map {|fourth_oktet| "85.26.#{third_oktet}.#{fourth_oktet}"}},
    ( 0..255 ).to_a.map {|fourth_oktet| "85.26.189.#{fourth_oktet}"},
    ( 128..255 ).to_a.map {|fourth_oktet| "85.26.191.#{fourth_oktet}"},
    ( 192..195 ).to_a.map {|third_oktet| ( 0..255 ).to_a.map {|fourth_oktet| "85.26.#{third_oktet}.#{fourth_oktet}"}},
    ( 0..63 ).to_a.map {|fourth_oktet| "85.26.224.#{fourth_oktet}"},
    ( 227..230 ).to_a.map {|third_oktet| ( 0..255 ).to_a.map {|fourth_oktet| "85.26.#{third_oktet}.#{fourth_oktet}"}},
    ( 0..159 ).to_a.map {|fourth_oktet| "85.26.231.#{fourth_oktet}"},
    ( 232..235 ).to_a.map {|third_oktet| ( 0..255 ).to_a.map {|fourth_oktet| "85.26.#{third_oktet}.#{fourth_oktet}"}},
    ( 0..255 ).to_a.map {|fourth_oktet| "85.26.241.#{fourth_oktet}"},
    ( 0..255 ).to_a.map {|fourth_oktet| "37.29.88.#{fourth_oktet}"},
    ( 48..55 ).to_a.map {|third_oktet| ( 0..255 ).to_a.map {|fourth_oktet| "37.29.#{third_oktet}.#{fourth_oktet}"}},
    ( 96..111 ).to_a.map {|third_oktet| ( 0..255 ).to_a.map {|fourth_oktet| "37.29.#{third_oktet}.#{fourth_oktet}"}},
    ( 0..255 ).to_a.map {|fourth_oktet| "83.149.19.#{fourth_oktet}"},
    ( 192..199 ).to_a.map {|third_oktet| ( 0..255 ).to_a.map {|fourth_oktet| "46.29.#{third_oktet}.#{fourth_oktet}"}}
  ].flatten
end

end
