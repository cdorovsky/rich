class Providers::Wapclick::Shemrock::Sender

  def self.redirect( offer, buyer_request )
    offer_url = offer.url
    buyer_request_code = buyer_request.id
    url_tail = { 
      pt_id:  buyer_request_code 
    }
    target_url = offer_url + '?' + url_tail.to_query  
    target_url
  end

end
