class Providers::Wapclick::Billing8::Sender

  require 'rest_client'
  require 'json'

  attr_accessor :campaign, :responce

  def self.redirect( offer, buyer_request ) 
    buyer_request_code = buyer_request.id
    url = offer.url
    url_tail = { user_params: buyer_request_code }
    request = url + '&' + url_tail.to_query
    response_json = RestClient.get request
    response = JSON.parse response_json
    target_url = response["redirect"]
    target_url
  end

end
