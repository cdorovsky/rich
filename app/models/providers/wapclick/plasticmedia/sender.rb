class Providers::Wapclick::Plasticmedia::Sender

  def self.redirect( offer, buyer_request )
    offer_url = offer.url
    offer_ext_id = offer.external_id
    serviceSubscriptionId = buyer_request.id
    hash = Digest::MD5.hexdigest('4yh984y' + 'NEW' + "#{ offer_ext_id }" + "#{ serviceSubscriptionId }")
    if offer_ext_id == 2671 
      url_tail = {
        service_id:              offer_ext_id,
        service_subscription_id: serviceSubscriptionId,
      }
    else
      url_tail = {
        action:                'NEW',
        serviceId:             offer_ext_id,
        serviceSubscriptionId: serviceSubscriptionId,
        isWap:                 true,
        hash:                  hash
      }
    end
    target_url = "#{ offer_url }" + '?' + url_tail.to_query
    target_url   
  end

end
