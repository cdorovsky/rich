class Providers::Wapclick::Onlypay::Sender

  require 'rest_client'
  require 'nokogiri'
  require 'open-uri'

  attr_accessor :campaign, :responce

  def self.redirect( offer, buyer_request )
    return_url = offer.url
    buyer_request_code = buyer_request.id
    url = "http://api.onlypay.ru/subscribe/"
    params = { action:         'new',
               auth_hash:      '567b1a3a49d28f8cdb898c064edde26e',
               traffic_source: 'http://teasernet.com',
               service_id:     offer.external_id,
               returnurl:      return_url,
               mydata:         buyer_request_code }
    request = url + '?' + params.to_query
    response = RestClient.get request
    target_url = Nokogiri::XML( response ).xpath("//returnurl").children.text
    target_url
  end

end
