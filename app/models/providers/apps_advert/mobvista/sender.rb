class Providers::AppsAdvert::Mobvista::Sender

  def self.redirect( id, params = {} )
    offer_ext_id = params[ :external_id ]
    head_url = 'http://mobvista.go2cloud.org/aff_c'
    url_tail = { 
      offer_id: offer_ext_id, 
      aff_id:   2049,
      aff_sub:  id
    }
    target_url = head_url + '?' + url_tail.to_query
    target_url
  end

end
