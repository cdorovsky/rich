class Providers::AppsAdvert::Macromac::Sender

  def self.redirect( id, params = {} )
    head_url = params[ :seller_url ]
    url_tail = { 
      click_id: id,
      pubid: params[ :dealer_id ] }
    target_url = head_url + '&' + url_tail.to_query
    target_url
  end

end
