class Providers::AppsAdvert::Ucweb::Sender

  def self.redirect( id, params = {} )
    head_url = params[ :seller_url ]
    url_tail = { 
      uc_trans_1: id }
    target_url = head_url + '&' + url_tail.to_query
    target_url
  end

end
