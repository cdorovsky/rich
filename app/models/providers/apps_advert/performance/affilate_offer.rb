class Providers::AppsAdvert::Performance::AffilateOffer
  def self.get_offer( id, network_id )
    if network_id == 'appflood'
      request_params = {
        api_key: 'd92a4c23634f71f56d0e9b6f9994dd59a9beacd4970f2e0cadc05fc8428aa50d',
        NetworkId: 'appflood',
        Method: 'findById',
        id: id 
      }
    elsif network_id == 'revenues'
      request_params = {
        api_key: '95d359187417c431fdf89810b7ddf1f029fb9f3e01cccdba85a81d92c71c6edb',
        NetworkId: 'revenues',
        Method: 'findById',
        id: id 
      }
    elsif network_id == 'everyads'
      request_params = { 
        api_key: '126f5c4e3dcda2d8ec033481f5c858f231b2a25d5f2b95cb2c69624983b098a9',
        NetworkId: 'everyads',
        Method: 'findById',
        id: id
      }
    elsif network_id == 'iconpeak'
      request_params = {
        Method: 'findById',
        api_key: '0602c9a93d66578ed235337416116184d60dd24592c7f986c122cd89d681b2e9',
        NetworkId: 'iconpeak',
        id: id
      }
    else 
      request_params = { 
        api_key: '3d535272134664da7fc18017100b19209dcef0d6fca31861d628444df31b3575',
        NetworkId: 'mobvista',
        Method: 'findById',
        id: id
      }
    end
    @domain = 'api.hasoffers.com'
    @request_path = '/v3/Affiliate_Offer.json?' + request_params.to_query
    @raw_response = Net::HTTP.get_response( @domain, @request_path )
    @response = JSON.parse( @raw_response.body )[ 'response' ]
    if @response[ 'status' ] == 1
      @country_list = @response[ 'data' ][ 'Offer' ][ 'description' ]\
                    .split( '<b>Allowed Countries:</b> ' ).last\
                    .chomp( '.' ).gsub( 'St.', 'Saint' ).split( '\n' )
      @response[ 'data' ][ 'Offer' ][ 'country_list' ] = @country_list
    end
    @response
  end
end
