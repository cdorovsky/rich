class Providers::AppsAdvert::Performance::Sender
  
  def self.redirect( id, params = {} )
    offer_ext_id = params[ :external_id ]
    url_tail = { 
      offer_id: offer_ext_id, 
      aff_id:   8868,
      aff_sub:  id
    }
    target_url = 'http://tracking.performancerevenues.com/aff_c?' + url_tail.to_query
    target_url
  end

end
