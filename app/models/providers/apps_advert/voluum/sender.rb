class Providers::AppsAdvert::Voluum::Sender

  def self.redirect( id, params = {} )
    head_url = params[ :seller_url ]
    url_tail = {
      sid:  id
    }
    target_url = head_url + '?' + url_tail.to_query
    target_url
  end

end
