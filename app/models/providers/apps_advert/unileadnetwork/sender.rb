class Providers::AppsAdvert::Unileadnetwork::Sender

  def self.redirect( id, params = {} )
  	offer_ext_id = params[ :external_id ]
    head_url = 'http://go.unilead.net/aff_c'
    url_tail = { 
      offer_id: offer_ext_id, 
      aff_id:   8679,
      aff_sub:  id 
    }
    target_url = head_url + '?' + url_tail.to_query
    target_url
  end

end
