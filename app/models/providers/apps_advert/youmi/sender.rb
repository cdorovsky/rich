class Providers::AppsAdvert::Youmi::Sender

  def self.redirect( id, params = {} )
    url_tail = {
      offer_id: params[ :external_id ],
      aff_id:   1498,
      aff_sub:  id,
      aff_sub2: params[ :dealer_id ]
    }
    target_url = 'http://track.adxmi.com/aff_c?' + url_tail.to_query
    target_url
  end

end
