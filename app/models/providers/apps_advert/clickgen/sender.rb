class Providers::AppsAdvert::Clickgen::Sender

  def self.redirect( id, params = {} )
    head_url = params[ :seller_url ]
    url_tail = { 
      uid: id,
      a: params[ :dealer_id ] }
    target_url = head_url + '&' + url_tail.to_query
    target_url
  end

end
