class Providers::AppsAdvert::Appflooddirect::Sender

  def self.redirect( id, params = {} )
    offer_ext_id = params[ :external_id ]
    url_tail = {
      offer_id: offer_ext_id,
      aff_id:   440,
      aff_sub:  id 
    }
    target_url = 'http://usw.atracking.appflood.com/transaction/post_click?' + url_tail.to_query
    target_url
  end

end
