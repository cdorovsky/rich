class Providers::AppsAdvert::Mobvistadirect::Sender

  def self.redirect( id, params = {} )
    head_url = params[ :seller_url ]
    dealer_id = params[ :dealer_id ]
    url_tail = { 
      aff_sub: id,
      mb_subid: dealer_id
    }
    target_url = head_url + '&' + url_tail.to_query
    target_url
  end

end
