class Providers::AppsAdvert::Appsflyer::Sender

  def self.redirect( id, params = {} )
    head_url    = params[ :seller_url ]
    campaign_id = params[ :campaign_id ]
    af_site     = campaign_id.to_s + '_' + Digest::MD5.hexdigest( campaign_id.to_s )
    c_id        = 'richpays_' + af_site 
    url_tail = {
      clickid:   id,
      c:         c_id,
      af_siteid: af_site
    }
    target_url = head_url + '&' + url_tail.to_query
    target_url
  end

end
