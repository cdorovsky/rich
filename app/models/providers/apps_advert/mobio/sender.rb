class Providers::AppsAdvert::Mobio::Sender

  def self.redirect( id, params = {} )
    url_tail = {
      offer_id: params[ :external_id ],
      aff_id:   7306,
      aff_sub:  id,
      source: params[ :dealer_id ]
    }
    target_url =  'http://track.mobioplatform.com/aff_c?' + url_tail.to_query
    target_url
  end

end
