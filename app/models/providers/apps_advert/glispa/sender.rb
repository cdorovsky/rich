class Providers::AppsAdvert::Glispa::Sender

  def self.redirect( id, params = {} )
    head_url = params[ :seller_url ]
    url_tail = "#{ id }&placement=#{ params[ :dealer_id ] }"
    target_url = head_url + url_tail
    target_url
  end

end
