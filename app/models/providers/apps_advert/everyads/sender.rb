class Providers::AppsAdvert::Everyads::Sender

  def self.redirect( id, params = {} )
    offer_ext_id = params[ :external_id ]
    if params[ :apps_os  ]== 'ios'
      head_url = 'http://itunes.apples.com.ru/aff_c'
    else
      head_url = 'http://play.googles.ru.com/aff_c'
    end
    url_tail = { 
      offer_id: offer_ext_id, 
      aff_id:   2685,
      aff_sub:  id
    }
    target_url = head_url + '?' + url_tail.to_query
    target_url
  end

end
