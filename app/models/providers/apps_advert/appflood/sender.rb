class Providers::AppsAdvert::Appflood::Sender

  def self.redirect( id, params = {} )
    offer_ext_id = params[ :external_id ]
    url_tail = { 
      offer_id: offer_ext_id, 
      aff_id:   1823,
      aff_sub:  id
    }
    target_url = 'http://appflood.go2cloud.org/aff_c?' + url_tail.to_query
    target_url
  end

end
