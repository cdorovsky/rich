class Providers::AppsAdvert::Matomy::Sender

  def self.redirect( id, params = {} )
    url_tail = { 
      dp: id 
    }
    target_url = params[ :seller_url ] + '?' + url_tail.to_query
    target_url
  end

end
