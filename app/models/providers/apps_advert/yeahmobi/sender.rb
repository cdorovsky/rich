class Providers::AppsAdvert::Yeahmobi::Sender

  def self.redirect( id, params = {} )
  	offer_ext_id = params[ :external_id ]
    head_url = params[ :seller_url ]
    url_tail = { 
      aff_sub:  id
    }
    target_url = head_url + '&' + url_tail.to_query
    target_url
  end

end
