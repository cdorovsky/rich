class Providers::AppsAdvert::Domobmedia::Sender

  def self.redirect( id, params = {} )
    url_tail = {
      offer_id: params[ :external_id ],
      aff_id:   1788,
      aff_sub:  id,
      aff_sub5: params[ :dealer_id ]
    }
    target_url = 'http://tracking.domobmedia.com/aff_c?' + url_tail.to_query
    target_url
  end

end
