class LandingHeading < ActiveRecord::Base

  def self.default_for( opts={} )
    heading = find_by( app_kind: opts[ :kind ], is_default: true ) || create_default( app_kind: opts[:kind] )
  end

  def self.create_default( opts={} )
    default_heading_params = if opts[ :app_kind ] == 'game'
      {
        is_default: true,
        app_kind: 'game',
        body: ':loudspeaker::stuck_out_tongue_closed_eyes: Хочешь ураган эмоций? :rocket: Установи эту хитовую, увлекательную игру и получи их! :bomb::point_down:
             <small>(сделай это сейчас и не потеряй <strong>99.00 рублей</strong>)</small>'
      }
    else
      {
        is_default: true,
        app_kind: 'application',
        body: ':collision::ok_hand: Приложение недели. :iphone: Временно бесплатно в честь промо-акции. Не упусти возможность! :sunglasses::+1:
             <small>(установи сейчас и не потеряй <strong>99.00 рублей</strong> когда приложение вновь станет платным)</small>'
      }
    end
    create( default_heading_params )
  end

end
