class News < ActiveRecord::Base

  def self.convert_content_to_html( content )
    if content
      markdown = Redcarpet::Markdown.new(Redcarpet::Render::HTML, autolink: true, tables: true)
      markdown.render( content )
    else
      ''
    end
  end

end
