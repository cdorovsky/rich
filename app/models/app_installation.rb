class AppInstallation < ActiveRecord::Base

  belongs_to :dealer
  belongs_to :campaign
  belongs_to :offer
  has_one    :seller_request

  validates :transaction_id, uniqueness: true

  def self.trigger( seller_request, options = {})
    current_app_installation = app_installation_create( seller_request, options )
    seller_request.update_attribute( :app_installation_id, current_app_installation.id )
    current_app_installation
  end

  def self.app_installation_create( seller_request, options = {} )
    trigger_time = options[ :current ] ? Time.zone.at( options[ :current ] ) : Time.zone.now
    if %w( matomy mobvistadirect glispa ).include?( seller_request.seller_name ) 
      transaction_id = seller_request.buyer_request_id.to_s
    else
      transaction_id = seller_request.transaction_id
    end
  	AppInstallation.create( dealer: seller_request.dealer,
                            campaign: seller_request.campaign,
                            offer: seller_request.offer,
                            transaction_id: transaction_id,
                            triggered_at: trigger_time )
  end
end
