class OfferCountrySet < ActiveRecord::Base
  belongs_to :offer
  belongs_to :country

  after_create :reload_country_to_redis

  def reload_country_to_redis
    $redis.set "country_#{ country_id }_offers", country.offers.enabled.pluck( :id ).to_json
  end

end
