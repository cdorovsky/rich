class OfferLimit < ActiveRecord::Base

  belongs_to :offer

  scope :unlimited, -> { where( daily_limit_status: 'unlimited'       ) }
  scope :limited,   -> { where( daily_limit_status: [ 'available', 'expiring', 'exceeded' ] ) }

  before_save :change_limit_status
  after_save :check_and_alert_offer_limits

  def change_limit_status
    self.daily_limit_status = 'available' if daily_install_limit >  0 and daily_limit_status == 'unlimited'
    self.daily_limit_status = 'unlimited' if daily_install_limit == 0 and daily_limit_status != 'unlimited'
  end

  def self.check_and_reset_limit_counters
    limited.find_each do | limit |
      limit.reset_limit_counter if limit.limit_time_zone_midnight?
    end
  end

  def limit_time_zone_midnight?
   ( Time.now.utc + cap_time_zone_delta.hours ).hour == 0
  end

  def reset_limit_counter
    notify_expiring_offer_enabled if self.daily_limit_status == 'expiring'
    self.daily_limit_status = 'available'
    self.install_count      = 0
    self.save!
    offer.make_available if offer.disabled and offer.daily_limit_status == 'exceeded'
  end

  def check_and_alert_offer_limits
    case daily_limit_status
    when 'available'
      if ( install_count >= ( daily_install_limit * 0.8 ) and install_count < daily_install_limit )
        expiring_threshold
      elsif install_count >= daily_install_limit
        exceed_threshold
      end
    when 'expiring'
      exceed_threshold if install_count >= daily_install_limit
    end
  end

  def expiring_threshold
    self.daily_limit_status = 'expiring'
    save!
    offer.make_expire
    send_cap_notifications( 'expiring' )
  end

  def exceed_threshold
    self.daily_limit_status = 'exceeded'
    save!
    send_cap_notifications( 'exceeded' )
    offer.make_exceed
  end

  def notify_expiring_offer_enabled
    offer.update_column( :daily_limit_status, 'available' )
    dealers_list = offer.offer_sets.not_removed.pluck( :dealer_id ).uniq
    Dealer.where( id: dealers_list ).find_each do | dealer |
      if DailyAggregate.fifty_days_clicks_acceptable?( dealer, offer )
        campaign_ids = offer.offer_sets.not_removed.where( dealer_id: dealer.id ).pluck( :campaign_id )
        OfferDisablerHandler.perform_async( :enable, dealer.id, offer_id, campaign_ids )
      end
    end
  end

  def send_cap_notifications( status )
    if status == 'expiring'
      offer.offer_sets.available.pluck( :dealer_id ).uniq.each do | dealer_id |
        campaign_ids = offer.offer_sets.active.where( dealer_id: dealer_id ).pluck( :campaign_id )
        OfferCapLimitNotification.perform_async( :email_expiring, dealer_id, offer_id, campaign_ids )
        OfferCapLimitNotification.perform_async( :sms, dealer_id, offer_id, [] )
      end
    else
      offer.offer_sets.available.pluck( :dealer_id ).uniq.each do | dealer_id |
        campaign_ids = offer.offer_sets.active.where( dealer_id: dealer_id ).pluck( :campaign_id )
        OfferCapLimitNotification.perform_async( :email_exceeded, dealer_id, offer_id, campaign_ids )
      end
    end
  end

end
