module OffersFilter

  class << self
    def possible_offers( user_platform, user_country )
      platform( user_platform ) & country( user_country )
    end

    def platform( user_platform )
      platform_group =  user_platform + '_offers'
      filter_result_offer_set = JSON.parse( $redis.get platform_group ) || []
      filter_result_offer_set = filter_result_offer_set + JSON.parse( $redis.get 'mobile_offers' ) if user_platform != 'pc'
      filter_result_offer_set
    end

    def country( abbr )
      if abbr
        country_redis_string = $redis.get "country_offers_#{ abbr }"
        filter_result_offer_set = ( country_redis_string != nil ) ? JSON.parse( country_redis_string ) : []
      end
      filter_result_offer_set = filter_result_offer_set + JSON.parse( $redis.get "country_offers_ALL" )
    end

    def offers_to_show( redis_offers, country_platform_offers )
      redis_offers & country_platform_offers
    end

    def tb_reason( user_platform, abbr, redis_offers )
      if redis_offers.empty?
        reason = 'empty_campaign'
      elsif ( platform( user_platform ) & redis_offers ).empty?
        reason = 'platform'
      elsif ( country( abbr ) & redis_offers ).empty?
        reason = 'country'
      else
        reason = 'undefined'
      end
    end

  end

end
