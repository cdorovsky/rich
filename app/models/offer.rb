class Offer < ActiveRecord::Base

  require 'digest/md5'

  belongs_to :seller
  mount_uploader :icon, IconUploader
  mount_uploader :banner, BannerUploader
  has_many :offer_sets
  has_many :offer_country_sets
  has_many :offer_targets, dependent: :destroy
  has_many :request_logs
  has_many :subscribes
  has_many :rebills
  has_many :conversions
  has_many :campaigns, through: :offer_sets
  has_many :countries, through: :offer_country_sets
  has_many :restricted_offer_country_sets
  has_many :restricted_countries, through: :restricted_offer_country_sets, source: :country
  has_many :postbacks
  has_many :offer_screenshots
  has_many :app_installations
  has_many :buyer_requests
  has_many :dealer_increments
  has_many :session_showed_offers
  has_many :daily_aggregates
  has_many :landing_comment_sets
  has_many :offer_comments, through: :landing_comment_sets
  has_many :landing_heading_sets
  has_many :landing_headings, through: :landing_heading_sets
  has_many :direct_offers
  has_one :offer_limit
  has_one :offer_icon
  has_one :offer_disabler

  accepts_nested_attributes_for :offer_targets, allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :landing_headings, allow_destroy: false, reject_if: :all_blank
  accepts_nested_attributes_for :landing_heading_sets

  serialize :features_list, Array
  serialize :dialog_translations, Hash

  scope :system,       -> { find_by( system: true                        ) }
  scope :enabled,      -> { where( disabled: false                       ) }
  scope :not_hidden,   -> { where( hidden: false                         ) }
  scope :disabled,     -> { where( disabled: true                        ) }
  scope :wapclick,     -> { where( kind: 'wapclick'                      ) }
  scope :apps_advert,  -> { where( kind: 'apps_advert'                   ) }
  scope :cpa,          -> { where( kind: 'cpa'                           ) }
  scope :apps_and_cpa, -> { where( kind: [ 'apps_advert', 'cpa' ]        ) }
  scope :beeline,      -> { where( operator_name: 'beeline'              ) }
  scope :megafon,      -> { where( operator_name: 'megafon'              ) }
  scope :mts,          -> { where( operator_name: 'mts'                  ) }
  scope :ipad,         -> { where( apps_os: [ 'ios', 'ipad', '' ]        ) }
  scope :ipod_iphone,  -> { where( apps_os: [ 'ipod+iphone', 'ios', '' ] ) }
  scope :android,      -> { where( apps_os: [ 'android', '' ]            ) }
  scope :mobile,       -> { where( apps_os: [ 'mobile' ]                 ) }
  scope :pc,           -> { where( apps_os: [ 'pc' ]                     ) }
  scope :support,      -> { where( support: true                         ) }
  scope :landings,     -> { where( weblanding_ready: true, kind: 'apps_advert' ) }
  scope :unlimited,    -> { where( daily_limit_status: 'unlimited'       ) }
  scope :limited,      -> { where( daily_limit_status: [ 'available', 'expiring', 'exceeded' ] ) }


  validates_presence_of :name
  validates :kind, inclusion: { in: %w( cpa wapclick apps_advert mobile_content ) }

  before_create :assign_headings, if: proc { | offer | offer.apps_advert? }
  after_create :new_offer_disabler, :new_offer_limit
  before_save :clear_disablers, :assign_comments, :change_limit_status
  after_save :set_countries, 
             :set_restricted_countries, 
             :check_instant_disabler, 
             :check_landing_readiness, 
             :update_redis_platforms_and_countries, 
             :update_offer_sets,
             :update_direct_offers, 
             :update_offer_limits

  def system?
    system == true
  end

  def self.default_offer
    offer = find_by( system: true )
    unless offer
      offer = new
      offer.system    = true
      offer.name      = 'system_offer'
      offer.seller    = Seller.system_provider
      offer.countries << Country.find_by_abbr( 'N/A' )
      offer.country_list = 'N/A'
      offer.kind = 'apps_advert'
      offer.description = 'system_offer'
      offer.features_list = [ 'feature' ]
      offer.save
    end
    offer
  end

  def update_redis_platforms_and_countries
    Rediska.offer_updated
  end

  def update_offer_sets
    OfferSetsReloader.perform_async( offer_sets.pluck(:id) )
  end

  def update_direct_offers
    DirectOffersReloader.perform_async( direct_offers.pluck(:id) )
  end

  def update_offer_limits
    offer_limit.update( daily_install_limit: daily_install_limit, cap_time_zone_delta: cap_time_zone_delta )
  end

  def change_limit_status
    self.daily_limit_status = 'available' if daily_install_limit >  0 and daily_limit_status == 'unlimited'
    self.daily_limit_status = 'unlimited' if daily_install_limit == 0 and daily_limit_status != 'unlimited'
  end

  def disable
    update( disabled: true )
  end

  def disable_silent
    update_column( :disabled, true )
    offer_disabler.update( offer_disabled: true, disabling_time: nil )
  end

  def enable
    update( disabled: false, disabling_time: nil )
  end

  def make_available
    update( disabled: false, disabling_time: nil, daily_limit_status: 'available' )
  end

  def make_expire
    update_column( :daily_limit_status, 'expiring' )
  end

  def make_exceed
    update_column( :daily_limit_status, 'exceeded' )
    disable_silent
    update_offer_sets
  end

  def enable_silent
    update_column( :disabled, false )
    offer_disabler.update( offer_disabled: false, disabling_time: nil )
  end

  def heading( opts={} )
    number = opts[ :number ] ? opts[ :number ].to_i : 0
    landing_headings.at( number ).try( :body )
  end

  def enable_sms_form?( opts={} )
    opts[ :sms_form ] == '1'
  end

  def readable_name
    name + ' ( ' + seller.seller_name + ' )'
  end

  def countries_array
    self.countries.pluck( :name ).split( ', ' )
  end

  def check_landing_readiness
    landing_required_attrs = [
      description,
      votes_count,
      downloads_count,
      offer_icon,
      offer_screenshots,
      app_kind,
      features_list,
      landing_name,
      landing_description
    ]
    update_column( :weblanding_ready, landing_required_attrs.all?( &:present? ) )
  end

  def apps_advert?
    kind == 'apps_advert'
  end

  def assign_comments
    if apps_advert? && app_kind.present? && offer_comments.blank?
      offer_comments << OfferComment.send( 'rand_' + self.app_kind.to_s )
    end
    true
  end

  def assign_headings
    user_defined_headings = self.landing_headings.present? ? self.landing_headings : []
    deafult_heading = LandingHeading.default_for( kind: app_kind )
    self.landing_headings = ( [ deafult_heading ] + user_defined_headings )
  end

  def template
    if apps_os == 'ipod+iphone'
      'iphone_offer_landing'
    else
      apps_os + '_offer_landing'
    end
  end

  def android?
    apps_os == 'android'
  end

  def landing_os
    android? ? 'android' : 'ios'
  end

  def sms_domain
    android? ? 'http://play.google-com.me/' : 'http://itunes.apple-com.info/'
  end

  def store_name
    android? ? 'Google Play' : 'AppStore'
  end

  def webland_url_postfix
    landing_name.parameterize if landing_name
  end

  def self.load_collection( params={} )
    eval( sanitize_load_params( params ) )
    .not_hidden.enabled
    .paginate( pagination_params( params ) )
    .order( 'featured DESC, id DESC' )
    .uniq
  end

  def new_offer_disabler
    OfferDisabler.create( offer_id: self.id )
  end

  def new_offer_limit
    OfferLimit.create( offer_id: id,
                       cap_time_zone_delta: cap_time_zone_delta,
                       daily_install_limit: daily_install_limit )
  end

  def clear_disablers
    if disabled and disabling_time.present?
      disabling_time = nil
    end
  end

  def self.disabling_robot
    Offer.enabled.each do |offer|
      offer.disable if !offer.disabled and offer.disabling_time and offer.disabling_time.utc <= ( Time.now.utc + 3.hours )
    end
  end

  def check_instant_disabler
    offer_disabler.plan_disable_notification if disabling_time and offer_disabler.plan_note_sent_time.nil?
    offer_disabler.instant_disable if disabled and offer_disabler.offer_disabled == false
    offer_disabler.enable if !disabled and offer_disabler.offer_disabled == true
  end

  def country_codes
    list = countries.pluck( :abbr ).join( ', ' )
    list.present? ? list : 'All'
  end

  def fetch_dialogs
    if dialog_translations.present?
      dialog_translations
    else
      { default: js_dialogue }
    end
  end

  # only for testing purpose but it should be nice to
  # cache all offers and perform all offer searches only in frontend
  def self.records_for_caching( dealer={}, params={} )
    enabled
  end

  private

    def self.pagination_params( params={} )
      page = params.delete( :page ) || 1
      per_page = params.delete( :per_page ) || 10
      { page: page, per_page: per_page }
    end

    def self.sanitize_load_params( params={} )
      query_keys = %i( id apps_os kind motivated weblanding_ready adult )
      load_params = Hash.new
      query_keys.each do | query_key |
        load_params[ query_key ] = params[ query_key ] if params[ query_key ]
      end
      countries_params_string = if params[ :countries ]
        ".joins( :countries ).where( 'countries.name IN (?)', #{ params[ :countries ] } )"
      else
        ''
      end
      naming_params_string = if params[ :name ]
        ".where( 'offers.name ilike ?', '%#{ params[ :name ].downcase }%' )"
      else
        ''
      end
      "Offer" + countries_params_string + naming_params_string + ".where( #{ load_params } )"
    end

    def set_countries
      values_array = country_list.split(',').map { | attr | attr.strip }.compact
      self.countries = Country.where( 'countries.name IN (?) OR countries.abbr IN (?)', values_array, values_array )
      update_column( :country_list, countries.pluck( :abbr ).join( ', ' ) )
    end

    def set_restricted_countries
      values_array = restricted_country_list.split(',').map { | attr | attr.strip }.compact
      self.restricted_countries = Country.where( 'countries.name IN (?) OR countries.abbr IN (?)', values_array, values_array )
      update_column( :restricted_country_list, restricted_countries.pluck( :abbr ).join( ', ' ) )
    end

    def check_blank_country_list
      if countries.empty? || country_list.blank?
        all_countries = Country.all
        countries = all_countries
        update_attributes( country_list: all_countries.pluck( :name ).join(', ') )
      end
    end

end
