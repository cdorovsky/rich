class NotificationSettings < ActiveRecord::Base
  belongs_to :dealer
  before_save :check_dealer_params

  SALT = 'love and peace'

  def self.load_by_hashid( hashid )
    id = hashids_instance.decode hashid
    find_by( id: id )
  end

  def check_dealer_params
    unless dealer.try(:phone)
      assign_attributes(
        Hash[
          [:ticket_sms, :promo_sms, :offer_sms, :direct_offer_sms].product [false]
        ]
      )
    end
  end

  def to_param
    hashid
  end

  def hashid
    self.class.hashids_instance.encode id
  end

  def self.hashids_instance
    Hashids.new SALT, 8
  end
end
