module Rediska

  class << self

    def load_all
      load_countries
      load_platform
      load_campaigns
      load_offer_sets
    end

    def offer_updated
      SilentRedisReloader.perform_async
    end

    def empty_adekvat_dealers
      $redis.set( 'adekvat_dealers', { ids: [] }.to_json )
    end

    def adekvat_dealers
      JSON.parse( $redis.get('adekvat_dealers') ).with_indifferent_access[ :ids ]
    end

    def old_redirect_codes
      JSON.parse( $redis.get('old_redirect_codes') ).with_indifferent_access[ :codes ]
    end

    def new_adekvat( dealer_id )
      new_adekvat_dealers = adekvat_dealers + [ dealer_id ]
      $redis.set( 'adekvat_dealers', { ids: new_adekvat_dealers }.to_json )
    end

    def load_countries
      Country.all.each do |country|
        $redis.set "country_offers_#{ country.abbr }",        country.offers.map{ |offer| offer.offer_sets.pluck( :id ) }.flatten.to_json
        $redis.set "country_support_offers_#{ country.abbr}", country.offers.support.map{ |offer| offer.offer_sets.pluck( :id ) }.flatten.to_json
      end
    end

    def load_platform
      $redis.set 'ipad_offers',                Offer.ipad.map{ |offer| offer.offer_sets.pluck( :id ) }.flatten.to_json
      $redis.set 'ipad_support_offers',        Offer.ipad.support.map{ |offer| offer.offer_sets.pluck( :id ) }.flatten.to_json
      $redis.set 'ipod_iphone_offers',         Offer.ipod_iphone.map{ |offer| offer.offer_sets.pluck( :id ) }.flatten.to_json
      $redis.set 'ipod_iphone_support_offers', Offer.ipod_iphone.support.map{ |offer| offer.offer_sets.pluck( :id ) }.flatten.to_json
      $redis.set 'android_offers',             Offer.android.map{ |offer| offer.offer_sets.pluck( :id ) }.flatten.to_json
      $redis.set 'android_support_offers',     Offer.android.support.map{ |offer| offer.offer_sets.pluck( :id ) }.flatten.to_json
      $redis.set 'mobile_offers',              Offer.mobile.map{ |offer| offer.offer_sets.pluck( :id ) }.flatten.to_json
      $redis.set 'mobile_support_offers',      Offer.mobile.support.map{ |offer| offer.offer_sets.pluck( :id ) }.flatten.to_json
      $redis.set 'pc_offers',                  Offer.pc.map{ |offer| offer.offer_sets.pluck( :id ) }.flatten.to_json
    end

    def load_campaigns
      Campaign.all.each do | campaign |
        offer_sets = campaign.offer_sets.active.order( 'cost DESC' ).pluck( :id )
        tb_url     = campaign.trafficback_url
        m_tb_url   = campaign.m_trafficback_url
        result = { offers:      offer_sets,
                   hours:       campaign.campaign_limiter.hours,
                   days:        campaign.campaign_limiter.days,
                   show_limits: campaign.campaign_limiter.show_limits,
                   tb_url:      tb_url,
                   m_tb_url:    m_tb_url }.to_json
        $redis.set( "redirect_code_#{ campaign.hash_code }", result )
      end
    end

    def load_offer_sets
      os_params = OfferSet.all.pluck( :enabled,
                                      :external_id,
                                      :dealer_id,
                                      :tb_url,
                                      :m_tb_url,
                                      :offer_id,
                                      :campaign_id,
                                      :seller_name,
                                      :message,
                                      :seller_url,
                                      :preview_url,
                                      :apps_os,
                                      :id )
      os_params.each do | os |
        result = Hash.new
        result[ :enabled ]     = os[ 0 ]
        result[ :external_id ] = os[ 1 ]
        result[ :dealer_id ]   = os[ 2 ]
        result[ :tb_url ]      = os[ 3 ]
        result[ :m_tb_url ]    = os[ 4 ]
        result[ :offer_id ]    = os[ 5 ]
        result[ :campaign_id ] = os[ 6 ]
        result[ :seller_name ] = os[ 7 ]
        result[ :message ]     = os[ 8 ]
        result[ :seller_url ]  = os[ 9 ]
        result[ :preview_url ] = os[ 10 ]
        result[ :apps_os ]     = os[ 11 ]
        $redis.set("offer_set_#{ os[ 12 ] }", result.to_json)
      end
    end

    def load_dev_stuff
      if Rails.env.production?
        p 'YOU DIED!'
      else
        Rediska.load_all
        Rediska.empty_adekvat_dealers
        Dealer.all.each do | dealer_id |
          dashboard = Dashboard.wrap_cached_regions( dealer_id )
          $redis.set( "#{ dealer_id }_dashboard", dashboard.to_json )
          Precacher.replenish_stats_cache_for_dealer( dealer_id )
        end
      end
    end

  end

end
