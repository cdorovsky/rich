class SMS

  @@sms = Smsc::Sms.new( '41114', 'qdycT9FCerrh4pXc' )

  def self.sender
    @@sms
  end

  def self.send_message( options={} )
    parametrize_number options
    if ready_to_send( options )
      @@sms.message( options[ :text ], options[ :numbers ], sender: 'RICHPAYS' )
    else
      Rails.logger.debug "[#{ Time.now }] SMS sent to #{ options[ :numbers ] }, with text \"#{ options[ :text ] }\""
    end
  end

  def self.ready_to_send( options={} )
    Rails.env.production? && options[:blocking_condition] != false
  end

  def self.parametrize_number( options={} )
    numbers = [] << options[ :phone ] if options[ :phone ].try(:acts_like_string?)
    if numbers.try(:any?) # le c'oustyl c'oustyl
      numbers.map! { | number | '+' + number }
      options[ :numbers ] = numbers.compact.uniq
    end
    options
  end

  def self.prepare_text( options = {} )
    options[ :text ] = case options[ :request_type ]
    when 'admin_auth'
      "code: #{ options[ :random_digits ] }\ncountry: #{ options[ :country ] }"
    when 'number_change'
      "Код для смены номера телефона: #{ options[ :random_digits ] }"
    when 'plan_disable'
      "Оффер: #{ options[ :offer ].name } (ID: #{ options[ :offer ].id }) будет отключен: #{ ( options[ :offer ].disabling_time - 3.hours ).strftime( '%F %H:%M' ) }. Учтите информацию."
    when 'instant_disable'
      "Оффер: #{ options[ :offer ].name } (ID: #{ options[ :offer ].id }) отключен. Учтите информацию."
    when 'offer_enable'
      "Оффер: #{ options[ :offer ].name } (ID: #{ options[ :offer ].id }) снова активен. Учтите информацию."
    when 'near_cap_limit'
      "Исчерпано 80% от суточного лимита установок по офферу #{ options[ :offer ].name }. Приостановите трафик до наступления новых суток."
    when 'sidekiq_queue_warning'
      "ВНИМАНИЕ! Размер очереди в sidekiq достиг #{ options[ :sidekiq_queue ] } джобов"
    when 'direct_offers_moderation'
      many = options[ :offer_ids ].many?
      "Оффер#{ 'ы' if many } ID: #{ options[ :offer_ids ].join(', ') } #{ 'не' unless options[ :approved ] } активирован#{ 'ы' if many } для вашего аккаунта. #{ 'Детали на e-mail.' unless @approved }"
    end
    options
  end

end
