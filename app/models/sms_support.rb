class SmsSupport < SMS

  def self.send_answer_notification( ticket={} )
    options = {
      text: 'RichPays: Уважаемый партнер, вам поступило сообщение в тикете.',
      phone: ticket.dealer.phone,
      blocking_condition: ticket.notification_settings.ticket_sms
    }
    send_message( options )
  end

end
