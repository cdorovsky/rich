class SmsOfferNotification < SMS

  def self.send_by_type( options={} )
    options[ :phone ] = options[ :dealer ].phone
    options[ :blocking_condition ] = options[ :dealer ].notification_settings.offer_sms
    case options[ :type ]
    when :plan
      send_plan_offer_disable_nofification( options )
    when :instant
      send_instant_offer_disable_nofification( options )
    when :enable
      send_offer_enable_nofification( options )
    end
  end

  def self.send_plan_offer_disable_nofification( options={} )
    options[ :request_type ] = 'plan_disable'
    prepare_text options
    send_message options
  end

  def self.send_instant_offer_disable_nofification( options={} )
    options[ :request_type ] = 'instant_disable'
    prepare_text options
    send_message options
  end

  def self.send_offer_enable_nofification( options={} )
    options[ :request_type ] = 'offer_enable'
    prepare_text options
    send_message options
  end

  def self.send_offer_limit_notification( options={} )
    options[ :request_type ] = 'near_cap_limit'
    options[ :phone ] = options[ :dealer ].phone
    prepare_text options
    send_message options
  end

end
