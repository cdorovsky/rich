class BeelineIps

  def self.load_to_redis
    ip_list.each do | ip |
      $redis.set ip, 'beeline'
    end
  end

def self.ip_list
  [
    ( 0..255 ).to_a.map {|fourth_octet| "213.252.195.#{fourth_octet}"},
    ( 128..255 ).to_a.map {|third_octet| ( 0..255 ).to_a.map {|fourth_octet| "217.118.#{third_octet}.#{fourth_octet}"}},
    ( 32..63 ).to_a.map {|fourth_octet| "217.118.64.#{fourth_octet}"},
    ( 0..255 ).to_a.map {|fourth_octet| "217.118.93.#{fourth_octet}"},
    ( 0..255 ).to_a.map {|fourth_octet| "217.118.66.#{fourth_octet}"},
    ( 0..255 ).to_a.map {|fourth_octet| "217.118.78.#{fourth_octet}"},
    ( 0..255 ).to_a.map {|fourth_octet| "217.118.79.#{fourth_octet}"},
    ( 80..85 ).to_a.map {|third_octet| ( 0..255 ).to_a.map {|fourth_octet| "217.118.#{third_octet}.#{fourth_octet}"}},
    ( 89..94 ).to_a.map {|third_octet| ( 0..255 ).to_a.map {|fourth_octet| "217.118.#{third_octet}.#{fourth_octet}"}},
    ( 0..127 ).to_a.map {|fourth_octet| "217.118.95.#{fourth_octet}"},
    ( 96..127 ).to_a.map {|third_octet| ( 0..255 ).to_a.map {|fourth_octet| "217.118.#{third_octet}.#{fourth_octet}"}},
    ( 0..94 ).to_a.map {|third_octet| ( 0..255 ).to_a.map {|fourth_octet| "217.119.#{third_octet}.#{fourth_octet}"}},
    ( 0..192 ).to_a.map {|fourth_octet| "217.119.95.#{fourth_octet}"},
    ( 129..142 ).to_a.map {|fourth_octet| "217.148.194.#{fourth_octet}"},
    ( 2..62 ).to_a.map {|fourth_octet| "31.13.144.#{fourth_octet}"},
    ( 96..103 ).to_a.map {|third_octet| ( 0..255 ).to_a.map {|fourth_octet| "46.16.#{third_octet}.#{fourth_octet}"}},
    ( 2..62 ).to_a.map {|fourth_octet| "62.33.151.#{fourth_octet}"},
    ( 0..255 ).to_a.map {|fourth_octet| "83.220.227.#{fourth_octet}"},
    ( 1..255 ).to_a.map {|fourth_octet| "83.220.236.#{fourth_octet}"},
    ( 0..255 ).to_a.map {|fourth_octet| "83.220.237.#{fourth_octet}"},
    ( 0..255 ).to_a.map {|fourth_octet| "83.220.238.#{fourth_octet}"},
    ( 0..254 ).to_a.map {|fourth_octet| "83.220.239.#{fourth_octet}"},
    ( 1..254 ).to_a.map {|fourth_octet| "85.115.224.#{fourth_octet}"},
    ( 1..255 ).to_a.map {|fourth_octet| "85.115.234.#{fourth_octet}"},
    ( 0..254 ).to_a.map {|fourth_octet| "85.115.235.#{fourth_octet}"},
    ( 32..62 ).to_a.map {|fourth_octet| "85.115.243.#{fourth_octet}"},
    ( 0..255 ).to_a.map {|fourth_octet| "85.115.248.#{fourth_octet}"},
    ( 136..143 ).to_a.map {|fourth_octet| "89.188.224.#{fourth_octet}"}
  ].flatten
end

end
