class TicketMessage < ActiveRecord::Base

  belongs_to :dealer
  belongs_to :ticket

  default_scope { order( 'number DESC') }
  # я помню, что говорил Кирилл о дефолтном скопе,
  # но в данном случае - это наиболее удобный вариант.
  # Шипитько.

  def as_json( options={} )
    {
      message_text: self.message_text,
      ownership: self.ownership,
      number: self.number,
      created_at: self.created_at.strftime( '%d.%m.%Y %H:%M' )
    }
  end

end
