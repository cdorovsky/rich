class MongoEventAtomic
  include Mongoid::Document
  include Mongoid::Timestamps

  field :dealer_id,       type: Integer
  field :offer_id,        type: Integer
  field :offer_set_id,    type: Integer
  field :direct_offer_id, type: Integer
  field :campaign_id,     type: Integer
  field :event_name,      type: String
  field :counter,         type: Integer
  field :extras,          type: Hash

  belongs_to :mongo_event_container

  class << self

    def visitor( container, params = {} )
      extras = extras_for( :visitor, params )
      create( event_name:  'visitor', mongo_event_container: container, extras: extras,
              counter:     1,
              dealer_id:   params[ :dealer_id ],
              campaign_id: params[ :campaign_id ] )
    end

    def offer_set( container, params = {} )
      counter = params[ :web_params ][ :no_count ] ? 0 : 1
      extras = extras_for( :offer_set, params )
      m = create( event_name:   'offer_set', mongo_event_container: container, extras: extras,
                  counter:      counter,
                  dealer_id:    params[ :redis ][ :dealer_id ],
                  campaign_id:  params[ :redis ][ :campaign_id ],
                  offer_set_id: params[ :web_params][ :id ],
                  offer_id:     params[ :redis ][ :offer_id ] )
      m.id.to_s
    end

    def direct_conversion( container, direct_offer_redirect, params = {} )
      direct_offer_redirect.set_converted
      params[ :direct_offer_redirect ] = direct_offer_redirect
      extras = extras_for( :direct_conversion, params )
      create( event_name: 'conversion', mongo_event_container: container, extras: extras,
              counter: 1,
              dealer_id:       direct_offer_redirect.dealer_id,
              direct_offer_id: DirectOffer.find_by( hashid: direct_offer_redirect.from ).id,
              offer_id:        direct_offer_redirect.offer_id )
    end

    def conversion( container, offer_set_show, params = {} )
      offer_set_show.set_converted
      params[ :offer_set_show ] = offer_set_show
      extras = extras_for( :conversion, params )
      create( event_name: 'conversion', mongo_event_container: container, extras: extras,
              counter: 1,
              dealer_id:    offer_set_show.dealer_id,
              campaign_id:  offer_set_show.campaign_id,
              offer_set_id: offer_set_show.offer_set_id,
              offer_id:     offer_set_show.offer_id )
    end

    def traffic_back( container, params )
      extras = extras_for( :trafback, params )
      create( event_name:   'traffic_back', mongo_event_container: container, extras: extras,
              dealer_id:    params[ :dealer_id ],
              campaign_id:  params[ :campaign_id ],
              offer_id:     params[ :offer_id ],
              offer_set_id: params[ :offer_set_id ],
              counter:      1 )
    end

    def clicker( container, params )
      extras = extras_for( :clicker, params )
      create( event_name:  'clicker', mongo_event_container: container, extras: extras,
              dealer_id:    params[ :dealer_id ],
              campaign_id:  params[ :campaign_id ],
              offer_set_id: params[ :id ],
              offer_id:     params[ :offer_id ],
              counter:      1 )
    end

    def js_push( container, params )
      extras = extras_for( :js_push, params )
      create( event_name:  'js_push', mongo_event_container: container, extras: extras,
              dealer_id:    params[ :dealer_id ],
              campaign_id:  params[ :campaign_id ],
              offer_set_id: params[ :id ],
              offer_id:     params[ :offer_id ],
              counter:      1 )
    end

    def extras_for( fact, params = {} )
      case fact
      when :trafback
        { referer: params[ :referer ], reason: params[ :reason ] }
      when :clicker
        { uniq: params[ :uniq ] }
      when :js_push
        { uniq: params[ :uniq ] }
      when :visitor
        { sub_id: params[ :sub_id ], sub_id2: params[ :sub_id2 ], sub_id3: params[ :sub_id3 ] }
      when :offer_set
        { referer:       params[ :request ][ :referer ],
          ip:            params[ :request ][ :ip ],
          user_agent:    params[ :request ][ :user_agent ],
          user_platform: params[ :request ][ :user_platform ],
          converted:  false,
          uniq:    params[ :web_params ][ :uniq ],
          sub_id:  params[ :web_params ][ :sub_id ],
          sub_id2: params[ :web_params ][ :sub_id2 ],
          sub_id3: params[ :web_params ][ :sub_id3 ] }
      when :conversion
        web_params = { mongo_id:                params[ :mongo_id ],
                       external_offer_id:       params[ :external_offer_id ],
                       external_transaction_id: params[ :transaction_id ],
                       external_revenue:        params[ :external_revenue ] }
        offer_set_show_params = OfferSetShow.params_for_conversion( params[ :offer_set_show ] )
        web_params.merge offer_set_show_params
      when :direct_conversion
        web_params = { mongo_id: params[ :mongo_id ] }
        offer_set_show_params = DirectOfferRedirect.params_for_conversion( params[ :direct_offer_redirect ] )
        web_params.merge offer_set_show_params
      end
    end

  end
  def converted
    ( event_name == 'offer_set' ) && ( self.extras[ 'converted' ] == true )
  end
    
  def set_converted
    extras[ 'converted' ] = true
    save!
  end


  def for_offer_set_show
    begin
      ip = IPAddr.new( extras[ :ip ] )
    rescue IPAddr::InvalidAddressError
      ip = ''
    end
    country = Country.find_by( abbr: RequestInformation.find_country_by_ip( extras[ :ip ] ) )
    {
      mongo_id: id.to_s,
      referer: extras[ :referer ],
      sub_id:  extras[ :sub_id ],
      sub_id2: extras[ :sub_id2 ],
      sub_id3: extras[ :sub_id3 ],
      dealer_id:    dealer_id,
      campaign_id:  campaign_id,
      offer_id:     offer_id,
      offer_set_id: offer_set_id,
      country:      country,
      country_name: country.try( :name ) || '',
      country_code: country.try( :abbr ) || '',
      ip:            ip,
      converted:     extras[ :converted ],
      user_agent:    extras[ :user_agent ],
      user_platform: extras[ :user_platform ],
      triggered_at:  created_at
    }
  end

  def for_conversion( campaign, offer, dealer )
    country = Country.find_by( abbr: RequestInformation.find_country_by_ip( extras[ :ip ] ) )
    campaign_name = campaign ? campaign.name : nil
    { mongo_id:       extras[ :mongo_id ],
      offer_id:       offer_id,
      campaign_id:    campaign_id,
      dealer_id:      dealer_id,
      offer_name:     offer.name,
      dealer_revenue: extras[ :revenue ],
      campaign_name:  campaign_name,
      country:        country,
      country_name:   country.try( :name ) || '',
      country_code:   country.try( :abbr ) || '',
      kind:           offer.kind,
      conversion_reason: :install,
      crypted_id:     Conversion.generate_uniq_crypted_id,
      sub_id:         extras[ :sub_id ],
      sub_id2:        extras[ :sub_id2 ],
      sub_id3:        extras[ :sub_id3 ],
      triggered_at:   created_at,
      triggered_at_timestamp: created_at.to_i }
  end

  def for_postback( offer, campaign )
    country = Country.find_by( abbr: RequestInformation.find_country_by_ip( extras[ :ip ] ) )
    direct_offer = DirectOffer.find( direct_offer_id ) if direct_offer_id
    postback_url     = campaign_id ? campaign.postback_url : direct_offer.postback_url
    postback_enabled = campaign_id ? campaign.sent_postback : direct_offer.postback
    request_kind     = campaign_id ? campaign.request_kind : direct_offer.postback_action
    { mongo_id:         extras[ :mongo_id ],
      dealer_id:        dealer_id,
      campaign_id:      campaign_id,
      datetime:         created_at,
      offer_id:         offer_id,
      offer_name:       offer.name,
      conversion_type:  :install,
      country:          country,
      country_name:     country.try( :name ) || '',
      country_code:     country.try( :abbr ) || '',
      profit_usd:       extras[ :revenue ],
      postback_url:     postback_url,
      postback_enabled: postback_enabled,
      request_kind:     request_kind,
      sub_id:           extras[ :sub_id ],
      sub_id2:          extras[ :sub_id2 ],
      sub_id3:          extras[ :sub_id3 ] }
  end

  def for_dealer_increment( dealer )
    reason = offer_set_id ? OfferSetShow.find_by( mongo_id: extras[ :mongo_id ] ) : DirectOfferRedirect.find( extras[ :direct_offer_redirect_id ] )
    { dealer_balance: dealer.dealer_balance,
      dealer_id:      dealer_id,
      offer_id:       offer_id,
      campaign_id:    campaign_id,
      paid:           false,
      confirmed:      extras[ :revenue ],
      reason:         reason }
  end

end
