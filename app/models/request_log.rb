class RequestLog < ActiveRecord::Base

  belongs_to :dealer
  belongs_to :campaign
  belongs_to :offer
  scope :redirect,    -> { where( request_type: 'redirect' ) }
  scope :subscribe,   -> { where( request_type: 'subscribe_wapclick' ) }
  scope :unsubscribe, -> { where( request_type: 'ubsubscribe_wapclick' ) }
  scope :revenue,     -> { where( request_type: 'revenue' ) }

  def self.trigger( options = {} )
  #  RequestLog.create( dealer_id:      @dealer.id,
  #                     offer_id:       @offer.id,
  #                     campaign_id:    @campaign.id,
  #                     transaction_id: @sms_id,
  #                     datetime:       @date,
  #                     currency:       @currency,
  #                     country:        'Russian Federation',
  #                     dealer_revenue: payout,
  #                     tariff:         @tariff,
  #                     seller_name:    'onlypay',
  #                     request_type:   request_type )
  end
end
