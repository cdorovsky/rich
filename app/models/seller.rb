class Seller < ActiveRecord::Base

  has_many :offers
  has_many :offer_sets
  has_many :campaigns
  has_one :seller_balance
  has_many :seller_increments

  before_save  :set_provider_system_name
  after_create :new_balance

  validates_presence_of :seller_name

  scope :active,       -> { where( active: true ) }
  scope :apps_advert,  -> { where( kind: [ 'apps_advert', 'cpa' ] ) }
  scope :cpa,          -> { where( kind: 'apps_advert' ) }
  scope :wapclick,     -> { where( kind: 'wapclick' ) }

  def self.system_provider
    seller = find_by( kind: 'system' )
    unless seller
      seller = new
      seller.kind                 = 'system'
      seller.seller_name          = 'system_seller' 
      seller.provider_system_name = 'system'
      seller.prefix               = 'system'
      active                      = false
      seller.save
    end
    seller
  end

  def new_balance
    SellerBalance.create( seller_id: self.id )
  end

  def increase_balance( value, type = nil )
    type ||= "confirmed"
    seller_balance.increase( value, type )
    SellerIncrement.increase( self, value, type )
  end

  def decrease_balance( value, type = nil )
    type ||= "confirmed"
    seller_balance.decrease( value, type )
    SellerIncrement.decrease( self, value, type )
  end

  def recent_stats( options={} )
    now = Time.zone.now
    day = options[ :per ] ? Time.parse( options[ :per ] ) : now
    day_range = day.beginning_of_day..day.end_of_day
    seller_increments.where( created_at: day_range ).order( :created_at )
  end

  def set_provider_system_name
    self.provider_system_name = seller_name.downcase
  end

end
