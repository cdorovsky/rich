class LandingSubscriber < ActiveRecord::Base

  belongs_to :country
  belongs_to :offer_set
  has_many   :landing_visitors, dependent: :destroy
  has_one    :campaign,         through: :offer_set
  has_one    :offer,            through: :offer_set
  has_one    :dealer,           through: :campaign

  before_create :set_country

  def self.create_and_notify( landing_params={} )
    @offer_set = RedirectCode.find_by( code: landing_params[ :code ] ).offer_set
    if @offer_set
      @subscriber = init_subscriber landing_params
      link = @offer_set.sms_link
      landing_name = @offer_set.offer.landing_name || ''
      SmsLanding.send_app_link( @subscriber, landing_name,link )
    end
  end

  def self.init_subscriber( landing_params={} )
    @subscriber = LandingSubscriber.find_or_create_by( phone: landing_params[ :phone ] )
    @subscriber.landing_visitors << LandingVisitor.create( offer_set_id: @offer_set.id )
    @subscriber
  end

  def subscribe!
    update( active:  true )
  end

  def unsubscribe!
    update( active: false )
  end

  def set_country
  end

end
