class DailyAggregate < ActiveRecord::Base

  include Pagination

  belongs_to :dealer
  belongs_to :campaign
  belongs_to :offer

  scope :daily_scope, lambda { | date | where( :short_date => date.strftime( '%F' ) ) }
  scope :actual,           -> { joins( :campaign ).where( campaigns: { archived: false } ) }
  scope :today_aggregates, -> { where( short_date: DailyAggregate.current_short_date ) }
  scope :last_day,         -> { where( created_at: 1.day.ago..Time.zone.now ) }
  scope :ten_days,         -> { where( created_at: 10.days.ago..Time.zone.now ) }
  scope :fifty_days,       -> { where( created_at: 50.days.ago..Time.zone.now ) }
  scope :email_acceptable, -> { where( 'daily_visitor_count >= ?', 10 ) }
  scope :sms_acceptable,   -> { where( 'daily_revenue >= ?', 50 ) }

  def self.trigger( fact, context = {} )
    context[ :short_date ] = current_short_date
    context[ :dealer ] ||= context[ :campaign ].dealer
    search = context.except( :amount )
  	current_aggregate = DailyAggregate.find_or_create_by( search )
    if fact == :app_install
      offer_revenue_sum = context[ :dealer ].dealer_revenue( context[ :offer ] ) * context[ :amount ]
      seller_revenue_sum = context[ :offer ].tariff * context[ :amount ]
      current_aggregate.increment( :daily_install_count, context[ :amount ] )
      current_aggregate.increment( :daily_revenue, offer_revenue_sum )
      current_aggregate.increment( :seller_revenue, seller_revenue_sum )
    else
      current_aggregate.increment( :daily_visitor_count, context[ :amount ] )
    end
    current_aggregate.set_cr!
    current_aggregate.save!
  end

  def set_cr!
    visitors = daily_visitor_count.zero? ? 1 : daily_visitor_count
    installs = daily_install_count
    self.conversion_rate = ( 100.0 * installs / visitors ).round(2)
  end

  def self.current_short_date
    Time.zone.now.strftime( '%F' )
  end

  def self.email_acceptable_dealers
    last_day.email_acceptable.actual.pluck( :dealer_id ).uniq
  end

  def self.sms_acceptable_dealers
    last_day.sms_acceptable.pluck( :dealer_id ).uniq
  end

  def self.fifty_days_clicks_acceptable?( dealer, offer )
    fifty_days.where( dealer: dealer, offer: offer ).sum( :daily_visitor_count ) >= 10
  end

end