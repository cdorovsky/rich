class Stats
  attr_accessor :values

  def initialize( dealer, range, offer_type, about )
    @values = by_dealer( dealer )
    limit_by_range
  end


  def by_dealer( dealer )
    DealerStat.where( dealer_id: dealer )
  end

  def limit_by_range( range )
    @values.where( created_at: range )
  end

end
