class SellerIncrement < ActiveRecord::Base
  
  belongs_to :seller

  def self.check_new
    if !check_not_nil or check_time
      SellerIncrement.create( hour: Time.zone.now.hour, seller_id: @seller.id )
    end
    @increment = @seller.seller_increments.last
  end

  def self.check_not_nil
    @seller.seller_increments.any?
  end

  def self.check_time
    last = @seller.seller_increments.last.created_at  
    current_time = Time.zone.now.strftime("%Y%m%d%H") 
    last_time = last.strftime("%Y%m%d%H")
    current_time != last_time
  end

  def self.increase( seller, value, type )
    @seller = Seller.find( seller )
    check_new 
    current_value = @increment.send( "#{ type }" ) || 0
    new_value = current_value + value
    @increment.update_attributes( type => new_value )
  end

  def self.decrease( seller, value, type )
    @seller = Seller.find( seller )
    check_new 
    current_value = @increment.send( "#{ type }" ) || 0
    new_value = current_value - value
    @increment.update_attributes( type => new_value )
  end

end
