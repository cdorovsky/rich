class CampaignLimiter < ActiveRecord::Base

  belongs_to :campaign
  has_many :restrict_country_sets
  has_many :countries, through: :restrict_country_sets
  validates_presence_of :campaign_id

  # week: day 0 is Sunday

  def self.day_check( wday, day_limit )
    current_wday_index = 'd' + wday.to_s
    abstract_days_hash( day_limit ).map{ |index, value| index if value }.include?( current_wday_index )
  end

  def self.hour_check( hour, hour_limit )
    current_hour_index = 'h' + hour.to_s
    abstract_hours_hash( hour_limit ).map{ |index, value| index if value }.include?( current_hour_index )
  end

  def self.abstract_days_hash( days )
    hash = Hash.new
    ( "%07b" % days).split('').each_with_index.each do | day, index |
      index == 6 ? index = 0 : index = ( index + 1 )
      hash[ "d#{ index }" ] = day.to_i.zero? ? false : true
    end
    hash
  end

  def self.abstract_hours_hash( hours )
    hash = Hash.new
    ( "%024b" % hours ).split('').each_with_index.each do | hour, index |
      hash[ "h#{ index }" ] = hour.to_i.zero? ? false : true
    end
    hash
  end

  def days_hash
    CampaignLimiter.abstract_days_hash( days )
  end

  def hours_hash
    CampaignLimiter.abstract_hours_hash( hours )
  end

  def serialized_json
    as_json( only: [ :id, :campaign_id, :show_limits ],
             methods: [ :hours_hash, :days_hash ]
    )
  end
end