class BuyerSession

  include OffersFilter

  attr_accessor :redis_offers, :country, :platform, :country_platform_offers, :offers_to_show, :show_timeout, :m_tb_url, :tb_url, :tb_reason, :hours, :days, :show_limits

  def initialize( request, params = {} )
    redis_record  = redis_record( params[ :id ] )
    @redis_offers = redis_record[ :offers ]
    @m_tb_url     = redis_record[ :m_tb_url ]
    @tb_url       = redis_record[ :tb_url ]
    @hours        = redis_record[ :hours ]
    @days         = redis_record[ :days ]
    @show_limits  = redis_record[ :show_limits ]
    request_info  = RequestInformation.new( request )
    @country      = request_info.country
    @platform     = request_info.user_platform
    @country_platform_offers = OffersFilter.possible_offers( @platform, @country )
    @offers_to_show = OffersFilter.offers_to_show( @redis_offers, @country_platform_offers )
    @tb_reason = @offers_to_show.empty? ? OffersFilter.tb_reason( @platform, @country, @redis_offers ) : ''
    @show_timeout = 2
    MongoEventContainer.buyer_session( params )
  end

  def redis_record( redirect_code )
    JSON.parse( $redis.get "redirect_code_#{ redirect_code }").with_indifferent_access
  end

  def js_limits
    limiters    = Campaign.find( campaign_id ).campaign_limiter
    show_limit  = limiters.show_limits
    day_limit   = limiters.days
    hour_limit  = limiters.hours
    js_counters = Hash.new
    js_counters[ campaign_id.to_s ] = { counter: 0,
                                        show_limit: show_limit,
                                        day_limit: day_limit,
                                        hour_limit: hour_limit }
    js_counters
  end

  def self.process_offer( params = {} )
    id = params[ :offer_set_id ]
    @offer = $redis.get("offer_set_#{ id }")
  end
end
