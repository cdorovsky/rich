class DealerIncrement < ActiveRecord::Base

  belongs_to :dealer
  belongs_to :dealer_balance
  belongs_to :offer
  belongs_to :campaign
  belongs_to :ref_dealer, primary_key: :referrer_id, class_name: 'Dealer'
  belongs_to :reason, polymorphic: true


  scope :prev_month,          -> { where( created_at: 1.month.ago.beginning_of_month..1.month.ago.end_of_month ) }
  scope :current_month,       -> { where( created_at: Time.zone.now.beginning_of_month..Time.zone.now          ) }
  scope :yesterday,           -> { where( created_at: 1.day.ago.beginning_of_day..1.day.ago.end_of_day         ) }
  scope :yesterday_utc,       -> { where( created_at: 1.day.ago.utc.beginning_of_day..1.day.ago.utc.end_of_day ) }
  scope :today,               -> { where( created_at: Time.zone.today..Time.zone.now                           ) }
  scope :last_month,          -> { where( created_at: 1.month.ago..Time.zone.now                               ) }
  scope :last_hour,           -> { where( created_at: 1.hour.ago.beginning_of_hour..Time.zone.now              ) }
  scope :last_hour_utc,       -> { where( created_at: 1.hour.ago.utc.beginning_of_hour..Time.now.utc           ) }
  scope :day_week_ago,        -> { where( created_at: 1.week.ago.beginning_of_day..1.week.ago.end_of_day       ) }
  scope :only_seller_request, -> { where( reason_type: 'SellerRequest'                                         ) }
  scope :referrer,            -> { where( reason_type: 'DealerIncrement', referrer: true                       ) }
  scope :offer_set_show,      -> { where( reason_type: 'OfferSetShow'                                          ) }
  scope :from_offers,         -> { where( reason_type: ['OfferSetShow', 'DirectOfferRedirect']                 ) }

  def self.weekly_increase_for_checkout
    if Time.zone.now.wday == 1
      ActiveRecord::Base.transaction do
        DealerBalance.all.each do |db|
          if db.confirmed > 0 && db.confirmed != db.for_checkout
            di_params = {
              dealer_id: db.dealer_id,
              created_at: 1.week.ago.beginning_of_week..1.week.ago.end_of_week,
              reason_type: [ 'OfferSetShow', 'AdminRequest', 'DealerIncrement', 'DirectOfferRedirect' ]
            }
            di_sum =  DealerIncrement.where( di_params ).sum( :confirmed )
            db.increment!( :for_checkout, di_sum )
          end
        end
      end
    end
  end

  def self.last_week_increments
    last_week_monday = ( Time.now.wday.days.ago - 6.days ).beginning_of_day
    last_week_sunday = ( Time.now.wday.days.ago ).end_of_day
    where( created_at: last_week_monday..last_week_sunday )
  end

  def as_json( options={} )
    json = super( { only: [ :created_at,
      :waiting, :confirmed,
      :onlypay_redirect,
      :onlypay_subscribe,
      :onlypay_rebill,
      :onlypay_unsubcribe
      ] } )
    json[:created_at] = self.created_at.strftime("%d.%m %H:00")
    json
  end

  def self.commit_to_balance( dealer_id, sum_confirmed )
    dealer_balance = DealerBalance.find_by( dealer_id: dealer_id )
    dealer_balance.confirmed += sum_confirmed
    dealer_balance.save
  end

  def self.recent_dealer_ids
    where( paid: false, created_at: 25.minutes.ago..Time.zone.now ).pluck( :dealer_id ).uniq
  end

  def self.pay_to_balance( dealer_id )
    recent_increments_for_dealer = where( dealer_id: dealer_id, paid: false )
    sum_money = recent_increments_for_dealer.sum( :confirmed )
    dealer = Dealer.find( dealer_id )
    commit_to_balance( dealer_id, sum_money )
    if dealer.referrer_id
      referrer = dealer.referrer
      referrer_revenue = sum_money * referrer.referrer_rate
      commit_to_balance( dealer.referrer_id, referrer_revenue )
      incr_objects = []
      recent_increments_for_dealer.from_offers.each do | incr |
        incr_objects << new(
          dealer_id: referrer.id,
          dealer_balance_id: referrer.dealer_balance.id,
          reason_type: 'DealerIncrement',
          reason_id: incr.id,
          confirmed: ( incr.confirmed * referrer.referrer_rate ),
          referrer_id: incr.dealer_id,
          referrer: true,
          paid: true
        )
      end
      import incr_objects
    end
    recent_increments_for_dealer.update_all( paid: true )
  end

  def self.cached_for( dealer_id )
    total_money = DealerStat.dashboard_total_money( dealer_id )
    dealer_balance = DealerBalance.find_by( dealer_id: dealer_id )
    total_money[ "available" ] = ( dealer_balance.for_checkout - dealer_balance.requested )
    total_money[ "requested" ] = dealer_balance.requested
    total_money[ "for_checkout" ] = dealer_balance.for_checkout
    total_money
  end

end
