class OfferTarget < ActiveRecord::Base
  belongs_to :offer
  validates_presence_of :name
end
