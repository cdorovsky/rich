class HttpRedirectRequest < BuyerRequest

  BUYER_REQUEST_SALT = 'god love peace'

  attr_accessor :redirect_url, :message, :img_url, :redis_offer, :uniq_id, :request_info, :offer_set_id, :locale, :target

  def initialize( request_info, params={} )
    @redis_offer = offer_from_redis( params[ :id ] )
    offer_available = @redis_offer ? ( @redis_offer[ :enabled ] and @redis_offer[ :approved ] ) : false
    if offer_available
      @uniq_id = MongoEventContainer.buyer_request( request: request_info, redis: @redis_offer, web_params: params )
      @redirect_url = generate_redirect_url( @uniq_id, @redis_offer )
      Clicker.new( params )
    else
      @uniq_id = BSON::ObjectId.new.to_s
      @redirect_url = ( params[ :subdomain ] == 'target' ) ? 'http://www.google.com' : 'about:blank'
    end
  end

  def offer_from_redis( id )
    raw_redis = $redis.get( "direct_offer_#{ id }" )
    raw_redis ? JSON.parse( raw_redis ).with_indifferent_access : raw_redis
  end

  def generate_redirect_url( id, params = {} )
    provider_url_generator = 'Providers::AppsAdvert::' + params[ :seller_name ] + '::Sender.redirect( id, params )'
    target_url = eval( provider_url_generator )
    target_url
  end

end
