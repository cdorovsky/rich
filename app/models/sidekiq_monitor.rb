class SidekiqMonitor
  require 'sidekiq/api'

  def self.check_resque_queue
    ( Sidekiq::Queue.new.size >= 100 ) ? warning_sender : false
  end

  def self.warning_sender
    options = SMS.prepare_text( request_type: 'sidekiq_queue_warning', sidekiq_queue: Sidekiq::Queue.new.size )
    developer_phones = Dealer.where( email: [ "shipitko.work@gmail.com" ] ).pluck( :phone )
    developer_phones.each do |phone|
      options[ :phone ] = phone
      SMS.send_message( options ) unless Rails.env.test?
    end
    true
  end

end
