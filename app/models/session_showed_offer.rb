class SessionShowedOffer < ActiveRecord::Base

  belongs_to :buyer_session
  belongs_to :campaign
  belongs_to :dealer
  belongs_to :offer

end
