class SmsAuth < SMS

  def self.send_admin_auth_code( options = {} )
    options[ :request_type ] = 'admin_auth'
    build_and_send_message options
  end

  def self.send_verification_for_phone_update( options = {} )
    options[ :request_type ] = 'number_change'
    options[ :random_digits ] = random_digits
    build_and_send_message options
  end

  def self.build_and_send_message( options={} )
    prepare_text( options )
    send_message( options ) unless Rails.env.development?
    options[ :random_digits ]
  end

  def self.random_digits
    unless Rails.env.development?
      rand.to_s[ 2 ... 6 ] 
    else
      '1111'
    end
  end

end
