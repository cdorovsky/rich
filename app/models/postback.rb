class Postback < ActiveRecord::Base

  require 'rest_client'
  require 'rack/utils'

	belongs_to :campaign
	belongs_to :offer
	belongs_to :dealer
  belongs_to :conversion
  belongs_to :country
  has_one    :seller_request

  scope :to_send, -> { where( postback_enabled: true, sended: false ) }

  SYSTEM_POSTBACK_PARAMS = %w( datetime conversion_code conversion_type campaign_id offer_id offer_name country_code profit_usd sub_id sub_id2 sub_id3 )

  def self.trigger( seller_request )
    Postback.init_and_send_postback( seller_request ) if seller_request.campaign.ready_to_send_postback?
  end

  def self.init_and_send_postback( seller_request )
    dealer          = seller_request.dealer
    campaign        = seller_request.campaign
    offer           = seller_request.offer
    conversion_type = seller_request.reason
    datetime        = seller_request.datetime
    conversion      = seller_request.conversion
    conversion_code = conversion.crypted_id
    offer_name      = offer.name
    country         = seller_request.country
    country_name    = country.name
    country_code    = country.abbr
    profit_usd      = seller_request.offer_dealer_revenue.round( 2 )
    postback_url    = postback_url
    sub_id          = seller_request.sub_id
    sub_id2         = seller_request.sub_id2
    sub_id3         = seller_request.sub_id3

    params = { dealer:          dealer,
               campaign:        campaign,
               offer:           offer,
               conversion_type: conversion_type,
               datetime:        datetime,
               conversion:      conversion,
               conversion_code: conversion_code,
               offer_name:      offer_name,
               country:         country,
               country_name:    country_name,
               country_code:    country_code,
               profit_usd:      profit_usd,
               postback_url:    postback_url,
               sub_id:          sub_id,
               sub_id2:         sub_id2,
               sub_id3:         sub_id3,
               sended_at:       Time.zone.now
    }
    postback = Postback.create( params )
    seller_request.update_attribute( :postback_id, postback.id )
    postback.send_notification
    postback
  end

  def send_notification
    target_url         = postback_target_url
    custom_params_list = postback_params_list
    notification_params = build_notification_params( custom_params_list )
    begin
        request_gun( request_kind, target_url, notification_params )
    rescue => e
        logger.warn "Unable to send postback, will ignore: #{e}"
    end
  end

  def ready_to_send_postback?
    postback_enabled and !postback_url.blank?
  end

  def postback_target_url
    postback_url.split( '?' )[ 0 ]
  end

  def postback_params_list
    postback_url_params = postback_url.split( '?' )[ 1 ]
  end

  def build_notification_params( custom_params_list )
    notification_params = {}
    SYSTEM_POSTBACK_PARAMS.each do | param |
      custom_params_list = custom_params_list.gsub( "{#{ param }}", eval( param ).to_s )
    end
    notification_params = Rack::Utils.parse_nested_query( custom_params_list )
    notification_params
  end

  def request_gun( request_method, target_url, notification_params )
    if request_method.downcase == 'get'
      response = RestClient.get target_url + '?' + notification_params.to_query
    else
      response = RestClient.post target_url, notification_params.to_json, :content_type => :json, :accept => :json
    end
  end

  def self.send_to_partners( dealer_id )
    AmqpRegular.new( 'postback_sender', "send_postbacks_for_dealer_id_#{ dealer_id }" )
  end

  def find_and_relate_conversion!
    conv = Conversion.find_by( mongo_id: mongo_id )
    self.conversion_id = conv.id
    self.conversion_code = conv.crypted_id
    save!
  end

  def self.support_sender
    PostbackWorker.perform_async
  end

end
