class SidebarNotifications < ActiveRecord::Base

  belongs_to :dealer

  def change_by( counter_type, value )
    update_column( counter_type, send( counter_type ) + value )
  end

end
