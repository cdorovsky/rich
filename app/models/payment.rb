class Payment < ActiveRecord::Base

  require 'csv'

  has_many :payment_requests

  scope :regular, -> { where( payment_type: 'regular' ) }
  scope :request, -> { where( payment_type: 'request' ) }
  scope :unpaid, -> { where( paid: false ) }

  def pay!
    update_column( :paid, true )
  end

  def self.create_with_requests( request_ids )
    requests = PaymentRequest.joins( :wallet ).where( id: request_ids )
    if requests.payment_ready?( request_ids )
      wallets = Wallet.where( id: requests.pluck( :wallet_id ) )
      payment = self.create( wallet_type: wallets.kind, payment_type: requests.payment_type )
      requests.update_all( status: 'pending', payment_id: payment.id )
      payment
    end
  end

  def process_payout( opts={} )
    action = sanitize_update_action( opts[ :payment_action ] )
    if action
      requests = payment_requests.pending.where( id: opts[ :request_ids ] )
      requests.each { | req | req.send( action ) }
      pay! unless payment_requests.pluck( :status ).any?{ | s | s == 'pending' }
      save
    end
  end

  def sanitize_update_action( action )
    action if ( %w( cancel complete ).include?( action ) ) 
  end

  def rollback
    rollback_list = PaymentRequest.where( payment_id: id )
    rollback_list.each { |last_payment| last_payment.rollback_single }
  end

  def as_csv( request_ids, opts={} )
    requests = payment_requests.pending.where( id: request_ids )
    CSV.generate( opts ) do | csv |
      requests.each do | request |
        csv << request.to_csv_row
      end
    end
    .gsub(/"""/, '"')
  end

end
