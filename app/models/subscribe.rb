class Subscribe < ActiveRecord::Base

  belongs_to :dealer
  belongs_to :campaign
  belongs_to :offer
  has_many   :rebills
  has_many   :seller_request

  scope :not_active,   -> { where( active: false ) }
  scope :active,       -> { where( active: true ) }

  scope :onlypay,      -> { where( billing: 'onlypay' ) }
  scope :billing8,     -> { where( billing: 'billing8' ) }
  scope :shemrock,     -> { where( billing: 'shemrock' ) }
  scope :plasticmedia, -> { where( billing: 'plasticmedia' ) }

  scope :mts,          -> { where( operator: 'mts' ) }
  scope :megafon,      -> { where( operator: 'megafon' ) }
  scope :beeline,      -> { where( operator: 'beeline' ) }

  scope :recent,          -> { where( created_at: 3.days.ago.beginning_of_day..Time.zone.now.end_of_day ) }
  scope :recent_rebilled, -> { where( rebilled_at: 30.minutes.ago..20.minutes.ago ) }
  scope :rebilled,        -> { where( 'rebill_count >= 1' ) }
  scope :buyoutable,      -> { where( buyoutable: true ).where.not( billing: 'shemrock').where.not( billing: 'plasticmedia' ) }
 
  before_save :set_billing
  before_create :check_buyoutable

  def self.trigger( reason, seller_request )
    if seller_request.seller_name == 'shemrock'
      subscription_id = seller_request.id
    else 
      subscription_id == seller_request.external_sub_id
    end
    case reason
    when :subscribe
      sub = Subscribe.find_or_create_by( sub_id: seller_request.external_sub_id ) do |sub| 
        sub.dealer_id    = seller_request.dealer.id 
        sub.campaign_id  = seller_request.campaign.id
        sub.offer_id     = seller_request.offer.id
        sub.phone        = seller_request.phone 
        sub.active       = true 
        sub.country      = seller_request.country.name 
        sub.operator     = seller_request.offer.operator_name 
        sub.platform     = '' 
        sub.billing      = seller_request.seller_name
        sub.country_code = seller_request.country.abbr
      end
      seller_request.update_attribute( :subscribe_id, sub.id )
      sub
    when :unsubscribe
    if seller_request.seller_name == 'shemrock'
      sub = Subscribe.find_by( campaign: seller_request.campaign, offer: seller_request.offer, active: true )
    else
      sub = Subscribe.find_by( sub_id: seller_request.external_sub_id )
    end
      if sub
        sub.deactivate
        seller_request.update_attribute( :subscribe_id, sub.id )
      end
      sub
    end
      
  end

  def update_rebill
    update_attributes( rebill_count: ( rebill_count + 1 ), rebilled_at: Time.zone.now )
  end

  def deactivate
    update_attribute :active, false
  end

  def buyout
    buyout = Buyout.new( self )
    buyout.process_buyout
  end

  def self.to_buyout
    recent.rebilled.recent_rebilled.buyoutable
  end

  def check_buyoutable
    if offer_id && campaign_id
      self.buyoutable  = set_buyout_for billing
      true
    end
  end

  def set_billing
    if offer_id
      self.billing = Offer.find( offer_id ).seller.provider_system_name 
    end
  end

  def set_buyout_for seller_name
    result = false
    result = Campaign.find( campaign_id ).megafon_buyout if seller_name == 'onlypay'
    result = Campaign.find( campaign_id ).beeline_buyout if seller_name == 'billing8'
    result 
  end

  def buyout_price 
    offer.buyout_price
  end

  def can_buyout_because_campaign_settings
    case billing
    when 'onlypay'
      campaign.megafon_buyout
    when 'billing8'
      campaign.beeline_buyout
    else
      false
    end
  end

  def rebill_stats_triggered_immediately
    already_buyoted = dealer == Dealer.subscribe_buyer
    shemrok = billing == 'shemrock'
    plasticmedia = billing == 'plasticmedia'
    ( !can_buyout_because_campaign_settings || shemrok || plasticmedia || already_buyoted )
  end

end
