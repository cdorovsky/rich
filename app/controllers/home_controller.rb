class HomeController < ApplicationController

  skip_before_filter :verify_authenticity_token, only: :support

  def index
  end

  def support
    render text: 'Currently unavailable.', layout: false, content_type: 'text/plain'
    #ticket = ticket_sanitizer
    #if ticket.all? { | k, v | !v.blank? }
    #  SupportMailer.send_ticket( ticket ).deliver
    #end
    #redirect_to root_path
  end

  def robots
    robots = File.read(Rails.root + "app/controllers/api/robots.txt")
    render :text => robots, :layout => false, :content_type => "text/plain"
  end

  def property
    sign = 'Домен принадлежит партнерской программе RichPays.com. Для жалоб на обман и мошенничество, пожалуйста напишите на e-mail нашей службы поддержки: support@richpays.com'
    render :text => sign, :layout => false, :content_type => "text/plain"
  end

  private

    def ticket_sanitizer
      params.permit( :name, :email, :message )
    end

end
