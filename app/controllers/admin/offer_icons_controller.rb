class Admin::OfferIconsController < Admin::MainController

  before_filter :admin_required
  skip_before_filter :verify_authenticity_token

  def create
    @offer_icon = OfferIcon.new( file: params[ :file ] )
    if @offer_icon.save
      respond_to do | f |
       f.json { render json: @offer_icon }
      end
    else
      respond_to do | f |
       f.json { render json: @offer_icon.errors }
      end
    end
  end
end