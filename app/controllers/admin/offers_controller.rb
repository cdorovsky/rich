class Admin::OffersController < Admin::MainController

  before_filter :admin_required
  layout 'admin/layouts/logged_in'

  def new
    @offer = Offer.new
  end

  def create
    screens_ids = params.delete( :offer_screenshots ).map { | scr | scr[ :id ] } if params[ :offer_screenshots ]
    icon_id = params[ :offer ].delete :icon_id
    @offer = Offer.new( offer_params )
    if @offer.save
      @offer.offer_icon = OfferIcon.find( icon_id ) if !icon_id.blank?
      OfferScreenshot.where( id: screens_ids ).update_all( offer_id: @offer.id )
      OfferScreenshot.remove_unwanted_records
      respond_to do |f|
        f.html { redirect_to admin_offers_path }
        f.json { head :no_content }
      end
    else
      respond_to do |f|
        f.html { render action: 'new' }
        f.json { render json: @offer.errors }
      end
    end
  end

  def index
    @offers = Offer.enabled.order( 'id DESC' )
    @disabled_offers = Offer.disabled.order( 'id DESC' )
  end

  def show
    @offer = Offer.find( params[ :id ] )
  end

  def edit
    @offer = Offer.find( params[ :id ] )
  end

  def update
    screens_ids = params.delete( :offer_screenshots ).map { | scr | scr[ :id ] } if params[ :offer_screenshots ]
    icon_id = params[ :offer ].delete :icon_id
    @offer = Offer.find( params[ :id ] )
    if params[:offer].delete( :remove_dialog_translations )
      @offer.dialog_translations = {}
    end
    disable_day = offer_params.delete( :disable_day )
    disable_time = offer_params.delete( :disable_time ) || '00:00'
    unless disable_day.blank?
      offer_params[ :disabling_time ] = DateTime.parse( disable_day + ' ' + disable_time )
    else
      offer_params[ :disabling_time ] = nil
    end
    if @offer.update_attributes( offer_params )
      @offer.offer_icon = OfferIcon.find( icon_id ) if !icon_id.blank?
      OfferScreenshot.where( id: screens_ids ).update_all( offer_id: @offer.id )
      OfferScreenshot.remove_unwanted_records
      respond_to do |f|
        f.html { redirect_to admin_offers_path }
        f.json { render json: @offer }
      end
    else
      respond_to do |f|
        f.html { render action: 'edit' }
        f.json { render json: @offer.errors }
      end
    end
  end

  def mass_switch
    @offers = Offer.where( id: params[ :offer_ids ] )
    unless @offers.update_all( disabled: params[ :disabled ] ).zero?
      render json: { success: true  }
    else
      render json: { success: false }
    end
  end

  def disable
    @offer = Offer.find params[ :offer_id ]
    @offer.disable
    redirect_to admin_offers_path
  end

  def cached
    render json: Precacher.load( Offer, Dealer.new, params: PrecacherParams.offers_index, options: { expiring: 1.day.from_now.to_i } )
  end

  private
    def offer_params
      params.fetch( :offer, {} ).permit!
    end

end
