class Admin::OfferScreenshotsController < Admin::MainController

  before_filter :admin_required
  skip_before_filter :verify_authenticity_token

  def create
    @screenshot = OfferScreenshot.new( file: params[ :file ] )
    if @screenshot.save
      respond_to do | f |
       f.json { render json: @screenshot }
      end
    else
      respond_to do | f |
       f.json { render json: @screenshot }
      end
    end
  end

  def destroy
    @screenshot = OfferScreenshot.find( params[ :id ] )
    @screenshot.destroy
    respond_to do | f |
      f.json { render json: @screenshot }
    end
  end
end
