class Admin::MainController < ApplicationController

  protect_from_forgery with: :exception


  private

  def admin_required
    redirect_to root_url unless session[ :admin ]
  end

end
