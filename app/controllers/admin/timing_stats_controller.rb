class Admin::TimingStatsController < Admin::MainController

  def index
    @aggregates = Admin::Stats::Timing.records_for_caching( nil, params )
    render 'index.json.jbuilder', locals: {}
  end

end
