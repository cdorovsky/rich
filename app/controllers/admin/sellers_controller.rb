class Admin::SellersController < Admin::MainController

  before_filter :admin_required
  layout 'admin/layouts/logged_in'

  def new
    @seller = Seller.new 
    @offers = @seller.offers
  end

  def create
    @seller = Seller.new( seller_params )
    if @seller.save
      redirect_to admin_sellers_path
    else
      render action: 'new'
    end
  end

  def index
    @sellers = Seller.active
  end

  def show
    @seller = Seller.find( params[ :id ] )
    @offers = @seller.offers
  end

  def edit
    @seller = Seller.find( params[ :id ] )
  end

  def update
    @seller = Seller.find( params[ :id ] )
    if @seller.update_attributes( seller_params )
      redirect_to admin_seller_path
    else
      render action: 'edit'
    end
  end

  def destroy
  end

  def common_stats
    @seller = Seller.find( params[ :admin_seller_id ] )
    @balance = @seller.seller_balance    
  end

  def recent_stats
    @seller = Seller.find( params[ :admin_seller_id ] )
    @stats = @seller.recent_stats( per: params[ :start_date ] )
  end

  def date_converter( date )
    datetime = "#{date["day"]}-#{date["month"]}-#{date["year"]} #{date["hour"]}:#{date["minute"]}".to_time
  end

  private

  def seller_params
    params.fetch( :seller, {} ).permit( :seller_name, :seller_url, :contact_name, :contact_phone, :contact_email, :active )
  end

end
