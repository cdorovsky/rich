class Admin::PaymentRequestsController < Admin::MainController
  require 'payments_helper'
  include PaymentsHelper
  before_filter :admin_required

  def index
    @payment_requests =
    PaymentRequest
    .joins( :wallet )
    .where index_params
  end

private

  def index_params
    to_created_at params.permit(
      :dealer_id,
      :confirmed,
      :created_at,
      :date_start,
      :date_end,
      :payment_id,
      :payment_type,
      :status,
      :wallet_id,
      wallets: [ :kind, :number ]
    )
  end

end
