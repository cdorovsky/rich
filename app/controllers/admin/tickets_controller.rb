class Admin::TicketsController < Admin::MainController

  before_filter :admin_required
  layout 'admin/layouts/logged_in'
  before_action :set_ticket, only: [ :show, :edit, :update ]
  protect_from_forgery except: :update

  def index
    @tickets = Ticket.load_collection( params )
  end

  def show
  end

  def edit
  end

  def update
    notify = params[ :message ].present? #костыли
    if notify #костыли
      @ticket.message = params[ :message ]
      @ticket.message_counter += 1
      @ticket.last_answered = 'support'
      message = @ticket.create_message( ownership: :support )
    end
    respond_to do |format|
      if @ticket.update( ticket_params )
        if notify #костыли
          @ticket.notify_dealer
        end
        format.html { redirect_to admin_tickets_path, notice: 'Ticket was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def set_ticket
    @ticket = Ticket.find( params[ :id ] )
  end

  def ticket_params
    params.permit( :message, :closed )
  end

end
