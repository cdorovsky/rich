class Admin::AdminRequestsController < Admin::MainController

  before_filter :admin_required

  def index
    @requests = AdminRequest.where( index_params )
  end

  def create
    @request = AdminRequest.new( create_params )
    if @request.save
      render json: @request
    else
      render json: @request.errors, status: :unprocessable_entity
    end
  end

private

  def index_params
    params.permit( AdminRequest.column_names.map( :to_i ) )
  end

  def create_params
    params.permit( :dealer_id, :value, :description )
  end

end
