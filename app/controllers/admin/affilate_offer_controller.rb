class Admin::AffilateOfferController < Admin::MainController

  before_filter :admin_required
  layout 'admin/layouts/logged_in'

  def show
    @offer_id = params[ :offer_id ]
    @network_id = params[ :network_id ]
    @response = Providers::AppsAdvert::Performance::AffilateOffer.get_offer( @offer_id, @network_id )
    render json: @response
  end

end
