class Admin::SessionController < Admin::MainController

  def new
    if AdminAuthHelper.is_an_admin?( current_dealer )
      if AdminAuthHelper.session_actual?( session )
        redirect_to admin_index_path
      elsif session[ :admin_auth_code ]
        render layout: 'admin/layouts/login'
      else
        AdminAuthHelper.build_session( session )
        flash[ :message ] = ''
        render layout: 'admin/layouts/login'
      end
    else
      redirect_to root_url
    end
  end

  def resend
    ip = current_ip
    country = GeoIP.new( 'lib/geoip/GeoIP.dat' ).country( ip ).country_name
    phone = current_dealer.phone
    random_digits = session[ :admin_auth_code ] 
    SmsAuth.send_admin_auth_code random_digits: random_digits, ip: ip, country: country, phone: phone
    flash[ :message ] = 'Смс выслано'
    redirect_to admin_login_path
  end

  def renew
    AdminAuthHelper.clear_session( session )
    AdminAuthHelper.build_session( session )
    resend
  end

  def admin_auth
    if params[ 'auth_code' ] == session[ :admin_auth_code ]
      AdminAuthHelper.update_admin_session( session )
      redirect_to admin_index_path
    else
      flash[ :message ] = 'Введён неверный код, попробуйте снова'
      redirect_to admin_login_path
    end
  end

  def destroy
    AdminAuthHelper.clear_session( session )
    redirect_to root_url
  end

end
