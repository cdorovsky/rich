class Admin::NewsController < Admin::MainController

  before_filter :admin_required
  layout 'admin/layouts/logged_in'

  def index
    @news = News.order( 'updated_at DESC' )
  end

  def show
    @news = News.find( params[ :id ] )
  end

  def new
    @news = News.new
  end

  def edit
    @news = News.find( params[ :id ] )
  end

  def create
    @news = News.new( news_params )
    if @news.save
      redirect_to admin_news_index_path
    else
      render action: 'new'
    end
  end

  def update
    @news = News.find( params[ :id ] ) 
    if @news.update_attributes( news_params )
      redirect_to admin_news_index_path
    else
      render action: 'edit'
    end
  end

  def destroy
    @news = News.find( params[ :id ] )
    @news.destroy
    redirect_to admin_news_index_path
  end

  private

    def news_params
      params.require( :news ).permit( :title, :content, :short_content )
    end

end
