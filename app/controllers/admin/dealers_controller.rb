class Admin::DealersController < Admin::MainController

  before_filter :admin_required
  layout 'admin/layouts/logged_in'

  def index
    @dealers = Dealer.load_collection params
  end

  def show
    @dealer = Dealer.find( params[ :id ] )
  end

  def requests
    @payment_requests = Dealer.joins( :wallets ).payment_requests( params )
  end

  def destroy
  end

  def update
    @dealer = Dealer.find( params[ :id ] )
    if @dealer.update( dealer_params )
      respond_to do |f|
        f.html { redirect_to admin_offers_path }
        f.json { render json: @dealer }
      end
    else
      respond_to do |f|
        f.html { render action: 'edit' }
        f.json { render json: @dealer.errors, status: :unprocessable_entity }
      end
    end
  end

  def dealers_list
    @dealers = Dealer.all
  end

  def get_scope
    render json: { dealer_ids: DealerHelper.generate_ids_array( params ) }
  end

  def change_balance
    @dealer = Dealer.find( params[ :id ] || params[ :dealer_id ] )
    value = params[ :value ].to_f
    if value > 0
      case params[ :balance ][ :change ]
        when "increase" then AdminRequest.trigger( @dealer,  value )
        when "decrease" then AdminRequest.trigger( @dealer, -value )
        else false
      end
    end
    redirect_to admin_dealer_path( id: params[ :dealer_id ] )
  end

  def become
    return unless session[ :admin ]
    sign_in( :dealer, Dealer.find( params[ :dealer_id ] ) )
    redirect_to root_url # or user_root_url
  end

  def cached
    render json: Precacher.load( Dealer, Dealer.new, params: PrecacherParams.dealers_index, options: { expiring: 1.day.from_now.to_i } )
  end

  private

  def dealer_params
    params[:dealer].delete(:password) if params[:dealer][:password].try(:blank?)
    params.require( :dealer ).permit(
      :email,
      :skype,
      :icq,
      :jabber,
      :phone,
      :name,
      :blocked,
      :password,
      :tariff_id
    )
  end
end
