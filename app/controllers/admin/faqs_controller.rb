class Admin::FaqsController < Admin::MainController

  before_filter :admin_required
  layout 'admin/layouts/logged_in'

  def index
    @faqs = Faqs.order( 'updated_at DESC' )
  end

  def show
    @faqs = Faqs.find( params[ :id ] )
  end

  def new
    @faqs = Faqs.new
  end

  def edit
    @faqs = Faqs.find( params[ :id ] )
  end

  def create
    @faqs = Faqs.new( faqs_params )
    if @faqs.save
      redirect_to admin_faqs_path
    else
      render action: 'new'
    end
  end

  def update
    @faqs = Faqs.find( params[ :id ] ) 
    if @faqs.update_attributes( faqs_params )
      redirect_to admin_faqs_path
    else
      render action: 'edit'
    end
  end

  def destroy
    @faqs = Faqs.find( params[ :id ] )
    @faqs.destroy
    redirect_to admin_faqs_path
  end

  private

    def faqs_params
      params.require( :faqs ).permit( :question, :answer )
    end

end
