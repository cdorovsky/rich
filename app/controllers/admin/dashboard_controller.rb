class Admin::DashboardController < Admin::MainController

  before_filter :admin_required
  layout 'admin'

  def index
    Rails.logger.debug controller_name
  end

end
