class Admin::DirectOffersController < Admin::MainController

  before_filter :admin_required
  before_filter :set_direct_offer, only: [ :show, :update ]
  layout 'admin/layouts/logged_in'

  def index
    @direct_offers = DirectOffer.where( params.to_hash.except( 'format', 'action', 'controller' ) )
  end

  def create # aka mass approve/reject
    render json: DirectOffer.moderate!( moderate_params )
  end

  def update
    unless @direct_offer.update( update_params )
      render @direct_offer.errors, status: :unprocessable_entity
    end
  end

private

  def set_direct_offer
    @direct_offer = DirectOffer.find( params[ :id ] )
  end

  def moderate_params
    #params.permit( :direct_offer_ids, :approved, :reason ) da shit won't working!
    {
      direct_offer_ids: params[ 'direct_offer_ids' ],
      approved: params[ 'approved' ].to_b,
      reason: params[ 'reason' ]
    }
  end

  def index_params
    params.permit( :offer_id, :dealer_id, :approved )
  end

  def date_params
    params.permit( :date_start, :date_end )
  end

  def update_params
    params.permit( :approved )
  end

end
