class Admin::PaymentsController < Admin::MainController
  require 'payments_helper'
  include PaymentsHelper
  before_filter :admin_required
  layout 'admin/layouts/logged_in'
  before_action :set_payment, only: [ :show, :update, :as_csv ]
  skip_before_filter :verify_authenticity_token, :only => :as_csv

  def index
    @payments = Payment.where( payments_params ).order( 'paid, DATE(created_at) DESC' )
  end

  def show
  end

  def create
    @payment = Payment.create_with_requests( params[ :request_ids ] )
    if @payment
      render json: { id: @payment.id, success: true }
    else
      render json: { id: 'new', success: false }
    end
  end

  def as_csv
    send_data @payment.as_csv( splitted_ids, col_sep: ';' ), filename: "Payment_by_Request_#{ Time.zone.now.strftime( '%F' ) }.csv"
  end

  def update
    if @payment.process_payout( request_ids: params[ :request_ids ], payment_action: params[ :payment_action ] )
      render json: @payment, success: true
    else
      render json: @payment.errors, success: false
    end
  end

private

  def splitted_ids
    params[ :request_ids ].split( ',' )
  end

  def set_payment
    @payment = Payment.find( params[ :id ] )
  end

  def payments_params
    to_created_at params.permit( :id, :wallet_type, :created_at, :date_start, :date_end, :paid )
  end

end
