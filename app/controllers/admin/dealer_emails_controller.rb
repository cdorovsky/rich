class Admin::DealerEmailsController < Admin::MainController

  def send_emails
    mailing_id = params[ :dealer_email ][ :id ]
    if mailing_id
      @dealer_email = DealerEmail.find( mailing_id )
      @dealer_email.attributes = dealer_email_params
    else
      @dealer_email = DealerEmail.new( dealer_email_params )
    end
    if @dealer_email.save
      @dealer_email.deliver if params[ :deliver_after_save ]
      render json: @dealer_email
    else
      render json: @dealer_email.errors, status: :unprocessable_entity
    end
  end

  def index
    @dealer_mailings = DealerEmail.all
  end

  def show
    @dealer_mailing = DealerEmail.find( params[ :id ] )
  end

  private

  def dealer_email_params
    params.require( :dealer_email ).permit!
  end

end
