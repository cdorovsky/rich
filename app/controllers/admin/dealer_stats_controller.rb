class Admin::DealerStatsController < Admin::MainController

  def index
    render json: Precacher.load( Admin::Stats::Dealers, Dealer.new, params: params, options: { expiring: 10.minutes.from_now.to_i } )
  end

  def top_cr
    render json: Precacher.load( Admin::Stats::Dealers::TopCR, Dealer.new, params: PrecacherParams.top_cr, options: { expiring: 10.minutes.from_now.to_i } )
  end

end
