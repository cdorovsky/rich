class SessionsController < Devise::SessionsController
  def new
    render json: { status: 'not authorized' }
  end

  def create
    self.resource = warden.authenticate!( auth_options )
    yield resource if block_given?
    render json: { success: true }
  end

  def failure
    render json: {
      dealer: {
        sign_in_input: 'Неверный логин или пароль'
      }
    }
  end

  def destroy
    respond_to do |format|
      if session[ :admin ]
        redirect_to admin_index_path + "#/dealers/#{ current_dealer.id }"  and return
      else
        session.destroy
      end
      format.all { head :no_content }
      format.any(*navigational_formats) { redirect_to after_sign_out_path_for(resource_name) }
    end
  end

  protected

  def auth_options
    {
      scope: resource_name,
      recall: "#{controller_path}#failure"
    }
  end

end
