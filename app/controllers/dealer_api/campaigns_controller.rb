class DealerApi::CampaignsController < DealerApi::ApplicationController

  before_action :set_campaign, only: [ :show, :mass_offer_set, :set_all_offers ]

  def index
    @campaigns = api_dealer.campaigns
  end

  def show
  end

  def mass_offer_set
    if CampaignOfferSetter.perform_async( @campaign.id, params[ :offer_ids ] )
      render json: { success: true }
    else
      render json: { success: false }
    end
  end

  def set_all_offers
    all_offers_by_id = Offer.enabled.pluck( :id )
    if CampaignOfferSetter.perform_async( @campaign.id, all_offers_by_id )
      render json: { success: true }
    else
      render json: { success: false }
    end
  end

  private

    def set_campaign
      if api_dealer.campaigns.pluck( :id ).include?( params[ :id ].to_i )
        @campaign = Campaign.find( params[ :id ] )
      else
        render json: { status: '403' }
      end
    end


end
