class DealerApi::OffersController < DealerApi::ApplicationController

  respond_to :json

  def index
    @offers = Offer.enabled
  end

  def show
    @offer = Offer.find( params[ :id ] )
  end

end
