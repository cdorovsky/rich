class DealerApi::ApplicationController < ApplicationController
  skip_before_filter :verify_authenticity_token, :authenticate_dealer
  before_filter :authorize_api_client, except: [ :property ]

  def property
    sign = 'Домен принадлежит партнерской программе RichPays.com. Для жалоб на обман и мошенничество, пожалуйста напишите на e-mail нашей службы поддержки: support@richpays.com'
    render :text => sign, :layout => false, :content_type => "text/plain"
  end

  def api_dealer
    Dealer.find_by( id: params[ :dealer_id ] )
  end

  private

    def authorize_api_client
      sign = params[ :sign ]
      dealer_id = params[ :dealer_id ]
      timestamp = params[ :timestamp ]
      unless Dealer.authorize_as_api_client( sign, dealer_id, timestamp )
        render json: { status: '401' }
      end
    end

end
