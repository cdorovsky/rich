class Personal::OfferSetsController < ApplicationController

  def show
    @offer_set = current_dealer.offer_sets.find_by( id: params[ :id ] ) #костыли
  end

  def new
    @campaign = Campaign.find params[ :campaign_id ]
    @offer_set = @campaign.offer_sets.build
  end

  def create
    @offer_set = OfferSet.find_or_initialize_by( offer_set_params )
    if @offer_set.campaign.dealer_id == current_dealer.try( :id ) #костыли
      @offer_set.removed = false
      @offer_set.dealer_id = current_dealer.id
      @offer_set.offer_kind = @offer_set.offer.kind
      if @offer_set.save
        render json: @offer_set
      else
        render json: @offer_set.errors, status: :unprocessable_entity
      end
    end
  end

  def update
    @offer_set = OfferSet.find_by( id: params[ :id ], dealer_id: current_dealer.id )
    if @offer_set.update( offer_set_params )
      render json: { removed: @offer_set.removed }
    else
      render json: { error: "422", status: :unprocessable_entity }
    end
  end

  def offer_set_params
    params.require( :offer_set ).permit( :campaign_id, :offer_id, :removed )
  end
end
