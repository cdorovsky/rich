class Personal::FaqsController < ApplicationController
  respond_to :json

  def index
    @faqs = Faqs.order( 'updated_at DESC' )
  end

  def show
    @faq = Faqs.find params[ :id ]
    @answer = News.convert_content_to_html( @faq[ :answer ] )
  end

end
