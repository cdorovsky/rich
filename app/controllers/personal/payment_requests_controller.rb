class Personal::PaymentRequestsController < ApplicationController
  protect_from_forgery except: :create

  def index
    @payment_requests = current_dealer.payment_requests.unpaid.to_json
  end

  def new    
  end

  def create
    @payment_request = PaymentRequest.trigger( current_dealer, params[ :payment_request_value ] )
    respond_to do |format|
      if @payment_request
        format.json { head :no_content }
      else
        format.json { render json: @payment_request.errors, status: :unprocessable_entity }
      end
    end
  end

end
