class Personal::DealerBalanceController < ApplicationController
  respond_to :json

  def index
    total_money = DealerStat.dashboard_total_money( current_dealer )

    @dayly_wapclick       = total_money[ :wapclick_daily ]
    @monthly_wapclick     = total_money[ :wapclick_monthly ]

    @dayly_apps           = total_money[ :apps_advert_daily ]
    @monthly_apps         = total_money[ :apps_advert_monthly ]

    @dayly_cpa            = total_money[ :cpa_daily ]
    @monthly_cpa          = total_money[ :cpa_monthly ]

    @dealer_balance       = current_dealer.dealer_balance
    @balance_available    = ( current_dealer.dealer_balance.for_checkout - current_dealer.dealer_balance.requested )
    @balance_requested    = current_dealer.dealer_balance.requested
    @balance_for_checkout = current_dealer.dealer_balance.for_checkout
  end

  def show

  end

end
