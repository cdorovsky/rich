class Personal::SidebarNotificationsController < ApplicationController
  respond_to :json

  def show
    @sidebar_notifications = current_dealer.sidebar_notifications
  end

end
