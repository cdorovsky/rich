class Personal::DashboardController < ApplicationController

  attr_accessor :dealer_id, :payment_request_value

  respond_to :json

  def index
    render json: Dashboard.get_json( current_dealer.id )
  end

  def show

  end

  def create_payment_request
    value = params[ :payment_request_value ]
    wallet_id = params[ :chosen_wallet_id ]
    @request = PaymentRequest.trigger( current_dealer, wallet_id, value )
    render json: Dashboard.partial_update( :payment_request, current_dealer.id )
  end

  def offers
    @offers = Offer.enabled.order( 'weekly_epc DESC' ).last(5)
    @countries = Hash.new
    @offers.each {|offer| @countries[ "#{offer.id}" ] = offer.offer_country_sets.count }
  end

   def offer
    @offer = Offer.find params[ :id ]
    @countries = @offer.offer_country_sets.count
  end

  def campaigns
    render json: Campaign.cached_dashboard_json( current_dealer.id )
  end

  def conversions
    render json: Conversion.cached_dashboard_json( current_dealer.id )
  end

end
