class Personal::NotificationsController < ApplicationController
  respond_to :json

  def index
    @notes = Notification.where( dealer_id: [ nil, current_dealer.id ] ).order( 'created_at DESC' ).first(5)
  end

  def show
    @notes = Notification.find params[ :id ]
  end

end
