class Personal::WalletsController < ApplicationController
  before_action :set_wallet, only: [ :show, :edit, :update, :destroy ]

  def index
    @wallets = current_dealer.wallets.order('updated_at DESC')
  end

  def show
  end

  def new
    @wallet = Wallet.new
  end

  def edit
  end

  def create
    @wallet = Wallet.new( wire_wallet_params )
    @wallet.dealer_id = current_dealer.id
    @wallet.pending = true
    respond_to do |format|
      if @wallet.save
        format.json { render json: @wallet }
      else
        format.json { render json: @wallet.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @wallet.pending = true
    respond_to do |format|
      if @wallet.update( edit_wallet_params )
        format.json { render json: @wallet }
      else
        format.json { render json: @wallet.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @wallet.destroy
    respond_to do |format|
      format.html { redirect_to wallets_url }
      format.json { head :no_content }
    end
  end

  private

    def set_wallet
      @wallet = Wallet.find( params[:id] )
    end

    def wmz_wallet_params
      params.require( :wallet ).permit( :name, :kind, :number )
    end

    def edit_wallet_params
      params.require( :wallet ).permit( :name )
    end

    def wire_wallet_params
      params.require( :wallet ).permit(
        :name,
        :kind,
        :receiver,
        :receiver_address,
        :bank_name,
        :bank_address,
        :bank_city,
        :bank_country,
        :details,
        :account_number_iban,
        :swift_code
      )
    end
end
