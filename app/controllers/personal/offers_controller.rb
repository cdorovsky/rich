class Personal::OffersController < ApplicationController
  respond_to :json

  def index
    @offers = Offer.load_collection( params )
  end

  def show
    @offer = Offer.find params[ :id ]
  end

  def countries
    @offer = Offer.find params[ :id ]
    render text: "Country codes: (#{ @offer.country_codes })", layout: false, content_type: "text/plain"
  end

end
