class Personal::SettingsController < ApplicationController
  before_action :set_dealer

  def show
    render 'show.json'
  end

  def request_confirmation_code
    phone = ( @dealer.try( :phone ) or params[ :new_phone ] )
    session[ :new_phone ] = params[ :new_phone ] if @dealer.try( :phone ).blank?
    session[ :phone_update ] = SmsAuth.send_verification_for_phone_update phone: phone
    if session[ :phone_update ].present?
      render json: { status: 'Сообщение с кодом подтверждения отправлено.' }
    else
      render json: { status: 'Ошибка.' }
    end
  end

  def update
    if looks_like_phone_number? params[ :new_phone ]
      if request_params_matching_session
        @dealer.update phone: params[ :new_phone ]
      end
    end
    session.delete :phone_update
    session.delete :new_phone
    respond_to do | format |
      if @dealer.update( dealer_params )
        sign_in @dealer, bypass: true
        format.json { head :no_content }
      else
        format.json { render json: @dealer.errors, status: :unprocessable_entity }
      end
    end
  end

  private

    def looks_like_phone_number?( string='' )
      if string
        string.acts_like_string? && Math.log10( string.to_i ) + 1 >= 11 # minimum 11 digits for mobile number
      end
    end

    def request_params_matching_session
      sms_code_valid = (
        session[ :phone_update ] == params[ :phone_update ]
      )
      numbers_matches = (
        session[ :new_phone ] ? params[ :new_phone ] == session[ :new_phone ] : true
      )
      numbers_matches and sms_code_valid
    end

    def set_dealer
      if current_dealer
        @dealer = current_dealer
      else
        render json: :no_content
      end
    end

    def dealer_params
      params.permit( 
          :name,
          :email,
          :skype,
          :icq,
          :jabber,
          :password_confirmation,
          :password,
          :sign_in_count,
          notification_settings_attributes: [ 
            :ticket_email,
            :offer_email,
            :ticket_sms,
            :offer_sms,
            :direct_offer_email,
            :direct_offer_sms
          ]
      )
    end

end
