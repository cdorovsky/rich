class Personal::TicketsController < ApplicationController
  before_action :set_ticket, only: [ :show, :edit, :update, :ticket_messages ]
  respond_to :json

  def index
    @tickets = current_dealer.tickets.order( 'id DESC' )
  end

  def ticket_messages
    @ticket_messages = @ticket.ticket_messages
  end

  def show
    @ticket = Ticket.find_and_read current_dealer.id, params[ :id ]
  end

  def new
    @ticket = current_dealer.tickets.build
  end

  def edit
  end

  def create
    @ticket = current_dealer.tickets.build( ticket_params )
    @ticket.message_counter = 1
    @ticket.message = params[ :message ]
    @ticket.last_answered = 'dealer'
    respond_to do |format|
      if @ticket.save
        @message = TicketMessage.create( dealer_id:    @ticket.dealer_id,
                                         ticket_id:    @ticket.id,
                                         message_text: @ticket.message,
                                         number:       @ticket.message_counter,
                                         ownership:    @ticket.last_answered )
        format.json { render action: 'show', status: :created, location: @ticket }
      else
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @ticket.message = params[ :message ]
    @ticket.message_counter += 1
    @message = TicketMessage.create( dealer_id: @ticket.dealer_id, ticket_id: @ticket.id, message_text: @ticket.message, number: @ticket.message_counter, ownership: 'dealer' )
    respond_to do |format|
      if @ticket.update( upd_params )
        format.json { render action: 'index' }
      else
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    def set_ticket
      @ticket = current_dealer.tickets.find( params[ :id ] )
    end
    def ticket_params
      params.require( :ticket ).permit( :title, :message, :dealer_id )
    end
    def upd_params
      params.require( :ticket ).permit( :message )
    end
end
