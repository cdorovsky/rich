class Personal::NewsController < ApplicationController
  respond_to :json

  def index
    @news = News.order( 'updated_at DESC' )
  end

  def show
    @news_item = News.find params[ :id ]
  end

end
