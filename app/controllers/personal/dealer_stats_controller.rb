class Personal::DealerStatsController < ApplicationController
  before_filter :authenticate_dealer!
  respond_to :json
  layout false

  def table
    render json: Precacher.load( DealerStat, current_dealer, params: params )
  end

  def dashboard_graph
    render json: DealerStat.cached_dashboard_json( current_dealer.id )
  end

  def subscribes
    @subscribes = DealerStat.load_subscribes( current_dealer )
  end

  def conversion
    @conversions = Conversion.load_collection( current_dealer, params )
  end

  def stats_params
    params.permit( :date_start, :date_end, :date_scope, :kind, :format, :action, :controller, :grouping ).deep_symbolize_keys
  end

end
