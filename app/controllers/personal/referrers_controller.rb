class Personal::ReferrersController < ApplicationController
  respond_to :json

  def index
    @refs = current_dealer.referals
  end

end
