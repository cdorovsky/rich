class Personal::DirectOffersController < ApplicationController

  before_filter :set_offer, only: [ :create ]
  before_filter :set_offer_by_hashid, only: [ :show, :update ]

  def index
    @direct_offers = current_dealer.direct_offers
  end

  def create
    unless @direct_offer.save
      render json: @direct_offer.errors, status: :unprocessable_entity
    end
  end

  def update
    unless @direct_offer.update( direct_offer_update_params )
      render json: @direct_offer.errors, status: :unprocessable_entity
    end
  end

private

  def set_offer
    @direct_offer = current_dealer.direct_offers.find_or_initialize_by( direct_offer_params )
  end

  def set_offer_by_hashid
    @direct_offer = current_dealer.direct_offers.find_by( hashid: params[ :hashid ] )
  end

  def direct_offer_update_params
    params.permit( :postback, :postback_url, :postback_action )
  end

  def direct_offer_params
    params.permit( :offer_id, :traffic_sources )
  end

end
