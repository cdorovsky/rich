class Personal::RequestsHistoryController < ApplicationController
  respond_to :json

  def index
    @payment_requests = current_dealer.payment_requests.order( 'updated_at DESC' )
    @admin_requests = current_dealer.admin_requests.order( 'id DESC' )
  end

  def show
    @requests_history = current_dealer.payment_requests.find params[ :id ]
  end

end
