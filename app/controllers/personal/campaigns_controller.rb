class Personal::CampaignsController < ApplicationController
  before_action :set_campaign, only: [ :show,
                                       :edit,
                                       :update,
                                       :destroy,
                                       :check_pdf,
                                       :weblanding ]

  after_action :update_dashboard_cache, only: [ :create, :update ]

  def index
    @campaigns = current_dealer.campaigns.order( 'id' )
  end

  def light_index
    @campaigns = current_dealer.campaigns.order( 'id ASC' )
    @offers = Offer.enabled.pluck( :name )
  end

  def show
  end

  def new
    @campaign = Campaign.new
  end

  def edit
  end

  def create
    @campaign = Campaign.new create_params
    @campaign.dealer = current_dealer
    respond_to do | format |
      if @campaign.save
        format.json { render json: @campaign }
      else
        format.json { render json: @campaign.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do | format |
      if @campaign.update( update_params )
        format.json { head :no_content }
      else
        format.json { render json: @campaign.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @campaign.destroy
    respond_to do | format |
      format.json { head :no_content }
    end
  end

  def check_pdf
    render json: @campaign.check_wapclick_readiness
  end

  def weblanding
    @offer_sets = @campaign.offer_sets.with_weblanding
  end

  private

    def update_dashboard_cache
      Dashboard.partial_update( :campaign, @campaign.dealer_id )
    end

    def set_campaign
      @campaign = current_dealer.campaigns.find_by( id: params[ :id ] ) #костыли
    end

    def update_params
      # в params['campaign']['restricted_countries_ids'] передается массив айди стран к отключению из кампаний
      transform_campaign_limiter_params if params[ "campaign" ][ "campaign_limiter_attributes" ]
      ( create_params ).except( :arbitrage )
    end

    def transform_campaign_limiter_params
      days_boolean_hash  = params[ "campaign" ][ "campaign_limiter_attributes" ].try( :delete, "days_hash" ) || {}
      hours_boolean_hash = params[ "campaign" ][ "campaign_limiter_attributes" ].try( :delete, "hours_hash" ) || {}
      params[ "campaign" ][ "campaign_limiter_attributes" ][ 'days' ]  = PpParamsHelper.turn_binary_hash_to_int( days_boolean_hash )
      params[ "campaign" ][ "campaign_limiter_attributes" ][ 'hours' ] = PpParamsHelper.turn_binary_hash_to_int( hours_boolean_hash )
      params
    end

    def create_params
      params.require( :campaign ).permit(
        :dealer_id,
        :name,
        :id,
        :created_at,
        :enabled,
        :adult,
        :trafficback_url,
        :m_trafficback_url,
        :broken_offer_behavior,
        :arbitrage,
        :postback_url,
        :sent_postback,
        :request_kind,
        :archived,
        :disable_weblandings,
        user_domains_attributes: [
          :id,
          :url
        ],
        campaign_limiter_attributes: [
          :hours,
          :days,
          :show_limits,
          :id
        ]
      )
    end
end
