class Personal::MoneyOptionsController < ApplicationController
  before_action :set_money_options, only: [ :show, :edit, :update, :destroy ]

  def index
    @money_options = current_dealer.money_options
  end

  def show
    @money_options = current_dealer.money_options
  end

  def new
  end

  def edit
    @money_options = current_dealer.money_options
  end

  def create
  end
  
  def update
    respond_to do |format|
      if @money_options.update( edit_options_params ) and current_dealer.has_valid_phone?
        format.html { redirect_to @money_options, notice: 'Options was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @money_options.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
  end

  private
    def set_money_options
      @money_options = current_dealer.money_options
    end

    def edit_options_params
      params.require( :money_options ).permit( :id, :dealer_id, :wallet_id, :pay_type, :pay_timing, :pay_day, :pay_value, :enabled )
    end
end
