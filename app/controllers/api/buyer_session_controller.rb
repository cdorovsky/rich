class Api::BuyerSessionController < Api::MainController

  skip_before_action :verify_authenticity_token

  #layout 'landing', only: [ :show ]

  def handler
    dealer_id = RedirectCode.decode( params[ :id ] )[ :dealer_id ]
    @redirect_code = params[ :id ]
    @src_domain = request.original_url.split( '/s/' )[ 0 ]
    @sub_params = request.original_url.split( '?' )[ 1 ]
    adekvat_dealers = Rediska.adekvat_dealers
    @async = adekvat_dealers.include?( dealer_id )
  end

  def wapclick
    unless RequestInformation.is_ru_mobile?( request )
      dealer_id = RedirectCode.decode( params[ :id ] )[ :dealer_id ]
      @redirect_code = params[ :id ]
      @src_domain = request.original_url.split( '/w/' )[ 0 ]
      @sub_params = request.original_url.split( '?' )[ 1 ]
      adekvat_dealers = Rediska.adekvat_dealers
      @async = adekvat_dealers.include?( dealer_id )
    else
      render nothing: true, status: 200
    end
  end

  def create
    dealer_and_campaign = RedirectCode.decode( params[ :id ] )
    if dealer_and_campaign.empty? or ( $redis.get "redirect_code_#{ params[ :id ] }").nil?
      respond_to do | format |
        format.js   { render nothing: true, status: 200 }
        format.json { render nothing: true, status: 200 }
        format.html { redirect_to 'http://www.google.com' }
      end
    else
      dealer_id = dealer_and_campaign[ :dealer_id ]
      @redirect_code = params[ :id ]
      @src_domain = request.original_url.split( '/c/' )[ 0 ]
      @sub_params = request.original_url.split( '?' )[ 1 ]
      @buyer_session = BuyerSession.new( request, params )
      adekvat_dealers = Rediska.adekvat_dealers
      @async = adekvat_dealers.include?( dealer_id )
    end
  end

  def show
    if request.format == 'text/html'
      unless cookies[ "direct_rich_#{ params[ :id ] }"]
        cookies["direct_rich_#{ params[ :id ] }"] = true
        params[ :uniq ] = true
      end
    end
    unless $redis.get( "offer_set_#{ params[ :id ] }" )
      respond_to do | format |
        format.js   { render nothing: true, status: 200 }
        format.json { render nothing: true, status: 200 }
        format.html { redirect_to 'http://www.google.com' }
      end      
    else
      @buyer_request = BuyerRequest.new( request, params )
      respond_to do | format |
        format.js   { render nothing: true, status: 200 }
        format.json
        format.html { redirect_to @buyer_request.redirect_url }
      end
    end
  end

  def target_show
    if request.format == 'text/html'
      unless cookies[ "direct_rich_#{ params[ :id ] }"]
        cookies["direct_rich_#{ params[ :id ] }"] = true
        params[ :uniq ] = true
      end
    end
    @buyer_request = BuyerRequest.new( request, params )
    redirect_to @buyer_request.redirect_url
  end

  def tb
    TrafficBack.new( :campaign, params )
    render json: { nothing: true, status: 200 }
  end

  def clicker
    Clicker.new( params )
    render json: { nothing: true, status: 200 }
  end

  def save_stat( fact )
    @fact = fact
    StatsHandler.save_visitor_stats( fact, @buyer_request )
  end

end
