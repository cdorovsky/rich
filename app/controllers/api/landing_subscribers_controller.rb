class Api::LandingSubscribersController < Api::MainController

  skip_before_action :verify_authenticity_token

  layout 'landing', only: [ :agreement, :unsubscribe ]

  def sms_subscribe
    LandingSubscriber.create_and_notify( landing_params )
    render json: { success: true }
  end

  def sms_unsubscribe
    @sub = LandingSubscriber.find_by( phone: landing_params[ :phone ] )
    @sub.try( :unsubscribe! )
    render json: { success: true }
  end

  def agreement
    render 'agreement', locals: { domain: LandingHelper.landing_domain( request ), title: 'Лицензионное соглашение' }
  end

  def unsubscribe
    render 'unsubscribe', locals: { domain: LandingHelper.landing_domain( request ), title: 'Отписаться от рассылки' }
  end

  private

    def landing_params
      params.permit( :phone, :code )
      params[ :phone ] = params[ :phone ].reverse.chomp( '+' ).reverse
      params
    end

end