class Api::ApiController < Api::MainController

  def property
    sign = 'Домен принадлежит партнерской программе RichPays.com. Для жалоб на обман и мошенничество, пожалуйста напишите на e-mail нашей службы поддержки: support@richpays.com'
    render :text => sign, :layout => false, :content_type => "text/plain"
  end  

  def robots
    robots = File.read(Rails.root + "app/controllers/api/robots.txt")
    render :text => robots, :layout => false, :content_type => "text/plain"
  end

end