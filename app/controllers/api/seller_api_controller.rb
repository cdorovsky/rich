class Api::SellerApiController < Api::MainController

  skip_before_filter :verify_authenticity_token

  def apps_advert
    SellerRequest.new( params )
    render nothing: true, status: 200
  end

end
