class Api::MainController < ApplicationController

  layout false
  protect_from_forgery with: :exception
  before_filter :set_ip

  private

  def set_ip
    if request.env[ 'HTTP_X_FORWARDED_FOR' ]
      @ip = request.env[ 'HTTP_X_FORWARDED_FOR' ].split(',').first
    else
      @ip = request.remote_ip
    end
  end

end
