=begin
class Api::ScriptController < Api::MainController

  def handler
    #buyer_session = BuyerSession.new( :direct, request, params )   
    #common_handle( buyer_session )
  end

  def code_handler
    buyer_session = BuyerSession.new( :with_code, request, params )   
    render nothing: true, status: 200
    #common_handle( buyer_session )
  end

  def common_handle( buyer_session )
    @campaign = buyer_session.campaign
    seen_offers = seen_offers_list
    @offer = buyer_session.offer( seen_offers )
    @url = buyer_session.url
    if @offer
      if @offer.kind == 'apps_advert'
        @offers_has_suitable_os = true  
        @js_dialogue = @offer.js_dialogue || "БЕСПЛАТНО! Приложение дня! Не пропустите, только сегодня оно БЕСПЛАТНО! СКАЧАТЬ СЕЙЧАС?"
      end
      uniq = check_and_write_session
      save_stats( uniq )
      save_request
      render "redirect.js.coffee"
    else
      render nothing: true
    end
  end

  def save_stats( uniq )
    DealerStat.trigger( :visitor, campaign: @campaign, offer: @offer )
    if @offer
      DealerStat.trigger( :uniq_visitor, campaign: @campaign, offer: @offer ) if uniq
    else
      fact = uniq ? :uniq_traf_back : :traf_back
      DealerStat.trigger( fact, campaign: @campaign, offer: @offer )
    end
  end

  private

  def save_request
    RequestInformation.save_request_information( request )
  end

  def rich_session_arr
    rich_keys_string = session[ :rich_keys ] || ''
    rich_key_arr = rich_keys_string.split( ' ' )
    rich_key_arr
  end

  def seen_offers_list
    seen_offers = rich_session_arr ? rich_session_arr.map{ |key| key.split( 'd' )[1] } : [ ]
    seen_offers.delete( '00' )
    seen_offers
  end

  def check_and_write_session
    offer_id = @offer ? @offer.id.to_s : '00'
    rich_key = "#{ @campaign.id }d#{ offer_id }"
    rich_key_arr = rich_session_arr
    uniq = !rich_key_arr.include?( rich_key )
    write_session( rich_key ) if uniq
    uniq
  end

  def write_session( rich_key )
    session[ :rich_keys ] = session[ :rich_keys ] ? session[ :rich_keys ] += ' ' + rich_key : session[ :rich_keys ] = rich_key
  end

  def campaign
    @campaign = Campaign.find( params[ :id ] )
    @host = request.host
    @offers_has_suitable_os = ( @offers_os and @offers_os.include? request.env[ 'HTTP_USER_AGENT' ].downcase )
    if @render_script
      render "redirect.js.coffee"
    else
      render nothing: true
    end
  end

  def welcome
    render 'welcome.js.coffee'
  end

  def thankyou
    if Rails.env.production?
      @domain = 'dorovsky.com'
    else
      @domain = 'rich.test'
    end
    render 'thankyou.js.coffee'
  end

  def confirm
    #buyer = Buyer.find(params[:id])
    #if buyer
        # Confirmation will be here
    #    buyer.destroy
    #end
    render 'confirm.js.coffee'
  end

  def check_wapclick_ips
    @campaign = Campaign.find( params[ :id ] )
    if @campaign.offers.pluck( :kind ).uniq == [ 'wapclick' ]
      if MtsIps.ip_list.include?( @ip ) || BeelineIps.ip_list.include?( @ip ) || MegafonIps.ip_list.include?( @ip )
        @render_script = true
      else
        @render_script = false
      end
    else
      @render_script = true
    end
  end

  def check_billing_offer
    @campaign = Campaign.find( params[ :id ] )
    @offers = @campaign.offers
    @operator = check_billing
    operator_is_suitable = ( @operator and @offers.pluck( :operator_name ).uniq.include?( @operator ) )
    case @offers.pluck( :kind ).uniq.sort
    when [ 'wapclick' ]
      if operator_is_suitable
        @render_script = true
      else
        @render_script = false
      end
    when [ 'apps_advert' ]
      @offers_os = @offers.pluck( :apps_os ).uniq
      @render_script = true
    when [ 'apps_advert', 'wapclick' ]
      if operator_is_suitable
        @render_script = true
      else
        @offers_os = @offers.pluck( :apps_os ).uniq
        @render_script = true
      end
    end
  end

  def check_billing
    operator = 'mts' if MtsIps.ip_list.include?( @ip )
    operator = 'beeline' if BeelineIps.ip_list.include?( @ip )
    operator = 'megafon' if MegafonIps.ip_list.include?( @ip )
    operator
  end

end
=end
