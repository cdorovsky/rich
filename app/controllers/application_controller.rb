class ApplicationController < ActionController::Base
  before_filter :configure_permitted_parameters, if: :devise_controller?
  before_filter :capture_referal
  layout :layout_by_resource
  protect_from_forgery with: :exception

  def http_200_ok
    render status: 200, text: 'OK', layout: false, content_type: 'text/plain'
  end
  
  protected

   def configure_permitted_parameters
     devise_parameter_sanitizer.for(:sign_in){ |u| u.permit(:email, :password) }
     devise_parameter_sanitizer.for(:sign_up){ |u| u.permit(:email, :password, :wmr_wallet, :referral_code) }
     devise_parameter_sanitizer.for(:account_update){ |u| u.permit(:email, :password, :wmr_wallet) }
   end

  def layout_by_resource
    current_dealer ? 'personal' : 'login'
  end

  def capture_referal
    session[:referral] = params[:referral] if params[:referral]
  end

  def current_ip
    if request.env[ 'HTTP_X_FORWARDED_FOR' ]
      @ip = request.env[ 'HTTP_X_FORWARDED_FOR' ].split(',').first
    else
      @ip = request.remote_ip
    end
    @ip
  end

end
