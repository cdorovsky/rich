class NotificationSettingsController < ApplicationController
  skip_before_filter :verify_authenticity_token
  before_action :load_settings
  layout 'application'

  def show
  end

  def update
    if @n_settings.update( settings_params )
      redirect_to @n_settings, notice: 'Настройки сохранены.'
    else
      redirect_to @n_settings, notice: 'Ошибка.'
    end
  end

  private

    def load_settings
      @n_settings = NotificationSettings.load_by_hashid( params[:id] )
      redirect_to root_path unless @n_settings
    end

    def settings_params
      params.require( :notification_settings ).permit( :ticket_email, :offer_email )
    end

end
