class RegistrationsController < Devise::RegistrationsController

  def new
    @dealer = Dealer.new
    @dealer.wallets.new( name: 'Главный кошелек' )
  end

  def create
    resource = Dealer.new sign_up_params

    if resource.save
      if resource.active_for_authentication?
        set_flash_message :notice, :signed_up if is_navigational_format?
        sign_in(resource_name, resource)
        render json: { success: true }
      end
    else
      clean_up_passwords resource
      render json: { dealer: resource.errors.messages }
    end
  end

  def sign_up_params
    params.require( :dealer ).permit( :email,
      :referral_code,
      :password,
      :password_confirmation,
      :name,
      :skype,
      :jabber,
      :icq,
      :reg_params,
      wallets_attributes: [ :dealer_id, :kind, :prefix, :name, :number ] )
  end

end
