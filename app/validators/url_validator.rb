class UrlValidator < ActiveModel::EachValidator
  def validate_each( record, attribute, value )
    record.errors[attribute] << ( options[ :message ] || 'Ссылка введена неверно!' ) unless url_valid?( value )
  end

  def url_valid?( url )
    url = URI.parse( url ) rescue false
    url.kind_of?( URI::HTTP ) || url.kind_of?( URI::HTTPS )
  end

  def self.url_valid?( url )
    return false unless url.to_s.include?( '//' )
    url = URI.parse( url ) rescue false
    url.kind_of?( URI::HTTP ) || url.kind_of?( URI::HTTPS )
  end

end
