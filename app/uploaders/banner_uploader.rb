# encoding: utf-8
class BannerUploader < ImageUploader
  process resize_to_fill: [ 300, 300 ]

  def default_url
    "http://res.cloudinary.com/hypbrxe8v/image/upload/v1406742311/300x300-placeholder_shgsxe_vrhyyd.png"
  end
  
end
