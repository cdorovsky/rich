# encoding: utf-8

class ImageUploader < CarrierWave::Uploader::Base
  include CarrierWave::RMagick
  include Cloudinary::CarrierWave
  def cache_dir
    "#{Rails.root}/tmp/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end
  process :convert => 'png'
end
