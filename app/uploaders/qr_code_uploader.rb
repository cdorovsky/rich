# encoding: utf-8
class QrCodeUploader < ImageUploader

  def default_url
    'http://res.cloudinary.com/hypbrxe8v/image/upload/v1415779999/static/608x151-image-placeholder-115x115.png'
  end

end
