# encoding: utf-8
class IconUploader < ImageUploader
  process resize_to_fill: [ 160, 160 ]

  version :thumb do
  	process resize_to_fill: [ 40, 40 ]
  end

  def default_url
    "http://res.cloudinary.com/hypbrxe8v/image/upload/v1406742206/banner_150x75_e3wrl0.jpg"
  end

end
