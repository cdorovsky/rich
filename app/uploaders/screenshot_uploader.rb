# encoding: utf-8
class ScreenshotUploader < ImageUploader

  version :thumb do
    process resize_to_fill: [ 110, 220 ]
  end

end